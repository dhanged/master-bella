package com.bella.desktop.stepDefinition;

import com.bella.desktop.page.Bella.gameDetailsPage;
import com.bella.desktop.page.cogs.Cogs_PlayerDetailsPage;
import com.bella.desktop.page.cogs.Cogs_PlayerMessagesPage;
import com.bella.desktop.page.cogs.Cogs_PlayerSearchPage;
import com.bella.desktop.page.cogs.Cogs_PlayerTransaction;
import com.bella.desktop.page.cogs.Cogs_PromoGroup;
import com.generic.WebActions;

import cucumber.api.java.en.Then;

public class CogsPlayerMessagesPageSteps {

	private Cogs_PlayerMessagesPage objCogs_PlayerMessagesPage;
	private Cogs_PlayerSearchPage objCogs_PlayerSearchPage;
	private Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage;
	private Cogs_PlayerTransaction objCogs_PlayerTransaction;
	private Cogs_PromoGroup objCogs_PromoGroup;
	WebActions objwebaction;
	String playerId;

	public CogsPlayerMessagesPageSteps(Cogs_PlayerMessagesPage playerMessagesPage,Cogs_PlayerSearchPage objPlayerSearchPage,Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage,
			WebActions objwebaction,Cogs_PlayerTransaction objCogs_PlayerTransaction,Cogs_PromoGroup objCogs_PromoGroup) 
	{		
		this.objCogs_PlayerMessagesPage = playerMessagesPage;
		this.objCogs_PlayerSearchPage = objPlayerSearchPage;
		this.objCogs_PlayerDetailsPage = objCogs_PlayerDetailsPage;
		this.objCogs_PlayerTransaction = objCogs_PlayerTransaction;
		this.objCogs_PromoGroup = objCogs_PromoGroup ;
		this.objwebaction = objwebaction;
	}


	@Then("^Verify Player online flag is displayed in \"([^\"]*)\"$")
	public void verify_Player_online_flag_is_displayed_in(String flag) throws Throwable {
		objwebaction.pageRefresh();

		objCogs_PlayerDetailsPage.verifyPlayerOnlineFlag(flag);
	}

	@Then("^Select \"([^\"]*)\" submenu from \"([^\"]*)\" menu$")
	public void select_submenu_from_menu(String arg1, String arg2) throws Throwable {
		objCogs_PlayerMessagesPage.selectMenu(arg2, arg1);
	}

	@Then("^Search players by username as \"([^\"]*)\"$")
	public void search_players_by_username_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//objCogs_PlayerSearchPage.setSearchString(objCogs_PlayerSearchPage.getUserNameFromConfig());
		objCogs_PlayerSearchPage.setSearchString(arg1);
		objCogs_PlayerSearchPage.searchPlayerByStatus("Inactive");
		objCogs_PlayerSearchPage.clickOnSearchPlayers();
		playerId = objCogs_PlayerSearchPage.getPlayerID();
	}

	@Then("^Search players using username$")
	public void search_players_using_username_as() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		objCogs_PlayerSearchPage.setSearchString(objCogs_PlayerSearchPage.getUserNameFromConfig());
		objCogs_PlayerSearchPage.searchPlayerByStatus("Inactive");
		objCogs_PlayerSearchPage.clickOnSearchPlayers();
		playerId = objCogs_PlayerSearchPage.getPlayerID();
	}

	@Then("^Search Message \"([^\"]*)\"$")
	public void search_Message(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		objCogs_PlayerMessagesPage.searchMessageBySubject(arg1);
		objCogs_PlayerMessagesPage.clickSerachMessageButton();
		objCogs_PlayerMessagesPage.selectExpleoMessage(arg1);
	}

	@Then("^Schedule Message$")
	public void schedule_Message() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		objCogs_PlayerMessagesPage.clickOnSchedule();
		objCogs_PlayerMessagesPage.switchToMessageTempleIframe();
		objCogs_PlayerMessagesPage.selectUplodTypeAsTextArea();
		objCogs_PlayerMessagesPage.setSendDateAndTime();
		objCogs_PlayerMessagesPage.setDays("1");
		objCogs_PlayerMessagesPage.setData(playerId);
		objCogs_PlayerMessagesPage.clickOnScheduleSend();
		objCogs_PlayerMessagesPage.acceptAlert();
		objCogs_PlayerMessagesPage.verifyUpdateSuccessfulMessage();
	}

	@Then("^Search players by username$")
	public void search_players_by_username(){
		String unm = objCogs_PlayerSearchPage.getUserNameFromConfig();
		objCogs_PlayerSearchPage.setSearchString(unm);
		objCogs_PlayerSearchPage.searchPlayerByStatus("Inactive");
		objCogs_PlayerSearchPage.clickOnSearchPlayers();
		objCogs_PlayerSearchPage.clickOnPlayerDetails(unm);
	}

	@Then("^Verify Player Account status is displayed as \"([^\"]*)\"$")
	public void verify_Player_Account_status_is_displayed_as(String arg1){
		objCogs_PlayerDetailsPage.verifyPlayerCurrentStatus(arg1);
	}

	@Then("^Verify Take a break end date is displayed correctly$")
	public void verify_Take_a_break_end_date_is_displayed_correctly() {

	}

	@Then("^Remove break period$")
	public void remove_break_period() {
		objCogs_PlayerDetailsPage.ClickOnCancelBreak();
		objCogs_PlayerDetailsPage.verifyPlayerTakeABreakPopup();
		objCogs_PlayerDetailsPage.SetReason("Test");
		objCogs_PlayerDetailsPage.ClickOnSubmit();
		objCogs_PlayerDetailsPage.acceptAlert();
		objCogs_PlayerDetailsPage.verifyPlayerStatus("Enabled");
	}

	@Then("^Verify Player account status is displayed as \"([^\"]*)\"$")
	public void Verify_Player_account_status_is_displayed_as(String arg1){
		objCogs_PlayerDetailsPage.verifyPlayerStatus(arg1);
	}


	@Then("^Change player status to active$")
	public void change_player_status_to_active() {
		objCogs_PlayerDetailsPage.ClickOnAccountStatusEditButton();
		objCogs_PlayerDetailsPage.verifyEditPlayerStatusPopUp();
		objCogs_PlayerDetailsPage.SetPlayerStatusOption("Enabled");
		objCogs_PlayerDetailsPage.SetChangeReason("Test");
		objCogs_PlayerDetailsPage.ClickOnUpdate();
		objCogs_PlayerDetailsPage.VerifyUpdateSuccesfulMessage();
		objCogs_PlayerDetailsPage.closePopup();
		objCogs_PlayerDetailsPage.verifyPlayerStatus("Enabled");
	}
	@Then("^Search players using \"([^\"]*)\"$")
	public void search_players_using(String unm) throws Throwable {
		objCogs_PlayerSearchPage.setSearchString(unm);
		objCogs_PlayerSearchPage.searchPlayerByStatus("Inactive");
		objCogs_PlayerSearchPage.clickOnSearchPlayers();
		objCogs_PlayerSearchPage.clickOnPlayerDetails(unm);
	}
	@Then("^the selected option should be updated in cogs player details$")
	public void the_selected_option_should_be_updated_in_cogs_player_details() {
		objCogs_PlayerDetailsPage.verifyMarketingPreferencesCheckboxesIsSelectedOrNot("Email");
	}


	@Then("^Navigate to Player \"([^\"]*)\" tab$")
	public void navigate_to_Player_tab(String arg1){
		// Write code here that turns the phrase above into concrete actions
		objCogs_PlayerTransaction.selectTopMenu(arg1);
		objCogs_PlayerTransaction.verifyPageTitle(arg1);
	}


	@Then("^Filter transactions By Quick Date$")
	public void filter_transactions_by_quick_date() {
		objCogs_PlayerTransaction.selectQuickDate("This Week");
		objCogs_PlayerTransaction.clickOnSumbit();
		objCogs_PlayerTransaction.verifyTransactionHistory("Transaction","Deposit via");
	}


	@Then("^Filter transactions By Transaction Type$")
	public void filter_transactions_by_Transaction_Type() {
		objCogs_PlayerTransaction.clickOnDropdown("Transaction Type");
		objCogs_PlayerTransaction.ClearTransactionType();
		objCogs_PlayerTransaction.selectTransactionType("Deposit");
		objCogs_PlayerTransaction.clickOnSumbit();
		objCogs_PlayerTransaction.verifyTransactionHistory("Transaction","Deposit via");

		objCogs_PlayerTransaction.ClearTransactionType();
		objCogs_PlayerTransaction.selectTransactionType("Withdrawal");
		objCogs_PlayerTransaction.clickOnSumbit();
		objCogs_PlayerTransaction.verifyTransactionHistory("Transaction","Withdrawal");
	}

	@Then("^Filter transactions By Device Category$")
	public void filter_transactions_By_Device_Category() {
		objCogs_PlayerTransaction.selectDeviceCategory("Web");
		objCogs_PlayerTransaction.clickOnSumbit();
		objCogs_PlayerTransaction.verifyTransactionHistory("Device Category","Web");
	}

	@Then("^Add player id in \"([^\"]*)\" Promo group$")
	public void add_player_id_in_Promo_group(String arg1) {
		objCogs_PromoGroup.searchPromoGroupByName(arg1);
		objCogs_PlayerMessagesPage.clickSerachMessageButton();
		objCogs_PlayerMessagesPage.selectExpleoMessage(arg1);
		objCogs_PromoGroup.clickOnEdit();
		objCogs_PromoGroup.enterPlayerID(playerId);
		objCogs_PromoGroup.clickOnSaveButton();
		objCogs_PromoGroup.verifyPromoStatusIsDisplayedAsEnabled();
	}
}
