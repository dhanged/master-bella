Feature: Reality Check

@Desktop
Scenario: Check that system allows to set the reality check for the user
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Reality Check" tab from my account screen
Then Verify infotext on Reality checks screen
Then Verify following reminder options
|15 mins|
|30 mins|
|1 hour|
Then Select reminder option
And Click on 'Save a changes' button
Then Verify updated option displayed correctly