
package com.generic.utils;

/**
 * @author Harhvardhan Yadav, Namrata Donikar (Expleo)
 *
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class TestRun 
{
	// tests="4" started="4" failures="1" errors="0" ignored="1"
	String testrunName;
	int tests;
	int started;
	int failures;
	int errors;
	int ignored;
	List<TestSuite> testSuites;

	TestRun(String name, int tests, int started, int failures, int errors,
			int ignored)
	{
		this.testrunName = name;
		testSuites = new ArrayList<TestSuite>();
		this.tests = tests;
		this.started = started;
		this.failures = failures;
		this.errors = errors;
		this.ignored = ignored;
	}
}

class TestSuite
{
	String suiteName;
	String suitetime;
	List<TestCase> testcases;

	TestSuite(String sName, String stime)
	{
		this.suiteName = sName;
		this.suitetime = stime;
		testcases = new ArrayList<TestCase>();
	}

}

class TestCase
{
	String name;
	String classname;
	String failure;
	String time;

	TestCase(String name, String className, String failure, String time)
	{
		this.name = name;
		this.classname = className;
		this.time = time;
		this.failure = failure;
	}

}

public class GenerateXMLReport
{
	static TestRun testrun;

	public static boolean readResultXml(String filePath)
	{
		// String temp="target_junit\\cucumber1.xml";
		File file = new File(filePath);
		if (file.exists())
		{
			Document dom;
			int failure = 0;
			int tests = 0;
			DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
			try
			{
				DocumentBuilder db = dFact.newDocumentBuilder();
				dom = db.parse(file);
				Element e = dom.getDocumentElement();
				failure = Integer.parseInt(e.getAttribute("failures"));
				tests = Integer.parseInt(e.getAttribute("tests"));
				testrun = new TestRun("", tests, tests, failure, 0, 0);
				List<TestCase> lstTestCases = new ArrayList<TestCase>();
				NodeList lstNode = dom.getElementsByTagName("testcase");
				float totalTime = 0;
				for (int i = 0; i < lstNode.getLength(); i++)
				{
					String testCaseName = "";
					String classname = "";
					String time = "";
					Node node = lstNode.item(i);
					NamedNodeMap nodeAttr = node.getAttributes();
					classname = nodeAttr.getNamedItem("classname").toString()
							.split("=")[1].replace("\"", "");
					String failureMsg = "";
					testCaseName = nodeAttr.getNamedItem("name").toString()
							.split("=")[1].replace("\"", "");
					time = nodeAttr.getNamedItem("time").toString().split("=")[1]
							.replace("\"", "");
					if (node.hasChildNodes())
					{
						NodeList nl = node.getChildNodes();
						for (int j = 0; j < nl.getLength(); j++)
						{
							if (nl.item(j) instanceof Element)
							{
								Element ele = (Element) nl.item(j);
								if (ele.getTagName().contains("failure"))
								{
									nodeAttr = nl.item(j).getAttributes();
									failureMsg = nodeAttr
											.getNamedItem("message").toString()
											.split("=")[1].replace("\"", "");
									ele = null;
								}

							}
						}
					}
					totalTime = totalTime + Float.parseFloat(time.trim());
					lstTestCases.add(new TestCase(testCaseName, classname,
							failureMsg, time));
				}
				if (lstTestCases.size() > 0)
					testrun.testSuites.add(new TestSuite(
							lstTestCases.get(0).classname, String
									.valueOf(totalTime)));
				else
					testrun.testSuites.add(new TestSuite("NA", "0.00"));

				for (TestCase t : lstTestCases)
				{
					testrun.testSuites.get(0).testcases.add(t);
				}

			}
			catch (Exception e)
			{

			}
			return true;
		}
		else
		{
			return false;
		}
	}

	public static void writeResultXMLinTPformat(String testRunName,
			String projectName,String priority)
	{
		try
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// root element
			Element rootElement = doc.createElement("testrun");
			rootElement.setAttribute("name", testRunName);
			rootElement.setAttribute("project", projectName);
			rootElement.setAttribute("tests", String.valueOf(testrun.tests));
			rootElement
					.setAttribute("started", String.valueOf(testrun.started));
			rootElement.setAttribute("failures",
					String.valueOf(testrun.failures));
			rootElement.setAttribute("errors", String.valueOf(testrun.errors));
			rootElement
					.setAttribute("ignored", String.valueOf(testrun.ignored));
			doc.appendChild(rootElement);
			for (TestSuite ts : testrun.testSuites)
			{
				Element testsuite = doc.createElement("testsuite");
				testsuite.setAttribute("name", testRunName);
				testsuite.setAttribute("time", ts.suitetime);
				rootElement.appendChild(testsuite);
				for (TestCase tc : ts.testcases)
				{
					Element tcEle = doc.createElement("testcase");
					tcEle.setAttribute("name", tc.name);
					tcEle.setAttribute("classname", testRunName);
					tcEle.setAttribute("time", tc.time);
					Element failEle;
					if (tc.failure.length() > 1)
					{
						failEle = doc.createElement("failure");
						failEle.appendChild(doc.createTextNode(tc.failure));
						tcEle.appendChild(failEle);
						testsuite.appendChild(tcEle);
					}
					else
					{
						testsuite.appendChild(tcEle);
					}
				}
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			String fileName="" ;
			switch(projectName)
			{
				case "LoanMechanics":
					fileName="LM"+priority;
					break;				
				default:
					fileName="SquadName"+priority;
			}
			
			StreamResult result = new StreamResult(
					new File("target\\"+fileName+".xml"));
			transformer.transform(source, result);
			// Output to console for testing
			StreamResult consoleResult = new StreamResult(System.out);
			transformer.transform(source, consoleResult);
		}


		catch (ParserConfigurationException ex)
		{

		}
		catch (Exception e)
		{

		}

	}
	
}