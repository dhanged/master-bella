Feature: Login 

@Desktop
Scenario: Check whether system displays Title, close “X“ icon, Fileds, Buttons, Links on Login page
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
And Verify close 'X' icon
And Verify 'Username' field
And Verify 'Password' field
And Verify Toggle within password field
And Verify Remember me checkbox
And Verify 'Log In' CTA
And Verify 'Forgot username' link
And Verify 'Forgot password' link
And Verify 'New player?' text
And Verify 'Sign up' link
#And Verify 'Live Chat' link

@Desktop
Scenario: Check whether system allows to Login digital only customer to the site without any issue
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
#Then User enters username "rtyui2"
#And User enters password "Password1234"
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Verify following links are displayed in header
|Mail|
|Deposit|
|Balance|
|My Account|
Then Click on My Account
#Then Verify "rtyui2" username is displayed on header
Then Verify username is displayed on header
Then Click on logout button

@Desktop
Scenario: Check whether user can select an option the username to be remembered
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And Select 'Remember UserName' checkbox
Then Verify 'Remember UserName' checkbox is get selected


@Desktop
Scenario: Check whether all future logins will contain the username by default which user have been logged in
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
#Then User enters username "rtyui2"
Then User enters username
Then Select 'Remember UserName' checkbox
#And User enters password "Password1234"
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Click on logout button
Then User clicks on Login Button from header of the Page
#Then Verify Username auto pupulated with "rtyui2"
Then Verify Username is auto pupulated in username field

@Desktop
Scenario: Check whether password can be shown to check if it has been typed in correctly
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters password "Password1234"
And Click on show toggle button
Then Verify user can see typed password correctly


@Desktop
Scenario: Check whether system displays error message when incorrect username and Password is entered
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "dfdfrt"
Then User enters password "jjfgh"
Then User clicks on login button
Then Verify error message "Sorry, your login details are not valid"

@Desktop
Scenario: Account Lock_Check whether account gets lock after 5 attempts of incorrect Password.
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
Then User enters password "jjfgh"
Then User clicks on login button
Then Verify error message "Sorry, your login details are not valid"
Then User clicks on login button
Then Verify error message "Sorry, your login details are not valid"
Then User clicks on login button
Then Verify error message "Sorry, your login details are not valid"
Then User clicks on login button
Then Verify error message "Sorry, your login details are not valid"
Then User clicks on login button
Then Verify error message "The user is suspended."
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
And Verify Player Account status is displayed as "Multiple invalid login attempts"
Then Verify Player account status is displayed as "Multiple invalid login attempts"
Then Change player status to active
Then Back to Bell site
And User enters password
And User clicks on login button
Then Verify user is successfully logged in

@Desktop
Scenario: Check system displays success message when user selects Forgotten username Option form Login Page
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then Verify 'Forgot username' link
Then Click on 'Forget username' link
Then Forget username screen should be presented along with 'Email address' and Send username reminder CTA
#Then Enter Email id "vaishali.bhad@expleogroup.com"
Then Enter Email id "txcert@mailinator.com"
And Click on 'Send username reminder'
Then Verify following info text is displayed
|If you entered the email associated with your account, you will have a Username Reminder waiting in your inbox|
|Didn't receive an email? Check your Junk or Promotions Folders. Still no email?|
|Resend Username Reminder Email|
|0330 102 8582|

@Desktop
Scenario: Check whether system displays appropriate error message when user provide invalid email address while request for username
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then Verify 'Forgot username' link
Then Click on 'Forget username' link
Then Forget username screen should be presented along with 'Email address' and Send username reminder CTA
Then Enter Email id "vaishlss@dfd.sdf"
And Click on 'Send username reminder'
Then Verify "Your Email Address was not recognised. Please reenter" error message

@Desktop
Scenario: Check whether system displays appropriate error message provided user enters invalid username to reset the password
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then Verify 'Forgot password' link
Then Click on 'Forget password' link
Then Forget password screen should be presented along with 'Username' and submit CTA
Then Enter invalid username
And Click on 'Submit'
Then Verify "Your Username was not recognised. Please reenter" error message

@Desktop
Scenario: Check whether system displays success message provided user enters valid username to reset the password
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then Verify 'Forgot password' link
Then Click on 'Forget password' link
Then Forget password screen should be presented along with 'Username' and submit CTA
Then Enter valid username as "qaeswsqa" to reset password
#Then Enter valid username as "vaishaliopst1" to reset password
And Click on 'Submit'
Then Verify following info text is displayed on screen
|Your Password Reset instructions are waiting in your inbox...|
|Resend Password Reset email|
|0330 102 8582|