package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class BonusesPage {

	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public BonusesPage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
	}

	By expandBTN = By.xpath("//button[@class='toggle-module-btn ']");

	public void verifyEnterBonusCodeTXTDisplayed()
	{
		By EnterBonusCodeTXT= By.xpath("//p[contains(text(),'Enter bonus code')]");
		logReporter.log("Verify 'Enter bonus Code' Text/label displayed", 
				objWebActions.checkElementDisplayed(EnterBonusCodeTXT));
	}

	public void setPromoCode(String promocode)
	{
		By inputPromoCode = By.xpath("//input[@id='enter-code']");

		logReporter.log(" Set Promocode/Bonus code ", promocode,
				objWebActions.setTextWithClear(inputPromoCode, promocode));
	}
	public void verifyClearBTN()
	{
		By ClearBtn = By.xpath("//button[contains(text(),'Clear')]");
		logReporter.log("Verify 'Clear' button displayed", 
				objWebActions.checkElementDisplayed(ClearBtn));
	}
	public void clickSubmitcode()
	{
		By submitCodeBtn= By.xpath("//button[contains(text(),'Submit')]");

		logReporter.log(" Click on Submit ", 
				objWebActions.click(submitCodeBtn));

		/*	By errorMsg = By.xpath("//li[contains(text(),'Incorrect Bonus Code')]");

		if (objWebActions.checkElementDisplayed(errorMsg))
		{
			logReporter.log(" 'Incorrect Bonus Code.' error message is displayed", 
					objWebActions.checkElementDisplayed(errorMsg));
		}*/
	}

	public void verifyIncorrectCodeMSG()
	{
		By errorMsg = By.xpath("//li[contains(.,'Incorrect Bonus Code')]");
		logReporter.log("Verify 'Incorrect Bonus Code.' error message is displayed", 
				objWebActions.checkElementDisplayed(errorMsg));
	}

	public String getBonusName()
	{
		By bonusname = By.xpath("//p[@class='heading' and contains(text(),'Enter bonus code')]/following::p[@class='heading']");

		return objWebActions.getText(bonusname);
	}
	public void verifyTermsnConditionsLinkDisplayed()
	{
		By TermsnConditionsLink = By.xpath("//a[@class='help-link' and contains(text(),'View Terms and Conditions')]");
		logReporter.log("Verify 'Terms n Conditions' Link displayed", 
				objWebActions.checkElementDisplayed(TermsnConditionsLink));
	}

	public void verifyAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='joinnow-agree']");
		logReporter.log("Verify 'Accept Checkbox' displayed", 
				objWebActions.checkElementDisplayed(AcceptCheckbox));
	}

	public void verifyAcceptTnCsLabelDisplayed()
	{
		By AcceptTnCsLabel = By.xpath("//label[contains(text(),'I accept the Terms and Conditions for this promotion')]");
		logReporter.log("Verify 'I accept the Terms and Conditions for this promotion' label displayed", 
				objWebActions.checkElementDisplayed(AcceptTnCsLabel));
	}

	public void SelectAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='joinnow-agree']");

		logReporter.log(" Select 'Accept Checkbox' displayed", 
				objWebActions.click(AcceptCheckbox));
	}

	public void verifyClaimPromotionBTN()
	{
		By ClaimPromotionBTN = By.xpath("//button[contains(text(),'Claim promotion')]");
		logReporter.log(" Verify 'Claim Promotion BTN' displayed", 
				objWebActions.checkElementDisplayed(ClaimPromotionBTN));
	}
	public void clickClaimPromotionBTN()
	{
		By ClaimPromotionBTN = By.xpath("//button[contains(text(),'Claim promotion')]");
		logReporter.log(" Click on 'Claim Promotion BTN' displayed", 
				objWebActions.click(ClaimPromotionBTN));
	}
	public void verifyCancelLinkDisplayed()
	{
		By cancelLink = By.xpath("//a[contains(text(),'Cancel')]");
		logReporter.log("Verify 'Cancel Link' displayed", 
				objWebActions.checkElementDisplayed(cancelLink));
	}
	public void verifySuccessHeaderDisplayed()
	{
		By SuccessHeader = By.xpath("//h4[contains(text(),'Success')]");
		logReporter.log("Verify 'Success Header' displayed", 
				objWebActions.checkElementDisplayed(SuccessHeader));
	}

	public void verifyClaimedBonusNameDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//p[@class='link-only']");

		if(Bonusname.equals(objWebActions.getText(bonusname)))
		{
			System.out.println("Claimed bonus name is displayed correctly");
		}
		else 
			System.out.println("there is mismatch in Claimed bonus name displayed ");
	}

	public void verifyCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button[contains(text(),'Close')]");
		logReporter.log("Verify 'Close BTN' displayed", 
				objWebActions.checkElementDisplayed(CloseBTN));

	}
	public void ClickCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button[contains(text(),'Close')]");
		logReporter.log(" Click on 'Close BTN' displayed", 
				objWebActions.click(CloseBTN));
	}
	public void verifyActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button[contains(text(),'Active promotions')]");
		logReporter.log("Verify 'Active Promotions BTN' displayed", 
				objWebActions.checkElementDisplayed(ActivePromotionsBTN));
	}
	public void clickActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button[contains(text(),'Active promotions')]");
		logReporter.log(" Click on 'Active Promotions BTN' displayed", 
				objWebActions.click(ActivePromotionsBTN));
	}

	public boolean verifyExpandButtonDisplayed(){
		return objWebActions.checkElementDisplayed(expandBTN);
	}

	public void clickExpandButton()
	{
		//By expandBTN = By.xpath("//button[@class='toggle-module-btn ']");
		logReporter.log(" click 'expand + BTN' ", 
				objWebActions.click(expandBTN));
	}
	public void VerifyCollapseButton()
	{
		By collapseBTN = By.xpath("//button[@class='toggle-module-btn active']");
		logReporter.log(" Verify 'collapse - BTN' displayed", 
				objWebActions.checkElementDisplayed(collapseBTN));
	}

	public void clickCollapseButton()
	{
		By collapseBTN = By.xpath("//button[@class='toggle-module-btn active']");
		logReporter.log(" click 'collapse - BTN' displayed", 
				objWebActions.click(collapseBTN));
	}

	public void verifyNoitemsfoundMessageDisplyedOrNot()
	{
		By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
		logReporter.log(" 'No items found' message is displayed on Bonuses screen ", 
				objWebActions.checkElementDisplayed(noTransactionFoundMSG));
	}
	public void verifyActiveBonusesLabelsDisplayed(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labels2+"')]");

				logReporter.log(" Verify "+labels2+" is displayed on Active Bonuses screen ",  
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else 
		{ 
			By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labels+"')]");
			logReporter.log(" Verify "+labels+" is displayed on Active Bonuses screen ",  
					objWebActions.checkElementDisplayed(locator));
		}
	}

	public void verifyViewTCsLinkDisplayed()
	{
		By ViewTCsLink = By.xpath("//a[contains(text(),'View T&Cs')]");
		logReporter.log("Verify 'View T&Cs Link' displayed on Active Bonuses screen ", 
				objWebActions.checkElementDisplayed(ViewTCsLink));
	}
	public void verifyOptOutLinkDisplayed()
	{
		By OptOutLink = By.xpath("//a[contains(text(),'Opt out')]");
		logReporter.log("Verify 'OptOut Link' displayed on Bonuses screen ", 
				objWebActions.checkElementDisplayed(OptOutLink));
	}

	public void clickOptOutLink()
	{
		By OptOutLink = By.xpath("//a[contains(text(),'Opt out')]");
		logReporter.log(" click on 'OptOut Link' displayed on Bonuses screen ", 
				objWebActions.click(OptOutLink));
	}

	public void verifyAreUSureTXTonPopupDisplayed()
	{
		By AreUSureTXT = By.xpath("//h4[contains(text(),'Are you sure?')]");
		logReporter.log("Verify 'Are You Sure TXT' displayed on confirmation popup ", 
				objWebActions.checkElementDisplayed(AreUSureTXT));
	}
	public void verifyOptOutTXTonPopupDisplayed()
	{
		By OptOutTXT = By.xpath("//p[contains(text(),'Opt out')]");
		logReporter.log("Verify 'Opt Out TXT' displayed on confirmation popup ", 
				objWebActions.checkElementDisplayed(OptOutTXT));
	}

	public void verifyYesBTNonPopupDisplayed()
	{
		By YesBTN = By.xpath("//button[@class='yellow btn btn-default' and contains(text(),'Yes')]");
		logReporter.log("Verify 'Yes Button' displayed on confirmation popup ", 
				objWebActions.checkElementDisplayed(YesBTN));
	}

	public void clickYesBTNonPopup()
	{
		By YesBTN = By.xpath("//button[@class='yellow btn btn-default' and contains(text(),'Yes')]");
		logReporter.log(" Click 'Yes Button' displayed on confirmation popup ", 
				objWebActions.click(YesBTN));
	}
	public void verifyNoBTNonPopupDisplayed()
	{
		By NoBTN = By.xpath("//button[@class='blue btn btn-default' and contains(text(),'No')]");
		logReporter.log("Verify 'No Button' displayed on confirmation popup ", 
				objWebActions.checkElementDisplayed(NoBTN));
	}

	public void closeConfirmationPopupDisplayed()
	{
		By closeXBTN = By.xpath("//button[@class='close']");
		logReporter.log("Verify 'Close X Button' displayed on confirmation popup ", 
				objWebActions.checkElementDisplayed(closeXBTN));

		logReporter.log(" click 'Close X Button' displayed on confirmation popup ", 
				objWebActions.click(closeXBTN));
	}

	public void validateBonusStatus(String status)
	{
		By locator = By.xpath("//span[@class='list-detail' and contains(text(),'Bonus status')]/following::span[contains(text(),'"+status+"')]");

		logReporter.log(" Verify Bonus Status displayed is : "+status,
				(status.equals(objWebActions.getText(locator))));
	}

	// Bonus History page

	public void verifyFilterByBonusTypeLabelDisplayed()
	{
		By FilterByBonusTypeLabel = By.xpath("//p[@class='dropdown-label' and contains( text(),'Filter by bonus type:')]");
		logReporter.log("Verify 'Filter By Bonus Type' Label displayed on Active Bonuses screen ", 
				objWebActions.checkElementDisplayed(FilterByBonusTypeLabel));
	}

	public void verifyBonusTypesDisplayed(String type)
	{
		if(type.contains(","))
		{
			String[] arr1 = type.split(",");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//ul[@class='dropdown-menu' ]/li/a[contains( text(),'"+labels2+"')]");

				logReporter.log(" Verify bonus type : "+labels2+" is displayed under filter dropdown list ",  
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//ul[@class='dropdown-menu' ]/li/a[contains( text(),'"+type+"')]");
			logReporter.log(" Verify bonus type : "+type+" is displayed under filter dropdown list ",  
					objWebActions.checkElementDisplayed(locator));	
		}
	}

	public void selectBonusType(String bonus)
	{
		By bonustype = By.xpath("//ul[@class='dropdown-menu' ]/li/a[contains( text(),'"+bonus+"')]");

		logReporter.log(" Select Bonus type as : " +bonus +" from list ", 
				objWebActions.click(bonustype));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void clickBonusType(String bonus)
	{
		By bonustype = By.xpath("//button[@class='btn dropdown-toggle bonus-toggle' and contains( text(),'"+bonus+"')]");

		logReporter.log(" click on selected Bonus type as : " +bonus +" on Bonus history ", 
				objWebActions.click(bonustype));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyTypeHDRDisplayed(String type)
	{
		By typeValue= By.xpath("//span[@class='list-info']");
		if(type.equals(objWebActions.getText(typeValue))){

			logReporter.log(" Filtered transaction Bonus type is : " +type +" on Bonus history ", 
					objWebActions.checkElementDisplayed(typeValue));
		}
		else 
		{
			By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
			logReporter.log(" 'No items found' message is displayed on Bonuses screen ", 
					objWebActions.checkElementDisplayed(noTransactionFoundMSG));
		}
	}
	public void verifyFilterByDateRangeLabelDisplayed()
	{
		By FilterByDateRangeLabel = By.xpath("//div[@class='myaccount-details']/p[contains(text(),'Filter by date range')]");

		logReporter.log("Verify 'Filter By Date Range Label' displayed on Active Bonuses screen ", 
				objWebActions.checkElementDisplayed(FilterByDateRangeLabel));

	}
	public void selectCurrentMonth(String currentMonth , String CurrentDay)
	{
		By Month = By.xpath("//div[@class='react-datepicker__current-month']");
		By Day = By.xpath("//div[@class='react-datepicker__week']/div[text()='"+CurrentDay+"'][not(contains(@class, '--disabled'))]");

		//a[contains(@title, 'Design') and not(contains(@class, 'reMode_selected'))]
		System.out.println("currentMonth : "+currentMonth +"CurrentDay"+CurrentDay);

		while(true)
		{
			if(currentMonth.equals(objWebActions.getText(Month)))
			{
				break;
			}
			else 
			{
				By NextMonthBTN = By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--next' and contains(text(),'Next month')]");
				logReporter.log(" Click on the Next month button on datePicker", 
						objWebActions.click(NextMonthBTN));
			}
		}
		logReporter.log(" Selected the Day as: "+ objWebActions.getText(Day)+"on datePicker ", 
				objWebActions.click(Day));
	}

	public void setFromDate(String fromdate )
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='react-date-input']");

		logReporter.log(" Click 'from date' field to select the date ", 
				objWebActions.click(inpFromDate));

		this.selectCurrentMonth(objUtilities.getRequiredDay("-1", "MMMM yyyy", ""),objUtilities.getRequiredDay("-1","d",""));
	}

	public void setFromDateSameDay()
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='react-date-input']");

		logReporter.log(" Click 'from date' field to select the date as current day ", 
				objWebActions.click(inpFromDate));

		this.selectCurrentMonth(objUtilities.getRequiredDay("-1", "MMMM yyyy", ""),objUtilities.getRequiredDay("0","d",""));
	}

	public void verifyToDate(String currDate )
	{
		By inpToDate = By.xpath("//div[@class='react-datepicker__input-container']/input[@value='"+currDate+"']");

		if (currDate.equals(objWebActions.getAttribute(inpToDate,"value")))
		{
			logReporter.log(" Verify Todate displayed is "+currDate, 
					objWebActions.checkElementDisplayed(inpToDate));
		}
	}
	public void verifyResetLinkdisplayed()
	{
		By ResetLink = By.xpath("//div[@class='myaccount-details']/a[contains(text(),'Reset')]");
		logReporter.log(" Verify 'Reset Link' displayed on Bonus History screen ", 
				objWebActions.checkElementDisplayed(ResetLink));
	}
	public void clickResetLink()
	{
		By ResetLink = By.xpath("//div[@class='myaccount-details']/a[contains(text(),'Reset')]");
		logReporter.log(" click 'Reset Link' ", 
				objWebActions.click(ResetLink));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void verifyFilterButtonDisplayed()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		logReporter.log("Verify 'Filter Button'displayed on Bonuses screen ", 
				objWebActions.checkElementDisplayed(FilterBTN));
	}

	public void clickFilterButton()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		logReporter.log(" click on 'Filter Button'displayed on  Bonuses screen ", 
				objWebActions.click(FilterBTN));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyBonusNameOnBonusHistoryDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//div[@class='toggle-module-content']/p[contains(text(),'"+Bonusname+"')]");

		if(Bonusname.equals(objWebActions.getText(bonusname)))
		{
			logReporter.log("Claimed bonus name '"+ Bonusname +"'' is displayed correctly under Bonus history / Active Bonuses screen ", 
					objWebActions.checkElementDisplayed(bonusname));
		}
		else {

			logReporter.log("Claimed bonus name "+ Bonusname +" is not displayed correctly under Bonus history / Active Bonuses screen", 
					objWebActions.checkElementDisplayed(bonusname));
		}
	}
	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");

		logReporter.log(" click on ' X' Button'displayed ", 
				objWebActions.click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");

		logReporter.log(" click on logout-link displayed  ", 
				objWebActions.click(logoutLink));
	}

	public void verifyBonusDetailsAfterExpandingBonus(String details)
	{
		switch (details) 
		{
		case "Type": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Bonus status": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Amount": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Wagering target":
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Progress": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Activation": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Expiry": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Opt out link":
			this.verifyOptOutLinkDisplayed();
			break;
		case "View T&Cs link":
			this.verifyViewTCsLinkDisplayed();
			break;
		}
	}
	
	public String getBonusActivationDate()
	{
		By locator = By.xpath("//span[@class='list-detail' and contains(text(),'Activation')]//following-sibling::span[@class='list-info']");
		return objWebActions.getText(locator);
	}
	
	public void verifyBonusHistoryIsDisplayedAsPerSelectedDateRange(String frmdate,String toDate)
	{
		String currentBonusActivationDate = getBonusActivationDate();
		currentBonusActivationDate = currentBonusActivationDate.substring(0,currentBonusActivationDate.indexOf(" "));
		System.out.println("********************* formated  "+currentBonusActivationDate);
		frmdate =	objUtilities.getFormatedDate(frmdate,"dd MMMM yyyy", "dd-MM-YY");
		toDate = objUtilities.getFormatedDate(toDate,"dd/MM/yyyy", "dd-MM-YY");
		
		System.out.println("********** gh gh***********  "+frmdate+ "  _____   "+toDate);
		if(currentBonusActivationDate.contains(frmdate)||currentBonusActivationDate.contains(toDate)){
			System.out.println("****dddd");
		logReporter.log(" Bonus history displayed correctly", true);}
		else
		{logReporter.log(" incorrect Bonus history display", false);}
	}
	
}


