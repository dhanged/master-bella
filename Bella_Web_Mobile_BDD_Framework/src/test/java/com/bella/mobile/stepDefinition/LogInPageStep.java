/**
 * 
 */
package com.bella.mobile.stepDefinition;

import com.bella.mobile.page.Bella.HomePage;
import com.bella.mobile.page.Bella.LogInPage;
import com.generic.utils.Configuration;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

/**
 * @author vaishali bhad
 *
 */
public class LogInPageStep {

	private LogInPage objLogInPage;
	private HomePage objHomePage;
	private Configuration configuration;
	
	public LogInPageStep(LogInPage logInPage, HomePage homePage,Configuration configuration) {

		this.objLogInPage = logInPage;
		this.objHomePage = homePage;
		this.configuration= configuration;
	}

	@Then("^Verify Login Page is displayed$")
	public void verify_Login_Page_is_displayed() {
		objLogInPage.verifyLoginPopupDisplayed();
	}

	@Then("^Verify close 'X' icon$")
	public void verify_close_X_icon() {
		objLogInPage.verifyCloseXicon();
	}

	@Then("^Verify 'Username' field$")
	public void verify_Username_field() {
		objLogInPage.verifyUserNameDisplayed();
	}

	@Then("^Verify 'Password' field$")
	public void verify_Password_field() {
		objLogInPage.verifyPasswordDisplayed();
	}

	@Then("^Verify Toggle within password field$")
	public void verify_Toggle_within_password_field() {
		objLogInPage.verifyShowButtonDisplayedInPasswordField();
	}

	@Then("^Verify Remember me checkbox$")
	public void verify_Remember_me_checkbox() {
		objLogInPage.verifyRemeberUserNameCheckboxDisplayed();
	}

	@Then("^Verify 'Log In' CTA$")
	public void verify_Log_In_CTA() {
		objLogInPage.verifyLogInCTA();
	}

	@Then("^Verify 'Forgot username' link$")
	public void verify_Forgot_username_link() {
		objLogInPage.VerifyForgottenUserNameLink();
	}

	@Then("^Verify 'Forgot password' link$")
	public void verify_Forgot_password_link() {
		objLogInPage.VerifyForgottenPasswordLink();
	}

	@Then("^Click on 'Forget password' link$")
	public void click_on_Forgot_password_link() {
		objLogInPage.clickForgottenPasswordLink();
	}
	
	@Then("^Click on 'Forget username' link$")
	public void click_on_Forgot_username_link() {
		objLogInPage.clickForgottenUserNameLink();
	}
		
	@Then("^Forget password screen should be presented along with 'Username' and submit CTA$")
	public void Forget_password_screen_should_be_presented_along_with_Username_and_submit_CTA() {
		objLogInPage.verifyResetPasswordHeaderDisplayed();
		objLogInPage.verifyArticleBlockHeaders("p;We will email Password Reset instructions to the username below");
		objHomePage.verifyLinksOnFlogScreen("Username");
	}
	@Then("^Forget username screen should be presented along with 'Email address' and Send username reminder CTA$")
	public void Forget_username_screen_should_be_presented_along_with_Emailaddress_and_submit_CTA() {
		objLogInPage.verifyUsernameReminderHeaderDisplayed();
		objLogInPage.verifyArticleBlockHeaders("p;Having trouble with your username?  No problem - we can send you a reminder right away.");
		objHomePage.verifyLinksOnFlogScreen("Password");
	}
	
	@Then("^Verify following info text is displayed on screen$")
	public void verify_following_checkboxes_are_selected(DataTable dt){
		objLogInPage.verifyPasswordResetMailSend();
		objHomePage.verifyEmailDisplayedOnFlogConfirmationScreen("receive email?","Resend Password Reset email");
		objHomePage.verifyTelephoneNumberDisplayedOnFlogConfirmationScreen();
	}
	
	@Then("^Verify following info text is displayed$")
	public void verify_following_info_text_is_displayed(DataTable dt){
		objLogInPage.verifyArticleBlockHeaders("p;If you entered the email associated with your account, you will have a Username Reminder waiting in your inbox~p;Live Help");
		objHomePage.verifyEmailDisplayedOnFlogConfirmationScreen("receive an email? Check your Junk or Promotions Folders. Still no email?","Resend Username Reminder Email");
		objHomePage.verifyTelephoneNumberDisplayedOnFlogConfirmationScreen();
	}
	

	
	@Then("^Verify 'New player\\?' text$")
	public void verify_New_player() {
		objLogInPage.verifyNewPlayerTextIsDisplayed();
	}

	@Then("^Click on 'New player\\?' link$")
	public void click_on_New_player_link() {
		objLogInPage.clickSignUPButtonDisplayed();
	}
	
	@Then("^Verify 'Sign up' link$")
	public void verify_Sign_up_link() {
		objLogInPage.verifySignUPButtonDisplayed();
	}


	@Then("^Verify 'Live Chat' link$")
	public void verify_Live_support_link() {
		objLogInPage.verifyLiveHelpLinkDisplayed();
	}

	@Then("^User enters username \"([^\"]*)\"$")
	public void user_enters_username(String userName) {
		objLogInPage.setUserName(userName);
	}

	@Then("^User enters password \"([^\"]*)\"$")
	public void user_enters_password(String password) {
		objLogInPage.setPassword(password);
	}

	@Then("^User clicks on login button$")
	public void user_clicks_on_login_button() {
		objLogInPage.clickLoginOnLoginPopup();
		//objLogInPage.verifyUnauthorizedErrorMessage();
	}

	@Then("^Click on logout button$")
	public void click_on_logout_button() {
		objLogInPage.clickLogoutlnk();
	}
	
	@Then("^Select 'Remember UserName' checkbox$")
	public void select_Remember_UserName_checkbox(){
		objLogInPage.SelectRemeberUserNameCheckbox();
	}

	@Then("^Verify 'Remember UserName' checkbox is get selected$")
	public void verify_Remember_UserName_checkbox_is_get_selected()  {
		objLogInPage.verifyRemberUserNameCheckboxIsSelectedOrNot();
	}

	@Then("^Click on My Account$")
	public void click_on_My_Account(){
	   objHomePage.clickMyAccount();
	}
	@Then("^Verify \"([^\"]*)\" username is displayed on header$")
	public void verify_username_is_displayed_on_header(String userName)
	{objLogInPage.verifyUserLoggedInSuccessfully(userName);
	}
	
	@Then("^Verify Username auto pupulated with \"([^\"]*)\"$")
	public void verify_Username_auto_pupulated_with(String userNm){
		objLogInPage.verifyTextFromUserNameField(userNm);
	}
	
	@Then("^Click on show toggle button$")
	public void click_on_show_toggle_button(){
		objLogInPage.clickShowButtonDisplayedInPasswordField();
	}

	@Then("^Verify user can see typed password correctly$")
	public void verify_user_can_see_typed_password_correctly()  {
		objLogInPage.verifyPasswordIsDisplayedInTextForm();  
	}
	
	@Then("^User enters username$")
	public void user_enters_username() {
		objLogInPage.setUserNameFromConfig();
	}

	@And("^User enters password$")
	public void user_enters_password() {
		objLogInPage.setPasswordFromConfig();
	}
	
	@Then("^Enter invalid username$")
	public void  enter_invalid_username() {
		objLogInPage.setResetPasswordUserName("Tfgsbcvbn");
	}
	
	@And("^Click on 'Submit'$")
	public void  click_on_Submit() {
		objLogInPage.clickSubmitForResetPassword();
	}
	
	@And("^Click on 'Send username reminder'$")
	public void  click_on_Send_username_reminder() {
		objLogInPage.clickSendUsernameReminder();
	}
	@Then("^Enter valid username as \"([^\"]*)\" to reset password$")
	public void  enter_valid_username_as_to_reset_password(String userNm) {
		objLogInPage.setResetPasswordUserName(userNm);
	}
	
	@Then("^Enter Email id \"([^\"]*)\"$")
	public void  enter_emailId(String emailAddress) {
		objLogInPage.setUsernameReminderEmailAddress(emailAddress);
	}
	@Then("^Verify username is displayed on header$")
	public void verify_username_is_displayed_on_header() {
		objLogInPage.verifyUserLoggedInSuccessfully(configuration.getConfig("web.userName"));
	}

	@Then("^Verify Username is auto pupulated in username field$")
	public void verify_Username_is_auto_pupulated_in_username_field(){
		objLogInPage.verifyTextFromUserNameField(configuration.getConfig("web.userName"));
	}
}

