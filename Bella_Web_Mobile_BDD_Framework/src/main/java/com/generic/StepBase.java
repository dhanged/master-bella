package com.generic;

import java.io.IOException;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.appiumDriver.AppiumManager;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.webdriver.DriverProvider;

import cucumber.api.Scenario;

/**
 * @ScriptName : StepBase
 * @Description : This class contains generic functionalities like
 *              initialize/teardown test environment
 * @Author : Harshvardhan Yadav(SQS)
 * @modified :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class StepBase {

	// Local variables
	private Configuration objConfiguration;
	private DriverProvider objDriverProvider;
	private Utilities objUtilities;
	private LogReporter objLogReporter;
	private AppiumDriverProvider objAppiumDriverProvider;
	private AppiumManager objAppiumManager;

	public StepBase(Configuration configuration, DriverProvider driverProvider, Utilities utilities, 
			AppiumDriverProvider appiumDriverProvider, LogReporter logReporter) {
		this.objConfiguration = configuration;
		this.objDriverProvider = driverProvider;
		this.objUtilities = utilities;
		this.objAppiumDriverProvider = appiumDriverProvider;	
		this.objLogReporter = logReporter;
	}


	/**
	 * initialize environment
	 * 
	 * @throws IOException
	 * @param objScenario
	 */
	public void initializeEnvironment(Scenario objScenario) {
		// updated code to get the environment based on Tag used in scenarion i.e CMS or
		//System.out.println(objScenario.getSourceTagNames());		
		try {
			objDriverProvider.initialize();
			objDriverProvider.getWebDriver().get(objConfiguration.getConfig("web.Url"));
 		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
 
	/**
	 * initialize android environment without scenario
	 * 
	 * @author :Harhvardhan Yadav, Namrata Donikar (Expleo)
	 */
	public void initializeAndroidEnvironment(Scenario objScenario) {
		try {
			objConfiguration.loadConfigProperties();
			objAppiumManager = new AppiumManager();
			objAppiumManager.startAppium();
			objAppiumDriverProvider.initializeConsoleBaseAppium("android", objConfiguration,
					objAppiumManager.getCurrentAppiumIpAddress(), objAppiumManager.getCurrentAppiumPort());
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * initialize android environment without scenario
	 * 
	 * @author :Harhvardhan Yadav, Namrata Donikar (Expleo)
	 */
	public void initializeIOSEnvironment() {

		try {
			objConfiguration.loadConfigProperties();
			objAppiumDriverProvider.initializeDesktopAppium("ios", objConfiguration);
 		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown environment
	 * 
	 * @author Harshvardhan Yadav(SQS)
	 */
	public void tearDownEnvironment(Scenario objScenario) {
		try {
 		if (objScenario.isFailed()) {
 			objLogReporter.takeScreenShotBytes(objScenario);
			}
			this.tearDownEnvironment();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void tearDownEnvironment() {
		try {
			objDriverProvider.tearDown();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown appium environment for android
	 * 
	 * @author Dheerendra Pal(expleo)
	 */

	public void tearDownAppiumEnvironmentAndroid(Scenario scenario) {
		try {
			if (scenario.isFailed()) {
				objLogReporter.takeScreenShotBytesFoMobile(scenario);
			}
			objAppiumDriverProvider.tearDown();
			objAppiumManager.stopServer();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown appium environment for android
	 * 
	 * @author Dheerendra Pal(expleo)
	 */

	public void tearDownAppiumEnvironmentIOS(Scenario scenario) {
		try {
			if (scenario.isFailed()) {
				objLogReporter.takeScreenShotBytesFoMobile(scenario);
			}
			objAppiumDriverProvider.tearDown();
		} catch (Exception exception) {
			exception.printStackTrace();			
		}
	}
}
