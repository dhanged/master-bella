package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class DepositLimitPage {

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	boolean flag = false;

	public DepositLimitPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void switchToDepositLimitIframe()
	{
		objWebActions.switchToFrameUsingNameOrId("deposit-limits-iframe");
	}

	public void verifyCashierTitle(String title)
	{
		By locator = By.xpath("//div[@class='cashier-title'][contains(.,'"+title+"')]");
		logReporter.log("Verify " +title , 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyDepositLimitText(String limit)
	{
		By locator = By.xpath("//p[contains(.,' Only allow me to have a "+limit+"') and contains(.,' deposit loss of up to ')]");
		By locator1 = By.xpath("//p[contains(.,' Only allow me to have "+limit+"') and contains(.,' deposits up to ')]");
		
		if(objWebActions.checkElementDisplayed(locator))
		{logReporter.log("Verify 'Only allow me to have " +limit + "'  deposits loss of up to ' text", 
				objWebActions.checkElementDisplayed(locator));}
		else
		{	logReporter.log("Verify 'Only allow me to have " +limit + "'  deposits up to ' text", 
				objWebActions.checkElementDisplayed(locator1));}
		
		
	}

	public void clickOneditLimit(String lmt)
	{
		By locator = By.xpath("//p[contains(.,'"+lmt+"')]//span[text()='Edit']");
		logReporter.log(" Edit "+lmt , 
				objWebActions.click(locator));
	}

	public void verifyDropdownField(String lbl)
	{
		By locator = By.xpath("//label[text()='"+lbl+" ']//following-sibling::select");
		logReporter.log(" verify  "+lbl +" field on screen" , 
				objWebActions.checkElementDisplayed(locator));
	}

	public void selectDropdownField(String lbl,String value)
	{
		By locator = By.xpath("//label[text()='"+lbl+" ']//following-sibling::select");
		logReporter.log(" verify  "+lbl +" field on screen" , 
				objWebActions.checkElementDisplayed(locator));

		logReporter.log("select deposit limit type as "+value , 
				objWebActions.selectFromDropDown(locator, value,"Text"));
	}
	public void setDepositLimitAmount(String amt)
	{
		By locator = By.xpath("//label[text()='Other amount ']//following-sibling::input");
		logReporter.log(" Enter Other amount as " +amt, 
				objWebActions.setTextWithClear(locator, amt));
	}

	public String getLimitValue(String lmtnm)
	{
		By locator = By.xpath("//p[contains(.,' Only allow me to have a "+lmtnm+"') and contains(.,' deposit loss of up to ')]");
		By locator1 = By.xpath("//p[contains(.,' Only allow me to have "+lmtnm+"') and contains(.,' deposits up to ')]");
		String value = null ;

		if(objWebActions.checkElementDisplayed(locator))
		{ value = objWebActions.getText(locator);}
		else
		{
			value = objWebActions.getText(locator1);
		}
		value = value.substring(value.indexOf("£"));
		value =value.substring(1,5);
		System.out.println("value ::: "+value);
		if(value.contains(" ")) {
			value =value.replace(" ","");
		}
		return value;
	}


	public void verifyLimit(String lmt,String value)
	{
		lmt = lmt.toLowerCase();
		By locator = By.xpath("//p[contains(.,' Only allow me to have a "+lmt+"') and contains(.,' deposit loss of up to ') and contains(.,' £"+value+"')]");
		By locator1 = By.xpath("//p[contains(.,' Only allow me to have a "+lmt+"') and contains(.,' deposits up to ') and contains(.,' £"+value+"')]");
		
		if(objWebActions.checkElementDisplayed(locator))
		{logReporter.log(" verify  "+lmt +" limit is " , 
				objWebActions.checkElementDisplayed(locator));}
		else
		{	logReporter.log(" verify  "+lmt +" limit is " , 
					objWebActions.checkElementDisplayed(locator1));}
	}
	
	public void verifyDepositLimitErrorMessage(String msg)
	{
		By locator = By.xpath("//div[@class='error-box'][contains(.,'"+msg+"')]");
		logReporter.log(" verify ' " +msg+ " '", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void closeMyAccountScreen()
	{
		By locator = By.xpath("//a[@class='close']");
		logReporter.log(" click on 'X'" , 
				objWebActions.click(locator));
	}

	public void verifyFailureMessage(String txt) 
	{
		By imgsymbolsuccess = By.xpath("//*[@class='symbol failure-mark medium']");
		logReporter.log("Verify 'Failure image ' ", 
				objWebActions.checkElementDisplayed(imgsymbolsuccess));

		//By locator = By.xpath("//p[@class=\"text-center text-success\"][contains(.,'Your withdrawal request for £')]");
		By locator = By.xpath("//p[@class='text-center text-failure'][contains(.,'"+txt+"')]");
		logReporter.log("Verify '"+txt+ " ' ", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyNewlyAddedDepositLimitIsDisplayedInPending(String lmt,String value)
	{
		By locator;
		switch(lmt)
		{
		case "Day" :
			locator = By.xpath("//th[@class='pending'][contains(.,'Day')]//following::tr/td[1][@class='pending'][contains(.,' £"+value+"')]//button[text()='Pending request']");
			logReporter.log("Verify '"+lmt+ " ' is displayed in pending ", 
					objWebActions.checkElementDisplayed(locator));
			break;	 
		case "Weekly" :
			locator = By.xpath("//th[@class='pending'][contains(.,'Weekly')]//following::tr/td[2][@class='pending'][contains(.,' £"+value+"')]//button[text()='Pending request']");
			logReporter.log("Verify '"+lmt+ " ' is displayed in pending ", 
					objWebActions.checkElementDisplayed(locator));
			break;
		case "Monthly" :
			locator = By.xpath("//th[@class='pending'][contains(.,'Monthly')]//following::tr/td[3][@class='pending'][contains(.,' £"+value+"')]//button[text()='Pending request']");
			logReporter.log("Verify '"+lmt+ " ' is displayed in pending ", 
					objWebActions.checkElementDisplayed(locator));
			break;
		}
	}

}
