package com.bella.desktop.stepDefinition;


import com.bella.desktop.page.Bella.MessagePage;
import com.bella.desktop.page.Bella.MyAccountPage;
import com.generic.WebActions;
import com.generic.utils.WaitMethods;

import cucumber.api.java.en.Then;

public class MessagePageStep {
	WebActions objwebaction;
	private MessagePage objMessagePage;
	private MyAccountPage objMyAccountPage;
	String messageCount ;
	private WaitMethods waitMethods;
	
	public MessagePageStep(MessagePage messagePage ,MyAccountPage myAccountPage,WebActions objwebaction,WaitMethods waitMethods) {
		this.objMessagePage =  messagePage;
		this.objMyAccountPage = myAccountPage ;
		this.objwebaction = objwebaction ;
		this.waitMethods =  waitMethods ;
	}

	@Then("^Get current message count from my account screen$")
	public void Get_current_message_count_from_my_account_screen()  {
		messageCount = objMessagePage.getMessageCountFromMyAccountScreen();
	}

	@Then("^Verify system displays notification on the My Account screen for new message in the inbox$")
	public void verify_system_displays_notification_on_the_My_Account_screen_for_new_message_in_the_inbox(){
		waitMethods.sleep(50);
		objwebaction.pageRefresh();
		waitMethods.sleep(3);
		
		String currentMsgCount = objMessagePage.getMessageCountFromMyAccountScreen();
		objMessagePage.verifySystemdisplaysUpdatedMessageCountorNot(messageCount,currentMsgCount);
	}


	@Then("^Click on Message option$")
	public void click_on_Message_option() {
		objMyAccountPage.selectMyAccountMenu("Message", null);  
	}

	@Then("^Verify user receive the message$")
	public void verify_user_receive_the_message() {
		//objMessagePage.verifyRecivedMessageDetails("SQS Automation","Auto_Test message","read");
		objMessagePage.verifyRecivedMessageDetails("Customer Services","expleo test message","read");
	}

	@Then("^Verify latest message display on top$")
	public void verify_latest_message_display_on_top() {
		objMessagePage.verifyLatestMessageDisplayInTop();
	}

	@Then("^Verify message display on screen$")
	public void verify_message_display_on_screen() {
		objMessagePage.verifyMessageDisplayedInMessageScreen();
	}

	@Then("^verify unread message icon is display for unread message$")
	public void verify_unread_message_icon_is_display_for_unread_message() {
		String currentMsgCount = objMessagePage.getMessageCountFromMyAccountScreen();
		objMessagePage.verifyunreadMessageCountOnMyAccountScreen(currentMsgCount);
	}

	@Then("^Open Message from list of message$")
	public void open_Message_from_list_of_message() {
		objMessagePage.selectAnyMessageFromList();
		objMessagePage.verifyPaginationLinkOnMessageBodyScreen();
		objMessagePage.verifyMessageDeleteIcon("enable");
		objMessagePage.verifyMessageContentDetailsScreen();		
	}

	@Then("^Click on unread Message from list of message$")
	public void click_on_unread_Message_from_list_of_message() {
		//objMessagePage.selectUnreadMessage("SQS Automation");
		objMessagePage.selectUnreadMessage("Customer Services");
		
	}

	@Then("^Verify message subject,sender name and message content is display on screen$")
	public void verify_message_subject_sender_name_and_message_content_is_display_on_screen() {
	//	objMessagePage.verifyMessageBodyDetails("SQS Automation", "Auto_Test message", "Test purpose only");
		objMessagePage.verifyMessageBodyDetails("Customer Services", "expleo test message", "test purpose only");
	}

	@Then("^Navigate back to screen$")
	public void navigate_back_to_screen() {
		objMyAccountPage.clickBackBTN();
	}

	@Then("^verify unread message icon change into read icon$")
	public void verify_unread_message_icon_change_into_read_icon() {
		//objMessagePage.verifyRecivedMessageDetails("SQS Automation","Auto_Test message","read");
		objMessagePage.verifyRecivedMessageDetails("Customer Services", "expleo test message","read");
	}

	@Then("^Verify Delete icon display in disable mode$")
	public void verify_Delete_icon_display_in_disable_mode() {
		objMessagePage.verifyMessageDeleteIcon("disable");
	}

	@Then("^select message to delete$")
	public void select_message_to_delete() {
		//objMessagePage.selectMessageCheckbox("Auto_Test message");
		objMessagePage.selectMessageCheckbox("expleo test message");
	}

	@Then("^click on Delete icon$")
	public void click_on_Delete_icon() {
		objMessagePage.clickMessageDeleteIcon();
	}

	@Then("^verify selected message gets delete$")
	public void verify_selected_message_gets_delete() {
		//objMessagePage.verifyDeletedMessageNotDisplayedInList("SQS Automation","Auto_Test message");
		objMessagePage.verifyDeletedMessageNotDisplayedInList("Customer Services", "expleo test message");
	}

	@Then("^Verify 'Your inbox is empty' message is displayed when user delete all message$")
	public void verify_Your_inbox_is_empty_message_is_displayed_when_user_delete_all_message() {
		objMessagePage.SelectAllMessageFromInbox();
		objMessagePage.verifyYourInboxiIsEmptyMessageIsDisplayed();
	}
	
	@Then("^verify user receive configured message$")
	public void verify_user_receive_configured_message() {
	    // Write code here that turns the phrase above into concrete actions
		waitMethods.sleep(50);
		objwebaction.pageRefresh();
		objMessagePage.verifyRecivedMessageDetails("Customer Services", "Bella Casino Test", "unread");
		//objMessagePage.verifyRecivedMessageDetails("Expleo_Automation","AutoTestMessage_", "unread");
	}
	@Then("^verify Content of the messages like images, text and CTAs$")
	public void verify_Content_of_the_messages_like_images_text_and_CTAs() {
	    // Write code here that turns the phrase above into concrete actions
		objMessagePage.verifyImage();
		objMessagePage.verifyCTAText("Get Started");
	}
	
}
