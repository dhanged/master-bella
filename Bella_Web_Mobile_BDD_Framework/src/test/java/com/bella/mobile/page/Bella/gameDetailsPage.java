package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;

public class gameDetailsPage {
	
	private MobileActions objMobileActions ;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public gameDetailsPage(MobileActions mobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration) {
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}
	
	public void moveTheCursorToGame()
	{
		By locator = By.xpath("//div[@class='component_game-panels']//game-panel//div//div[@class='game-panel-labels']");
		logReporter.log("Verify Bella Casinos logo displayed.", 
				objMobileActions.performMouseHover(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
	}
	public void clickOnGameInfoIcon()
	{
		By locator = By.xpath("//div[@class='component_game-panels']//game-panel//div//following-sibling::div[@class='right-link']");
		logReporter.log("Click on game info icon", 
				objMobileActions.click(locator));
	}
	
	public void verifyGameDetailsPageDisplayed() {
		By pageGameDetails = By.xpath("//div[@class='details-games']");
		logReporter.log("Verify game Details page displayed", 
				objMobileActions.checkElementDisplayed(pageGameDetails));
	}
	
	public void verifyGameDetailsTab(String tabName)
	{
		if(tabName.contains(",")){
			String[] arr1 = tabName.split(",");
			for (String links : arr1  ) {
				
				By lnkTAB = By.xpath("//div[contains(@class,'component description-tabs')]//ul[@class='tab-links']//li//a[text()='"+links+"']");
				logReporter.log(links+" displayed on game details page ",
						objMobileActions.checkElementDisplayed(lnkTAB));}}
		else{
			By lnkTAB = By.xpath("//div[contains(@class,'component description-tabs')]//ul[@class='tab-links']//li//a[text()='"+tabName+"']");
			logReporter.log(tabName+" displayed  on game details page",
					objMobileActions.checkElementDisplayed(lnkTAB));}
	}
	public void ClickGameDetailsTab(String tabName)
	{			By lnkTAB = By.xpath("//div[contains(@class,'component description-tabs')]//ul[@class='tab-links']//li//a[text()='"+tabName+"']");
			
			logReporter.log("Click on "+tabName+"from game details page",
					objMobileActions.clickUsingJS(lnkTAB));
	}
	
	public void clickOnPlayButton()
	{
		By locator = By.xpath("//div[@class='component_game-panels']//game-panel//div//button[text()='Play']");
		logReporter.log("Click on Play button", 
				objMobileActions.click(locator));
	}
}
