package com.bella.desktop.page.Bella;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class DepositPage {

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	boolean flag = false;
	public DepositPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	By PlayableBalanceAmount = By.xpath("//span[@class='myaccount-preview__amount preview-amount']");

	public void verifyLiveHelpLinkDisplayed() { 
		By lnkLiveHelp = By.xpath("//a[text()='Live Help']");
		logReporter.log("Verify 'Live Help' link displayed", 
				objWebActions.checkElementElementDisplayedWithMidWait(lnkLiveHelp));
	}

	public void switchToDepositIframe()
	{
		objWebActions.switchToFrameUsingNameOrId("payment-process");
	}

	public void switchToPaysafeIframe()
	{
		waitMethods.sleep(8);
		objWebActions.switchToChildWindow();
		waitMethods.sleep(5);
		//objDriverProvider.getWebDriver().switchTo().defaultContent();
		By locator = By.xpath("//iframe[contains(@src,'paysafecard')]");
		objWebActions.switchToFrameUsingIframe_Element(locator);
		//objDriverProvider.getWebDriver().switchTo().frame(objWebActions.processElement(locator));
		waitMethods.sleep(3);
	}
	public void switchToCardDepositIframe()
	{
		waitMethods.sleep(10);

		By locator = By.xpath("//iframe[@name=\"cdeIframe\"]");
		List<WebElement> lst = objDriverProvider.getWebDriver().findElements(locator);
		System.out.println(lst.size());
		objDriverProvider.getWebDriver().switchTo().frame(objWebActions.processElement(locator));
		waitMethods.sleep(3);
	}
	public boolean verifyAddNewMethodButton()
	{
		By locator = By.xpath("//button[contains(.,' Add New Method')]");
		return objWebActions.checkElementElementDisplayedWithMidWait(locator);
	}

	public void clickAddNewMethodButton()
	{
		By locator = By.xpath("//button[contains(.,' Add New Method')]");
		logReporter.log("click 'Add new payment method' ",
				objWebActions.click(locator));

	}
	public void verifyAddNewPaymentMethodScreen()
	{

		By locator = By.xpath("//div[contains(@class,'cashier-title')][contains(.,' Add new payment method')]");
		logReporter.log("Verify 'Add new payment method' screen display",
				objWebActions.checkElementDisplayed(locator));
	}

	public boolean  verifyAddedMethoddisplayedOrNot()
	{
		By locator = By.xpath("//div[contains(@class,'cashier-title')][contains(.,' Select payment method')]//following::app-account-types//ul//li[contains(@class,'list-group')]");
		return objWebActions.checkElementDisplayed(locator);
	}

	public void selectAddedPaymentMethod(String methodNm)
	{
		By locator = By.xpath("//div[contains(@class,'cashier-title')][contains(.,' Select payment method')]//following::app-account-types//ul//li[contains(@class,'list-group')]//div//img");
		String paymentMethodName = objWebActions.getAttribute(locator, "alt");
		System.out.println("************** paymentMethodName "+paymentMethodName);
		/*	if((paymentMethodName.contains(methodNm))||paymentMethodName.contains(methodNm))
		{
			logReporter.log("Select card payment method ",
					objWebActions.click(locator));
		}
		else if(paymentMethodName.contains(methodNm))
		{
			logReporter.log("Select 'PaySafeCard' method ",
					objWebActions.click(locator));
		}
		else*/
		{
			logReporter.log("Select 'Paypal' method ",
					objWebActions.click(locator));
		}

	}
	public void selectDepositPaymentMethod(String paymentMethod) {
		By locator = null;

		switch(paymentMethod.toLowerCase()) 
		{
		case "card":
			locator = By.xpath("//div[contains(@class,'account-type')]//div//img[@alt='Debit Card logo']");
			logReporter.log("Select Deposit method as  card",  
					objWebActions.click(locator));
			break;

		case "paypal":
			locator = By.xpath("//div[contains(@class,' account-type')]//div//img[@alt='PayPal logo']");
			logReporter.log("Select Deposit method as paypal",  
					objWebActions.click(locator));
			break;
		case "paysafecard":
			locator = By.xpath("//div[contains(@class,' account-type')]//div//img[@alt='PaySafeCard logo']");
			logReporter.log("Select Deposit method as PaySafeCard",  
					objWebActions.click(locator));
			break;
		}
		waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
	}

	public void setCardNumber(String cardNumber)
	{
		//By inpCardNumber = By.id("cardNumber");
		By inpCardNumber = By.id("CardNumber");
		waitMethods.sleep(5);
		objWebActions.checkElementElementDisplayedWithMidWait(inpCardNumber);
		logReporter.log("Set Card number ", cardNumber, 
				objWebActions.setTextWithClear(inpCardNumber, cardNumber));
	}

	public void setExpMonth(String expMon)
	{
		By inpExpYear = By.xpath("//select[@id='ExpiryMonth']");
		logReporter.log("Set expiry Month ", expMon, 
				objWebActions.selectFromDropDown(inpExpYear,expMon,"Value"));
	}

	public void setExpYear(String expYr)
	{
		By inpExpYear = By.xpath("//select[@id='ExpiryYear']");
		logReporter.log("Set expiry Year ", expYr, 
				objWebActions.selectFromDropDown(inpExpYear,expYr,"Value"));
	}

	public void setCVVcode(String securityCodeCVV) {

		By inpSecurityCodeCVV = By.id("cvn");
		logReporter.log("Set security date (CVV) ", securityCodeCVV, 
				objWebActions.setTextWithClear(inpSecurityCodeCVV, securityCodeCVV));
	}

	public void switchToCVNiframe()
	{

		By locator = By.xpath("//iframe[@name=\"cvnIframe\"]");
		List<WebElement> lst = objDriverProvider.getWebDriver().findElements(locator);
		System.out.println(lst.size());
		objDriverProvider.getWebDriver().switchTo().frame(objWebActions.processElement(locator));
		waitMethods.sleep(5);
	}

	public void setCVncode(String securityCodeCVV) {

		By inpSecurityCodeCVV = By.xpath("//input[@name=\"cvn\"]");
		logReporter.log("Set security date (CVV) ", securityCodeCVV, 
				objWebActions.setTextWithClear(inpSecurityCodeCVV, securityCodeCVV));
	}
	public void verifyErrormessage(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {

				//By errMsg = By.xpath("//p[@class='error-text'][contains(.,'"+links+"')]");
				By errMsg = By.xpath("//div[@class='error validation'][contains(.,'"+links+"')]");
				logReporter.log(links+" displayed  ",
						objWebActions.checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//div[@class='error validation'][contains(.,'"+err+"')]");
			logReporter.log(err+" displayed  ",
					objWebActions.checkElementDisplayed(errMsg));}
	}

	public void verifyFeildLabels(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("/label[text()='"+links+"']");
				logReporter.log(links+" displayed  ",
						objWebActions.checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//p[@class='error-text'][contains(.,'"+err+"')]");
			logReporter.log(err+" displayed  ",
					objWebActions.checkElementDisplayed(errMsg));}
	}

	public boolean verifySubmitButton()
	{
		By btnAddCard = By.xpath("//input[@id='submitbtn']");
		return objWebActions.checkElementDisplayed(btnAddCard);
	}
	public void ClickAddCardButton()
	{
		//By btnAddCard = By.xpath("//button[@class='active cashier-button'][contains(.,'Add card')]");
		By btnAddCard = By.xpath("//input[@id='submitbtn']");
		logReporter.log(" Click on 'Add card' button ",  
				objWebActions.click(btnAddCard));
	}
	public String getPlayableBalanceAmount()
	{
		return objWebActions.getText(PlayableBalanceAmount);
	}

	public void clickExpandBalanceBTN()
	{
		By balanceExpandBTN = By.xpath("//button[@class='preview-toggle reset']");
		logReporter.log(" Click on Expand Balance (+) button ",
				objWebActions.click(balanceExpandBTN));
	}

	public String getBalance()
	{
		By locator = By.xpath("//strong[@class='balance-amount']");
		String val = objWebActions.getText(locator);
		return val.substring(1);

	}
	public void verifyLabelsOnMyAccount(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");

			for (String labels2 : arr1) 
			{
				By label01 = By.xpath("//div[@class='myaccount-balance']/ul/li/span[contains(text(),'"+labels2+"')]");

				logReporter.log(" Verify "+labels2+" is displayed on Balance >>My account screen",  
						objWebActions.checkElementDisplayed(label01));
			}
		}
	}

	public String getValueofLabelOnDetailedView(String label)
	{
		By value = By.xpath("//span[contains(text(),'"+label+"')]//following::span[@class='value']");
		String val = objWebActions.getText(value);
		return val.substring(1);
	}

	public void clickOnButton(String btnName)
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By locator = By.xpath("//button[contains(text(),'"+btnName+"')]");
		logReporter.log(" Click on ' "+btnName+" ' button ",  
				objWebActions.clickUsingJS(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void verifyButton(String btnName)
	{
		By locator = By.xpath("//button[contains(text(),'"+btnName+"')]");
		logReporter.log(" verify ' "+btnName+" ' button ",  
				objWebActions.checkElementDisplayed(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void validateBalanceDisplayed(String HomepageBalance)
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		String PlayableBal = this.getPlayableBalanceAmount();
		logReporter.log(" Balance displayed on Homepage is: "+HomepageBalance +" matching the Playable Balance on My account screen " ,
				HomepageBalance.equals(PlayableBal));
	}

	public void selectBonusOption(String option)
	{
		By locator = By.xpath("//label[text()='"+option+"']//preceding::input[1]");
		logReporter.log("Select ' "+option+" '",  
				objWebActions.click(locator));
	}
	public void setDepositAmount(String amt) {

		By locator = By.xpath("//input[@placeholder='Or other amount']");
		logReporter.log("Enter deposit amount as " ,amt, 
				objWebActions.setTextWithClear(locator, amt));
	}

	public void clickOnDeposit()
	{
		By btnNext = By.xpath("//button[contains(.,'Next')]");

		By btnDeposit = By.xpath("//button[contains(.,'Deposit')]");

		if(verifySubmitButton())
		{
			ClickAddCardButton();
		}
		else if(objWebActions.checkElementDisplayed(btnNext))
		{
			logReporter.log( "Click on next",  
					objWebActions.click(btnNext));
		}
		else if(objWebActions.checkElementDisplayed(btnDeposit))
		{
			logReporter.log( "Click on Deposit",  
					objWebActions.click(btnDeposit));
		}

	}
	public void verifyPOPFCheckbox() 
	{
		By locator = By.xpath("//label[contains(.,' I acknowledge that in the event of insolvenc')]//preceding::input[1]");
		logReporter.log("Verify POPF checkbox" , 
				objWebActions.checkElementDisplayed(locator));

	}

	public boolean verifyAcknowledgeCheckbox() 
	{
		By locator = By.xpath("//label[contains(.,' I acknowledge that in the event of insolvenc')]//preceding::input[1]");
		return objWebActions.checkElementElementDisplayedWithMidWait(locator);

	}
	public void selectAcknowledgeCheckbox() {
		By locator = By.xpath("//label[contains(.,' I acknowledge that in the event of insolvenc')]//preceding::input[1]");
		logReporter.log("Select checkbox ",  
				objWebActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public boolean verifyNext() 
	{
		By btnNext = By.xpath("//button[@type='submit' and contains(text(),'Next')]");
		return objWebActions.checkElementDisplayed(btnNext);

	}
	public void clickNext() {
		By btnNext = By.xpath("//button[@type='submit' and contains(text(),'Next')]");
		logReporter.log("Click 'Next' button ",  
				objWebActions.click(btnNext));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void setPinForPaySafeCard(String pin) {

		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		//By locator = By.id("classicPin-addPinField");
		By locator = By.xpath("//input[@id='classicPin-addPinField']");
		logReporter.log("Set PIN for paysafecard", pin, 
				objWebActions.setText(locator, pin));
	}

	public void selectAcceptTermsCheckboxForPaySafeCard() {
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By locator = By.xpath("//input[@id='acceptTerms']");
		logReporter.log("Select Yes, I agree to the general terms and conditions",  
				objWebActions.clickUsingJS(locator));
	}

	public void clickPayButtonOnPaySafeCard() {
		By locator = By.xpath("//span[text()='Pay']/parent::a[@id='payBtn']");
		logReporter.log("Click 'Pay' button", 
				objWebActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}


	public void setPayPalEmailAddress(String emailAddress) 
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By loc = By.xpath("//section[@id='login']//p[@class='paypal-logo paypal-logo-long']");
		System.out.println(objWebActions.checkElementDisplayed(loc));
		this.verifyCookiesPopupForPaypal();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By locator = By.xpath("//input[@id='email' and @placeholder='Email address or mobile number']");
		logReporter.log("Set paypal email address", emailAddress, 
				objWebActions.setText(locator, emailAddress));
		this.verifyCookiesPopupForPaypal();
	}

	public void verifyCookiesPopupForPaypal()
	{
		By lnkAcceptCookies = By.xpath("//button[@id='acceptAllButton']");
		if(objWebActions.checkElementElementDisplayedWithMidWait(lnkAcceptCookies))
		{	
			logReporter.log("Click 'Accept Cookies' button", 
					objWebActions.click(lnkAcceptCookies));
			flag = true;
			System.out.println("set flag " +flag);
		}
	}
	public void setPayPalPassword(String password) {
		By locator = By.xpath("//input[@id='password']");
		logReporter.log("Set paypal password", password, 
				objWebActions.setText(locator, password));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void clickLoginButtonOnPaypal() {
		By locator = By.xpath("//button[@id='btnLogin']");
		logReporter.log("Click login button", 
				objWebActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void clickPayNowButtonOnPaypal() 
	{
		boolean flag=false;
		By locator = By.xpath("//button[text()='Pay Now']");
		do {
			flag = objWebActions.checkElementDisplayed(locator);
			if(flag==true)
			{logReporter.log("Click 'Pay Now' button", 
					objWebActions.click(locator));}
			System.out.println(flag);
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		}while(flag);

	}

	public void verifyPageTitle(String expTitle)
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		String title = objDriverProvider.getWebDriver().getTitle();
		logReporter.log("Verify page redirect to ' "+expTitle, 
				title.equalsIgnoreCase(expTitle));
	}

	public void verifyDepositConfirmation()
	{
		By imgsymbolsuccess = By.xpath("//*[@class='symbol success-mark medium']");
		logReporter.log("Verify ' success image ' ", 
				objWebActions.checkElementDisplayed(imgsymbolsuccess));
	}


	public void verifyDepositAmountIsDisplayedCorrectlyOrNot(String amt)
	{
		By depositAmt = By.xpath("//span[text()='Deposit amount']//following-sibling::span");
		String currentAmount = objWebActions.getText(depositAmt);
		System.out.println(currentAmount);
		System.out.println(amt);
		logReporter.log("Verify 'Deposit Amount is displayed as  "+currentAmount, 
				currentAmount.contains(amt));
	}

	public String verifyBonusReceived()
	{
		By bonusReceived = By.xpath("//span[text()='Bonus received']//following-sibling::span"); 
		logReporter.log("Verify 'Bonus received' ", 
				objWebActions.checkElementDisplayed(bonusReceived));
		String bonusReceivedAmt =objWebActions.getText(bonusReceived).substring(1);
		System.out.println(bonusReceivedAmt);
		return bonusReceivedAmt;
	}
	public void verifyCurrentBalanceOnSuccessScreen(String initialBalnce,String depAmt)
	{
		By currentBalance = By.xpath("//span[text()='Current balance']//following-sibling::span"); 
		String HomePageBalAfterDep =objWebActions.getText(currentBalance).substring(1);
		System.out.println("HomePageBalAfterDep : "+HomePageBalAfterDep);
		float SumOfinitialBalDepAmt =Float.parseFloat(initialBalnce) + Float.parseFloat(depAmt) ;
		System.out.println("SumOfinitialBalDepAmt : "+SumOfinitialBalDepAmt);

		logReporter.log(" Balance after deposit is  : "+HomePageBalAfterDep ,
				String.valueOf(Float.parseFloat(HomePageBalAfterDep)).equals(String.valueOf(SumOfinitialBalDepAmt)) );
	}

	public void verifyPlayNowButton()
	{
		By btnPlayNow = By.xpath("//button[contains(.,' Play now')]");
		logReporter.log("Verify 'Play Now button' ", 
				objWebActions.checkElementDisplayed(btnPlayNow));
	}

	public void verifyAndCloseButton()
	{
		By btnPlayNow = By.xpath("//button[contains(@class,'cashier-button')][contains(.,'Close')]");
		logReporter.log("Verify  Close button' ", 
				objWebActions.checkElementDisplayed(btnPlayNow));
		logReporter.log("Click on  Close button' ", 
				objWebActions.click(btnPlayNow));
	}

	public void selectBonusFromList()
	{
		By locator = By.xpath("//ul[contains(@class,'list-group')]//app-promotion-bonus-item"); 
		logReporter.log("Select bonus ", 
				objWebActions.click(locator));
	}

	public void verifyYourbonusIsDisplayedOnDepsoitScreen()
	{
		By locator = By.xpath("//label[contains(.,'Your bonus')]"); 
		logReporter.log("Verify 'Your bonus' section", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyBonusBalanceDisplayedCorrectly(String initialBonusBalance,String recivedAmt)
	{
		String currentBalane =  getValueofLabelOnDetailedView("Bonuses");
		System.out.println("***** currentBalane " +currentBalane);

		///currentBalane =String.valueOf(Float.parseFloat(currentBalane));
		//System.out.println("***** currentBalane " +currentBalane);
		System.out.println("***** initialBonusBalance :::  "+initialBonusBalance);

		initialBonusBalance = initialBonusBalance.substring(1);
		//float expectedBonusBalance =Float.parseFloat(initialBonusBalance) + Float.parseFloat(recivedAmt) ;
		//	int expectedBonusBalance =Integer.valueOf(initialBonusBalance) + Integer.parseInt(recivedAmt) ;
		//System.out.println("*****recivedAmt " +recivedAmt+  "    initialBonusBalance:::   "+initialBonusBalance);
		/*logReporter.log(" Bonus Balance  : "+expectedBonusBalance ,
				String.valueOf((currentBalane)).equals(String.valueOf((expectedBonusBalance))) );*/

		if(Integer.valueOf(currentBalane) > Integer.valueOf(initialBonusBalance)){
			logReporter.log("Balance displayed correctly " ,true);
		}
		else{
			logReporter.log("Incorrect Balance displayed " ,false);}
	}

	public void validateBalanceAfterDeposit(String initialBal , String depAmt)
	{
		waitMethods.sleep(5);
		By BalanceOnHomepage = By.xpath("//li[@class='text-data']/a/div/span/strong"); 

		String HomePageBalAfterDep = objWebActions.getText(BalanceOnHomepage).substring(1);

		float SumOfinitialBalDepAmt =Float.parseFloat(initialBal) + Float.parseFloat(depAmt) ;
		System.out.println("SumOfinitialBalDepAmt : "+SumOfinitialBalDepAmt);

		logReporter.log(" Balance after deposit is  : "+HomePageBalAfterDep ,
				String.valueOf(Float.parseFloat(HomePageBalAfterDep)).equals(String.valueOf(SumOfinitialBalDepAmt)) );
	}
}

