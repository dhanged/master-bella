Feature: PMI-380

@Desktop
Scenario: Check marketing pref
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Marketing Preferences" tab from my account screen
Then User will get the option to select between email, phone, sms, post and Select all
Then User will select any options
And Click on 'Update'
Then User will get "Account details updated" confirmation message
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
Then the selected option should be updated in cogs player details

@Desktop
Scenario: Check wheather updated Phone number is displayed in COGS
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
Then Change the phone number
And Enter password
And Click on 'Update'
Then User will get "Account details updated" confirmation message
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
Then Updated phone number should be display