package com.bella.mobile.stepDefinition;

import com.bella.mobile.page.Bella.FooterPage;
import com.bella.mobile.page.Bella.HomePage;
import com.bella.mobile.page.Bella.PageValidation;
import com.generic.MobileActions;
import com.generic.utils.Configuration;
import cucumber.api.java.en.Then;

public class FooterSteps {

	private FooterPage objFooterPage;
	private PageValidation objPageValidation ;
	private HomePage objHomePage ;
	private Configuration objconfiguration;
	private MobileActions objWebActions;
	String url;

	public FooterSteps(FooterPage objFooterPage,PageValidation objPageValidation,HomePage objHomePage,Configuration configuration ,MobileActions objWebActions) {		
		this.objFooterPage = objFooterPage;
		this.objPageValidation = objPageValidation ;
		this.objHomePage = objHomePage ;
		this.objconfiguration = configuration ;
		this.objWebActions = objWebActions ;
	}

	@Then("^Verify Regulators \\(UK and Alderney\\) and associated license statements is displayed under License section of the footer$")
	public void verify_Regulators_UK_and_Alderney_and_associated_license_statements_is_displayed_under_License_section_of_the_footer() {
		objFooterPage.verifyUKGamblingCommissionLinkDisplayed();
		objFooterPage.verifyAlderneyGamblingControlCommissionLinkDisplayed();
		objFooterPage.verifyAssociatedLicenseStatement();
	}

	@Then("^Click and verify 'UK Gambling Commission' link redirect to the correct page$")
	public void click_and_verify_UK_Gambling_Commission_link_redirect_to_the_correct_page(){
		objFooterPage.clickUKGamblingCommissionLink();
	}
	@Then("^Verify \"([^\"]*)\" link is displayed on under \"([^\"]*)\" footer section$")
	public void verify_link_is_displayed_on_under_footer_section(String arg1, String arg2) {
		objFooterPage.verifyFooterLinks(arg2,arg1);
	}

	@Then("^Verify Terms & Conditions information should easily be accessible from any place on the site$")
	public void verify_Terms_Conditions_information_should_easily_be_accessible_from_any_place_on_the_site() {
		objFooterPage.verifyFooterLinks("HELP","Responsible Gambling~Terms & Conditions~Privacy Policy~Support~FAQs");
		url = objconfiguration.getConfig("web.Url");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Introduction and customer services~h3;Changes to our Terms of Use~h3;Opening an account and gambling with us – your responsibilities and commitments~h3;Proof of your age, identity and source of funds~h3;Geographical Restrictions");


		objHomePage.navigateToPrimaryTabs("Slots");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Introduction and customer services~h3;Changes to our Terms of Use~h3;Opening an account and gambling with us – your responsibilities and commitments~h3;Proof of your age, identity and source of funds~h3;Geographical Restrictions");

		objHomePage.navigateToPrimaryTabs("Jackpots");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Introduction and customer services~h3;Changes to our Terms of Use~h3;Opening an account and gambling with us – your responsibilities and commitments~h3;Proof of your age, identity and source of funds~h3;Geographical Restrictions");


		objHomePage.navigateToPrimaryTabs("Promos");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Introduction and customer services~h3;Changes to our Terms of Use~h3;Opening an account and gambling with us – your responsibilities and commitments~h3;Proof of your age, identity and source of funds~h3;Geographical Restrictions");


		objHomePage.navigateToPrimaryTabs("Casino");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Introduction and customer services~h3;Changes to our Terms of Use~h3;Opening an account and gambling with us – your responsibilities and commitments~h3;Proof of your age, identity and source of funds~h3;Geographical Restrictions");
	}

	@Then("^Verify 'Privacy Policy' link is accessible on the site$")
	public void verify_Privacy_Policy_link_is_accessible_on_the_site() {
		url = objconfiguration.getConfig("web.Url");
		objFooterPage.clickOnFooterLinks("HELP","Privacy Policy");
		objWebActions.switchToWindowUsingTitle("Privacy Policy | Bella Casino");
		objFooterPage.verifyURLRedirection(url+"privacy-policy#privacy");
		//PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Privacy Policy~h2;Cookies Policy");
		objPageValidation.verifyArticleBlockHeaders("h1;Privacy Policy");
	}

	@Then("^Verify \"([^\"]*)\" logo is displayed in footer section$")
	public void verify_logo_is_displayed_in_footer_section(String arg1){
		objFooterPage.verifyLogoInFooterSection(arg1);
	}

	@Then("^Verify \"([^\"]*)\" logo hyperlinked to the correct page$")
	public void verify_logo_hyperlinked_to_the_correct_page(String arg1) {
		objFooterPage.verifyLogoLinkRedirectToTheCorrectPage(arg1);
	}

	@Then("^Verify (\\d+)\\+ logo should not be a hyperlink$")
	public void verify_logo_should_not_be_a_hyperlink(int arg1){
		objFooterPage.verify18LogoDisplayed();
	}

	@Then("^Access Terms & Conditions link and Verify that information is available to the customer regarding cheating, recovered player funds and how to complain$")
	public void access_Terms_Conditions_link_and_Verify_that_information_is_available_to_the_customer_regarding_cheating_recovered_player_funds_and_how_to_complain(){
		objFooterPage.verifyFooterLinks("HELP","Responsible Gambling~Terms & Conditions~Privacy Policy~Support~FAQs");

		url = objconfiguration.getConfig("web.Url");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Prohibited Conduct~p;17.1 We reserve the right to seek criminal and other sanctions against you if you have, or we suspect you have, engaged in any form of criminal activity, collusion (including in relation to chargebacks), cheating (including obtaining any unfair advantage), fraudulent practice or unlawful, improper or dishonest activity (together ‘Prohibited Conduct’). You agree that we may disclose any information relating to Prohibited Conduct (including your personal information) to the relevant authorities and other relevant parties (such as other gambling operators).");
	}

	@Then("^Access Terms & Conditions link and Verify that information is available to the customer regarding third party software$")
	public void access_Terms_Conditions_link_and_Verify_that_information_is_available_to_the_customer_regarding_third_party_software() {
		url = objconfiguration.getConfig("web.Url");
		System.out.println("****url "+url);
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions");
		objPageValidation.verifyArticleBlockHeaders("h1;Our terms and conditions~h3;Software~p;19.1 For certain services we offer, you may need to download or use certain additional software, including software provided by third parties.~p; You may be required to enter into end user terms and conditions of use in order to make use of such software. You agree to be bound by the terms of any such agreement.~p;19.3 You shall not use (other than for its specified purpose), interfere with, copy, modify, decode, reverse engineer, disassemble, decompile, translate, convert or create derivative works from any software provided to you by us and/or any third party or attempt to do so.");
	}

	@Then("^Click \"([^\"]*)\" link is displayed on under \"([^\"]*)\" footer section$")
	public void click_link_is_displayed_on_under_footer_section(String arg1, String arg2) {
		objFooterPage.clickOnFooterLinks(arg2,arg1);
	}

	@Then("^Verify that Phone number and Email is displayed on the screen$")
	public void verify_that_Phone_number_and_Email_is_displayed_on_the_screen()  {
		objPageValidation.verifyArticleBlockHeaders("h3;Call us from 8am - midnight on 0330 102 8582~h3;Email us at:~a;support@bellacasino.com");
	}

	@Then("^Verify 'Userful Links' and 'Help' sections is displayed in footer section$")
	public void verify_Userful_Links_and_Help_sections_is_displayed_in_footer_section() {
		objFooterPage.verifyUsefulLinksDisplayed();
		objFooterPage.verifyHelpLinksDisplayed();
	}
	@Then("^Click on different links and verify system redirects to the correct page$")
	public void click_on_different_links_and_verify_system_redirects_to_the_correct_page(){
		url = objconfiguration.getConfig("web.Url");
		objFooterPage.clickOnFooterLinks("HELP","Terms & Conditions,"+url+"terms-and-conditions"+"~Support,"+url+"support"+"~FAQs,"+url+"faqs");

		objFooterPage.clickOnFooterLinks("HELP","Support");
		objPageValidation.verifyArticleBlockHeaders("h3;Call us from 8am - midnight on 0330 102 8582~h3;Email us at:~a;support@bellacasino.com");
	
		objFooterPage.clickOnFooterLinks("USEFUL LINKS","Casino,"+url+"casino"+"~Play Slots,"+url+"slots-and-games/all-slots");
		objFooterPage.clickOnFooterLinks("HELP","Privacy Policy");
		objFooterPage.clickOnFooterLinks("HELP","Responsible Gambling");
		
		objFooterPage.clickOnFooterLinks("USEFUL LINKS","Grosvenor Casinos");
		objWebActions.switchToWindowUsingTitle("Privacy Policy | Bella Casino");
		objFooterPage.verifyURLRedirection(url+"privacy-policy#privacy");
		objWebActions.switchToWindowUsingTitle("Welcome to the Rank Group’s dedicated responsible gambling website - Keep it fun");
		objFooterPage.verifyURLRedirection("https://keepitfun.rank.com/");
		objWebActions.switchToWindowUsingTitle("Online Casino | Play Online with the UK's Biggest Casino Brand");
		objFooterPage.verifyURLRedirection("https://tst01-gros-cms3.rankgrouptech.net/");
		//objFooterPage.verifyURLRedirection("https://www.grosvenorcasinos.com/");
	}
}
