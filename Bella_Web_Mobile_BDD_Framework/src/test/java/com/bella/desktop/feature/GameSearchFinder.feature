Feature: Game Search Finder

@Desktop
Scenario: Check that system displays the appropriate options(Game name) on entering text in the search box with Game image, game name and Play button on search window
Given Invoke the Bella site on Desktop
When User clicks on search field
Then Verify Search overlay opens
When Search Game "action" in Search field from header
Then Verify system displays search results to user 
Then Verify system displays following options for search game "action"
|Image|
|Name|
|Play Now CTA|
|Info CTA|


@Desktop
Scenario: Check that system displays Error message if no search is found for the entered text
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
When User clicks on search field
When Search Game "xcs" in Search field from header
Then Verify system displays no search is found error message


@Desktop
Scenario: Check that system allows to browse the sections on clicking on error messages observed for wrong search
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
When User clicks on search field
When Search Game "tdf" in Search field from header
Then System displays browse section with error message
And Click on browse section
Then System allows to browse the sections on clicking on error messages observed for wrong search