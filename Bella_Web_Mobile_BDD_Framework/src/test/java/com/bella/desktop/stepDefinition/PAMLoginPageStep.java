package com.bella.desktop.stepDefinition;


import com.bella.desktop.page.PAM.PAMLoginPage;
import com.bella.desktop.page.cogs.Cogs_LoginPage;
import com.generic.WebActions;
import com.generic.utils.Configuration;

import cucumber.api.java.en.Then;

public class PAMLoginPageStep {

	PAMLoginPage objPAMLoginPage;
	private WebActions objWebActions;
	Cogs_LoginPage objCogs_LoginPage ;
	private Configuration configuration;
	
	public PAMLoginPageStep(Cogs_LoginPage objCogs_LoginPage,WebActions objWebActions,PAMLoginPage objPAMLoginPage,Configuration configuration ) {		
		this.objCogs_LoginPage = objCogs_LoginPage;
		this.objWebActions = objWebActions;
		this.objPAMLoginPage = objPAMLoginPage;
		this.configuration = configuration;
	}


	@Then("^Enter username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void Enter_username_as_and_password_as(String userName, String password) {
		// Write code here that turns the phrase above into concrete actions
		//objPAMLoginPage.setUserName(userName);
		//objPAMLoginPage.setPassword(password);
		objCogs_LoginPage.verifyPrivacyError();
		objCogs_LoginPage.setUserName(userName);
		objCogs_LoginPage.setPassword(password);
	}

	@Then("^Click on Sign In button$")
	public void Click_on_Sign_In_button() {
		// Write code here that turns the phrase above into concrete actions
		//objPAMLoginPage.clickSignIn();
		objCogs_LoginPage.clickLogin();
	}

	@Then("^Invoke the Cogs portal \"([^\"]*)\"$")
	public void invoke_the_Cogs_portal(String arg1){
	    // Write code here that turns the phrase above into concrete actions
		objPAMLoginPage.invokePAMURL(arg1);
	}
	
	@Then("^Navigate to new tab$")
	public void navigate_to_new_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		objWebActions.openNewTab();
		objWebActions.switchToChildWindow();
	}
	
	@Then("^Invoke the Bede Admin portal \"([^\"]*)\"$")
	public void invoke_the_Bede_Admin_portal(String arg1){
	    // Write code here that turns the phrase above into concrete actions
		objPAMLoginPage.invokePAMURL(arg1);
	}

	@Then("^Back to Bell site$")
	public void back_to_Bell_site() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("************** window");
	    objWebActions.switchToWindowUsingTitle("Play Casino Games Online with Bella Casino");
	}
	
	@Then("^Navigate to \"([^\"]*)\" site$")
	public void navigate_to_site(String arg1) {
		System.out.println("************** window");
	    objWebActions.switchToWindowUsingTitle(arg1);
	}
	
	@Then("^Invoke Bella site$")
	public void invoke_Bell_site() {
		objPAMLoginPage.invokePAMURL(configuration.getConfig("web.Url"));
	}
}
