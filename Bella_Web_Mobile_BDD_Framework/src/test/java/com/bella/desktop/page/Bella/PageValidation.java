package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class PageValidation {
	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private DriverProvider objDriverProvider;
	String url;

	public PageValidation(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,DriverProvider driverProvider) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
		this.objDriverProvider = driverProvider;
	}
	public void verifyPrimaryTabs(String tabName)
	{	if(tabName.contains(",")){
		String[] arr1 = tabName.split(",");
		for (String links : arr1  ) {
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
			logReporter.log("verify '"+links+"' link is displayed in Primary navigation bar ",
					objWebActions.checkElementDisplayed(lnkPrimaryTAB));}}
	else{
		By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
		logReporter.log("verify '"+tabName+"' link is displayed in Primary navigation bar ",
				objWebActions.checkElementDisplayed(lnkPrimaryTAB));
	}
	}
	public void verifyCarouselDisplayedOnPage()
	{
		By  carouselblock = By.xpath("//div[@class='component promo-carousel']//div[@class='block carouselblock ']");
		logReporter.log("Verify promo-carousel ",
				objWebActions.checkElementDisplayed(carouselblock));
	}
	public void navigateToPrimaryTabs(String tabName)
	{
		if(tabName.contains(",")){
			String[] arr1 = tabName.split(",");
			for (String links : arr1  ) {
				By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
				logReporter.log(links+" displayed in Primary navigation bar ",
						objWebActions.click(lnkPrimaryTAB));}}
		else{
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
			logReporter.log(tabName+" displayed in Primary navigation bar ",
					objWebActions.click(lnkPrimaryTAB));}
	}


	public void verifyPrimayTabDisplayedAsSelectedOrNot(String tabName)
	{
		By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(@class,'active')][contains(.,'"+tabName+"')]");
		logReporter.log(tabName+" displayed as seleced in Primary navigation bar ",
				objWebActions.checkElementDisplayed(lnkPrimaryTAB));
	}

	public void verifySecondaryTabDisplayedAsSelectedOrNot(String tabName)
	{
		By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(@class,'active')][contains(text(),'"+tabName+"')]");
		logReporter.log(tabName+" displayed as seleced in secondary navigation bar ",
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifySecondaryTABdisplayed(String links){
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" verify "+ links2+" link displayed from secondary navigation",
						objWebActions.checkElementDisplayed(locator));}
		}
		else
		{
			By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" verify "+ links+" link displayed from secondary navigation",
					objWebActions.checkElementDisplayed(locator));
		}
	}

	public void clickSecondaryTABdisplayed(String links)
	{
		if(links.contains(",")){
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" Click on "+ links2+" link displayed from secondary navigation",
						objWebActions.click(locator));}
		}
		else
		{
			By locator2= By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" Click on "+ links+" link displayed from secondary navigation",
					objWebActions.click(locator2));
		}
	}
	public void verifySectionLinks(String lnks)
	{
		String[] arrSingleEntry = lnks.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[contains(text(),'"+entry[0]+"')]//following::a[contains(.,'"+entry[1]+"')]");
			logReporter.log(" Verify '"+entry[1]+"' lnk is displayed againest '"+entry[0]+"' section ",
					objWebActions.checkElementDisplayed(locator));	}
	}

	public void verifyArticleBlockHeaders(String hdrs)
	{
		String[] arrSingleEntry = hdrs.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(";");
			By locator = By.xpath("//"+entry[0]+"[contains(.,'"+entry[1]+"')]");	
			logReporter.log(" Verify '"+entry[1]+"' ",
					objWebActions.checkElementDisplayed(locator));	}
	}

	public void verifyExpandLinks(String lnk)
	{
		By locator = By.xpath("//h3//a[text()='"+lnk+"']");	
		logReporter.log(" Verify '"+lnk,
				objWebActions.checkElementDisplayed(locator));			
	}
	public void clickExpandLinks(String lnk)
	{
		By locator = By.xpath("//h3//a[text()='"+lnk+"']");	
		logReporter.log(" Verify '"+lnk,
				objWebActions.click(locator));			
	}
	public void verifyGamesSection(String classHeader)
	{
		String[] arrSingleEntry = classHeader.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[@class='"+entry[0]+"'and contains(text(),'"+entry[1]+"')]//following::div[@class='mobile-game-panels']");
			logReporter.log(" Verify 'Games' is displayed under '"+entry[1]+"' section ",
					objWebActions.checkElementDisplayed(locator));}
	}
	public void verifyHomepageSectionHeadersdisplayed(String classHeader)
	{

		String[] arrSingleEntry = classHeader.split("~"); //
		for(String singleEntry : arrSingleEntry) 
		{
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[@class='"+entry[0]+"'and contains(text(),'"+entry[1]+"')]");

			logReporter.log(" Verify "+entry[1]+" section is displayed with "+entry[0]+" icon",
					objWebActions.checkElementDisplayed(locator));

		}	
	}
	public void verifySortByText()
	{
		By lblSortBy = By.xpath("//div[@class='show-game-panels']//div[@class='ordering-bar']//span[contains(.,'Sort By')]");
		logReporter.log(" Verify 'Sort By' text ",
				objWebActions.checkElementDisplayed(lblSortBy));	}

	public void verifyGamesOnSlotsPage()
	{
		By lblgamesPanel = By.xpath("//div[@class='game-panels container-fluid']//div[@class='row']//div[contains(@class,'game-panel')]");
		logReporter.log(" Verify games panel ",
				objWebActions.checkElementDisplayed(lblgamesPanel));	
	}

	public void verifySectionsOnG1(String sectNm)
	{
		if(sectNm.contains(",")){
			String[] arr1 = sectNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+links+"')]");
				logReporter.log(" verify  "+ links+" displayed on G1 page",
						objWebActions.checkElementDisplayed(locator));}
		}
		else{
			By locator2 = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+sectNm+"')]");
			logReporter.log(" verify  "+ sectNm+" displayed on G1 page",
					objWebActions.checkElementDisplayed(locator2));}
	}



	public void clickOnSectionsOnG1(String sectNm)
	{
		if(sectNm.contains(",")){
			String[] arr1 = sectNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+links+"')]");
				logReporter.log(" Click  on "+ links+" ",objWebActions.click(locator));}
		}
		else
		{
			By locator2 = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+sectNm+"')]");
			logReporter.log(" Click  on "+ sectNm+" ",
					objWebActions.click(locator2));
		}

	}

	public void verifyGrosvenorOneCasinoscardiMG()
	{
		By imgGrosvenorOneCasinoscard = By.xpath("//img[@alt='Grosvenor One Casinos card']");
		logReporter.log(" Verify 'Grosvenor One Casinos card' image on G1 page ",
				objWebActions.checkElementDisplayed(imgGrosvenorOneCasinoscard));	
	}

	public void verifySerachFinderInHeader()
	{
		By btnSearch = By.xpath("//div[@class='component search']//button[@class='search__open-btn']");
		logReporter.log(" verify game Serach Finder in Header ",
				objWebActions.checkElementDisplayed(btnSearch));	
	}
	public void verifyEnterBonusCodeFieldOnPromotionPage()
	{
		By inpEnterBonusCode = By.xpath("//div[@class='bonus-link']//a[contains(.,'Enter bonus code')]");
		logReporter.log(" Verify 'Enter bonus code' field on Promotion Page ",
				objWebActions.checkElementDisplayed(inpEnterBonusCode));	
	}

	public void verifyBonusHistoryLinkOnPromotionPage()
	{
		By lnkBonusHistory = By.xpath("//div[@class='bonus-link']//a[contains(.,'Enter bonus code')]//following-sibling::a[text()='Bonus History']");
		logReporter.log(" Verify 'Bonus History' link on Promotion Page ",
				objWebActions.checkElementDisplayed(lnkBonusHistory));	
	}

	public void verifyPromoetionsONPromosPage()
	{
		By lstPromos = By.xpath("//div[@class='component latest-promo promo-opt']//div[@class='row']//div[@class='promotions']");
		logReporter.log(" Verify 'promotions'  on Promos Page ",
				objWebActions.checkElementDisplayed(lstPromos));	
	}

	public void verifyGamesOnCasinoPage()
	{
		By lstGames = By.xpath("//div[@class='block gameslistblock ']//div[@class='row']//div[contains(@class,'game-panel')]");
		logReporter.log(" Verify 'games'  on Casino Page ",
				objWebActions.checkElementDisplayed(lstGames));	
	}

	public void verifyPrimaryTabsRedirectToTheCorrectPage(String tabName)
	{
		switch (tabName) 
		{
		case "Slots": 
			url = configuration.getConfig("web.Url");
			url = url+"slots-and-games";
			this.navigateToPrimaryTabs(tabName);
			this.verifyLinkRedirectToCorrectPage(url);
			break;
		case "Jackpots": 
			url = configuration.getConfig("web.Url");
			url = url+"jackpots";
			this.navigateToPrimaryTabs(tabName);
			this.verifyLinkRedirectToCorrectPage(url);
			break;
		case "Casino": 
			url = configuration.getConfig("web.Url");
			url = url+"casino";
			this.navigateToPrimaryTabs(tabName);
			this.verifyLinkRedirectToCorrectPage(url);
			break;

		case "Promos":
			url = configuration.getConfig("web.Url");
			url = url+"promotions";
			this.navigateToPrimaryTabs(tabName);
			this.verifyLinkRedirectToCorrectPage(url);
			break;

		}
	}

	public void verifyLinkRedirectToCorrectPage(String ExpectedURL)
	{
		String currentURL = objDriverProvider.getWebDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(ExpectedURL));
	}
	
	public void verifySecondaryPagesContent(String primaryTab)
	{
		switch (primaryTab) 
		{
		case "Slots": 
			this.navigateToPrimaryTabs("Slots");
			this.verifySecondaryTABdisplayed("Featured,New,Games,All Slots,Jackpots");
			this.verifyPrimayTabDisplayedAsSelectedOrNot("Slots");
			
			verifyCarouselDisplayedOnPage();
			verifyHomepageSectionHeadersdisplayed("seven:Top Games~seven:New~seven:Rainbow Riches~trophy-star:Daily Jackpot~trophy-star:Jackpot King~trophy-star:Daily Jackpot");
			verifySectionLinks("Top Games:View All~New:View All~Daily Jackpot:View All~Jackpot King:View All");


			this.clickSecondaryTABdisplayed("Featured");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Featured");
			this.verifySortByText();
			this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("New");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("New");
			this.verifySortByText();
			this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("Games");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Games");
			//this.verifySortByText();
			//this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("All Slots");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("All Slots");
			this.verifySortByText();
			this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("Jackpots");
			this.verifyPrimayTabDisplayedAsSelectedOrNot("Jackpots");
			break;
			
		case "Jackpots": 
			this.verifyPrimayTabDisplayedAsSelectedOrNot("Jackpots");
			this.verifySecondaryTABdisplayed("Jackpots at Bella,Daily Jackpots,Jackpot King,Mega Jackpots,All Jackpots");
			this.verifyCarouselDisplayedOnPage();


			//this.verifyHomepageSectionHeadersdisplayed("seven:Jackpot King~seven:Mega Jackpots~seven:Daily Jackpot");
			//this.verifySectionLinks("Jackpot King:View All~Mega Jackpots:View All~Daily Jackpot:View All");


			this.clickSecondaryTABdisplayed("Daily Jackpots");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Daily Jackpots");
		//	this.verifySortByText();
		//	this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("Jackpot King");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Jackpot King");
			//this.verifySortByText();
			//this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("Mega Jackpots");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Mega Jackpots");
			
			//this.verifySortByText();
			//this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("All Jackpots");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("All Jackpots");
			//this.verifySortByText();
			//this.verifyGamesOnSlotsPage();

			this.clickSecondaryTABdisplayed("Jackpots at Bella");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Jackpots at Bella");
			this.verifyArticleBlockHeaders("h2;Jackpots at Bella");
		//	this.verifyArticleBlockHeaders("h2;Jackpots at Bella~h3;Local Jackpots~h3;Jackpot King~h3;Network Jackpots~h3;Daily, Must-Go, Cashpots, and King Jackpots");
			this.verifyExpandLinks("Click to expand +");
			this.clickExpandLinks("Click to expand +");
			this.verifyExpandLinks("Click to expand -");

			this.verifyExpandLinks("Click for full jackpot information +");
			this.clickExpandLinks("Click for full jackpot information +");
			this.verifyExpandLinks("Click for full jackpot information -");
			break;
		case "Casino": 
			//casino
			
			this.verifySecondaryTABdisplayed("Table and Card,Live");
			this.verifyCarouselDisplayedOnPage();
			//this.verifyPrimayTabDisplayedAsSelectedOrNot("Casino");
			//this.verifyHomepageSectionHeadersdisplayed("star:Top Picks~roulette:Roulette~hearts:Blackjack~chip-solid:Other Games");
			
			this.clickSecondaryTABdisplayed("Table and Card");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Table and Card");
			//this.verifyGamesOnCasinoPage();
			
			this.clickSecondaryTABdisplayed("Live");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("Live");
			//this.verifyGamesOnCasinoPage();	
			break;

		case "Promos":
			//promos
			this.verifySecondaryTABdisplayed("All,My Promotions");
			this.verifyPrimayTabDisplayedAsSelectedOrNot("Promos");
			this.verifySecondaryTabDisplayedAsSelectedOrNot("All");
			this.verifyEnterBonusCodeFieldOnPromotionPage();
			this.verifyBonusHistoryLinkOnPromotionPage();
			this.verifyPromoetionsONPromosPage();
			break;

		}
	}
}
