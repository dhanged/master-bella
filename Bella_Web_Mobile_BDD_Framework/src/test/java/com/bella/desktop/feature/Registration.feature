Feature: Registration

@Desktop
Scenario: Check whether user allowed to enter email address successfully on pre-registration form.
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address
Then Click on Next button
Then Verify Registration Step2 is displayed

@Desktop
Scenario: Check whether error message is displayed if already taken email address is entered.
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter existing email address as "vaishali.bhad@expleogroup.com"
Then Click on Next button
Then Verify "The supplied email address is already associated with a different user" error message


@Desktop
Scenario: Check whether error message is displayed if incorrect email address is entered in email ID field
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address as "sadd"
Then Click on Next button
Then Verify "You must enter a valid email address" error message

@Desktop
Scenario: Check whether error message is displayed if user keeps any of the following field empty:
 Username Password Title (Mr., Miss, Mrs. or Ms.) First Name (minimum 2 characters) Surname Date of Birth Country of Residence Address Mobile Number
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address
Then Click on Next button
Then Click on Register
Then Verify below error message
|You must enter a username..|
|Please enter your password|
|You must enter a first name|
|You must enter a surname|
|You must enter your full date of birth..|
|Enter your mobile phone number, 11 digits only|
|You must enter an address|
#|You must select if you wish to set a deposit limit or not|

@Desktop
Scenario: Check whether error message is displayed if already taken username is entered
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address
Then Click on Next button
Then Verify Registration Step2 is displayed
And Enter already taken username
Then Verify "Sorry the username has been taken" error message

@Desktop
Scenario: Check whether error message is displayed when user enters invalid mobile number.
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address
Then Click on Next button
Then Verify Registration Step2 is displayed
Then Enter "5842" invalid mobile number
Then Verify "Mobile number should be valid format with 11 digits - only numbers" error message

@Desktop
Scenario: Check whether system accepts the username in following format:
-Minimum 8 and Maximum 15 characters
-Username starting with no any special characters or symbols

Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address
Then Click on Next button
Then Verify Registration Step2 is displayed
Then Enter username as "asd"
Then Verify "Username must be between 6 and 15 characters" error message
Then Enter username as "@#%$1214"
Then Verify "Username can only contain letters, numbers and underscores" error message
Then Enter valid username
And Verify system does not display any below error message
|Username must be between 6 and 15 characters|
|Username can only contain letters, numbers and underscores|

@Desktop
Scenario:  Check that the error message is displayed if customer enters Password in following format:
- less than 8 character password
- Password with Space
- Password same as username

Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Enter password less than 8 character password
Then Verify "Minimum 8 characters in length, 1 lower case letter, 1 upper case letter, 1 number." error message 
Then Enter Username and Password as "Test1234"
Then Verify "Must be different from username" error message 
Then Enter Password with Space
Then Verify "Minimum 8 characters in length, 1 lower case letter, 1 upper case letter, 1 number." error message 


@Desktop
Scenario: Check that the system accepts the Password when customer enters:
-a minimum of 8 characters
-a mix of letters and numbers in Password field
-No spaces while Registration
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Enter username
Then Enter 8 character password and a mix of letters and numbers
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select country as "United Kingdom"
Then Enter address as "W22SZ"
Then Verify address suggestion list is displayed
Then Select address "Flat 20, Chelwood House, Gloucester Square, LONDON W2" from the list
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop
Scenario: Check whether user gets registered when all mandatory data is entered
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop
Scenario: Check that the error message is displayed if customer enters only character OR only numbers in password field
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Enter username
Then Enter only characters in password field
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select country as "United Kingdom"
Then Enter address as "wr53da"
Then Verify address suggestion list is displayed
Then Select address "Lock Keepers Cottage, Basin Road, WORCESTER WR5 " from the list
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify "Minimum 8 characters in length, 1 lower case letter, 1 upper case letter, 1 number." error message 
Then Enter only numbers in password field
Then Click on Register
Then Verify "Minimum 8 characters in length, 1 lower case letter, 1 upper case letter, 1 number." error message 


@Desktop
Scenario: Check whether error message is displayed if user enters age less than 18 years
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Verify Registration Step2 is displayed
And Enter the age less than 18 years in DOB field
Then Verify "You must be 18 or over to use this site" error message 


@Desktop
Scenario: Check whether system accepts the Correct DOB when user enters the correct DOB in DOB field
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Verify Registration Step2 is displayed
And Enter the correct age i.e. Greater than 18 years in DOB field
Then Verify system does not display any below error message
|You must be 18 or over to use this site|

@Desktop
Scenario: Check that the address suggestions list should be populated when customer enters postcode in Address field while registration
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Enter FirstName
Then Select Title
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select country as "United Kingdom"
Then Enter address as "WR53DA"
Then Verify address suggestion list is displayed
Then Select address "Loqate, Waterside, Basin Road, WORCESTER WR5" from the list
Then Verify following fields get populated with correct data
|Fields with value|
|Address Line 1-Loqate|
|Address Line 2-Waterside|
|Town/City-WORCESTER|
|County-|
|Postcode-WR5 3DA|
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop77
Scenario: Check that the customer can skip the Deposit limit setup by selecting the 'No' option while Registration
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop
Scenario: Check that the customer can enter the address manually by selecting the enter it manually option available near Address field
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
And Verify link 'enter it manually available' near Address field
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop
Scenario: Check that system displays 5 options to opt in for Marketing preferences on Registration page
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Verify following Marketing preferences options are displayed
|Email|
|SMS|
|Phone|
|Post|
|Select All|

@Desktop
Scenario: Check whether user can navigate to the registration screen from the login screen
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then Verify 'New player?' text
Then Click on 'New player?' link
Then Verify Registration page is appered

@Desktop
Scenario: Check whether Help link is accessible on Registration page
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Verify 'Live Help' link on Registartion page
Then Click on 'Live Help' link on Registartion page
#And Verify "Live Help" Link opened in New tab
And Verify live help Link opened in New tab

@Desktop
Scenario: Check that system allows to select All the preferences, no preferences, or one or more preferences on registration form
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
Then Click on Next button
Then Click on select all in marketing preference section
Then Verify following checkboxes are selected
|Email|
|SMS|
|Phone|
|Post|
Then unselect all marketing preference options
Then Select "Email" marketing preference option
Then verify "Email" checkboxe is selected

@Desktop
Scenario: Check that the system displays the address suggestions list when customer enters the valid postcode for the countries UK, Ireland and Gibraltar
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select country as "United Kingdom"
Then Enter address as "BT1 1AA"
Then Verify address suggestion list is displayed
Then Select address "Royal Mail, 20 Donegall Quay, BELFAST " from the list
Then Verify following fields get populated with correct data
|Fields with value|
|Address Line 1-Royal Mail|
|Address Line 2-20 Donegall Quay|
|Town/City-BELFAST|
|County- |
|Postcode-BT1 1AA|
Then Click on Clear link
Then Enter address as "SL62TE"
Then Verify address suggestion list is displayed
Then Select address "31 Badger Close, MAIDENHEAD" from the list
Then Verify following fields get populated with correct data
|Fields with value|
|Address Line 1-31 Badger Close|
|Address Line 2- |
|Town/City-MAIDENHEAD|
|County-BERKSHIRE|
|Postcode-SL6 2TE|
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header

@Desktop
Scenario: Check whether all titles are displayed on second step of registration journey:
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Verify all titles are displayed on page
|Mr|
|Ms|
|Miss|
|Mrs|