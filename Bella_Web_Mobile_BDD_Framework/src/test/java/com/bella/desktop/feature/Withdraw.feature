Feature: Withdraw

@Desktop1
Scenario: Check whether system displays message that you need to do a deposit before you can make a withdrawal when player never did deposit
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
Then Verify "You may only withdraw after you have made a successful deposit." message
And Verify 'Please contact support should you need any assistance.' message