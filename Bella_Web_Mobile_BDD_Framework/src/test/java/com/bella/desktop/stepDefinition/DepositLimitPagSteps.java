package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.DepositLimitPage;
import com.bella.desktop.page.Bella.DepositPage;
import com.bella.desktop.page.Bella.MyAccountPage;
import com.bella.desktop.page.Bella.WithdrawPage;
import com.generic.WebActions;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DepositLimitPagSteps {


	private WebActions objWebActions;
	private DepositPage objDepositPage;
	private DepositLimitPage objDepositLimitPage;
	private Utilities objUtilities;
	private MyAccountPage objMyAccountPage;
	private WithdrawPage objWithdrawPage;
	String balance,depAmt,initalbonuseBalance,receivedBonusAmount,MonthlyLmt,WeeklyLmt,dailyLmt;
	int lmt;
	public DepositLimitPagSteps(DepositPage objDepositPage,Utilities objUtilities,DepositLimitPage DepositLimitPage ,WebActions objWebActions,WithdrawPage objWithdrawPage) {		
		this.objDepositPage = objDepositPage;
		this.objUtilities = objUtilities;
		this.objDepositLimitPage = DepositLimitPage;
		this.objWebActions = objWebActions;
		this.objWithdrawPage = objWithdrawPage ;
	}

	@Then("^User will see the below information about deposit limit$")
	public void user_will_see_the_below_information_about_deposit_limit(DataTable dt){

		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objWithdrawPage.verifyInfoText(list.get(i));}
	}

	@Then("^User will get the \"([^\"]*)\" screen$")
	public void user_will_get_the_screen(String title) {
		objDepositLimitPage.switchToDepositLimitIframe();
		objDepositLimitPage.verifyCashierTitle(title);

	}

	@Then("^User will get error message \"([^\"]*)\"$")
	public void user_will_get_error_message(String errMsg) {
		objDepositLimitPage.verifyDepositLimitErrorMessage(errMsg);
	}
	@When("^Get \"([^\"]*)\" limit value$")
	public void get_limit_value(String arg1)  {
		dailyLmt =  objDepositLimitPage.getLimitValue(arg1);
	}
	@Then("^User will get message \"([^\"]*)\"$")
	public void user_will_get_messages(String msg) {
		objDepositLimitPage.verifyFailureMessage(msg);
	}
	@Then("^User will get \"([^\"]*)\" message$")
	public void user_will_get_message(String txt) {
		objWithdrawPage.verifyInfoText(txt);
	}

	@Then("^Enter deposit amount greater than deposit limit set by the customer$")
	public void enter_deposit_amount_greater_than_deposit_limit_set_by_the_customer() 
	{ 
		/*if(dailyLmt.contains(" "))
		{dailyLmt = dailyLmt.replace(" ", "");}*/
		int a = Integer.valueOf(dailyLmt) +2;
		System.out.println(a);
		objDepositPage.setDepositAmount(String.valueOf(a));
		if(objDepositPage.verifyAcknowledgeCheckbox())
			objDepositPage.selectAcknowledgeCheckbox();
	}
	@When("^User clicks on the edit \"([^\"]*)\" limit button$")
	public void user_clicks_on_the_edit_limit_button(String lmt)  {
		dailyLmt =  objDepositLimitPage.getLimitValue("daily");
		MonthlyLmt = objDepositLimitPage.getLimitValue("monthly");
		WeeklyLmt = objDepositLimitPage.getLimitValue("weekly");
		System.out.println("WeeklyLmt  :: "+WeeklyLmt + "  MonthlyLmt " +MonthlyLmt);
		objDepositLimitPage.clickOneditLimit(lmt);

	}
	@Then("^Enter deposit limit Lower than Previous deposit limit$")
	public void Enter_deposit_limit_Lower_than_Previous_deposit_limit() {
		
		 lmt = Integer.valueOf(dailyLmt) -5;
		objDepositLimitPage.setDepositLimitAmount(String.valueOf(lmt));
	}
	
	@Then("^Enter deposit limit higher than Previous deposit limit$")
	public void Enter_deposit_limit_higher_than_Previous_deposit_limit() {
		
		 lmt = Integer.valueOf(dailyLmt) + 2;
		objDepositLimitPage.setDepositLimitAmount(String.valueOf(lmt));
	}
	@Then("^User set weekly limit greater than monthly$")
	public void user_set_weekly_limit_greater_than_monthly(){

		 lmt = Integer.valueOf(MonthlyLmt) +2;
		this.user_set_limit_as("Weekly", String.valueOf(lmt));
	}

	@Then("^User will get the \"([^\"]*)\" screen with following fields$")
	public void user_will_get_the_screen_with_following_fields(String arg1, DataTable dt){
		objDepositLimitPage.verifyCashierTitle(arg1);
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objDepositLimitPage.verifyDropdownField(list.get(i));}	
	}

	@Then("^User set \"([^\"]*)\" limit as \"([^\"]*)\"$")
	public void user_set_limit_as(String arg1, String amt){
		objDepositLimitPage.selectDropdownField("Deposit limit type", arg1);
		objDepositLimitPage.setDepositLimitAmount(amt);
	}

	@Then("^User will get a message \"([^\"]*)\"$")
	public void user_will_get_a_message(String txt) {
		objWithdrawPage.verifyYourWithdrawalSuccessMessage(txt);
	}

	@Then("^Close My account screen$")
	public void close_My_account_screen() {

		objWebActions.switchToDefaultContent();
		objDepositLimitPage.closeMyAccountScreen();
	}

	
	@And("^New deposit limit value will be display in the pending$")
	public void new_deposit_limit_value_will_be_display_in_the_pending(){
		
		objDepositLimitPage.verifyNewlyAddedDepositLimitIsDisplayedInPending("Day", String.valueOf(lmt));
		//objDepositLimitPage.verifyNewlyAddedDepositLimitIsDisplayedInPending("Weekly",WeeklyLmt );
		//objDepositLimitPage.verifyNewlyAddedDepositLimitIsDisplayedInPending("Monthly", MonthlyLmt);

	}
	
	@And("^Verify updated deposit limit is displayed$")
	public void verify_updated_deposit_limit_is_displayed(){
		objDepositLimitPage.verifyLimit("daily", String.valueOf(lmt));

	}
	@And("^Verify \"([^\"]*)\" limit is displayed as \"([^\"]*)\"$")
	public void verify_limit_is_displayed_as(String arg1, String amt){
		objDepositLimitPage.verifyLimit(arg1, amt);

	}
}
