package com.bella.desktop.page.Bella;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class PromotionsPage {

	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	String promoTitle;
	public PromotionsPage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	}

	public void clickMoreinfolink()
	{
		By moreinfoLink = By.xpath("//div[@class='overlay-cta promotions__button-group']/a[@class='more-info promotions__ghost']");
		logReporter.log("Click on the more info Link displayed",
				objWebActions.click(moreinfoLink));
	}
	public void verifyCalendarIconDisplayed()
	{
		By calendarIcon = By.xpath("//i[@class='icon-Calendar']");

		logReporter.log(" Verify calendar Icon displayed on promotion info page",
				objWebActions.checkElementDisplayed(calendarIcon));
	}

	public void validateActiveExpiryDate()
	{
		By promoDate = By.xpath("//div[@class='promo-date']/p");
		String promodt = objWebActions.getText(promoDate);

		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		String input = objWebActions.getText(promoDate);
		String dts [] = input.split("-");
		if (dts != null && dts.length > 1) {
			String toDateStr = dts[1].trim().replaceAll("(\\d+).* (.*) (.*)", "$1 $2 $3");
			String formDateStr = dts[0].trim().replaceAll("(\\d+).* (.*)", "$1 $2");
			System.out.println("From Date: " + formDateStr);
			System.out.println("To Date: " + toDateStr);
			try {
				Date toDate = dateFormat.parse(toDateStr);
				formDateStr += " " + (1900 + toDate.getYear());
				Date fromDate = dateFormat.parse(formDateStr);
				logReporter.log(" Verify Active date is before the expiry date",
						fromDate.before(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	public void clickClaimBTN()
	{
		By claimBTN= By.xpath("//button[@class='opt-in-button button']");
		logReporter.log("Click on the claim/OPTIN Button displayed",
				objWebActions.click(claimBTN));
	}
	public boolean findPromoBTN(String btn)
	{
		By promoBTN = By.xpath("//button[contains(text(),'"+btn+"')]");
		//promoTitle = this.getPromotitle(btn);
		/*logReporter.log("Verify "+btn+" button displayed on the Promotions page ",
				objWebActions.checkElementDisplayed(promoBTN));*/
		return objWebActions.checkElementElementDisplayedWithMidWait(promoBTN);
	}

	public String getPromotitle(String btnName)
	{
		By locator = By.xpath("//button[contains(text(),'"+btnName+"')]//preceding::p[@class='promotions__event-info-header'][1]");
		return objWebActions.getText(locator);
	}

	public void clickPromoBTNonPromotions(String btn)
	{
		By promoBTN = By.xpath("//button[contains(text(),'"+btn+"')]");
		logReporter.log(" click on "+btn+" button displayed on the Promotions page ",
				objWebActions.click(promoBTN));
	}

	public void clickFollowingMoreInfoLink(String btn)
	{
		System.out.println("***********into click");
		waitMethods.sleep(8);
		
		By followingMoreinfoLink = By.xpath("//button[contains(text(),'"+btn+"')]/following-sibling::a[contains(text(),'More info')]");
		logReporter.log("Click on the More info link following "+btn+" Button displayed",
				objWebActions.click(followingMoreinfoLink));
		System.out.println("***********into clickedddddddddd");
		waitMethods.sleep(8);
	}

	public void verifyTickIMGDisplayed()
	{
		By locator = By.xpath("//div[@id='opted-in']/img[@src='/assets/grosvenor/img/promo/tick.png']");

		logReporter.log(" Verify Green Tick image displayed ",
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyOptedInTXTDisplayed()
	{
		By locator = By.xpath("//div[@id='opted-in']/p[contains(text(),'Opted-in')]");

		logReporter.log(" Verify Opted-in text displayed when clicked on OptIn BTN",
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyOptedInTXTonPromotionsPage(String promoTitle)
	{
		//By locator = By.xpath("//div[@class='opted-in claim-success']/p[contains(text(),'Opted-in')]");
		By locator = By.xpath("//div[@class='opted-in claim-success']/p[contains(text(),'Opted-in')]//preceding::p[@class='promotions__event-info-header'][contains(.,'"+promoTitle+"')]");
		logReporter.log(" Verify Opted-in text displayed when clicked on OptIn BTN on Promotions list",
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyOptOutBTNDisplayed()
	{
		By locator = By.xpath("//button[@class='opt-in-button button' and contains(text(),'Opt out')]");

		logReporter.log(" Verify Opt out Button displayed when clicked on OptIn BTN",
				objWebActions.checkElementDisplayed(locator));

	}

	public void verifyClaimedTXTonPromotionsPage(String promoTitle)
	{
		//By locator = By.xpath("//div[contains(@class,'opted-in claim-success')]/p[contains(text(),'Claimed')]");
		By locator = By.xpath("//div[@class='opted-in claim-success']/p[contains(text(),'Claimed')]//preceding::p[@class='promotions__event-info-header'][contains(.,'"+promoTitle+"')]");
		logReporter.log(" Verify Claimed text displayed when clicked on Claim BTN on Promotions list",
				objWebActions.checkElementDisplayed(locator));
	}
	// BUY IN Bonus

	By BuyInBTN =By.xpath("//button[@class='submit' and contains(text(),'Buy-in')]");
	By cancelBTN = By.xpath("//button[@class='cancel' and contains(text(),'Cancel')]");
	By availableCashLabel = By.xpath("//h3[@class='heading' and contains(text(),'Your available cash: ')]");
	By OkBTN =By.xpath("//button[@class='submit' and contains(text(),'OK')]");

	public void verifyavailableCashLabeldisplayed()
	{
		logReporter.log(" Verify 'Available Cash : ' Label displayed on popup",
				objWebActions.checkElementDisplayed(availableCashLabel));
	}
	public String getAvailableCashAmount()
	{
		By availableCashLocator = By.xpath("//h3[@class='heading' and contains(text(),'Your available cash: ')]/span");
		return objWebActions.getText(availableCashLocator);
	}

	public String getBuyInForCashAmount()
	{
		By BuyInForCashLocator = By.xpath("//div[@class='first']/label[@class='number']");
		return objWebActions.getText(BuyInForCashLocator);
	}

	public String getBuyInBonusAmount()
	{
		By BuyInBonusAmountLocator = By.xpath("//div[@class='second']/label[@class='number']");
		return objWebActions.getText(BuyInBonusAmountLocator);
	}

	public String getPlayWithAmount()
	{
		By PlayWithAmountLocator = By.xpath("//div[@class='third']/label[@class='number']");
		return objWebActions.getText(PlayWithAmountLocator);
	}

	public void verifyBuyInBTNdisplayed()
	{
		logReporter.log(" Verify BuyIn Button displayed on popup",
				objWebActions.checkElementDisplayed(BuyInBTN));
	}

	public void clickBuyInBTN()
	{
		logReporter.log(" Click BuyIn Button displayed on popup",
				objWebActions.click(BuyInBTN));
	}
	public void verifyCancelBTNdisplayed()
	{
		logReporter.log(" Verify Cancel Button displayed on popup",
				objWebActions.checkElementDisplayed(cancelBTN));
	}

	public void verifyConfirmBuyingInHDRDisplayed()
	{
		By confirmHDR = By.xpath("//h2[contains(text(),'Confirm')]");
		By buyingInHDR = By.xpath("//h2[contains(text(),'Buying-in')]");
		logReporter.log(" Verify confirm HDR displayed on confirmation popup",
				objWebActions.checkElementDisplayed(confirmHDR));
		logReporter.log(" Verify buyingIn HDR displayed on confirmation popup",
				objWebActions.checkElementDisplayed(buyingInHDR));
	}
	public void verifyOKBTNonPopup()
	{
		logReporter.log(" Verify Ok Button displayed on confirmation popup",
				objWebActions.checkElementDisplayed(OkBTN));
	}
	public void clickOKBTNonPopup()
	{
		logReporter.log(" Click Ok Button displayed on confirmation popup",
				objWebActions.click(OkBTN));
	}
	public void verifyCancelBTNonPopup()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons ']/div/button[@class='cancel' and contains(text(),'Cancel')]");

		logReporter.log(" Verify Cancel BTN displayed on confirmation popup",
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyBuyInSuccessMsgDisplayed()
	{
		By congratulationsHDR = By.xpath("//h3[contains(text(),'Congratulations')]");
		By youHaveReceivedTXT = By.xpath("//h2[contains(text(),'You have received')]");
		logReporter.log(" Verify congratulations text displayed on Success msg popup",
				objWebActions.checkElementDisplayed(congratulationsHDR));
		logReporter.log(" Verify 'You have received..' displayed on Success msg popup",
				objWebActions.checkElementDisplayed(youHaveReceivedTXT));
	}

	public void validateReceivedAmount(String playwithamt )
	{
		By locator = By.xpath("//h3[text()='Congratulations']//following-sibling::h2");
		String actualPlayAmount = objWebActions.getText(locator);
		actualPlayAmount = actualPlayAmount.substring(actualPlayAmount.indexOf("£"),actualPlayAmount.indexOf("B"));
		actualPlayAmount = actualPlayAmount.replace(" ", "");

		logReporter.log(" Verify 'You have received amount : "+playwithamt +" is displayed correctly on Success msg popup",
				actualPlayAmount.equals(playwithamt));
	}

	public void verifyBuyInDetailsOnPopup() {

		By locator= By.xpath("//p[@class='buy-in-bonus_confirmation__description' and contains(text(),'Here are the details of your buy-in operation: ')]");

		logReporter.log(" Verify 'Here are the details of your buy-in operation: ' displayed on Success msg popup",
				objWebActions.checkElementDisplayed(locator));
	}

	public void validateBonusDetailOnPopup(String availableCash,String BuyInForCash,String BuyInBonusAmount)
	{
		By availableCashLocator = By.xpath("//p[@class='buy-in-bonus_confirmation__description']");
		String availableCashAmount = objWebActions.getText(availableCashLocator);

		String [] numbers = availableCashAmount.replaceAll(("[^0-9]+"), " ").split(" ");

		Float popupAvailableCash = Float.parseFloat(availableCash.substring(1)) - Float.parseFloat(BuyInForCash.substring(1)) ;

		logReporter.log(" Your available cash: amount displayed on popup is correct "+popupAvailableCash ,
				popupAvailableCash.equals(Float.parseFloat(numbers[3])));

		Float popupBonusBalance =Float.parseFloat(BuyInForCash.substring(1)) + Float.parseFloat(BuyInBonusAmount.substring(1)) ;

		logReporter.log(" You bonus balance: amount displayed on popup is correct "+popupBonusBalance ,
				popupBonusBalance.equals(Float.parseFloat(numbers[6])));
	}


	public boolean verifyInSufficientfundavailableinaccountDisplayedOrNot()
	{
		By locator = By.xpath("//div[@class='deposit'][text()='InSufficient fund available in account']");
		return objWebActions.checkElementDisplayed(locator);
	}

	public void ClickOnDeposit()
	{
		By locator = By.xpath("//div[@class='deposit'][text()='InSufficient fund available in account']//following::button[text()='Deposit']");

		logReporter.log(" Click on Deposit",
				objWebActions.click(locator));
	}

	public void verifyPromotionDisplyedOrNot(String pnm)
	{
		By locator = By.xpath("//div[@class='promotions__overlay-block']//p[contains(.,'"+pnm+"')]");
		logReporter.log("verify promotion : "+pnm,
				objWebActions.click(locator));
	}
	public void verifyPromoetionsONPromosPage()
	{
		By lstPromos = By.xpath("//div[@class='component latest-promo promo-opt']//div[@class='row']//div[@class='promotions']");
		logReporter.log(" Verify 'promotions'  on Promos Page ",
				objWebActions.checkElementDisplayed(lstPromos));	
	}

	public void verifyDonthaveanyactivepromotionsInformativeMessage()
	{
		By locator = By.xpath("//p[contains(.,'have any active promotions')]");

		if(objWebActions.checkElementElementDisplayedWithMidWait(locator))
		{logReporter.log(" Verify 'Don't have any active promotions' message ",
				objWebActions.checkElementDisplayed(locator));	}
		else {
			locator = By.xpath("//p[contains(.,'No Promotions Claimed by you')]");
			logReporter.log(" Verify 'Don't have any active promotions' message ",
					objWebActions.checkElementDisplayed(locator));}	
	}
}
