/**
 * 
 */
package com.bella.desktop.page.Bella;

import java.sql.Driver;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

/**
 * @author vaishali bhad
 *
 */
public class HomePage {

	private By btnContinue = By.xpath("//div[@class='cookie-message-text']/following-sibling::button[text()='Continue']");
	private By btnOpenLogin = By.xpath("//button[@class='open-login' and text()='Login']");
	private By logoBC = By.xpath("//a[@class='logo']/img[contains(@src,'globalassets/brand/bella')]");
	private By btnJoinNow = By.xpath("//li[@class='join-now-btn']/a[@class='btn' and text()='Sign Up']");
	private By lnkMyAccount = By.xpath("//a[@class='open-myaccount']");
	By SearchBtn = By.xpath("//button[@class='search__open-btn']");
	By inpSearchTxt = By.xpath("//input[@class='search-bar__input icon-search-white' and @placeholder='Search for a game']");
	By PlayBtnOnSearchResults = By.xpath("//button[@class='search-results__play-btn btn']");
	By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
	By browseSectionBTN = By.xpath("//button[@class='search-no-results__browse-btn' and contains(text(),'Browse sections')]");
	By GameInfoOnSearchResults = By.xpath("//a[@class='search-results__info']/i[@class='search-results__info-icon']");

	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private DriverProvider objDriverProvider;

	public HomePage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,DriverProvider driverProvider) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
		this.objDriverProvider = driverProvider;
	}

	public void verifyBellaCasinosLogoDisplayed(){
		logReporter.log("Verify Bella Casinos logo displayed.", 
				objWebActions.checkElementDisplayed(logoBC));
		if(objWebActions.checkElementElementDisplayedWithMinWait(btnContinue))
			this.clickContinueCookie();
	}

	public void clickContinueCookie() {
		logReporter.log("Click 'Continue' Cookie button", 
				objWebActions.click(btnContinue));
	}

	public void VerifyLogin() {
		logReporter.log("Verify 'Login' button", 
				objWebActions.checkElementDisplayed(btnOpenLogin));

	}
	public void clickLogin() {
		logReporter.log("Click 'Login' button", 
				objWebActions.click(btnOpenLogin));
	}
	public void verifyJoinNow()
	{
		logReporter.log("Verify 'Join Now ' button displayed in header", 
				objWebActions.checkElementDisplayed(btnJoinNow));
	}

	public void clickJoinNow() {
		logReporter.log("Click 'Sign Up' button", 
				objWebActions.click(btnJoinNow));
	}

	public void verifyUserLoggedOutSuccessfuly()
	{
		logReporter.log("User Logged Out Successfully - Join Now displayed", 
				objWebActions.checkElementDisplayed(btnJoinNow));
		logReporter.log("User Logged Out Successfully - Login button Displayed", 
				objWebActions.checkElementDisplayed(btnOpenLogin));
	}

	public void VerifyMyAccount() {
		logReporter.log("Verify 'My Account' link is displayed in header", 
				objWebActions.checkElementDisplayed(lnkMyAccount));
	}
	public void clickMyAccount() {
		logReporter.log("Click 'My Account' link", 
				objWebActions.click(lnkMyAccount));
	}
	public void NavigateToInvalidUrl() {
		logReporter.log("navigate to invalid url", objWebActions.navigateToInvalidUrl());
	}
	public void verifyErrorTitle() {
		By error = By.xpath("//p[contains(.,'404 Error')]");
		logReporter.log("verify 404 error title", objWebActions.checkElementDisplayed(error));
	}
	public void navigateToPrimaryTabs(String tabName)
	{
		if(tabName.contains(",")){
			String[] arr1 = tabName.split(",");
			for (String links : arr1  ) {
				By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
				logReporter.log(links+" displayed in Primary navigation bar ",
						objWebActions.click(lnkPrimaryTAB));}}
		else{
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
			logReporter.log(tabName+" displayed in Primary navigation bar ",
					objWebActions.click(lnkPrimaryTAB));}
	}
	public void verifySecondaryTABdisplayed(String links){
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" verify "+ links2+" link displayed from secondary navigation",
						objWebActions.checkElementDisplayed(locator));}
		}
		else
		{
			By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" verify "+ links+" link displayed from secondary navigation",
					objWebActions.checkElementDisplayed(locator));
		}
	}

	public void clickSecondaryTABdisplayed(String links)
	{
		if(links.contains(",")){
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" Click on '"+ links2+"' link displayed from secondary navigation",
						objWebActions.click(locator));}
		}
		else
		{
			By locator2= By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" Click on '"+ links+" link 'displayed from secondary navigation",
					objWebActions.click(locator2));
		}
	}


	public void verifyGamesOnSlotsPage()
	{
		By lblgamesPanel = By.xpath("//div[@class='game-panels container-fluid']//div[@class='row']//div[contains(@class,'game-panel')]");
		logReporter.log(" Verify games panel ",
				objWebActions.checkElementDisplayed(lblgamesPanel));	
	}
	public void verifyMessageLinkDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='mail-btn']//a");
		logReporter.log("Verify 'Mail ' link is displayed in header ", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));	
	}
	public void verifyDepositLinkDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='deposit-btn']//a");
		logReporter.log("Verify 'Deposit' link is displayed in header ", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));	
	}
	public void verifyBalanceDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='text-data']//a[@id='component_balance']//div[text()='Balance']");
		logReporter.log("Verify 'Balance' text is displayed in header ", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));	

		By btnToggle = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='text-data']//a[@id='component_balance']//div[@class='balance-toggle balance-off']");
		logReporter.log("Verify 'Balance' Toggle button is displayed in header ", 
				objWebActions.checkElementElementDisplayedWithMidWait(btnToggle));	
	}
	public void clickCloseSignUp() {
		By btnSignUpClose = By.xpath("//div[@class='header']//header//a[@class='close' and contains(text(),'Close')]");
		//if(objWebActions.checkElementDisplayed(btnSignUpClose)) {
		logReporter.log("Close Sign Up Popup",   
				objWebActions.clickUsingJS(btnSignUpClose));
		//	}
	}

	public void verifyLinksOnFlogScreen(String lnk)
	{
		By locator = By.xpath("//p[@class='useful-links'][text()='Forgot your ']//following::a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify " +lnk + "on page", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));
	}

	public void verifyEmailDisplayedOnFlogConfirmationScreen(String txt,String lnk)
	{
		By locator = By.xpath("//p[@class='heading'][contains(.,'"+txt+"')]//following::span[@class='email']//a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify '"+lnk+ "' on flog confirmation page", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));
	}
	public void verifyTelephoneNumberDisplayedOnFlogConfirmationScreen()
	{
		By locator = By.xpath("//p[@class='heading'][text()='Live Help']//following::span[@class='telephone']//a[contains(.,'0330 102 8582')]");
		logReporter.log("Verify '0330 102 8582' on flog confirmation page", 
				objWebActions.checkElementElementDisplayedWithMidWait(locator));
	}

	//Search Functionality	
	public void verifySearchBtnDisplayed()
	{
		logReporter.log("Verify Search button is displayed on Homepage ",
				objWebActions.checkElementDisplayed(SearchBtn));
	}
	public void clickSearchBtn()
	{
		waitMethods.sleep(6);
		logReporter.log(" Click on Search button  on Homepage ",
				objWebActions.click(SearchBtn));
	}
	public void verifyinputSearchNameFieldDisplayed()
	{
		logReporter.log("Verify Search input field is displayed on Homepage ",
				objWebActions.checkElementDisplayed(SearchBtn));
	}
	public void setSearchNameField(String Gamename)
	{
		logReporter.log(" input text to search on Homepage ",
				objWebActions.setText(inpSearchTxt, Gamename));
	}

	public void verifyGameIamgeOnSearchResultsDisplayed(String Gamename)
	{
		By GameNameOnSearchResults = By.xpath("//div[@class='search-results__title heading']/span/strong[contains(text(),'"+Gamename+"')]/preceding::span/img");
		logReporter.log(" Verify Game image is displayed on Search Results ",
				objWebActions.checkElementDisplayed(GameNameOnSearchResults));
	}
	public void verifyGameNameOnSearchResultsDisplayed(String Gamename)
	{
		By GameNameOnSearchResults = By.xpath("//div[@class='search-results__title heading']/span/strong[contains(text(),'"+Gamename+"')]");

		logReporter.log(" Verify Game Name containing "+ objWebActions.getText(GameNameOnSearchResults)+ " On Search Results displayed",
				objWebActions.checkElementDisplayed(GameNameOnSearchResults));
	}

	public void verifyInfoIconOnSearchResultsDisplayed(String Gamename)
	{
		//a[@class='search-results__info']/i[@class='search-results__info-icon']/following::span[contains(text(),'euro')]

		logReporter.log("Verify Info Icon On Search Results displayed",
				objWebActions.checkElementDisplayed(GameInfoOnSearchResults));
	}

	public void clickInfoIconOnSearchResults()
	{
		logReporter.log(" click on Info Icon On Search Results displayed",
				objWebActions.click(GameInfoOnSearchResults));
	}

	public void verifyPlayBtnOnSearchResultsDisplayed()
	{
		logReporter.log("Verify Play Button On Search Results displayed",
				objWebActions.checkElementDisplayed(PlayBtnOnSearchResults));
	}

	public void clickPlayBtnOnSearchResults()
	{
		logReporter.log("Verify Play Button On Search Results displayed",
				objWebActions.click(PlayBtnOnSearchResults));
	}

	public void verifyNoResultsErrorMSGDisplayed()
	{
		By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
		logReporter.log(" Verify 'Sorry but no matches were found' Message displayed",
				objWebActions.checkElementDisplayed(NoResultsErrorMSG));
	}
	public void verifybrowseSectionBTNDisplayed()
	{
		logReporter.log(" Verify 'Browse Sections BTN' displayed",
				objWebActions.checkElementDisplayed(browseSectionBTN));
	}
	public void clickbrowseSectionBTN()
	{
		logReporter.log(" Click 'Browse Sections BTN' displayed",
				objWebActions.click(browseSectionBTN));
	}

	public void verifycloseBTNDisplayed()
	{
		By closeBTN = By.xpath("//button[@class='search-no-results__close-btn info' and contains(text(),'Close')]");
		logReporter.log(" Verify 'close BTN' on error message  displayed",
				objWebActions.checkElementDisplayed(closeBTN));
	}

	public void clickCloseSearchBTN()
	{
		By CloseSearchBTN = By.xpath("//button[@class='search__close-btn']");
		logReporter.log("click on X button to close the search results",
				objWebActions.click(CloseSearchBTN));
	}

	public void verifyIconsDisplayed()
	{
		By SevenIcon = By.xpath("//i[@class='sitemap__icon seven']");
		By chipsIcon = By.xpath("//i[@class='sitemap__icon chip-solid']");

		logReporter.log(" Verify 'Seven Icon' displayed",
				objWebActions.checkElementDisplayed(SevenIcon));

		logReporter.log(" Verify 'Solid Chips Icon' displayed",
				objWebActions.checkElementDisplayed(chipsIcon));

	}

	public void verifySectionsDisplayed(String sections)
	{
		if(sections.contains(","))
		{
			String[] arr1 = sections.split(",");

			for (String sections2 : arr1) 
			{
				By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+sections2+"')]");

				logReporter.log(" Verify "+sections2+" is displayed when clicked on Browse section button ",  
						objWebActions.checkElementDisplayed(sectionName));
			}
		}
	}
	public void clickOnSection(String secname , String url ,String pageTitle)
	{
		By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+secname+"')]");
		objWebActions.openNewWindow(sectionName);
		waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		objWebActions.switchToWindowUsingTitle(pageTitle);
		waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		String currentURL = objDriverProvider.getWebDriver().getCurrentUrl();
		System.out.println("currentURL :: "+currentURL);
		logReporter.log(secname +" Redirected to the correct url : ",currentURL.equalsIgnoreCase(url));
		objWebActions.switchToWindowUsingTitle("Play Casino Games Online with Bella Casino");
	}

	public void verifyPlayBtnOnGameDetails()
	{
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		logReporter.log(" Verify 'RealPlay Button' displayed",
				objWebActions.checkElementDisplayed(RealPlayButton));
	}

	public void clickPlayBtnOnGameDetails()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		logReporter.log(" click'RealPlay Button' displayed",
				objWebActions.click(RealPlayButton));
		waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
	}
	public void verifyDemoPlayBtnOnGameDetails()
	{
		By DemoPlayButton = By.xpath("//button[@class='load-freeplay-game column-4-exsmall column-4-small' and contains(text(),'Demo')]");
		logReporter.log(" Verify 'Demo Play Button' displayed",
				objWebActions.checkElementDisplayed(DemoPlayButton));
	}

	public void verifyTabsOnGameInfo(String tabname)
	{
		if(tabname.contains("~"))
		{
			String[] arr1 = tabname.split("~");

			for (String tabname2 : arr1) 
			{
				By sectionName = By.xpath("//a[contains(text(),'"+tabname2+"')]");
				logReporter.log(" Verify "+tabname2+" section is displayed on Game info page ",  
						objWebActions.checkElementDisplayed(sectionName));
			}
		}
	}

	public void clickCloseXonGameInfoPage()
	{
		By CloseXBTN = By.xpath("//button[@class='close']");
		logReporter.log("click on X button on game info page",
				objWebActions.click(CloseXBTN));
	}

	public void verifySearchOverLay() {
		By search_overLay = By.xpath("//div[@class='field-container']");
		logReporter.log("Check 'search OverLay' ", objWebActions.checkElementDisplayed(search_overLay));
	}


	public void verifySearchResults() {
		By search_overLay = By.xpath("//li[@class='search-results__category']");
		logReporter.log("Check 'search result section' ", objWebActions.checkElementDisplayed(search_overLay));
	}

	public void verifyGameDetails(String option,String gamenm) {
		switch (option) {
		case "Image": {
			verifyGameIamgeOnSearchResultsDisplayed(gamenm);
			break;}
		case "Name": {
			verifyGameNameOnSearchResultsDisplayed(gamenm);
			break;}
		case "Play Now CTA": {
			verifyPlayBtnOnSearchResultsDisplayed();
			break;
		}
		case "Info CTA": {
			verifyInfoIconOnSearchResultsDisplayed(gamenm);
			break;
		}}
	}
}