package com.bella.mobile.page.Bella;

import java.util.Random;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class ResponsibleGamblingPage 
{
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public ResponsibleGamblingPage(MobileActions objMobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}


	By HowLongWouldMsgTakeBreak = By.xpath("//fieldset[@class='form-checkbox-buttons']/p[contains(text(),'How long would you like to take a break?')]");
	By inpPwd = By.xpath("//input[@id='input-password']");

	By btnPasswordShow = By.xpath("//input[@id='input-password']//following::button[text()='Show']");

	By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'Are you sure?')]");
	By InfoOnPopup = By.xpath("//p[contains(text(),'you are choosing to start your break. Your account will be locked until')]");
	By TakeBreakAndLogoutBtn = By.xpath("//button[contains(text(),'Take a break & LogOut')]");
	By PopupCancelBtn = By.xpath("//button[contains(text(),'Cancel')]");
	By PopupXBtn = By.xpath("//div[@id='modalTakeBreak']/div/div/div/button[@class='close']");
	By PopupConfirmBtn = By.xpath("//button[contains(text(),'Confirm')]");

	//Self Exclusion Functionality

	By DoUFeelProblemWithGamblingTXT = By.xpath("//p[contains(text(),'you have a problem with gambling?')]");
	By btnYesSelfExclude = By.xpath("//div[@class='myaccount-menu']/ul/li/a[@class='reality-check' and contains(text(),'Yes')]");
	By btnNoSelfExclude = By.xpath("//div[@class='myaccount-menu']/ul/li/a[@class='reality-check' and contains(text(),'No')]");

	By YouCanBlockURSelfTXT = By.xpath("//div[@id='u37050']/p[contains(text(),'You can block yourself from playing with Bella for a chosen period of time.')]");
	By WhySelfExcludeTXT = By.xpath("//div[@class='tell-more__text' and contains(text(),'Why Self Exclude?')]");
	By expandWhySelfExcludebtn = By.xpath("//span[@class='collapse-button tell-more__icon closed enabled']");
	By HowLongDoUWantToLockACTxt = By.xpath("//fieldset[@class='form-checkbox-buttons']/p[contains(text(),'How long do you want to lock your account for?')]");
	By WantToSelfExcludeBtn = By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Yes, I want to Self Exclude')]");

	//Reality Checks

	By RealityCheckInfo1 = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games.')]");
	By RealityCheckInfo2 = By.xpath("//p[contains(text(),'You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval')]");
	By RealityCheckInfo3 = By.xpath("//p[contains(text(),'Your gaming session will begin when you place your first real money bet and will end when you log out.')]");
	By TellMeMorelnk = By.xpath("//div[contains(text(),'Tell me more')]");
	By TellMeMoreExpandBtn = By.xpath("//span[@class='collapse-button tell-more__icon closed enabled']");
	By TellMeMoreCollapseBtn = By.xpath("//span[@class='collapse-button tell-more__icon opened enabled']");

	By setReminderTXT = By.xpath("//p[contains(text(),'Set your reminder')]");
	By selectReminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'30 mins')]");
	By saveChangesBTN = By.xpath("//button[contains(text(),'Save changes')]");


	// Reality Checks Functionality

	public void verifyRealityChecksInfoMessages(String msg)
	{
		By RealityCheckInfo1 = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games.')]");

		logReporter.log("Verify"+msg+" msg displayed",
				objMobileActions.checkElementDisplayed(RealityCheckInfo1));
	}

	public void verifyTellMeMoreExpandBtnDisplayed()
	{
		logReporter.log("Verify Tell Me More expand button displayed",
				objMobileActions.checkElementDisplayed(TellMeMoreExpandBtn));
	}

	public void clickTellMeMoreExpandBtn()
	{
		logReporter.log(" click on Tell Me More expand button displayed",
				objMobileActions.click(TellMeMoreExpandBtn));
	}

	public void clickTellMeMoreCollapseBtn()
	{
		logReporter.log(" click on Tell Me More collapse button displayed",
				objMobileActions.click(TellMeMoreCollapseBtn));
	}

	public void verifyTellMeMorelinkDisplayed()
	{
		logReporter.log("Verify Tell Me More link displayed",
				objMobileActions.checkElementDisplayed(TellMeMorelnk));
	}

	public void verifySetReminderTXTDisplayed()
	{
		logReporter.log("Verify set Reminder TXT displayed",
				objMobileActions.checkElementDisplayed(setReminderTXT));
	}

	public void verifyReminderOptionsDispalyed(String remprd)
	{
		if(remprd.contains("~"))
		{
			String[] arr1 = remprd.split("~");
			for (String remprd2 : arr1) 
			{
				By Reminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+remprd2+"')]");

				logReporter.log(" Verify " +remprd2+" Reminder Period option displayed",
						objMobileActions.checkElementDisplayed(Reminderprd));
			}
		}

	}

	public void SelectReminderperiod(String remPeriod)
	{
		By selectReminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+remPeriod+"')]");

		logReporter.log("Select Reminder Period Button displayed",
				objMobileActions.click(selectReminderprd));
	}


	public void verifyReminderperiodSaved(String remPeriod)
	{
		By selectReminderprd = By.xpath("//div[@class='panel panel-default']/button[@class='secondary active'][contains(text(),'"+remPeriod+"')]");

		logReporter.log("Reminder Period saved properly ",
				objMobileActions.click(selectReminderprd));
	}
	public void verifySaveChangesBTNDisplayed()
	{
		logReporter.log("Verify SaveChanges Button displayed",
				objMobileActions.checkElementDisplayed(saveChangesBTN));
	}
	public void clickSaveChangesBTN()
	{
		logReporter.log(" click on SaveChanges Button displayed",
				objMobileActions.click(saveChangesBTN));
	}

	public void verifySavedBtnDisplayed()
	{
		By savedBtn =By.xpath("//fieldset[@class='form-buttons padded']/div/button[@class='saved']");
		logReporter.log(" Verify Saved Button is displayed",
				objMobileActions.checkElementDisplayed(savedBtn));
	}

	//Take A Break Functionality

	public void verifyInfoMsgTakeBreakDisplayed(){

		By infoMsgTakeBreak = By.xpath("//div[@class='component take-a-break']/div/p/p[contains(text(),'If you would like to take a temporary break from gambling')]");
		logReporter.log("Verify If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks. msg displayed",
				objMobileActions.checkElementDisplayed(infoMsgTakeBreak));
	}


	public void verifyInfoMessages(String msg)
	{
		By infoTxt01 = By.xpath("//div[@class='card card-block']/p[contains(text(),'"+msg+"')]");

		logReporter.log("Verify"+msg+" msg displayed",
				objMobileActions.checkElementDisplayed(infoTxt01));
	}


	public void verifyWhyTakeABreaklnkDisplayed(){
		By WhyTakeABreak = By.xpath("//div[@class='collapse-block__main']/div[contains(text(),'Why take a break?')]");
		By expandLnk = By.xpath("//div[@class='collapse-block__main']/span[@class='collapse-button tell-more__icon closed enabled']");

		logReporter.log("Verify 	Why take a break? msg displayed",
				objMobileActions.checkElementDisplayed(WhyTakeABreak));

		logReporter.log("Verify Expand link displayed next to Why take a break? msg",
				objMobileActions.checkElementDisplayed(expandLnk));

		logReporter.log(" click on Expand link displayed next to Why take a break? msg",
				objMobileActions.click(expandLnk));
	}

	public void verifyHowLongWouldTakeBreakMsgDisplayed(){
		logReporter.log("Verify How long would you like to take a break? msg displayed",
				objMobileActions.checkElementDisplayed(HowLongWouldMsgTakeBreak));
	}

	public void selectTakeABreakPeriod(String brkPeriod)
	{
		By BreakPeriod = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+brkPeriod+"')]");
		logReporter.log("Select Take a Break Period : ",brkPeriod,
				objMobileActions.click(BreakPeriod));
	}

	public void verifyBreakPeriodOptions(String brkPeriod)
	{
		By BreakPeriod = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+brkPeriod+"')]");
		logReporter.log("Verify Take a Break Period option as : ",brkPeriod,
				objMobileActions.checkElementDisplayed(BreakPeriod));
	}
	public void verifyPasswordFieldDisplayed()
	{
		logReporter.log("Verify Password field displayed",
				objMobileActions.checkElementDisplayed(inpPwd));
		logReporter.log("Verify show button displayed aginest password field",
				objMobileActions.checkElementDisplayed(btnPasswordShow));
	}

	public void inputPassword(String pwd)
	{
		// logReporter.log("Scroll till Take Break button visible ", objMobileActions.androidScrollToRequiredTextContains("Take a Break"));

		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		/*objMobileActions.scrollUsingTouchActions_ByElements(objMobileActions.
  						processMobileElement(HowLongWouldMsgTakeBreak), objMobileActions.processMobileElement(lnkLiveHelp)));*/

		logReporter.log("Enter Password to take a break / Self Exclude",
				objMobileActions.setTextWithClear(inpPwd,pwd));
		objMobileActions.pressKeybordKeys(inpPwd,"tab");
	}
	public void ClickTakeBreakBtnDisplayed()
	{
		By btnTakeABreak = By.xpath("//button[@type='submit' and contains(text(),'Take a Break')]");

		logReporter.log("Verify Take a break button displayed",
				objMobileActions.checkElementDisplayed(btnTakeABreak));

		logReporter.log("Click on the Take a Break Period",
				objMobileActions.click(btnTakeABreak));
	}

	public void verifyConfirmationPopupDisplayed()
	{
		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'Are you sure?')]");

		logReporter.log("Are you sure? Header is displayed on Popup",
				objMobileActions.checkElementDisplayed(AreYouSurePopupHDR));

		By InfoOnPopup = By.xpath("//span[contains(text(),'you are choosing to start your break. Your account will be locked until')]");
		logReporter.log("Popup Msg containing text -you are choosing to start your break. Your account will be locked until is displayed",
				objMobileActions.checkElementDisplayed(InfoOnPopup));
	}

	public void verifyTakeBreakAndLogoutBtnOnPopup()
	{ 		
		logReporter.log("Verify Take a Break And Logout Btn is displayed on popup",
				objMobileActions.checkElementDisplayed(TakeBreakAndLogoutBtn));
	}

	public void clickTakeBreakAndLogoutBtn(){

		logReporter.log("Click on Take a Break And Logout  Button",
				objMobileActions.click(TakeBreakAndLogoutBtn));
	}

	public void verifyCancelBtnOnPopupDisplayed()
	{
		logReporter.log("Verify cancel Btn is displayed on popup",
				objMobileActions.checkElementDisplayed(PopupCancelBtn ));
	}
	public void clickTakeBreakPopupCancelBtn(){
		logReporter.log("Click on Cancel  Button on popup",
				objMobileActions.click(PopupCancelBtn ));
	}

	public void verifyErrorMessageDisplayedOnLogin(String msg)
	{
		By errorUserOnBreak = By.xpath("//div[contains(@class,'error-msg')]//ul/li[contains(.,'"+msg+"')]");
		logReporter.log("Verify "+msg+" error message displayed on login screen ",
				objMobileActions.checkElementDisplayed(errorUserOnBreak));
	}

	//Self Exclusion Functionality

	public void verifyDoUFeelProblemWithGamblingTXT()
	{
		logReporter.log("Do U Feel Problem With Gambling text is displayed",
				objMobileActions.checkElementDisplayed(DoUFeelProblemWithGamblingTXT));
	}

	public void verifybtnYesSelfExcludeDisplayed(){
		logReporter.log("Yes button displayed",
				objMobileActions.checkElementDisplayed(btnYesSelfExclude));
	}

	public void verifybtnNoSelfExcludeDisplayed(){
		logReporter.log("No button displayed",
				objMobileActions.checkElementDisplayed(btnNoSelfExclude));
	}
	public void clickYesbtn(){
		logReporter.log("Click on Yes Button",
				objMobileActions.click(btnYesSelfExclude));
	}

	public void verifyYouCanBlockURSelfTXT()
	{
		logReporter.log("You Can Block URSelf from playing... text is displayed",
				objMobileActions.checkElementDisplayed(YouCanBlockURSelfTXT));
	}

	public void verifyHowLongDoUWantToLockACTxt()
	{
		logReporter.log("How Long Do U Want To Lock Account'text is displayed",
				objMobileActions.checkElementDisplayed(HowLongDoUWantToLockACTxt));
	}
	public void verifyWhySelfExcludeTXT()
	{
		logReporter.log("'Why Self Exclude? text is displayed",
				objMobileActions.checkElementDisplayed(WhySelfExcludeTXT));
		logReporter.log("Expand button next to 'Why Self Exclude?  is displayed",
				objMobileActions.checkElementDisplayed(expandWhySelfExcludebtn));

		logReporter.log(" click on Expand button next to 'Why Self Exclude?  is displayed",
				objMobileActions.click(expandWhySelfExcludebtn));
	}

	public void selectSelfExcludePeriod(String exclPeriod)
	{
		By ExcludePeriod = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+exclPeriod+"')]");
		logReporter.log("Select Self Exclusion Period",
				objMobileActions.click(ExcludePeriod));
	}
	public void verifyWantToSelfExcludeBtnDisplayed()
	{
		logReporter.log("Verify Yes, I want to self exclude button displayed",
				objMobileActions.checkElementDisplayed(WantToSelfExcludeBtn));
	}
	public void clickWantToSelfExcludeBtn()
	{
		logReporter.log(" Click on Yes, I want to self exclude button",
				objMobileActions.click(WantToSelfExcludeBtn));
	}

	public void verifySelfExclInfoMessages(String msg)
	{
		
		if(msg.contains("~"))
		{
			String[] arr1 = msg.split("~");
			for (String links : arr1  ) {
				By infoTxt01 = By.xpath("//div[@class='card card-block']/ul/li[contains(text(),'"+links+"')]");
				logReporter.log("Verify"+links+" msg displayed",
						objMobileActions.checkElementDisplayed(infoTxt01));}
			}
		else{
			By infoTxt01 = By.xpath("//div[@class='card card-block']/ul/li[contains(text(),'"+msg+"')]");
			logReporter.log("Verify"+msg+" msg displayed",
					objMobileActions.checkElementDisplayed(infoTxt01));}
	}


	public void verifyExclusionConfirmationPopupDisplayed()
	{
		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'ARE YOU SURE?')]");

		logReporter.log("Are you sure? Header is displayed on Popup",
				objMobileActions.checkElementDisplayed(AreYouSurePopupHDR));

		By InfoOnPopup = By.xpath("//p[contains(text(),'You are about to self-exclude from gambling online with ')]");
		logReporter.log("Popup Msg containing text -You are about to self-exclude from gambling online with. Your account will be locked until is displayed",
				objMobileActions.checkElementDisplayed(InfoOnPopup));
	}


	public void verifySelfExcludeNLogOutBtnOnPopup()
	{ 		
		By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude and log out')]");
		logReporter.log(" Verify Self exclude and log out Button is displayed on popup",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void clickSelfExcludeNLogOutBtn(){
		By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude and log out')]");
		logReporter.log("Click on Self exclude and log out Button Button",
				objMobileActions.click(locator));
	}

	public String calculateExclusionDate(String brktime)
	{
		String[]	brkYear= brktime.split(" ");
		String brkdate= objUtilities.getRequiredDateWithCustomYear(brkYear[0], "dd/MM/YYYY", "");
		return brkdate;

	}

	public void verifySelfExclusionCompletePopup()
	{ 		
		By SelfExclusionCompleteHDR = By.xpath("//h4[contains(text(),'SELF-EXCLUSION COMPLETE')]");
		By InfoOnPopup = By.xpath("//p[contains(text(),'Your account is locked until:')]");

		logReporter.log("Verify SELF-EXCLUSION COMPLETE header is displayed on popup",
				objMobileActions.checkElementDisplayed(SelfExclusionCompleteHDR));

		logReporter.log("Verify To extend your break from online gambling,.... text is displayed on popup",
				objMobileActions.checkElementDisplayed(InfoOnPopup));

	}

	public void clickOKonPopup()
	{
		By OkPopup = By.xpath("//button[@id='modal-opt-out' and contains(text(),'OK')]");

		logReporter.log("Click on Ok button displayed on popup",
				objMobileActions.click(OkPopup));

	}

	public void validateExclusionPeriod(String exclPeriod)
	{
		By lockUntilDate = By.xpath("//div[@class='lock-date']/p[contains(text(),'Lock until:')]");

		String[] lockuntildate= (objMobileActions.getText(lockUntilDate)).split(": ");

		System.out.println("lock untill date displayed on site : "+lockuntildate[1]);

		String exclusionPeriod= this.calculateExclusionDate(exclPeriod);
		System.out.println("exclusionPeriod selected : "+exclusionPeriod);

		logReporter.log("Self exclusion period selected matches with the date displayed on screen :",
				exclusionPeriod.matches(lockuntildate[1]));

	}

	public void verifyLockUntilText()
	{
		By locator = By.xpath("//p[contains(.,'Lock until:')]")	;
		logReporter.log("Verify 'Lock until:' text is displayed",
				objMobileActions.checkElementDisplayed(locator));
	}
	public static String getRandomReminderForUser() {
		String[] arrTitles = {"15 mins", "30 mins", "1 hour"};
		int idx = new Random().nextInt(arrTitles.length);
		return (arrTitles[idx]);
	}
}


