package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.DepositPage;
import com.bella.desktop.page.Bella.MyAccountPage;
import com.bella.desktop.page.Bella.WithdrawPage;
import com.generic.WebActions;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class WithdrawPageSteps {
	
	private WithdrawPage objWithdrawPage;
	private Utilities objUtilities;
	private MyAccountPage objMyAccountPage;
	private DepositPage objDepositPage;
	private WaitMethods waitMethods;
	String balance;
	WebActions objwebaction;
	public WithdrawPageSteps(WithdrawPage objWithdrawPage,Utilities objUtilities,MyAccountPage myAccountPage ,DepositPage objDepositPage,WaitMethods waitMethods,WebActions objwebaction) {		
		this.objWithdrawPage = objWithdrawPage ;
		this.objUtilities = objUtilities ;
		this.objMyAccountPage = myAccountPage ;
		this.objDepositPage = objDepositPage;
		this.waitMethods =  waitMethods ;
		this.objwebaction = objwebaction;
	}
	
	@Then("^Enter Withdraw amount as \"([^\"]*)\"$")
	public void enter_Withdraw_amount_as(String amount) {
		objWithdrawPage.setWithdrawAmount(amount);
	}
	
	@Then("^Verify following fields are displayed on withdraw screen$")
	public void verify_following_fields_are_displayed_on_withdraw_screen(DataTable dt) {
		//objwebaction.pageRefresh();
		waitMethods.sleep(5);
		if(objWithdrawPage.verifyAreYouSureToWithdrawHeadeDisplayed())
		{objWithdrawPage.clickContinue();}
	
		objWithdrawPage.switchToWithdrawalIframe();
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				objWithdrawPage.verifyTextOnWithdrawScreen((list.get(i)));
			}
	}

	@Then("^Verify \"([^\"]*)\" message is displayed with \"([^\"]*)\" button$")
	public void verify_message_is_displayed_with_button(String arg1, String arg2)  {
		objWithdrawPage.verifyYourWithdrawalSuccessMessage(arg1);
		objWithdrawPage.verifyTransactionWillAppearAsBellaInfoText();
		objDepositPage.verifyButton(arg2);
		String amt = objWithdrawPage.getAmount("Your current pending withdrawals are");
		
		String amt1 = objWithdrawPage.getAmount("Your current balance is");
		System.out.println(amt + "    " +amt1);
	}
	
	@Then("^Verify Menu screen get closed when click on \"([^\"]*)\" button$")
	public void verify_Menu_screen_get_closed_when_click_on_button(String arg1)  {
		objDepositPage.clickOnButton(arg1);
		objWithdrawPage.verifyMyAccountMenuScreenGetClosed();
		
	}
	
	@Then("^Verify \"([^\"]*)\" message$")
	public void verify_message(String arg1) {
		objWithdrawPage.switchToWithdrawalIframe();
		objWithdrawPage.verifyInfoText(arg1);
	}
	@Then("^Verify 'Please contact support should you need any assistance.' message$")
	public void verify_Please_contact_support_should_you_need_any_assistance_message() {
		objWithdrawPage.verifyPleasecontactsupportshouldyouneedanyassistancemessage();
	}
	
	@Then("^Verify 'The minimum amount you can withdraw is  £10.00 ' message should be display$")
	public void verify_The_minimum_amount_you_can_withdraw_message_should_be_display(){
		objWithdrawPage.verifyMinimumWithdrawLimitErrorMessage();
	}
}
