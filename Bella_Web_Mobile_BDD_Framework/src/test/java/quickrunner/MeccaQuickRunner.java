
package quickrunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;

import com.generic.utils.ReportBuilder;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */

@RunWith(Cucumber.class)
@CucumberOptions( features= {"."}
		
		,glue= {"com.hooks", "com.meccabingo.stepDefinition." + "web" }// "mobile"} "web"}
		,dryRun=true ,monochrome = true, strict = true
		,plugin= {"pretty", "html:target/cucumber-pretty",
				  "json:target/CucumberTestReport.json"	
				//,"json:target/JSON/Cucumber.json", "rerun:target/SyncFails.txt" 
				 }
		//,tags = {"@3207, @3210, @3211, @3213, @3216, @3219, @3220, @3223"} 

		,tags = {"Desktop"}
		)

public class MeccaQuickRunner{
	@AfterTest	
	public void generatePoshReport() throws Throwable {		
		String reportPath = "target/Cucumber_Report/Desktop_Rerun";
		File reportPickupDirectory = new File(reportPath);
		List<String> jsonReportFiles = new ArrayList<String>();
		jsonReportFiles.add("target/JSON/DesktopRerun.json");
		String buildNumber = "Mecca Automation Execution [NP01]";
		String buildProjectName = "CMS_Mecca_Web_Mobile_BDD_Framework";
		String pluginURLPath = "";
		Boolean skippedFails = true;
		Boolean undefinedFails = true;
		Boolean flashCharts = true;
		Boolean runWithJenkins = false;
		Boolean artificatsEnabled = false;
		String artifactsConfig = "";
		boolean highCharts = false;

		ReportBuilder reportBuilder = new ReportBuilder(jsonReportFiles, reportPickupDirectory, pluginURLPath,
				buildNumber, buildProjectName, skippedFails, undefinedFails, flashCharts, runWithJenkins,
				artificatsEnabled, artifactsConfig, highCharts);
		//reportBuilder.generateReports();		
	}
	
}
