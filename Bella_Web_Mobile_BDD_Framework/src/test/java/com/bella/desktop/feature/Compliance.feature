Feature: Compliance

@Desktop
Scenario: Page Layout > Check whether footers identify the regulators. A link should be present which should take the customer to the Gambling Commission page, which displays the relevant company license status
Given Invoke the Bella site on Desktop
Then Verify Regulators (UK and Alderney) and associated license statements is displayed under License section of the footer
And Click and verify 'UK Gambling Commission' link redirect to the correct page

@Desktop
Scenario: Page Layout > Check whether Terms & Conditions information is easily accessible from any place on the site
Given Invoke the Bella site on Desktop
Then Verify "Terms & Conditions" link is displayed on under "HELP" footer section
Then Verify Terms & Conditions information should easily be accessible from any place on the site

@Desktop
Scenario: Page Layout > Check Privacy and Cookies policy information is easily accessible from any place on the site
Given Invoke the Bella site on Desktop
Then Verify "Privacy Policy" link is displayed on under "HELP" footer section
Then Verify 'Privacy Policy' link is accessible on the site

@Desktop
Scenario: Page Layout > Check whether footer contains a Gamcare logo and it should be hyperlinked to correct landing page
Given Invoke the Bella site on Desktop
Then Verify "Gamcare" logo is displayed in footer section
Then Verify "Gamcare" logo hyperlinked to the correct page

@Desktop
Scenario: Page Layout > Check whether footer contains a UK Gambling Commission logo and it should be hyperlinked to correct landing page
Given Invoke the Bella site on Desktop
Then Verify "UK Gambling Commission" logo is displayed in footer section
Then Verify "UK Gambling Commission" logo hyperlinked to the correct page

@Desktop
Scenario: Page Layout > Check whether footer contains an Alderney Gambling Control Commission logo and it should be hyperlinked to correct landing page
Given Invoke the Bella site on Desktop
Then Verify "Alderney Gambling Control Commission" logo is displayed in footer section
Then Verify "Alderney Gambling Control Commission" logo hyperlinked to the correct page

@Desktop
Scenario: Page Layout > Check whether footer contains a GamStop logo and it should be hyperlinked to correct landing page
Given Invoke the Bella site on Desktop
Then Verify "GameStop" logo is displayed in footer section
Then Verify "GameStop" logo hyperlinked to the correct page

@Desktop
Scenario: Page Layout > Check whether footer contains a Keep it fun logo and it should be hyperlinked to correct landing page
Given Invoke the Bella site on Desktop
Then Verify "Keep it fun" logo is displayed in footer section
Then Verify "Keep it fun" logo hyperlinked to the correct page

@Desktop                               
Scenario: Page Layout > Check whether footer contains 18+ logo and it is NOT a hyperlink (It should be just a logo)
Given Invoke the Bella site on Desktop
Then Verify "18+" logo is displayed in footer section
Then Verify 18+ logo should not be a hyperlink

@Desktop
Scenario: Page Layout > Check whether footer contains logos for all the applicable payment methods
Given Invoke the Bella site on Desktop
Then Verify "Payment Methods" logo is displayed in footer section

@Desktop
Scenario: Limiting collusion/cheating > Ensure that information is available to the customer regarding cheating, recovered player funds and how to complain
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Verify "Terms & Conditions" link is displayed on under "HELP" footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding cheating, recovered player funds and how to complain

@Desktop
Scenario: Peer to Peer customers - Third party software > Ensure that information is available to the customer regarding the rules around the use of third-party software, and how to report suspected use that is against the operators T&Cs
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Verify "Terms & Conditions" link is displayed on under "HELP" footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding third party software

@Desktop
Scenario: Check that the 'Support' option is available for the customer in footer, and it navigates to Contact Us screen once click on it and Phone number and Email is displayed on the screen
Given Invoke the Bella site on Desktop
Then Verify "Support" link is displayed on under "HELP" footer section
Then Click "Support" link is displayed on under "HELP" footer section
And Verify that Phone number and Email is displayed on the screen