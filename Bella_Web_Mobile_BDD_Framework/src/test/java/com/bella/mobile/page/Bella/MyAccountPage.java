package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class MyAccountPage {
	private MobileActions objMobileActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public MyAccountPage(MobileActions mobileActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objMobileActions = mobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	By YesPleaseBTN = By.xpath("//button[contains(text(),'Yes please')]");	
	By NoThanksBTN = By.xpath("//button[contains(text(),'No thanks')]");
	By closeXbtn = By.xpath("//a[@class='close']");
	private By lnkLiveHelp = By.xpath("//a[text()='Live Help']");

	public void selectMyAccountMenu(String menu, String subMenu) 
	{
		By locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'" + menu + "')]");
		logReporter.log("Select main menu from My Account - ", menu,
				objMobileActions.click(locator));

		locator = By.xpath("//h2//span[contains(text(),'" + menu + "')]");
		logReporter.log("Verify "+menu+" header get displayed",
				objMobileActions.checkElementDisplayed(locator));

		if(subMenu != null)
		{
			if (subMenu.contains(","))
			{
				String[] arr1 = subMenu.split(",");

				for (String subMenu2 : arr1) 
				{
					By locator2 = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'"+ subMenu2+"')]");

					logReporter.log(" Verify "+subMenu2+" is displayed screen ",  
							objMobileActions.checkElementDisplayed(locator2));

					logReporter.log("click on sub menu get displayed",subMenu2,
							objMobileActions.click(locator2));
					waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
					By subMenuHDR = By.xpath("//h2//span[contains(text(),'" + subMenu2 + "')]");
					logReporter.log("Verify "+subMenu2+" header get displayed",
							objMobileActions.checkElementDisplayed(subMenuHDR));
					this.clickBackBTN();

				}
			} else
			{
				By locator3 = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'"+subMenu+"')]");

				logReporter.log(" click on "+subMenu+" displayed screen ",  
						objMobileActions.click(locator3));

				By subMenuHDR2 = By.xpath("//h2//span[contains(text(),'" + subMenu + "')]");
				logReporter.log("Verify "+subMenu+" header get displayed",
						objMobileActions.checkElementDisplayed(subMenuHDR2));

			}

		}
	}

	public void navigateMyAcountDetailsSubMenus(String subMenu)
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'"+subMenu+"')]");
		logReporter.log("Verify "+subMenu+" header get displayed on 'Account Details'",
				objMobileActions.click(locator));
	}
	public void navigateToMyAccountTab(String tabName)
	{
		switch (tabName) 
		{
		case "Transaction History":
			this.selectMyAccountMenu("Cashier","Transaction History");	
			break;
		case "Deposit":
			this.selectMyAccountMenu("Cashier","Deposit");
			break;
		case "Withdrawal":
			this.selectMyAccountMenu("Cashier","Withdrawal");
			break;
		case "Balance":
			this.selectMyAccountMenu("Cashier","Balance");
			break;
		case "Account Details":
			this.selectMyAccountMenu("Account Details",null);
			break;
		case "Change Password":
			this.selectMyAccountMenu("Account Details",null);
			this.navigateMyAcountDetailsSubMenus("Change Password");
			break;
		case "Marketing Preferences":
			this.selectMyAccountMenu("Account Details",null);
			this.navigateMyAcountDetailsSubMenus("Marketing Preferences");
			break;
		case "Enter bonus code":
			this.selectMyAccountMenu("Bonuses","Enter bonus code");
			break;
		case "Active Bonuses":
			this.selectMyAccountMenu("Bonuses","Active Bonuses");
			break;
		case "Bonus History":
			this.selectMyAccountMenu("Bonuses","Bonus History");
			break;
		case "Reality Check":
			this.selectMyAccountMenu("Responsible Gambling","Reality Check");
			break;
		case "Deposit Limits":
			this.selectMyAccountMenu("Responsible Gambling","Deposit Limits");
			break;
		case "Take a break":
			this.selectMyAccountMenu("Responsible Gambling","Take a break");
			break;
		case "Self Exclude":
			this.selectMyAccountMenu("Responsible Gambling","Self Exclude");
			break;
		}
	}
	public int getAvailableAmountToWithdraw() {
		By locator = By.xpath("//div[@class='amount-text']/span[@class='preview-amount']/strong");
		String availableAmount = objMobileActions.getText(locator);
		availableAmount = availableAmount.substring(1);
		availableAmount = availableAmount.substring(0, availableAmount.indexOf("."));
		int amountToWithdraw = Integer.parseInt(availableAmount);
		return amountToWithdraw;
	}

	//Deposit Functionality

	public void verifyliveHelpLnkDisplayed()
	{
		By liveHelpLnk = By.xpath("//a[contains(text(),'Live Help')]");
		logReporter.log(" Verify Live Help Link is Displayed",  
				objMobileActions.checkElementDisplayed(liveHelpLnk));
	}
	public void verifysetDepLimitsLnkDisplayed()
	{
		By setDepLimitsLnk = By.xpath("//a[contains(text(),'Set a Deposit limit')]");
		logReporter.log(" Verify set Deposit Limits Link is Displayed",  
				objMobileActions.checkElementDisplayed(setDepLimitsLnk));
	}

	public void clicksetDepLimitsLnkDisplayed(){
		By setDepLimitsLnk = By.xpath("//a[contains(text(),'Set a Deposit limit')]");
		logReporter.log(" click set Deposit Limits Link is Displayed",  
				objMobileActions.click(setDepLimitsLnk));
	}

	public void verifywelcomeBonusHDRDisplayed(){
		By welcomeBonusHDR = By.xpath("//div[@class='dialog-box first-time-deposit']/p[contains(text(),'Welcome Bonus')]");
		logReporter.log(" Verify header containing 'welcome Bonus' is Displayed",  
				objMobileActions.checkElementDisplayed(welcomeBonusHDR));
	}

	public void verifyYesPleaseCTA(){
		logReporter.log(" Verify 'Yes Please' button is Displayed on welcome bonus",  
				objMobileActions.checkElementDisplayed(YesPleaseBTN));
	}

	public void verifyNoThanksBTN(){
		logReporter.log("  Verify 'No Thanks' button is Displayed on welcome bonus",  
				objMobileActions.checkElementDisplayed(NoThanksBTN));
	}

	public void clickYesPleaseBTN()
	{
		logReporter.log(" Click on 'YesPlease' button is Displayed on welcome bonus",  
				objMobileActions.click(YesPleaseBTN));
	}
	public void clickNoThanksBTN()
	{
		logReporter.log(" Click on 'NoThanks' button is Displayed on welcome bonus",  
				objMobileActions.click(NoThanksBTN));
	}
	public void verifyDepositNowHDRDisplayed()
	{
		By DepositNowHDR = By.xpath("//p[contains(text(),'Deposit now')]");
		logReporter.log(" Verify header containing 'Deposit now' is Displayed",  
				objMobileActions.checkElementDisplayed(DepositNowHDR));
	}

	public void setDepositAmt(String inpDepAmt)
	{
		By inputDepAmount= By.xpath("//input[@id='deposit-deposit-amount']");

		logReporter.log(" Set/Enter the Deposit amount ",  
				objMobileActions.setTextWithClear(inputDepAmount, inpDepAmt));
	}
	public void verifyminMaxDepAmtTXT()
	{
		By minMaxDepAmtTXT = By.xpath("//p[contains(text(),'Minimum deposit is £5, maximum £10,000')]");
		logReporter.log(" Verify 'Minimum deposit is £5, maximum £10,000' text is Displayed",  
				objMobileActions.checkElementDisplayed(minMaxDepAmtTXT));
	}

	public void clickOnNext()
	{
		By btnNext = By.xpath("//button[contains(text(),'Next')]");
		logReporter.log("Click 'Next' button ",  
				objMobileActions.click(btnNext));
	}
	public void firstDepositPoPFAgree() {
		By PoPFTXT = By.xpath("//p[contains(text(),'We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission')]");
		By chkDepositAgree = By.xpath("//input[@id='deposit-agree']");
		logReporter.log(" Verify 'We hold your balance in a designated bank account so that...' PoPF text is Displayed",  
				objMobileActions.checkElementDisplayed(PoPFTXT));
		logReporter.log("Select Deposit Agree checkbox",  
				objMobileActions.click(chkDepositAgree));
	}

	public void clickOnCloseIcon()
	{
		By lnkClose = By.xpath("//a[@class='close']");
		logReporter.log("Click 'close' button on screen ",  
				objMobileActions.click(lnkClose));
	}
	public void clickBackBTN()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By backBTN = By.xpath("//button[@class='back ']");
		logReporter.log("Click 'Back' button on screen ",  
				objMobileActions.click(backBTN));
	}

	public void setCardNumber(String cardNumber)
	{
		By inpCardNumber = By.id("cc_card_number");
		logReporter.log("Set Card number ", cardNumber, 
				objMobileActions.setText(inpCardNumber, cardNumber));
	}

	public void setExpMonth(String expMon)
	{
		By inpExpMonth = By.xpath("//div[@data-id='cc_exp_month']");

		logReporter.log("click expiry Month ", 
				objMobileActions.click(inpExpMonth));

		By monthFrmList = By.xpath("//select[@id='cc_exp_month']/option[@value='"+expMon+"']");

		logReporter.log("Set expiry Month ", expMon, 
				objMobileActions.click(monthFrmList));
	}

	public void setExpYear(String expYr)
	{
		By inpExpYear = By.xpath("//div[@data-id='cc_exp_year']");

		logReporter.log("click expiry year ", 
				objMobileActions.click(inpExpYear));

		By yearFrmList = By.xpath("//select[@id='cc_exp_year']/option[@value='"+expYr+"']");

		logReporter.log("Set expiry Year ", expYr, 
				objMobileActions.click(yearFrmList));
	}
	public void setCVVcode(String securityCodeCVV) {

		By inpSecurityCodeCVV = By.id("cc_cvv2");
		logReporter.log("Set security date (CVV) ", securityCodeCVV, 
				objMobileActions.setText(inpSecurityCodeCVV, securityCodeCVV));

	}
	public void clickDeposit() {
		By btnDeposit = By.xpath("//input[@id='continueButton' and @value='Deposit']");
		logReporter.log(" Click on 'Deposit' button ",  
				objMobileActions.click(btnDeposit));
	}


	public void verifyDepositConfirmationPopUp()
	{
		By confirmationHDR = By.xpath("//h4[contains(text(),'Confirmation')]");
		logReporter.log(" Verify Confirmation HDR displayed ",  
				objMobileActions.checkElementDisplayed(confirmationHDR));
	}

	public void verifySuccessfullyDepositedMessage()
	{
		By successDepositedTXT = By.xpath("//p[contains(text(),'Successfully deposited')]");
		logReporter.log(" Verify Successfully Deposited TXT displayed ",  
				objMobileActions.checkElementDisplayed(successDepositedTXT));
	}

	public void validateSuccessMessage()
	{
		verifyDepositConfirmationPopUp();
		verifySuccessfullyDepositedMessage();
		By closeBTN = By.xpath("//button[@id='close' and contains(text(),'Close')]");
		logReporter.log(" Verify Close BTN displayed ",  
				objMobileActions.checkElementDisplayed(closeBTN));
		logReporter.log(" Click on Close BTN ",  
				objMobileActions.click(closeBTN));
	}

	public void clickCloseXbtn()
	{
		logReporter.log(" Click closeX BTN displayed ",  
				objMobileActions.click(closeXbtn));
	}
	public void validateBalanceAfterDeposit(String initialBal , String depAmt)
	{
		By BalanceOnHomepage = By.xpath("//li[@class='text-data']/a/div/span/strong"); 

		String HomePageBalAfterDep =objMobileActions.getText(BalanceOnHomepage).substring(1);

		float SumOfinitialBalDepAmt =Float.parseFloat(initialBal) + Float.parseFloat(depAmt) ;
		System.out.println("SumOfinitialBalDepAmt : "+SumOfinitialBalDepAmt);

		logReporter.log(" Balance after deposit is  : "+HomePageBalAfterDep ,
				String.valueOf(Float.parseFloat(HomePageBalAfterDep)).equals(String.valueOf(SumOfinitialBalDepAmt)) );
	}

	public void clickOnPaymentMethodDropdown()
	{
		By drpcardMethod = By.xpath("//b[@class='button']");
		logReporter.log("click on payment method dropdown ",  
				objMobileActions.click(drpcardMethod));
	}
	public void selectDepositMethod(String depositMethod) {
		By locator = null;
		clickOnPaymentMethodDropdown();
		switch(depositMethod.toLowerCase()) 
		{
		case "paypal":
			locator = By.xpath("//ul[contains(@class,'selectric-group ')]//li/span[@title='PayPal']");
			logReporter.log("Select deposit method as PayPal",  
					objMobileActions.click(locator));
			break;
		case "paysafecard":
			locator = By.xpath("//ul[contains(@class,'selectric-group ')]//li/span[@title='PaySafeCard']");
			logReporter.log("Select deposit method as PaySafeCard.",  
					objMobileActions.click(locator));
			break;
		}	
	}

	public void selectAddedCardMethod()
	{
		clickOnPaymentMethodDropdown();
		By locator = By.xpath("//li[text()='My payment methods']//following::li[@class='selected']//div[contains(@title,'Visa  - **** ****')]");

		logReporter.log(" Verify card payment method ",  
				objMobileActions.checkElementDisplayed(locator));
		logReporter.log("Select payment method as Card ",  
				objMobileActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void clickShowMore() {
		By locator = By.xpath("//span[contains(text(),'Show More')]");
		logReporter.log("Click 'Show More' button ",  
				objMobileActions.click(locator));
	}

	public void verifyDepositSuccessPopUpDisplayed() {
		By locator = By.xpath("//h2[contains(text(),'Deposit successful!')]");
		logReporter.log("Verify 'Deposit' successful message displayed",  
				objMobileActions.click(locator));
	}


	public void performDepositUsingCardPaymentMethod(String cardNumber,String expMon,String expYr,String securityCodeCVV)
	{
		objMobileActions.switchToFrameUsingNameOrId("payment-process");
		this.setCardNumber(cardNumber);
		this.setExpMonth(expMon);
		this.setExpYear(expYr);
		this.setCVVcode(securityCodeCVV);
		this.clickDeposit();
	}
	public void verifyMyAccountsubOptionsLoadsCorrectly(String str)
	{
		if(str.contains("-"))
		{
			String str1[] =str.split("-");
			if(str1[1].contains(","))
			{
				String[] arr1 = str1[1].split(",");
				for (String links : arr1  ) {
					this.navigateToMyAccountTab(links);
					this.clickBackBTN();
					this.clickBackBTN();}
			}
		}
	}
}
