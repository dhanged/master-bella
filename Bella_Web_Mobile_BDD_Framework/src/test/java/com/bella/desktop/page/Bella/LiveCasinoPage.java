package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class LiveCasinoPage {
	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private DriverProvider objDriverProvider;

	public LiveCasinoPage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,DriverProvider driverProvider) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
		this.objDriverProvider = driverProvider;
	}
	//xpath 
	private By liveCasinoTopPicks = By.xpath("//game-panel-block-container[@title='Top Picks']//game-panel[1]");
	private By liveCasinoRoulette = By.xpath("//game-panel-block-container[@title='Roulette']//game-panel[1]");
	private By liveCasinoBlackjack = By.xpath("//game-panel-block-container[@title='Blackjack']//game-panel[1]");
	private By btnLiveCasinoRouletteInfo = By.xpath("//game-panel-block-container[@title='Roulette']//game-panel[1]//img[@src='/assets/grosvenor/img/info.jpg']");
	
	private By pageGameDetails = By.xpath("//div[@class='details-games']");
	private By btnJoin = By.xpath("//div[contains(@class,'details-button')]//button[contains(.,'Join')]");
		
	
	public void verifyTopPicksGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Top Picks games Section", 
				objWebActions.checkElementDisplayed(liveCasinoTopPicks));
		
	}
	public void verifyRouletteGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Roulette Games section", 
				objWebActions.checkElementDisplayed(liveCasinoRoulette));
		
	}
	public void verifyBlackjackGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Blackjack games option", 
				objWebActions.checkElementDisplayed(liveCasinoBlackjack));
		
	}
	
	public void verifyRouletteGameInfoButtonDisplayed() {
		logReporter.log("Mouse Hover", 
				objWebActions.mouseHover(liveCasinoRoulette));
		logReporter.log("Verify Info button for Roulette Games displayed", 
				objWebActions.checkElementDisplayed(btnLiveCasinoRouletteInfo));
		
	}
	
	public void clickRouletteGameInfoButton() {
		logReporter.log("Mouse Hover", 
				objWebActions.mouseHover(liveCasinoRoulette));
		logReporter.log("Click Info button for Roulette Games", 
				objWebActions.click(btnLiveCasinoRouletteInfo));
		
	}
	
	public void verifyGameDetailsPageDisplayed() {
		logReporter.log("Verify game Details page displayed", 
				objWebActions.checkElementDisplayed(pageGameDetails));
		
	}
	
	public void verifyJoinButtonDisplayed() {
		logReporter.log("Verify Join Button on game Details page displayed", 
				objWebActions.checkElementDisplayed(btnJoin));
		
	}
}
