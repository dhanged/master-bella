package com.bella.mobile.page.Mailinator;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.server.handler.GetElementText;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;


public class MailinatorPage {

	
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public MailinatorPage(MobileActions mobileActions,  AppiumDriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objMobileActions = mobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}
	
	// Input
	private By inpInboxName = By.xpath("//input[@aria-label='Enter Inbox Name']");
	private By lnkLogin = By.xpath("//a[contains(text(),'LOGIN')]");
	private By loginCodeEmailIframe = By.id("msg_body");
	private By inputEmail = By.id("many_login_email");
	private By inputPassword= By.id("many_login_password");
	private By btnLogin = By.xpath("//a[contains(text(),'Log in')]");
	
	// Button 
	private By btnGO = By.xpath("//button[@aria-label='Go to public']");
	 	
	// Link
	//By ClickHereLnk = By.xpath("(//table[@class='intro-copy']/tbody/tr[5]/td[1]/p[7]/font/a[contains(.,'here')]");
	By ClickHereLnk = By.xpath("//table[@class='intro-copy']//tbody//tr[5]//td[1]//p//font//a[contains(.,'here')]");
	// Checkbox

	// Logo
	private By logoMailinator = By.xpath("//div[@class='nav-title' and normalize-space()='Mailinator']");
	
	// header
	private By hdrInbox = By.xpath("//div[@class='ng-binding' and contains(.,'inbox')]");
  	 	
	
	public void verifyMailinatorLogoDisplayed(){
		logReporter.log("Verify Mailinator logo displayed.", 
				objMobileActions.checkElementDisplayed(logoMailinator));
	}
	
	public void verifyLoginlnkDisplayed()
	{
		logReporter.log("Verify Login link displayed.", 
				objMobileActions.checkElementDisplayed(lnkLogin));
	}
	
	public void clickLoginlnk()
	{
		logReporter.log("Click 'Login' link", 
				objMobileActions.click(lnkLogin));
	}
	
	public void setLoginEmail(String email)
	{
		logReporter.log("Enter Email ID for Mailinator Inbox ", email,
				objMobileActions.setText(inputEmail, email));
		
	}
	
	public void setPassword(String pwd)
	{
		logReporter.log("Enter Password for Mailinator Inbox ", pwd,
				objMobileActions.setText(inputPassword, pwd));
	}
	
	public void clickLoginBtn()
	{
		logReporter.log("Click 'Login' button", 
				objMobileActions.click(btnLogin));
	}
	
	
	public void setInboxName(String emailAddress) {
 		logReporter.log("Enter Public Mailinator Inbox ", emailAddress,
				objMobileActions.setText(inpInboxName, emailAddress));
	}

	public void clickGO() {
 		logReporter.log("Click 'GO' button", 
				objMobileActions.click(btnGO));
	}
	
	public void verifyInboxDisplayed(){
		logReporter.log("Verify Inbox displayed.", 
				objMobileActions.checkElementDisplayed(hdrInbox));
 	}

	public void verifyForgotUserNameMailReceived() {
		
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Your username reminder']");
		logReporter.log("Verify forgot username mail received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openForgotUsernameMail() {
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Your username reminder']");
		logReporter.log("Open forgot username email.", 
				objMobileActions.click(locator));
	} 
	
	
	public void validateUsernameinEmail(String username)
	{
		objMobileActions.switchToFrameUsingIframe_Element(loginCodeEmailIframe);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By Username_Email = By.xpath("//p[contains(text(),'Your username is')]");
		
		String Usernameinemail = objMobileActions.getText(Username_Email);
		
		if(Usernameinemail.contains(username))
		{
			logReporter.log("UserName displayed correctly", true);
			
		}
		else
		{
			logReporter.log("Wrong UserName displayed", true);
			
		}
		
	}
	public void verifyForgotPasswordMailReceived() {
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Your password reset request']");
		logReporter.log("Verify forgot password mail received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openForgotPasswordMail() {
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Your password reset request']");
		logReporter.log("Open forgot password email.", 
				objMobileActions.click(locator));
	} 
	
	public void clickResetPasswordButtonFromMail() {
		By locator = By.xpath("//a[text()='here']");
		String url = objMobileActions.getAttribute(locator, "href");
		logReporter.log("Click here link to Reset Password.", 
		objMobileActions.checkElementDisplayed(locator));
		
		System.out.println(" url ;;;  "+url);
		objDriverProvider.getAppiumDriver().get(url);
	 	//logReporter.log("Click here link to Reset Password.", 
			//	objMobileActions.click(locator));
	  	
	}
	
	public String getResetPasswordLink()
	{
		By locator = By.xpath("//a[text()='Reset Password']");
		return objMobileActions.getAttribute(locator, "href");
		
	}
	
	public void verifyForgotCardNumberMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Here are your membership card details']");
		logReporter.log("Verify forgot password mail received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openForgotCardNumberMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Here are your membership card details']");
		logReporter.log("Open forgot password email.", 
				objMobileActions.click(locator));
	} 
	
	public void validateCardNumberinEmail(String cardnumber)
	{
		objMobileActions.switchToFrameUsingIframe_Element(loginCodeEmailIframe);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By CardNumber_Email = By.xpath("//p[contains(text(),'Your membership card number is:')]");
		
		String CardNoinEmail = objMobileActions.getText(CardNumber_Email);
		
		if(CardNoinEmail.contains(cardnumber))
		{
			logReporter.log("Card Number displayed correctly", true);
			
		}
		else
		{
			logReporter.log("Wrong Card Number displayed", true);
			
		}
	}
	
	public void verifyForgotPINMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
		logReporter.log("Verify Reset PIN email received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openForgotPINMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
		logReporter.log("Open PIN Reset email.", 
				objMobileActions.click(locator));
	} 
	
	
	
	public String GetResetPINLinkinEmail()
	{
		//objMobileActions.switchToFrameUsingIframe_Element(loginCodeEmailIframe);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	/*	objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
		logReporter.log("Verify Click Here link is displayed in email.", 
				objMobileActions.checkElementDisplayed(ClickHereLnk));*/
		//By ClickHereLnk2 = By.xpath("(//a[contains(text(),'here') and @target='_other'])[2]");
		
		return objMobileActions.getAttribute(ClickHereLnk,"href");
	}
	
	// Self Exclusion email
	
	public void verifySelfExclusionMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Important information from Grosvenor Casinos']");
		logReporter.log("Verify SelfExclusion email received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openSelfExclusionMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Important information from Grosvenor Casinos']");
		logReporter.log("Open SelfExclusion email.", 
				objMobileActions.click(locator));
	} 
	
	public void verifyExclusionONDigitalInfoDisplayed()
	{
		By locator = By.xpath("//table[@class='intro-copy']/tbody/tr[2]/td//p[contains(text(),'We have received your request to self-exclude from GrosvenorCasinos.com and you will now no unable to play with us online. This also applies to our sister site Mecca Bingo, and can only be reversed by written request after at least 6 months.')]");
		
		logReporter.log("Verify 'We have received your request to self-exclude from GrosvenorCasinos.com ' msg displayed in email received.", 
				objMobileActions.checkElementDisplayed(locator));
		
	}
	
	
	public void verifyRequestedLoginCodeMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
		logReporter.log("Verify 'You've requested a Login Code to access GrosvenorCasinos.com' mail received.", 
				objMobileActions.checkElementDisplayed(locator));
	} 
	
	public void openRequestedLoginCodeMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
		logReporter.log("Open requested a Login Code mail.", 
				objMobileActions.click(locator));
	} 
	
	public String getLoginCode() {
		By locator = By.xpath("//p[contains(.,'Login') and contains(.,'code')]");
		return objMobileActions.getText(locator).split(":")[1].trim();
	}
	 
	public void invokeMailinatorSite()
	{
		objDriverProvider.getAppiumDriver().get(configuration.getConfig("mailinator.URL"));
	}
	public void verifyWelcomeMailReceivedOrNot() {
		
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Welcome to Bella Casino!']");
		logReporter.log("Verify Welcome mail received after successful registration ", 
				objMobileActions.checkElementDisplayed(locator));
	}
	public void openWelcomeMail() {
		By locator = By.xpath("//div[@ng-repeat='email in emails']//a//div//div//b[text()='Welcome to Bella Casino!']");
		logReporter.log("Open Welcome mail received after successful registration ", 
				objMobileActions.click(locator));
	}

	public void verifyUsernameAndLoginLinkInSuccessfulRegistrationEmail(String username,String url)
	{
		objMobileActions.switchToFrameUsingIframe_Element(loginCodeEmailIframe);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By Username_Email = By.xpath("//p[contains(text(),'You’ve successfully registered an account with us and your username is')]");

		String Usernameinemail = objMobileActions.getText(Username_Email);
		System.out.println(" Usernameinemail:   "+Usernameinemail +  "   username "+username);
		if(Usernameinemail.contains(username.toLowerCase())){
			logReporter.log("UserName displayed correctly", true);}
		else{logReporter.log("Wrong UserName displayed", false);}

		By lnkLogin = By.xpath("//table//tbody//tr//td//a[text()='Log In']");
		logReporter.log("Verify 'Login' cta displayed in  email", 
				objMobileActions.checkElementDisplayed(lnkLogin));

		logReporter.log("Verify 'Login' cta redirect to the bella site", 
				objMobileActions.getAttribute(lnkLogin, "href").equalsIgnoreCase(url));
	}
}