package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.HomePage;
import com.bella.desktop.page.Bella.ResponsibleGamblingPage;
import com.generic.WebActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class TakeABreakPageSteps {
	private ResponsibleGamblingPage objResponsibleGamblingPage;
	private Utilities utilities;	
	private WebActions objWebActions;
	private Configuration configuration;
	private HomePage objHomePage;
	String userName ,password,emailAddress;

	public TakeABreakPageSteps(ResponsibleGamblingPage objResponsibleGamblingPage,Utilities utilities,WebActions objWebActions,Configuration configuration,HomePage objHomePage) {		
		this.objResponsibleGamblingPage = objResponsibleGamblingPage;
		this.utilities = utilities;
		this.objWebActions = objWebActions;
		this.configuration = configuration;
		this.objHomePage =objHomePage;
	}

	@Then("^Verify infotext messge on take a break screen$")
	public void verify_infotext_messge_on_take_a_break_screen()
	{
		objResponsibleGamblingPage.verifyInfoMsgTakeBreakDisplayed();
		objResponsibleGamblingPage.verifyWhyTakeABreaklnkDisplayed();
		objResponsibleGamblingPage.verifyHowLongWouldTakeBreakMsgDisplayed();
	}

	@Then("^Verify following break periods$")
	public void verify_following_break_periods(DataTable dt)  {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objResponsibleGamblingPage.verifyBreakPeriodOptions((list.get(i)));
		}
	}

	@Then("^Select break period as \"([^\"]*)\"$")
	public void select_break_period_as(String arg1){
		objResponsibleGamblingPage.selectTakeABreakPeriod(arg1);
	}

	@Then("^Verify date is displayed on the screen until the account will be locked$")
	public void verify_date_is_displayed_on_the_screen_until_the_account_will_be_locked() {
		objResponsibleGamblingPage.verifyLockUntilText();
	}

	@Then("^Keep password field blank$")
	public void keep_password_field_blank(){
		objResponsibleGamblingPage.inputPassword("");
	}

	@Then("^Enter invalid password as \"([^\"]*)\"$")
	public void enter_invalid_password_as(String arg1) {
		objResponsibleGamblingPage.inputPassword(arg1);
	}
	
	@Then("^Click on 'Take a break' button$")
	public void click_on_Take_a_break_button() {
		objResponsibleGamblingPage.ClickTakeBreakBtnDisplayed();
	}

	@Then("^Enter valid password and hit 'Yes, I want to Self Exclude' button$")
	public void enter_valid_password_and_hit_Yes_I_want_to_Self_Excludebutton() {
		String password = configuration.getConfig("web.password");
		objResponsibleGamblingPage.inputPassword(password);
		objResponsibleGamblingPage.clickWantToSelfExcludeBtn();
	}
	
	@Then("^Enter valid password and hit 'Take a break' button$")
	public void enter_valid_password_and_hit_Take_a_break_button() {
		String password = configuration.getConfig("web.password");
		objResponsibleGamblingPage.inputPassword(password);
		objResponsibleGamblingPage.ClickTakeBreakBtnDisplayed();
	}

	@Then("^Check that the confirmation pop up is displayed with Cancel and Confirm CTA$")
	public void check_that_the_confirmation_pop_up_is_displayed_with_Cancel_and_Confirm_CTA() {
		objResponsibleGamblingPage.verifyConfirmationPopupDisplayed();
		objResponsibleGamblingPage.verifyTakeBreakAndLogoutBtnOnPopup();
		objResponsibleGamblingPage.verifyCancelBtnOnPopupDisplayed();
	}

	@Then("^Select 'cancel' option and Verify user navigate back to Take a break page$")
	public void select_cancel_option_and_Verify_user_navigate_back_to_Take_a_break_page()  {
		objResponsibleGamblingPage.clickTakeBreakPopupCancelBtn();
	}

	@Then("^Select Confirm button$")
	public void select_Confirm_button() throws Throwable {
		objResponsibleGamblingPage.clickTakeBreakAndLogoutBtn();
	}

	@Then("^Verify user get logged out$")
	public void verify_user_get_logged_out() throws Throwable {
		objHomePage.verifyUserLoggedOutSuccessfuly();
	}
	
	@Then("^Verify error message 'Your account is currently locked as being on ''Take a Break''\\. Please contact support\\.'$")
	public void verify_error_message_Your_account_is_currently_locked_as_being_on_Take_a_Break_Please_contact_support() {
		objResponsibleGamblingPage.verifyErrorMessageDisplayedOnLogin("Your account is currently locked as being on");
	}
	
	@Then("^select the amount of time to self exclude the account$")
	public void select_the_amount_of_time_to_self_exclude_the_account(){
		objResponsibleGamblingPage.selectSelfExcludePeriod("1 Year");
	}
	
	@Then("^Click on 'Yes, I want to Self Exclude' button$")
	public void click_on_Yes_I_want_to_Self_Exclude_button() {
		objResponsibleGamblingPage.clickWantToSelfExcludeBtn();
	}
	
	@Then("^Check that the confirmation pop up is displayed with Cancel and Self exclude and log out CTA$")
	public void check_that_the_confirmation_pop_up_is_displayed_with_Cancel_and_Self_exclude_and_logout_CTA() {
		objResponsibleGamblingPage.verifyExclusionConfirmationPopupDisplayed();
		objResponsibleGamblingPage.verifyCancelBtnOnPopupDisplayed();
		objResponsibleGamblingPage.verifySelfExcludeNLogOutBtnOnPopup();
		objResponsibleGamblingPage.validateExclusionPeriod("1 Year");
	}
	
	@Then("^Select 'Self exclude and log out CTA' button$")
	public void Select_Self_exclude_and_logout_CTA_Sbutton() {
		objResponsibleGamblingPage.clickSelfExcludeNLogOutBtn();
	}
	
	@Then("^Accept Self exclusion complete pop up$")
	public void Accept_Self_exclusion_complete_pop_up() {
		objResponsibleGamblingPage.verifySelfExclusionCompletePopup();
		objResponsibleGamblingPage.clickOKonPopup();
	}

	@Then("^Verify Take a break info text$")
	public void Verify_Take_a_break_info_text() {
		objResponsibleGamblingPage.verifyInfoMessages("• The break will take effect immediately.");
		objResponsibleGamblingPage.verifyInfoMessages("• You will not be able to access your account.");
		objResponsibleGamblingPage.verifyInfoMessages("• Marketing emails will stop within 24 hours.");
		objResponsibleGamblingPage.verifyInfoMessages("You can request any other length of period by contacting our Customer Service team.");
		objResponsibleGamblingPage.verifyInfoMessages("• Your break will end automatically once the selected time period has passed");
	}
	
	@Then("^Verify self exclusion info text$")
	public void Verify_self_exclusion_info_text() {
		objResponsibleGamblingPage.verifyWhySelfExcludeTXT();
		objResponsibleGamblingPage.verifyHowLongDoUWantToLockACTxt();
		objResponsibleGamblingPage.verifySelfExclInfoMessages("All of your accounts on our other existing websites will also be locked immediately~It will not be possible to unlock your account prior to the time period~Once the selected period has passed, your account will remain deactivated~Marketing communications will stop within 24 hours~We recommend that you self-exclude from any other gambling operators’ websites that you may use~You accept that if you open a new account whilst self-excluded, you are personally responsible for any losses you incur and not entitled to any winnings.");
	}
	
	@Then("^'Do you feel you have a problem with gambling\\?' screen displayed with Yes and No option$")
	public void do_you_feel_you_have_a_problem_with_gambling_screen_displayed_with_Yes_and_No_option() {
		objResponsibleGamblingPage.verifyDoUFeelProblemWithGamblingTXT();
		objResponsibleGamblingPage.verifybtnYesSelfExcludeDisplayed();
		objResponsibleGamblingPage.verifybtnNoSelfExcludeDisplayed();
		objResponsibleGamblingPage.clickYesbtn();
	}
}
