Feature: Bonuses

@Android
Scenario: Check whether system displays an error message on the screen if the bonus code is incorrect.
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Enter bonus code" tab from my account screen
Then Enter invalid bonus code
Then Click on submit
Then Verify "Incorrect Bonus Code" error message

@Android
Scenario: Bonus Claim_Check that system allows to claim the bonus using Bonus code for the user
Given Invoke the Bella site
Then Invoke the Bede Admin portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players using username
Then Select "Promo Groups" submenu from "Promotions" menu
Then Add player id in "bella-staging-demo1" Promo group
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Enter bonus code" tab from my account screen
Then Enter bonus code as "test1234"
And Click on submit
Then Promotion details will display to user
Then Select the T & C option and click on Claim button
Then Bonus success screen will display to the user

@Android
Scenario: Bonus Claim_Check that system displays the claimed bonus details in bonus history tab
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Bonus History" tab from my account screen
Then Verify Claimed bonus details are displayed on screen


@Android
Scenario: Check whether the expanded view contains further details of each bonuses (activation, expiry date, a link to the T&Cs)
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
#Then User enters username
#And User enters password
Then User enters username "fv3maQI"
And User enters password "Redfox01"
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Enter bonus code" tab from my account screen
Then Enter bonus code as "test1234"
And Click on submit
Then Promotion details will display to user
Then Select the T & C option and click on Claim button
Then Bonus success screen will display to the user
Then Click on My Account
Then Navigate to "Active Bonuses" tab from my account screen
Then Click on + button of any claimed bonus
Then Verify following details should be displayed on Expanded view
|Type|
|Bonus status|
|Amount|
|Wagering target|
|Progress|
|Activation|
|Bonus status|
|Expiry|
|Opt out link|
|View T&Cs link|

@Android
Scenario: Check that system allows to expand & Collapsed each active bonus for the user
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Active Bonuses" tab from my account screen
Then Click on + button of any claimed bonus
Then System should expand bonus details
Then Click on - button of any claimed bonus
Then System should collapse the bonus details

@Android
Scenario: Bonus History _Check that the customer can filter the Bonus history based on bonus type
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Bonus History" tab from my account screen
Then Verify following options are displayed under bonus type filter dropdown
|All types|
|First Deposit|
|Deposit|
|Instant|
|Player Registration|
|Card Registration|
|Deposit Over Period|
|Cashback On Total Stake| 
|Cashback On Net Losses|
|Manual Adjustment|
|Opt In|
|Content|
|Buy In|
Then Select type as "Instant" from bonus type filter
Then Verify filter results are displayed as per selected bonus type
#Then Select type as "Opt In" from bonus type filter
#Then Verify filter results are displayed as per selected bonus type

@Android
Scenario: Bonus History_Check that the customer can filter the Bonus history based on date range
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
#Then User enters username "fv3maQI"
#And User enters password "Redfox01"
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Bonus History" tab from my account screen
Then Change the bonus Date and click on Filter button
Then Verify Bonus history is displayed as per selected bonus date

@Android
Scenario: Check that customer can Opt out from the Opted In promotion from Bonus history
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then Verify Login Page is displayed
Then User enters username
And User enters password
#Then User enters username "fv3maQI"
#And User enters password "Redfox01"
Then User clicks on login button
Then Verify user is successfully logged in
Then Click on My Account
Then Navigate to "Bonus History" tab from my account screen
Then Select bonus type as "Instant" from filter
Then Verify filter results are displayed as per selected bonus type
Then Click on + button of any claimed bonus
Then Verify following details should be displayed on Expanded view
|Type|
|Bonus status|
|Amount|
|Wagering target|
|Progress|
|Activation|
|Bonus status|
|Expiry|
|Opt out link|
|View T&Cs link|
Then Click on Opt Out link
Then Accept confirmation pop up
Then Verify that Bonus status displayed as "Cancelled"