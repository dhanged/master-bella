package com.bella.mobile.stepDefinition;

import org.apache.commons.lang.RandomStringUtils;

import com.bella.mobile.page.PAM.PAM_PlayerManagementPage;

import cucumber.api.java.en.Then;

public class PAMPlayerManagementPageStep {

	PAM_PlayerManagementPage objPAM_PlayerManagementPage;
	String messagSub;
	
	
	public PAMPlayerManagementPageStep(PAM_PlayerManagementPage objPAM_PlayerManagementPage) {		
		this.objPAM_PlayerManagementPage = objPAM_PlayerManagementPage;

	}
	@Then("^Search player \"([^\"]*)\"$")
	public void search_player(String player) {
		objPAM_PlayerManagementPage.setPlayerSearch(player);
		objPAM_PlayerManagementPage.clickSearch();
	}

	@Then("^Create new message for player$")
	public void Create_new_message_for_player() {
		// Write code here that turns the phrase above into concrete actions
		objPAM_PlayerManagementPage.selectPraimaryPlayerManagementTab("Messages");
		objPAM_PlayerManagementPage.ClickOnNewMessageButton();
		objPAM_PlayerManagementPage.setSenderName("SQS Automation");
		objPAM_PlayerManagementPage.setMessageTitle("Auto_Test message");
		objPAM_PlayerManagementPage.setMessageText("Test purpose only");
		objPAM_PlayerManagementPage.clickOnCreateButton();
		objPAM_PlayerManagementPage.clickOnConfirm();
	}

	@Then("^Navigate to Player Messanger$")
	public void navigate_to_Player_Messanger(){
		// Write code here that turns the phrase above into concrete actions
		objPAM_PlayerManagementPage.navigateToPlayerMessenger();
	}

	@Then("^Click on create new message$")
	public void click_on_create_new_message(){
		// Write code here that turns the phrase above into concrete actions
		objPAM_PlayerManagementPage.clickOnNewMessage();
	}

	@Then("^configure the message$")
	public void configure_the_message(){
		// Write code here that turns the phrase above into concrete actions

		objPAM_PlayerManagementPage.setMessageInternalName("SQS_Test SendAll Message");
		objPAM_PlayerManagementPage.setFrom("Expleo_Automation");
		
		messagSub = "AutoTestMessage_"+RandomStringUtils.randomAlphanumeric(2);

		objPAM_PlayerManagementPage.setSubject(messagSub);
		objPAM_PlayerManagementPage.setMessageBody("Test purpose only ");
		objPAM_PlayerManagementPage.clickOnInsertImage();
		objPAM_PlayerManagementPage.setinsertImageField("Source", "C:\\Users\\bhadv\\Desktop\\testimage.png");
		objPAM_PlayerManagementPage.setimageDescription("Test");
		objPAM_PlayerManagementPage.insertDimensions("600", "222");
		objPAM_PlayerManagementPage.clickOnOk();


		

		objPAM_PlayerManagementPage.includeFooterButton();
		objPAM_PlayerManagementPage.addFooterButton("Play now", "https://np03-bell-cms.rankgrouptech.net/");
		objPAM_PlayerManagementPage.selectSite("bellacasino.com");
		objPAM_PlayerManagementPage.selectTimeZone("United Kingdom Time - London");
		objPAM_PlayerManagementPage.setSendTime();
		objPAM_PlayerManagementPage.clickOnSaveSchedule();
		objPAM_PlayerManagementPage.verifyMessageCreatedSuccessfully(messagSub);
	}


}
