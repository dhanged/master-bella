package com.generic.utils;
 
/**
 * Command Prompt - this class contains method to run windows  
 * @Author        : harshvardhan yadav (SQS)
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandPrompt 
{
	// Local Variables
	static Process process;
	static ProcessBuilder builder;
 
	public static String runCommand(String command)  
	{
		String allLine="";
		try
		{
			String os = System.getProperty("os.name");
			//System.out.println(os);

			// build cmd proccess according to os
			if(os.contains("Windows")) // if windows
			{
				builder = new ProcessBuilder("cmd.exe","/c", command);
				builder.redirectErrorStream(true);
				Thread.sleep(1000);
				process = builder.start();
			}
		 
			// get std output
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = "";
			while((line = buffReader.readLine()) != null)
			{
				allLine = allLine + "" + line + "\n";
				if(line.contains("Console LogLevel: debug"))
					break;
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return allLine;
	}
}

