package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class LiveCasinoPage {
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private DriverProvider objDriverProvider;

	public LiveCasinoPage(MobileActions objMobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,DriverProvider driverProvider) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
		this.objDriverProvider = driverProvider;
	}
	//xpath 
	private By liveCasinoTopPicks = By.xpath("//game-panel-block-container[@title='Top Picks']//game-panel[1]");
	private By liveCasinoRoulette = By.xpath("//game-panel-block-container[@title='Roulette']//game-panel[1]");
	private By liveCasinoBlackjack = By.xpath("//game-panel-block-container[@title='Blackjack']//game-panel[1]");
	private By btnLiveCasinoRouletteInfo = By.xpath("//game-panel-block-container[@title='Roulette']//game-panel[1]//img[@src='/assets/grosvenor/img/info.jpg']");
	
	private By pageGameDetails = By.xpath("//div[@class='details-games']");
	private By btnJoin = By.xpath("//div[contains(@class,'details-button')]//button[contains(.,'Join')]");
		
	
	public void verifyTopPicksGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Top Picks games Section", 
				objMobileActions.checkElementDisplayed(liveCasinoTopPicks));
		
	}
	public void verifyRouletteGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Roulette Games section", 
				objMobileActions.checkElementDisplayed(liveCasinoRoulette));
		
	}
	public void verifyBlackjackGamesDisplayed() {
		logReporter.log("Verify Games dislpaying in Blackjack games option", 
				objMobileActions.checkElementDisplayed(liveCasinoBlackjack));
		
	}
	
	public void verifyRouletteGameInfoButtonDisplayed() {
		logReporter.log("Mouse Hover", 
				objMobileActions.performMouseHover(liveCasinoRoulette));
		logReporter.log("Verify Info button for Roulette Games displayed", 
				objMobileActions.checkElementDisplayed(btnLiveCasinoRouletteInfo));
		
	}
	
	public void clickRouletteGameInfoButton() {
		logReporter.log("Mouse Hover", 
				objMobileActions.performMouseHover(liveCasinoRoulette));
		logReporter.log("Click Info button for Roulette Games", 
				objMobileActions.click(btnLiveCasinoRouletteInfo));
		
	}
	
	public void verifyGameDetailsPageDisplayed() {
		logReporter.log("Verify game Details page displayed", 
				objMobileActions.checkElementDisplayed(pageGameDetails));
		
	}
	
	public void verifyJoinButtonDisplayed() {
		logReporter.log("Verify Join Button on game Details page displayed", 
				objMobileActions.checkElementDisplayed(btnJoin));
		
	}
}
