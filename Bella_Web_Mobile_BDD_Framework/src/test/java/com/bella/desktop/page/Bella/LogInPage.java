/**
 * 
 */
package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

/**
 * @author vaishali bhad
 *
 */
public class LogInPage {

	private By hdrLogin = By.xpath("//span[@class='heading ' and text()='Username Login']");
	private By hdrUsernameReminder = By.xpath("//span[text()='Username Reminder']");
	private By hdrMembershipNumberReminder = By.xpath("//span[text()='Membership number reminder']");
	private By hdrResetPassword = By.xpath("//span[text()='Reset Password']");
	private By hdrResetPIN = By.xpath("//span[text()='Reset PIN']");
	private By hdrYouHaveMail = By.xpath("//h4[text()='You have mail']");
	private By chkboxRemeberUserName = By.xpath("//input[@id='checkbox-remember']//following::label[text()='Remember Username']");
	private By btnPasswordShow = By.xpath("//input[@placeholder=\"Password\"]//following::button[text()='Show']");
	private By lnkLiveHelp = By.xpath("//div[@class='click-to-chat ']//div[@class='message-icon']//following-sibling::div//div[text()='Live Help']");

	private By btnSignUp = By.xpath("//P[text()='New player?']//following::button[text()='Sign Up']");
	private By btnclose = By.xpath("//button[@class='close']");
	private By btnLogin = By.xpath("//button[contains(@class,'loginBtn') and text()='Login']");
	private By btnSendUsernameReminder = By.xpath("//button[text()='Send username reminder']");
	private By btnSubmitForResetPassword = By.xpath("//button[@type='submit' and text()='Submit']");
	private By btnSubmitForMembershipNumberReminder = By.xpath("//button[@type='submit' and text()='Submit']");
	private By btnResetPasswordForPasswordReset = By.xpath("//button[@type='submit' and text()='Reset Password']");
	private By btnRequestANewPIN = By.xpath("//button[@type='submit' and text()='Request a new PIN']");

	private By inpUserName = By.xpath("//input[@id='input-username'][@placeholder='Username']");
	private By inpPassword = By.xpath("//input[@id='input-password'][@placeholder='Password']");
	private By inpUsernameReminderEmailAddress = By.xpath("//input[@id='fu-email']");
	private By inpResetPasswordUserName = By.xpath("//input[@placeholder='Username' and @autocomplete='new-password']");
	private By inpResetPINEmailAddress = By.xpath("//input[@id='login-membership-number']");	
	private By inpNewPasswordForPasswordReset = By.xpath("//input[@id='password' and @placeholder='New Password']");
	private By inpConfirmPasswordForPasswordReset = By.xpath("//input[@id='confirmPassword' and @placeholder='Confirm Password']");
	// Link
	private By lnkForgottenUserName = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Username']");
	private By lnkForgottenPassword = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Password']");
	private By lnkForgottenPIN = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='PIN']");
	private By lnkForgottenMembershipNumber = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Membership Number']");
	private By lnkLogout = By.xpath("//a[@class='logout-link' and text()='Logout']");


	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public LogInPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void verifyLoginPopupDisplayed() { 
		logReporter.log("Verify 'Login' popup displayed", 
				objWebActions.checkElementDisplayed(hdrLogin));
	}

	public void verifyLiveHelpLinkDisplayed() { 
		logReporter.log("Verify 'Live Help' link displayed", 
				objWebActions.checkElementDisplayed(lnkLiveHelp));
	}

	public void clickLiveHelpLinkDisplayed() { 
		logReporter.log("click 'Live Help' link displayed", 
				objWebActions.click(lnkLiveHelp));
	}
	public void verifyUserNameDisplayed() { 
		logReporter.log("Verify 'Username' Field is displayed", 
				objWebActions.checkElementDisplayed(inpUserName));
	}
	public void verifyPasswordDisplayed() { 
		logReporter.log("Verify 'Password' Field is displayed", 
				objWebActions.checkElementDisplayed(inpPassword));
	}


	public void setUserName(String userName) {
		logReporter.log("Set username ", userName,
				objWebActions.setTextWithClear(inpUserName, userName));
	}

	public void setPassword(String password) {
		logReporter.log("Set password ", password,
				objWebActions.setTextWithClear(inpPassword, password));
	}

	public void verifyRemeberUserNameCheckboxDisplayed() { 
		logReporter.log("Verify 'Remeber UserName ' checkbox is displayed", 
				objWebActions.checkElementDisplayed(chkboxRemeberUserName));
	}
	public void SelectRemeberUserNameCheckbox() { 
		logReporter.log("select 'Remeber UserName ' checkbox ", 
				objWebActions.click(chkboxRemeberUserName));
	}

	public void verifyRemberUserNameCheckboxIsSelectedOrNot()
	{
		By chkboxRemeberUsername = By.id("checkbox-remember");
		logReporter.log("verify 'Remeber UserName ' checkbox is selected ", 
				objDriverProvider.getWebDriver().findElement(chkboxRemeberUsername).isSelected());
	}

	public void verifyShowButtonDisplayedInPasswordField() { 
		logReporter.log("Verify 'show ' button is displayed In PasswordField", 
				objWebActions.checkElementDisplayed(btnPasswordShow));
	}

	public void clickShowButtonDisplayedInPasswordField() { 
		logReporter.log("click on 'show ' button is displayed In PasswordField", 
				objWebActions.click(btnPasswordShow));
	}
	public void verifyPasswordIsDisplayedInTextForm() { 
		By locator = By.xpath("//input[@id='input-password'][@type='text']");
		logReporter.log("Verify password display in text form ", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyTextFromUserNameField(String userNm)
	{
		By locator = By.xpath("//input[@id='input-username'][@value='"+userNm+"']");
		logReporter.log("verify ' "+userNm+ " ' is autopopulated in Username field", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyNewPlayerTextIsDisplayed()
	{
		By locator = By.xpath("//P[text()='New player?']");
		logReporter.log("Verify 'New player? ' text is displayed ", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifySignUPButtonDisplayed() { 
		logReporter.log("Verify 'New player? with sign up ' button is displayed ", 
				objWebActions.checkElementDisplayed(btnSignUp));
	}
	public void clickSignUPButtonDisplayed() { 
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		logReporter.log("click 'New customer with sign up '", 
				objWebActions.click(btnSignUp));
	}

	public void verifyLogInCTA()
	{
		logReporter.log("Verify 'Log In' cta on login screen", 
				objWebActions.checkElementDisplayed(btnLogin));
	}
	public void clickLoginOnLoginPopup() {

		logReporter.log("Click 'Login' button", 
				objWebActions.click(btnLogin));
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));


	}

	public void verifyUnauthorizedErrorMessage()
	{boolean flag =false;
	do
	{
		flag = verifyErrMessage("Unauthorized");
		System.out.println("************ into " +flag);
		if(flag)
		{
			System.out.println("************ into " +flag);
			clickLoginOnLoginPopup();
			
		}
		
	}while(flag);
	}
	public boolean verifyErrMessage(String msg)
	{
		//	Unauthorized
		By locator = By.xpath("//div[contains(@class,'error-msg')]//li[contains(.,'"+msg+"')]");
		return objWebActions.checkElementElementDisplayedWithMidWait(locator);
	}

	public void verifyUserLoggedInSuccessfully(String userName) {
		/*
		 * By locator =
		 * By.xpath("//div[@class='header']/div[@class='user' and contains(text(),'" +
		 * userName + "')]");
		 * logReporter.log("Verify user logged in successfully.",
		 * userName, objWebActions.checkElementDisplayed(locator));
		 */
		// locator changed because user name displayed is not case sensitive
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));

		By locator =  By.xpath("//div[@class='header']/div[@class='user']");
		logReporter.log("Verify user logged in successfully.", userName, 
				objWebActions.getText(locator).equalsIgnoreCase(userName));
	}

	public void clickCloseXicon()
	{
		logReporter.log("Close the my account ", objWebActions.click(btnclose));
	}


	public void verifyLogoutLnkDisplayed()
	{
		logReporter.log("Verify 'Logout' Link displayed", 
				objWebActions.checkElementDisplayed(lnkLogout));
	}

	public void clickLogoutlnk()
	{
		logReporter.log("Click on 'Logout' Link displayed", 
				objWebActions.click(lnkLogout));
	}


	public void verifyLoginDisabled() {
		By btnLogInDisabled = By.xpath("//button[contains(text(),'Login') and contains(@class,'button-disabled')]");
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(btnLogInDisabled));
	}

	public void verifyCloseXicon() {

		logReporter.log("Check Close X icon displayed", objWebActions.checkElementDisplayed(btnclose));
	}
	public void VerifyForgottenUserNameLink() {
		logReporter.log("Verify 'Forgotten UserName' link on login screen", 
				objWebActions.checkElementDisplayed(lnkForgottenUserName));
	}
	public void VerifyForgottenPasswordLink() {
		logReporter.log("Verify 'Forgotten Password' link on login screen", 
				objWebActions.checkElementDisplayed(lnkForgottenPassword));
	}

	public void clickForgottenUserNameLink() {
		logReporter.log("Click 'Forgotten UserName' link", 
				objWebActions.click(lnkForgottenUserName));
	}

	public void verifyUsernameReminderHeaderDisplayed() {
		logReporter.log("Verify 'Username Reminder' header displayed", 
				objWebActions.checkElementDisplayed(hdrUsernameReminder));
	}

	public void setUsernameReminderEmailAddress(String emailAddress) {
		logReporter.log("Set username remider email address ", emailAddress,
				objWebActions.setText(inpUsernameReminderEmailAddress, emailAddress));
	}

	public void clickSendUsernameReminder() {
		logReporter.log("Click 'Send username reminder' button", 
				objWebActions.click(btnSendUsernameReminder));
	}

	public void verifyYouHaveMailHeaderDisplayed() {
		logReporter.log("Verify 'You have mail' header displayed", 
				objWebActions.checkElementDisplayed(hdrYouHaveMail));
	}

	public void clickForgottenPasswordLink() {
		logReporter.log("Click 'Forgotten Password' link", 
				objWebActions.click(lnkForgottenPassword));
	}

	public void verifyResetPasswordHeaderDisplayed() {
		logReporter.log("Verify 'Reset Password' header displayed", 
				objWebActions.checkElementDisplayed(hdrResetPassword));
	}

	public void setResetPasswordUserName(String userName) {
		logReporter.log("Set reset password user name", userName,
				objWebActions.setTextWithClear(inpResetPasswordUserName, userName));
	}

	public void clickSubmitForResetPassword() {
		logReporter.log("Click 'Submit' button for reset password", 
				objWebActions.click(btnSubmitForResetPassword));
	} 

	public void clickSubmitForMembershipNumberReminder() {
		logReporter.log("Click 'Submit' button for reset password", 
				objWebActions.click(btnSubmitForMembershipNumberReminder));
	}

	public void verifyPasswordResetMailSend() {
		By locator = By.xpath("//p[contains(text(),'Your Password Reset instructions are waiting')]");
		logReporter.log("Verify 'Your Password Reset instructions are waiting in your inbox' message displayed", 
				objWebActions.checkElementDisplayed(locator));
	}

	// Reset Password Link from email
	public void verifyResetPasswordHdrDisplayed()
	{
		By resetPasswordHdr = By.xpath("//h1[text()='Reset Password']");

		logReporter.log("Verify Reset Password header is displayed when link is invoked", 
				objWebActions.checkElementDisplayed(resetPasswordHdr));

	}

	public void setNewPassword(String newpwd)
	{
		By inpnewpwd = By.xpath("//input[@id=\"Password\" and @type=\"password\"]");
		logReporter.log("Set new password as : ", newpwd,
				objWebActions.setText(inpnewpwd, newpwd));

	}
	public void clickSubmitForNewPassword()
	{
		By submitNewPasswordBtn = By.xpath("//button[@type='submit']");

		logReporter.log("Click 'Submit' button for reset password", 
				objWebActions.click(submitNewPasswordBtn));
	}

	public void verifyGoToHomepageBTNDisplayed()
	{
		By locator = By.xpath("//a[@class='btn' and text() ='GO TO THE HOMEPAGE']");

		logReporter.log(" Verify 'GO TO THE HOMEPAGE' button displayed on reset password success screen", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void clickContinueToLoginBtn()
	{
		By ContinueToLoginBtn = By.xpath("//a[@class='btn' and text() ='Continue to login']");

		logReporter.log("Click 'Continue to login' button on reset password", 
				objWebActions.click(ContinueToLoginBtn));
	}


	public void setConfirmPasswordForPasswordReset(String password) {
		logReporter.log("Set confirm password for password reset process", password,
				objWebActions.setText(inpConfirmPasswordForPasswordReset, password));
	}

	public void clickResetPasswordForPasswordReset() {
		logReporter.log("Click 'Reset Password' button for password reset process", 
				objWebActions.click(btnResetPasswordForPasswordReset));
	}

	public void verifyPasswordResetSuccessMessageDisplayed() {

		By locatorYourPasswordReset = By.xpath("//p[contains(text(),'You have successfully reset your password.')]");
		logReporter.log("Verify 'You have successfully reset your password' message displayed", 
				objWebActions.checkElementDisplayed(locatorYourPasswordReset));
	}

	public void clickForgottenMembershipNumberLink() {
		logReporter.log("Click 'Forgotten Membership Number' link", 
				objWebActions.click(lnkForgottenMembershipNumber));
	}

	public void verifyMembershipNumberReminderHeaderDisplayed() {
		logReporter.log("Verify 'Membership number reminder' header displayed", 
				objWebActions.checkElementDisplayed(hdrMembershipNumberReminder));
	}

	public void clickForgottenPINLink() {
		logReporter.log("Click 'Forgotten PIN' link", 
				objWebActions.click(lnkForgottenPIN));
	}

	public void verifyResetPINHeaderDisplayed() {
		logReporter.log("Verify 'Reset PIN' header displayed", 
				objWebActions.checkElementDisplayed(hdrResetPIN));
	}

	public void setResetPINEmailAddress(String emailAddress) {
		logReporter.log("Set reset PIN email address", emailAddress,
				objWebActions.setText(inpResetPINEmailAddress, emailAddress));
	}

	public void clickRequestANewPIN() {
		logReporter.log("Click 'Request a new PIN' button", 
				objWebActions.click(btnRequestANewPIN));
	}


	public void verifyResetPINForUseInGrosvenorCasinosHeaderDisplayed() {
		By locator = By.xpath("//h1[contains(text(),'Reset PIN for use in Grosvenor Casinos')]");
		logReporter.log("Verify 'Reset PIN for use in Grosvenor Casinos' header displayed.", 
				objWebActions.checkElementDisplayed(locator));
	}


	public void setNewPINForPasswordPIN(String pin) {
		By locator = By.xpath("//input[@type='password' and @id='pin']");
		logReporter.log("Set new PIN for PIN reset process", pin,
				objWebActions.setText(locator, pin));
	}


	public void setConfirmPINForPasswordPIN(String pin) {
		By locator = By.xpath("//input[@type='password' and @id='confirmPin']");
		logReporter.log("Set confirm PIN for PIN reset process", pin,
				objWebActions.setText(locator, pin));
	}

	public void clickSubmitForPINReset() {
		By locator = By.xpath("//button[@type='submit' and text()='Submit']");
		logReporter.log("Click 'Submit' button for PIN reset process", 
				objWebActions.click(locator));
	}

	public void verifyPINResetSuccessMessageDisplayed() {
		By locatorSuccess = By.xpath("//h1[text()='Success']");
		By locatorYourPasswordReset = By.xpath("//p[contains(text(),'Your in-club PIN is now ready to be used')]");
		logReporter.log("Verify 'Your in-club PIN is now ready to be used' message displayed", 
				(objWebActions.checkElementDisplayed(locatorSuccess) &&
						objWebActions.checkElementDisplayed(locatorYourPasswordReset)));
	}

	public void setUserNameFromConfig() {
		String userName = configuration.getConfig("web.userName");
		logReporter.log("Set username ", userName,
				objWebActions.setTextWithClear(inpUserName, userName));}

	public void setPasswordFromConfig() {
		String password = configuration.getConfig("web.password");
		logReporter.log("Set password ", password,
				objWebActions.setTextWithClear(inpPassword, password));}

	public void verifyArticleBlockHeaders(String hdrs)
	{
		String[] arrSingleEntry = hdrs.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(";");
			By locator = By.xpath("//"+entry[0]+"[contains(.,'"+entry[1]+"')]");	
			logReporter.log(" Verify '"+entry[1]+"' ",
					objWebActions.checkElementDisplayed(locator));	}
	}

}
