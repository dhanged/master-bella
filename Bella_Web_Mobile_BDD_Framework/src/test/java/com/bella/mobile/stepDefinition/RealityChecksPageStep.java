package com.bella.mobile.stepDefinition;

import java.util.List;

import com.bella.mobile.page.Bella.HomePage;
import com.bella.mobile.page.Bella.MyAccountPage;
import com.bella.mobile.page.Bella.ResponsibleGamblingPage;
import com.generic.WebActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class RealityChecksPageStep {
	private ResponsibleGamblingPage objResponsibleGamblingPage;
	private Utilities utilities;	
	private WebActions objWebActions;
	private Configuration configuration;
	private HomePage objHomePage;
	String userName ,password,emailAddress,reminderoption;
	private MyAccountPage objMyAccountPage;
	
	public RealityChecksPageStep(ResponsibleGamblingPage objResponsibleGamblingPage,Utilities utilities,WebActions objWebActions,Configuration configuration,HomePage objHomePage,MyAccountPage objMyAccountPage) {		
		this.objResponsibleGamblingPage = objResponsibleGamblingPage;
		this.utilities = utilities;
		this.objWebActions = objWebActions;
		this.configuration = configuration;
		this.objHomePage =objHomePage;
		this.objMyAccountPage = objMyAccountPage ;
	}

	@Then("^Verify infotext on Reality checks screen$")
	public void verify_infotext_messge_on_Reality_check_screen()
	{
		objResponsibleGamblingPage.verifyRealityChecksInfoMessages("Reality Checks are a helpful way of keeping track of the time you have spent playing our games.");
		objResponsibleGamblingPage.verifyRealityChecksInfoMessages("You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval");
		objResponsibleGamblingPage.verifyRealityChecksInfoMessages("Your gaming session will begin when you place your first real money bet and will end when you log out.");
		objResponsibleGamblingPage.verifyTellMeMorelinkDisplayed();
		objResponsibleGamblingPage.verifyTellMeMoreExpandBtnDisplayed();
		objResponsibleGamblingPage.clickTellMeMoreExpandBtn();
		objResponsibleGamblingPage.clickTellMeMoreCollapseBtn();
	}

	@Then("^Verify following reminder options$")
	public void verify_following_reminder_options(DataTable dt)  {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objResponsibleGamblingPage.verifyReminderOptionsDispalyed((list.get(i)));
		}
	}
	
	@And("^Click on 'Save a changes' button$")
	public void click_on_Save_a_changes_button() {
		objResponsibleGamblingPage.clickSaveChangesBTN();
		objResponsibleGamblingPage.verifySavedBtnDisplayed();
	}
	
	 
	@Then("^Verify updated option displayed correctly$")
	public void verify_updated_option_displayed_correctly() {
		objMyAccountPage.clickBackBTN();
		objMyAccountPage.clickBackBTN();
		objMyAccountPage.selectMyAccountMenu("Responsible Gambling","Reality Check");
		objResponsibleGamblingPage.verifyReminderperiodSaved(reminderoption);
	}
	
	@Then("^Select reminder option$")
	public void select_reminder_option() 
	{
		reminderoption = objResponsibleGamblingPage.getRandomReminderForUser();
		System.out.println("***********************   "+reminderoption);
		objResponsibleGamblingPage.SelectReminderperiod(reminderoption);
	}
}
