Feature: TakeABreak

@Android
Scenario: To verify that player is able to set take a break from Responsible gaming.
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Take a break" tab from my account screen
Then Verify infotext messge on take a break screen
And Verify following break periods
|1 Day|
|2 Days|
|1 Week|
|2 Weeks|
|4 Weeks|
|6 Weeks|
Then Select break period as "1 Day"
Then Verify date is displayed on the screen until the account will be locked
Then Verify Take a break info text
Then Keep password field blank 
And Click on 'Take a break' button
Then Verify "Please enter your password" error message
Then Enter invalid password as "dsf"
Then Click on 'Take a break' button
Then Verify "Password Error" error message
Then Enter valid password and hit 'Take a break' button
Then Check that the confirmation pop up is displayed with Cancel and Confirm CTA
Then Select 'cancel' option and Verify user navigate back to Take a break page
Then Click on 'Take a break' button
And Select Confirm button
Then Verify user get logged out 
And User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
And  Verify error message 'Your account is currently locked as being on ''Take a Break''. Please contact support.'
Then Invoke the Bede Admin portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
And Verify Player Account status is displayed as "Take a Break"
Then Verify Take a break end date is displayed correctly
Then Remove break period 
And User clicks on login button
Then Verify user is successfully logged in