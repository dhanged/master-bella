package com.bella.desktop.page.Bella;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class AccountDetailsPage {
	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public AccountDetailsPage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
	}

	By logoutLink = By.xpath("//a[@class='logout-link']");
	By lastLoginLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]");
	By NameLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Name')]");
	By EmailLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Email')]");
	By DOBLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Date of birth')]");
	By PhoneLabel =By.xpath("//span[@class='list-detail' and contains(text(),'Phone')]");
	By PostcodeLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Postcode')]");
	By EditLink = By.xpath("//a[@class='edit-details']");
	By NonEditableTXT = By.xpath("//span[@class='non-editable']");
	By editEmail = By.xpath("//input[@id='edit-email']");
	By editPhone = By.xpath("//input[@id='edit-phone']");
	By SearchBtn = By.xpath("//button[@class='search__open-btn']");
	By inpSearchTxt = By.xpath("//input[@class='search-bar__input icon-search-white' and @placeholder='Search for a game']");

	By PlayBtnOnSearchResults = By.xpath("//button[@class='search-results__play-btn btn']");
	By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
	By browseSectionBTN = By.xpath("//button[@class='search-no-results__browse-btn' and contains(text(),'Browse sections')]");
	By GameInfoOnSearchResults = By.xpath("//a[@class='search-results__info']/i[@class='search-results__info-icon']");

	public void clickAccountDetail()
	{
		By locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'Account Details')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Account details ",  
				objWebActions.click(locator));
	}

	public void clickChangePassword()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Change Password')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Change Password  ",  
				objWebActions.click(locator));
	}

	public void clickMarketingPreferences()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Marketing Preferences')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Marketing Preferences",  
				objWebActions.click(locator));

		By header = By.xpath("//span[@class='h2 '][contains(text(),'Marketing Preferences')]");
		logReporter.log(" Verify Marketing Preferences Header displayed ",  
				objWebActions.checkElementDisplayed(header));
	}

	//update Marketing Preferences

	public void verifyMarketingInfoText()
	{
		By infotxt = By.xpath("//fieldset[@class='change_password_marketing_preferences']//p[contains(text(),'How do you prefer to be kept up to date with awesome offers, prime promos and fun freebies on Bella')]");

		logReporter.log(" Verify 'How do you prefer to be kept up to date with awesome offers, prime promos and fun freebies on Bella ...' Info message displayed ",  
				objWebActions.checkElementDisplayed(infotxt));
	}


	public void updateMarketingPreferences(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				logReporter.log(" Click on Marketing Preferences: " +pref1,  
						objWebActions.click(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			logReporter.log(" Click on Marketing Preferences" +pref,  
					objWebActions.click(locator));
		}
	}

	public void verifyMarketingPreferencesCheckboxesIsSelectedOrNot(String pref) 
	{
		if(pref.contains("~")){
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) {
				By locator = By.xpath("//input[contains(@id,'"+pref1 +"')][@type='checkbox']");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objWebActions.isCheckBoxSelected(locator));}}
		else{
			By locator = By.xpath("//input[contains(@id,'"+pref+"')][@type='checkbox']");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objWebActions.isCheckBoxSelected(locator));}
	}

	public void verifyMarketingPreferencesOption(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objWebActions.checkElementDisplayed(locator));
		}
	}
	public void clickUpdatePreferencesBtn()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");

		logReporter.log(" Click on Update button",  
				objWebActions.click(locator));

	}
	//Change Password

	public void setCurrentPassword(String currpassword)
	{
		By locator= By.xpath("//input[@id='edit-current-password']");
		logReporter.log(" Set on Current Password  ",  
				objWebActions.setText(locator, currpassword));

	}
	public void setNewPassword(String newPwd)
	{
		By locator= By.xpath("//input[@id='edit-new-password']");
		logReporter.log(" Set New Password: "+newPwd,  
				objWebActions.setText(locator, newPwd));
	}

	public void clickUpdatePasswordBTN()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");
		logReporter.log(" Click on Update button to change password   ",  
				objWebActions.click(locator));

	}


	public void verifyLabels(String labels )
	{
		if(labels.contains(","))
		{
			String labl[] = labels.split(",");
			for (String labl1 : labl)
			{
				By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labl1+"')]");
				//System.out.println(" Label  " +labl1);
				logReporter.log(" Verify "+labels+"  is displayed",
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labels+"')]");
			//System.out.println(" Label  " +labl1);
			logReporter.log(" Verify "+labels+"  is displayed",
					objWebActions.checkElementDisplayed(locator));
		}
		//}
	}

	public String getLastLoginFormat()
	{
		By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");
		return objWebActions.getText(lastLoginDate);
	}


	public void verifyEditlinkDisplayed()
	{
		logReporter.log(" Verify Edit Link is displayed",
				objWebActions.checkElementDisplayed(EditLink));
	}

	public void clickEditLink()
	{
		logReporter.log(" Click on Edit Link is displayed",
				objWebActions.click(EditLink));
	}

	public void verifyNonEditableTXTDisplayed()
	{
		logReporter.log(" Verify Non Editable TXT is displayed",
				objWebActions.checkElementDisplayed(NonEditableTXT));
	}

	public void clickClearBtn()
	{
		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objWebActions.click(clearBtn));

	}
	public void EditEmail()
	{
		String newEmail = objUtilities.getRandomAlphanumericEmailString(10, "@mailinator.com");

		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objWebActions.click(clearBtn));

		logReporter.log(" Set Email ",  
				objWebActions.setText(editEmail, newEmail));
	}

	public void EditPhoneNumber(String newMobile)
	{
		//String newMobile ="077009" + objUtilities.getRandomNumeric(5);
		By clearBtn = By.xpath("//input[@id='edit-phone']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objWebActions.click(clearBtn));

		logReporter.log(" Set Mobile number ",  
				objWebActions.setText(editPhone, newMobile));
	}

	public void inputPassword(String password)
	{
		By inputPassword = By.xpath("//input[@id='edit-password']");
		logReporter.log(" Enter your password to update",  
				objWebActions.setText(inputPassword, password));
	}

	public void showPasswordBTNdisplayed()
	{
		By showBTN = By.xpath("//button[contains(text(),'Show')]");

		logReporter.log(" Verify Show button is displayed for Password ",
				objWebActions.checkElementDisplayed(showBTN));

		logReporter.log(" click on Show button is displayed for Password ",
				objWebActions.click(showBTN));

	}


	public void clickUpdateBtn()
	{
		By updateButton = By.xpath("//fieldset[contains(@class,'form-buttons ')]/div/button[contains(text(),'Update')]");
		logReporter.log(" Click on Update button  ",  
				objWebActions.click(updateButton));
	}

	public void verifyAccountUpdateSucsessMSGdisplayed(String msg)
	{
		By updateMsg = By.xpath("//div[@class='myaccount-success' and contains(text(),'"+msg+"')]");

		logReporter.log(" Verify Account details updated is displayed",
				objWebActions.checkElementDisplayed(updateMsg));

	}

	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");
		logReporter.log(" click on ' X' Button'displayed on Bonuse History screen ", 
				objWebActions.click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");

		logReporter.log(" click on logout-link displayed  ", 
				objWebActions.click(logoutLink));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

	}

	public void validateLastLoginFormatDisplayed(String lastLoginTime)
	{
		/*By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");
		String lastLogin =objWebActions.getText(lastLoginDate);
		 */
		/*if(lastLogin.equalsIgnoreCase("Never"))
		{
			logReporter.log("last login date displayed as: "+lastLogin , true);
		}
		else*/
		{
			lastLoginTime=lastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
			DateFormat lastFormat = new SimpleDateFormat("dd MMM yyyy, hh:mm:ss a");
			try
			{
				Date lastlogindate = lastFormat.parse(lastLoginTime);
				logReporter.log("last login date: "+lastLoginTime+" displayed in correct format" , true);

			}catch(Exception e)
			{
				logReporter.log("last login date :"+lastLoginTime+" displayed in correct format" , false);
			}
		}

	}

	public void verifyThatLastLoginTimeIsDisplayedCorrectly(String expectedLoginTime)
	{
		try {
			String CurrentlastLoginTime = this.getLastLoginFormat();
			CurrentlastLoginTime = CurrentlastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
			expectedLoginTime = expectedLoginTime.substring(0,expectedLoginTime.lastIndexOf(":"));
			CurrentlastLoginTime = CurrentlastLoginTime.substring(0,CurrentlastLoginTime.lastIndexOf(":"));
			DateFormat lastFormat = new SimpleDateFormat("dd MMMM yyyy, hh:mm");
			Date lastlogindate = lastFormat.parse(CurrentlastLoginTime);
			CurrentlastLoginTime = lastFormat.format(lastlogindate);
			System.out.println(" expectedLoginTime ::: "+expectedLoginTime+ "  CurrentlastLoginTime :: "+CurrentlastLoginTime);
			logReporter.log("last login Time is displayed correctly" , CurrentlastLoginTime.contains(expectedLoginTime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
