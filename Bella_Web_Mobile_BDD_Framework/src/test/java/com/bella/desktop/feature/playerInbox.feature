Feature: Player Inbox 

@Desktop
Scenario: Check whether system displays notification on the ‘My Account’ start screen if there is any new message in the inbox
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
#Then User enters username "ZqEl80C1"
#And User enters password "6hg3R1Qo"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Get current message count from my account screen
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "ZqEl80C1"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "expleo test message"
And Schedule Message
Then Back to Bell site
Then Verify system displays notification on the My Account screen for new message in the inbox

@Desktop
Scenario: Check whether user can see a clear visual cue on the ‘Messages’ list screen if a message is unread
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then verify unread message icon is display for unread message

@Desktop
Scenario: Check whether system displays all messages on the screen
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify user receive the message

@Desktop
Scenario: Check whether system displays latest message on top of the screen
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify latest message display on top


@Desktop
Scenario: Check whether unread message changes to read, when user reads the message
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Click on unread Message from list of message
Then Verify message subject,sender name and message content is display on screen
Then Navigate back to screen
Then verify unread message icon change into read icon

@Desktop
Scenario: Check whether user can open any message displayed on the ‘Messages’ list screen
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Open Message from list of message

@Desktop
Scenario: Check whether system deletes the selected message from message list
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Verify Delete icon display in disable mode
Then select message to delete
Then click on Delete icon
Then verify selected message gets delete

@Desktop
Scenario: Check whether system displays option empty inbox when user needs to delete all messages
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify 'Your inbox is empty' message is displayed when user delete all message


@Desktop
Scenario: Check that  content of the messages can be made up of images, text and CTAs
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "ZqEl80C1"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "Bella Casino Test"
And Schedule Message
Then Back to Bell site
Then Click on Message option
Then verify user receive configured message
Then Open Message from list of message
Then verify Content of the messages like images, text and CTAs