package com.generic.appiumDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.logging.LogEntries;

import com.generic.utils.Configuration;

import io.appium.java_client.AppiumDriver;
/**
 * used to initiate selenium webdriver for automation  
 * @author Harshvardhan Yadav(Expleo)
 *  @modified 	  : Harhvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class AppiumDriverProvider extends AppiumDriverFactory 
{
	
	By btnCookieContinue = By.xpath("//button[contains(text(),'Continue')]");
	// Local variables
	private AppiumDriver<?> appiumDriver;
	private LogEntries appiumLogEntries;
	
	/**
	 *  initialize mobile appium driver for automation
	 */
	public void initializeConsoleBaseAppium(String mobileEnvironment, Configuration objConfiguration, String appiumServerIP, String appiumServerPort) throws Exception
	{
		switch (mobileEnvironment.toLowerCase())
		{
		case "android":
			appiumDriver = (AppiumDriver<?>) this.setNPMAndroidDriver(objConfiguration, appiumServerIP, appiumServerPort);
			appiumDriver.manage().deleteAllCookies();
			appiumDriver.get(objConfiguration.getConfig("web.Url"));			
			/*
			 * WebElement cookiesContinue = appiumDriver.findElement(btnCookieContinue);
			 * System.out.println("Clicking on cookies continue element:" +
			 * btnCookieContinue); cookiesContinue.click();
			 */
			break;
			
		case "ios":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumIOSDriver(objConfiguration);
			appiumDriver.get(objConfiguration.getConfig("web.Url"));
			break;
		}
		//appiumLogEntries = appiumDriver.manage().logs().get(LogType.DRIVER); 
	}

	/**
	 *  initialize mobile appium driver for automation
	 */
	public void initializeDesktopAppium(String mobileEnvironment, Configuration objConfiguration) throws Exception
	{
		switch (mobileEnvironment.toLowerCase())
		{
		case "android":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumAndroidDriver(objConfiguration);
			break;
		case "ios":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumIOSDriver(objConfiguration);
			appiumDriver.manage().deleteAllCookies();
			appiumDriver.manage().timeouts().implicitlyWait(Integer.parseInt(objConfiguration.getConfig("driver.implicitlyWait")), TimeUnit.SECONDS);
			appiumDriver.manage().timeouts().pageLoadTimeout(Integer.parseInt(objConfiguration.getConfig("driver.pageLoadTimeout")), TimeUnit.SECONDS);
			appiumDriver.get(objConfiguration.getConfig("web.Url"));
			
			break;
		}

		//appiumLogEntries = appiumDriver.manage().logs().get(LogType.DRIVER);
	}

	/**
	 *  initialize appiumDriver for automation
	 */
	public void tearDown(){
		appiumDriver.quit();
	}

	/**
	 * @return - appiumDriver instance
	 */
	public AppiumDriver<?> getAppiumDriver(){
		return appiumDriver;
	}

	/**
	 * @return - appiumDriver instance
	 */
	public LogEntries getAppumLogEntries(){
		return appiumLogEntries;
	}
}
