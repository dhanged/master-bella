package com.hooks;

import java.io.IOException;
import java.io.InputStream;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.generic.StepBase;

import com.generic.cucumberReporting.CustomFormatter;
import com.generic.utils.Configuration;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * @ScriptName : Hooks
 * @Description : This class contains global hooks required for scenarios
 * @Author : Harhvardhan Yadav, Namrata Donikar (Expleo)
 * 
 */
public class GlobalHooks {
	private StepBase objStepBase;
	private Configuration objConfiguration;

	public GlobalHooks(StepBase stepBase, Configuration configuration) {
		this.objStepBase = stepBase;
		this.objConfiguration = configuration;
	}


	@Before
	public void applyHook(Scenario scenario) {
		//System.out.println("Executing Cucumber Feature :- " + CustomFormatter.getCurrentFeatureName());
		System.out.println("Tags Applied :- " + scenario.getSourceTagNames());
		
		objConfiguration.loadConfigProperties();
		System.err.println("---Initializing environment---");
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.initializeAndroidEnvironment(scenario);

		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.initializeIOSEnvironment();
		} else {
			objStepBase.initializeEnvironment(scenario);
		}
	}

	@After
	public void removeHook(Scenario scenario) {
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentAndroid(scenario);
		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentIOS(scenario);
		} else
			objStepBase.tearDownEnvironment(scenario);
		
	}

}
