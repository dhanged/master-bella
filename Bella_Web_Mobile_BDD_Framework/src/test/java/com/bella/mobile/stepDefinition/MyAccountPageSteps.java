package com.bella.mobile.stepDefinition;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;

import com.bella.mobile.page.Bella.MyAccountPage;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class MyAccountPageSteps
{
	private MyAccountPage objMyAccountPage;
	private Utilities utilities;

	public MyAccountPageSteps(MyAccountPage myAccountPage,Utilities utilities) {		
		this.objMyAccountPage = myAccountPage ;
		this.utilities = utilities;
	}

	@Then("^Verify First Time Deposit Bonus is displayed on screen$")
	public void verify_First_Time_Deposit_Bonus_is_displayed_on_screen() throws Throwable {
		objMyAccountPage.verifywelcomeBonusHDRDisplayed();
	}

	@Then("^Verify user can see 'Accept' and 'Decline' option$")
	public void verify_user_can_see_Accept_and_Decline_option() throws Throwable {
		objMyAccountPage.verifyYesPleaseCTA();
		objMyAccountPage.verifyNoThanksBTN();
	}

	@Then("^Accept first time deposit bonus$")
	public void accept_first_time_deposit_bonus() throws Throwable {
		objMyAccountPage.clickYesPleaseBTN();
	}

	@Then("^Enter deposit amount as \"([^\"]*)\"$")
	public void enter_deposit_amount_as(String inpDepAmt) throws Throwable {
		objMyAccountPage.setDepositAmt(inpDepAmt);
	}

	@Then("^Accept First Deposit PoPF Agree checkbox$")
	public void accept_First_Deposit_PoPF_Agree_checkbox() throws Throwable {
		objMyAccountPage.firstDepositPoPFAgree();
	}

	@Then("^Click on Next$")
	public void click_on_Next() throws Throwable {
		objMyAccountPage.clickOnNext();
	}

	@Then("^Enter following payment details$")
	public void enter_following_payment_details(Map<String,String> table) throws Throwable {
		objMyAccountPage.performDepositUsingCardPaymentMethod(table.get("Card Number"), table.get("Exp Month"), table.get("Exp Year"), table.get("CVV"));
	}

	@Then("^Accept Deposit Confirmation PopUp$")
	public void accept_Deposit_Confirmation_PopUp() throws Throwable {
		objMyAccountPage.validateSuccessMessage();
	}
	@Then("^Navigate to \"([^\"]*)\" tab from my account screen$")
	public void navigate_to_tab_from_my_account_screen(String tabNm) throws Throwable {
		objMyAccountPage.navigateToMyAccountTab(tabNm);
	}

	@Then("^Verify Help link on following \"([^\"]*)\" submenus$")
	public void verify_Help_link_on_following_submenus(String arg1, DataTable dt) throws Throwable {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objMyAccountPage.navigateToMyAccountTab(list.get(i));
			objMyAccountPage.clickBackBTN();
			objMyAccountPage.clickBackBTN();}
	}

	@Then("^Verify following tabs loads correctly$")
	public void verify_following_tabs_loads_correctly(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objMyAccountPage.verifyMyAccountsubOptionsLoadsCorrectly(list.get(i));}
	}
}
