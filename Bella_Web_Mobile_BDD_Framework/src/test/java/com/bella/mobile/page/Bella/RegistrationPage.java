package com.bella.mobile.page.Bella;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class RegistrationPage {

	private MobileActions objMobileActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public RegistrationPage(MobileActions mobileActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objMobileActions = mobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	private By inpEmail = By.xpath("//input[@type='email' and @placeholder='Email address']");
	private By inpUserName = By.id("joinnow-username");
	private By inpPassword = By.id("joinnow-password");
	private By inpFirstName = By.id("joinnow-firstname");
	private By inpSurName = By.id("joinnow-surname");
	private By inpMobileNumebr = By.id("joinnow-mobile");
	By inputaddressLine1 = By.id("joinnow-addressLine1");
	By inputaddressLine2 = By.id("joinnow-addressLine2");
	By inputtownCity = By.id("joinnow-townCity");
	By inputcounty = By.id("joinnow-county");
	By inputpostcode = By.id("joinnow-postcode");

	// Button 
	private By btnNext = By.xpath("//button[@type='submit' and text()='Next']");
	private By btnYesNewUser = By.xpath("//p[text()='New to Grosvenor casinos?']/following-sibling::p/button[text()='Yes']");
	private By btnDepositLimitYes = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'Yes')]"); 
	private By btnDepositLimitNo = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'No')]");
	private By btnRegister = By.xpath("//button[@type='submit' and contains(text(),'Register')]");
	private By btnSignUpClose = By.xpath("//div[@class='header']/header/a[@class='close' and contains(text(),'Close')]");
	private By chkJoinnowAgree = By.xpath("//input[@type='checkbox' and @name='joinnow-agree']");
	private By hdrSignUp = By.xpath("//h1[text()='JOIN NOW']");
	private By drpCountry = By.xpath("//select[@id='joinnow-country']");
	private By lnkLiveHelp = By.xpath("//a[text()='Live Help']");
	By lnkForManualAddress = By.xpath("//a[contains(text(),'Or enter it manually')]");
//	By lnkForManualAddress = By.xpath("//a[contains(text(),'Enter Address manually')]");
	public void selectNewUserYes() {
		logReporter.log("Click 'Yes' new user", 
				objMobileActions.click(btnYesNewUser));
	}

	public void verifySignUpHeaderDisplayed(){
		logReporter.log("Verify 'JOIN NOW' header displayed.", 
				objMobileActions.checkElementDisplayed(hdrSignUp));
	}

	public void setEmailAddress(String email){
		logReporter.log("Set Email Address", email, 
				objMobileActions.setTextWithClear(inpEmail, email));

		/*objMobileActions.set(inpEmail, email);*/
		objMobileActions.pressKeybordKeys(inpEmail, "tab");
	}

	public void selectJoinnowAgreeCheckbox() {
		logReporter.log("Select Joinnow Agree Checkbox",  
				objMobileActions.selectCheckbox(chkJoinnowAgree, true));
	}

	public void verifyJoinNowAgreeTnCDisplayed()
	{
		By loctaor = By.xpath("//div[@class='info-list']//p//span[contains(.,'I agree that I am at least 18 years old, that I agree with the')]");
		logReporter.log("Verify 'I agree that I am at least 18 years old, that I agree with the'  displayed.", 
				objMobileActions.checkElementDisplayed(loctaor));
		this.verifyLinksDisplayedUnderIAgreeTncSection("privacy policy");
		this.verifyLinksDisplayedUnderIAgreeTncSection("terms and conditions");
	}

	public void verifyLinksDisplayedUnderIAgreeTncSection(String lnk)
	{
		By locator = By.xpath("//div[@class='info-list']//p//a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify "+lnk+ " displayed under terms n condition section ", 
				objMobileActions.checkElementDisplayed(locator));
	}
	public void clickNext() {
		logReporter.log("Click 'Next' button", 
				objMobileActions.click(btnNext));
	}	

	public void setUserName(String userName){
		logReporter.log("Set user name", userName, 
				objMobileActions.setTextWithClear(inpUserName, userName));
		objMobileActions.pressKeybordKeys(inpUserName, "tab");
	}

	public void setPassword(String password){
		logReporter.log("Set password", password, 
				objMobileActions.setTextWithClear(inpPassword, password));
		objMobileActions.pressKeybordKeys(inpPassword, "tab");
	}
	public static String getRandomTitleForUser() {
		String[] arrTitles = {"Mr", "Miss", "Mrs", "Ms"};
		int idx = new Random().nextInt(arrTitles.length);
		return (arrTitles[idx]);
	}
	public void selectTitle(String title) {
		By locator = null;
		switch(title.toLowerCase()){
		case "mr":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mr']");
			break;
		case "miss":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Miss']");
			break;
		case "mrs":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mrs']");
			break;
		case "ms":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Ms']");
			break;
		}
		logReporter.log("Select title", title, 
				objMobileActions.click(locator));
	}

	public void setFirstName(String firstName){
		objMobileActions.androidScrollToElement(inpFirstName);
		logReporter.log("Set first name", firstName, 
				objMobileActions.setTextWithClear(inpFirstName, firstName));
	}

	public void setSurName(String surName){
		objMobileActions.androidScrollToElement(inpSurName);
		logReporter.log("Set sur name", surName, 
				objMobileActions.setTextWithClear(inpSurName, surName));
	}

	public void selectDateOfBirth(String dateOfBirth){
		
		String[] arrDateOfBirth = dateOfBirth.split("_");
		String dobDay = arrDateOfBirth[0];
		String dobMonth = arrDateOfBirth[1];
		String dobYear = arrDateOfBirth[2];
		By locator = By.xpath("//input[@placeholder='Date of birth']");
		objMobileActions.androidScrollToElement(locator);
		if(objMobileActions.checkElementDisplayed(locator))
		{
			logReporter.log("Activate DOB fileds",   
				objMobileActions.clickUsingJS(locator));
		}

		locator = By.id("dob-day");
		logReporter.log("Set dob day", dobDay, 
				objMobileActions.setTextWithClear(locator, dobDay));

		locator = By.id("dob-month");
		logReporter.log("Set dob month", dobMonth, 
				objMobileActions.setTextWithClear(locator, dobMonth));

		locator = By.id("dob-year");
		logReporter.log("Set dob year", dobYear, 
				objMobileActions.setTextWithClear(locator, dobYear));
		objMobileActions.pressKeybordKeys(locator, "tab");
	}

	public void selectCountry(String country) {
		objMobileActions.androidScrollToElement(drpCountry);
		logReporter.log("Select country", country, 
				objMobileActions.selectFromDropDown(drpCountry, country));
	}

	public void verifyAddressFieldGetPopuplatedWithData(String data)
	{
		String value;
		if(data.contains("-"))
		{
			String str[] = data.split("-");
			By locator = By.xpath("//label[text()='"+str[0]+"']//preceding::input[1]");
			value = objMobileActions.getAttribute(locator, "value");
			if(value.equals("")){
				logReporter.log("Verify '"+str[0]+" ' field is displayed blank ", true);}
			else{
				logReporter.log("Verify '"+str[0]+" ' field is populated with ' "+str[1]+" 'value ",   
						value.equals(str[1]));}
		}
	}
	public void setmanualAddress(String addressLine1, String addressLine2, 
			String townCity, String country, String postCode) 
	{
		//By lnkForManualAddress = By.xpath("//a[contains(text(),'enter it manually')]");
		//By lnkForManualAddress = By.xpath("//a[contains(text(),'Enter Address manually')]");

	/*	logReporter.log("Select enter it manually",   
				objMobileActions.click(lnkForManualAddress));*/
		objMobileActions.androidScrollToElement(inpMobileNumebr);

		if(!objMobileActions.checkElementDisplayedWithMinWait(inputaddressLine1))
			objMobileActions.click(lnkForManualAddress);

		logReporter.log("Set address line1", addressLine1, 
				objMobileActions.setText(inputaddressLine1, addressLine1));


		logReporter.log("Set address line2", addressLine2, 
				objMobileActions.setText(inputaddressLine2, addressLine2));


		logReporter.log("Set Town/City", townCity, 
				objMobileActions.setText(inputtownCity, townCity));


		logReporter.log("Set country", country, 
				objMobileActions.setText(inputcounty, country));


		logReporter.log("Set post code", postCode, 
				objMobileActions.setText(inputpostcode, postCode));
	}

	public void setMobileNumber(String mobileNumber) {
		objMobileActions.androidScrollToElement(inpSurName);
		logReporter.log("Set mobile number", mobileNumber, 
				objMobileActions.setText(inpMobileNumebr, mobileNumber));
	}
	public void verifyRegestion2StepFormDisplay()
	{
		By locator = By.xpath("//div[@id='component_stepmarkers']//div[@class='step2 three-steps']");
		logReporter.log("Verify Registration Step 2 form display ",
				objMobileActions.checkElementDisplayed(locator));
	}

	// set marketingPreferences from input field id attribute of page 
	// available are "email~sms~phone~post~selectAll"
	public void setMarketingPreferences(String marketingPreferences) {


		By mktprefHDR = By.xpath("//p[@class='marketing_preferences_header' and contains(text(),'Marketing preferences')]");
		objMobileActions.androidScrollToElement(mktprefHDR);

		logReporter.log("Verify Marketing preferences header displayed ",
				objMobileActions.checkElementDisplayed(mktprefHDR));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		if(marketingPreferences.equalsIgnoreCase("Select all"))
		{
			//By locator = By.xpath("//fieldset[@class='fieldset-inline']//button[text()='Select all']");
			By locator = By.xpath("//input[@id='selectAll' and @type='checkbox']");

			logReporter.log("Select marketing preference as 'Select all' ",  
					objMobileActions.clickUsingJS(locator));
		}
		else if(marketingPreferences.contains("~")) 
		{
			String[] arrMarketingPreferences = marketingPreferences.split("~");
			for(String marketingPreference : arrMarketingPreferences) {
				String mktpref = marketingPreference.toLowerCase();
				By locator = By.xpath("//input[@id='"+mktpref+"' and @type='checkbox']");
				//	By locator = By.xpath("//div[@class='buttonsGroup']//button[text()='"+mktpref+"']");

				logReporter.log("Select marketing preference", marketingPreference,  
						objMobileActions.clickUsingJS(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@id='"+marketingPreferences.toLowerCase()+"' and @type='checkbox']");
			//By locator = By.xpath("//div[@class='buttonsGroup']//button[text()='"+marketingPreferences+"']");
			logReporter.log("Select marketing preference", marketingPreferences,  
					objMobileActions.clickUsingJS(locator));
		}
	}
	public void verifyMarketingPreferencesCheckboxesIsSelectedOrNot(String checkboxes) 
	{
		System.out.println("**** checkboxes "+checkboxes);

		switch (checkboxes) 
		{
		case "Email":
			System.out.println("**** into checkboxes "+checkboxes);
			By chkboxEmail = By.xpath("//input[@id='email' and @type='checkbox']");
			logReporter.log("email checkbox is selected > >", objMobileActions.isCheckBoxSelected(chkboxEmail));
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms checkbox is selected > >", objMobileActions.isCheckBoxSelected(chkboxSMS));
			break;
		case "Phone":
			By chkboxphone = By.xpath("//input[@id='phone' and @type='checkbox']");
			logReporter.log("phone checkbox is selected > >", objMobileActions.isCheckBoxSelected(chkboxphone));
			break;
		case "Post":
			By chkboxpost = By.xpath("//input[@id='post' and @type='checkbox']");
			logReporter.log("post checkbox is selected > >", objMobileActions.isCheckBoxSelected(chkboxpost));
			break;
		}

	}
	public void setMarketingPreferencesAsIdrathernot() 
	{
		By locator = By.xpath("//input[@id='clearAll'][@type='checkbox']");
		logReporter.log("Select 'I'd rather not ' checkbox",  
				objMobileActions.click(locator));
	}
	public void selectDepositLimitYes() {
		logReporter.log("Select deposit limit now 'Yes'",   
				objMobileActions.clickUsingJS(btnDepositLimitYes));
	}

	public void selectDepositLimitNo() {
		logReporter.log("Select deposit limit now 'No'",   
				objMobileActions.click(btnDepositLimitNo));
	}

	public void clickRegister() {
		waitMethods.sleep(5);
		logReporter.log("Click register",   
				objMobileActions.clickUsingJS(btnRegister));
	}

	public void verifyKYC()
	{
		boolean flag=false;
		int count =0;
		//waitMethods.sleep(2);
		By locator = By.xpath("//div[@id='verificationAge_overlay']//div//div[@class='verificationAge_content']//div//p[contains(.,'We are checking your details and setting up your secure account.')]");	
		//objPojo.getLogReporter().mobileLog(" Verify 'We are checking your details and setting up your secure account' ",
		//	objPojo.getWebDriverProvider().getDriver().findElement(locator).isDisplayed());
		do
		{

			flag = objDriverProvider.getWebDriver().findElement(locator).isDisplayed();
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
			count++;
			System.out.println(flag +  "  "+count);
		}while(!flag);

	}

	public void verifysuccessVerificationMessage()
	{
		By locator = By.xpath("//div[@class='verificationUploadDocs']//div[@class='successVerification_text'][text()='Documents Required']");
		logReporter.log("Verify 'Documents Required ' ",
				objMobileActions.checkElementDisplayed(locator));
	}
	public void clickCloseSignUp() {
		if(objMobileActions.checkElementDisplayed(btnSignUpClose)) {
			logReporter.log("Close Sign Up Popup",   
					objMobileActions.click(btnSignUpClose));
		}
	}

	public void verifyYoumustagreetothetermsandconditionsErrorMessage()
	{
		By errMsg = By.xpath("//div[@class='error-msg']//li[text()='You must agree to the terms and conditions...']");
		logReporter.log("Verify 'You must agree to the terms and conditions... ' ",
				objMobileActions.checkElementDisplayed(errMsg));
	}

	public void verifyErrorMessage(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+" displayed  ",
						objMobileActions.checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" displayed  ",
					objMobileActions.checkElementDisplayed(errMsg));}

	}

	public boolean setAttributeValue(By locator, String attributeName, String attributeValue)
	{
		try {
			objMobileActions.click(locator);
			JavascriptExecutor js = (JavascriptExecutor) objDriverProvider.getWebDriver();
			WebElement element = objMobileActions.processMobileElement(locator);
			js.executeScript("arguments[0].setAttribute('value', arguments[1])",element,attributeValue);

			return true;
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	public void selectDateOfBirthUsingJavaScript(String dateOfBirth){
		String[] arrDateOfBirth = dateOfBirth.split("_");
		String dobDay = arrDateOfBirth[0];
		String dobMonth = arrDateOfBirth[1];
		String dobYear = arrDateOfBirth[2];
		By locator = By.xpath("//input[@placeholder='Date of birth']");

		if(objMobileActions.checkElementDisplayed(locator))
		{logReporter.log("Activate DOB fileds",   
				objMobileActions.click(locator));}


		locator = By.id("dob-day");
		logReporter.log("Set dob day", dobDay, 
				this.setAttributeValue(locator,"value",dobDay));

		locator = By.id("dob-month");
		logReporter.log("Set dob month", dobMonth, 
				this.setAttributeValue(locator,"value", dobMonth));

		locator = By.id("dob-year");
		logReporter.log("Set dob year", dobYear, 
				this.setAttributeValue(locator,"value", dobYear));
	}

	public void setDepositLimit(String lmt,String value)
	{
		By lblDepositLmt = By.xpath("//fieldset[@id='joinnow-depositLimit']//following-sibling::div//fieldset//div//button[text()='"+lmt+"']");

		logReporter.log("Clik on "+lmt,   
				objMobileActions.click(lblDepositLmt));
		clickOnResetLimit();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By inputDepositLimit = By.xpath("//input[@id='joinnow-depositLimit']");
		logReporter.log("Set" +lmt + " as " , value,
				objMobileActions.setTextWithClear(inputDepositLimit, value));
	}
	public void clickOnResetLimit()
	{
		By lnkResetLimit = By.xpath("//a[@class='input-inner-link'][contains(.,'Reset Limit')]");
		logReporter.log("Click on 'Reset Limit''",
				objMobileActions.click(lnkResetLimit));
	}
	public void verifyDepositNowHeader()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		//By lblDepositNow = By.xpath("//p[@class='heading'][text()='Deposit now']");
		By lblDepositNow = By.xpath("//form[@class=\"safecharge\"]");
		logReporter.log(" verify 'Deposit now'  " ,
				objMobileActions.checkElementDisplayed(lblDepositNow));
	}

	public void verifyLiveHelpLinkDisplayed() { 
		logReporter.log("Verify 'Live Help' link displayed", 
				objMobileActions.checkElementDisplayed(lnkLiveHelp));
	}

	public void clickOnLiveHelpLink() { 
		logReporter.log("Click on 'Live Help' link", 
				objMobileActions.click(lnkLiveHelp));
	}
	public void setAddress(String postcode)
	{
		By inputaddress = By.id("joinnow-address");
		logReporter.log("Set postcode" , postcode,
				objMobileActions.setTextWithClear(inputaddress, postcode));
		objMobileActions.pressKeybordKeys(inputaddress, "tab");
	}
	public void verifyAddressSuggestionPopUp()
	{
		By locator = By.xpath("//div[@class='address-results']");
		logReporter.log(" verify Address suggestion pop up window  " ,
				objMobileActions.checkElementDisplayed(locator));
	}
	public void selectAddresFromLookup(String address)
	{
		By locator = By.xpath("//div[@class='address-results']//ul//li//a[text()='"+address+"']");
		logReporter.log(" verify ' "+address+"' is displayed in address result " ,
				objMobileActions.checkElementDisplayed(locator));
		logReporter.log("Select address as "+address,   
				objMobileActions.click(locator));
	}

	public void clickOnClearLink()
	{
		By locator = By.xpath("//fieldset[@id='joinnow-address']/p/a[text()=' Clear ']");
		logReporter.log("Click on 'Clear' link ",   
				objMobileActions.click(locator));
	}
	public void selectDepositLimit(String option)
	{
		if(option.equals("No"))
			this.selectDepositLimitNo();
		else
			this.selectDepositLimitYes();
	}

	public String returnExpectedUrl(String linkName)
	{
		String url = configuration.getConfig("web.Url");
		switch (linkName) 
		{
		case "Live Help":
			url =url+"support";
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms checkbox is selected > >", objMobileActions.isCheckBoxSelected(chkboxSMS));
			break;
		}

		return url;
	}

	public void verifyPageRedirection(String currentURL ,String expectedurl)
	{
		logReporter.log("Redirect to the  '"+currentURL + "'", currentURL.equalsIgnoreCase(expectedurl));
	}

	public void verifyerrorMessageOnScreen(String msg)
	{
		By errMsg = By.xpath("//div[contains(@class,'error-msg')]//li[text()='"+msg+"']");
		logReporter.log("Verify "+msg,
				objMobileActions.checkElementDisplayed(errMsg));
	}

	public void verifyErrorMessageDoesNotDisplay(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+"  not displayed  ",
						objMobileActions.checkElementNotDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" not displayed  ",
					objMobileActions.checkElementNotDisplayed(errMsg));}

	}

	public void verifyTitle(String title)
	{
		By locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='"+title+"']");
		logReporter.log("Verify title display as '" +title+" '", 
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyAddressField(String fieldnm)
	{
		By locator = By.xpath("//label[text()='"+fieldnm+"']//preceding::input[1]");
		logReporter.log("Verify '"+fieldnm+" ' field is displayed when enter the address manually ", true);
	}
	public void verifyenteritmanuallylink()
	{
		logReporter.log("verify ' enter it manually' link",   
				objMobileActions.checkElementDisplayed(lnkForManualAddress));
	}

	public void Clickenteritmanuallylink()
	{
		logReporter.log("Select enter it manually",   
				objMobileActions.clickUsingJS(lnkForManualAddress));
	}
	public void verifyMarketingPreferencesOption(String checkboxes) 
	{
		switch (checkboxes) 
		{
		case "Email":
			By chkboxEmail = By.xpath("//input[@id='email' and @type='checkbox']");
			logReporter.log("Email as Marketing Preferences Option is displayed > >", objMobileActions.checkElementDisplayed(chkboxEmail));
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms as Marketing Preferences Option is displayed  > >", objMobileActions.checkElementDisplayed(chkboxSMS));
			break;
		case "Phone":
			By chkboxphone = By.xpath("//input[@id='phone' and @type='checkbox']");
			logReporter.log("phone as Marketing Preferences Option is displayed  > >", objMobileActions.checkElementDisplayed(chkboxphone));
			break;
		case "Post":
			By chkboxpost = By.xpath("//input[@id='post' and @type='checkbox']");
			logReporter.log("post as Marketing Preferences Option is displayed  > >", objMobileActions.checkElementDisplayed(chkboxpost));
			break;
		case "Select All":
			By locator = By.xpath("//input[@id='selectAll' and @type='checkbox']");
			logReporter.log("'Select All' as Marketing Preferences Option is displayed  > >", objMobileActions.checkElementDisplayed(locator));
			break;
		}
	}
}
