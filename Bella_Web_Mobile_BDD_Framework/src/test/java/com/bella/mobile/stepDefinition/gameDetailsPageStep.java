package com.bella.mobile.stepDefinition;

import com.bella.mobile.page.Bella.gameDetailsPage;

import cucumber.api.java.en.Then;

public class gameDetailsPageStep {
	
	private gameDetailsPage objgameDetailsPage;
	
	public gameDetailsPageStep(gameDetailsPage objGameDetailsPage) {		
		this.objgameDetailsPage = objGameDetailsPage;
	}
	
	@Then("^Click on 'I' button for any game$")
	public void click_on_I_button_for_any_game(){
	    // Write code here that turns the phrase above into concrete actions
		objgameDetailsPage.moveTheCursorToGame();
		objgameDetailsPage.clickOnGameInfoIcon();
	}

	@Then("^Click on Ts & Cs for the selected games\\.$")
	public void click_on_Ts_Cs_for_the_selected_games(){
	    // Write code here that turns the phrase above into concrete actions
		objgameDetailsPage.verifyGameDetailsPageDisplayed();
	//	objgameDetailsPage.verifyGameDetailsTab("Description,Features,Terms & Conditions");
		objgameDetailsPage.verifyGameDetailsTab("Description,Terms & Conditions");
		objgameDetailsPage.ClickGameDetailsTab("Terms & Conditions");
	}

	@Then("^Click on Play button$")
	public void click_on_Play_button(){
		objgameDetailsPage.moveTheCursorToGame();
		objgameDetailsPage.clickOnPlayButton();
	}
}
