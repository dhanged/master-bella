package com.bella.mobile.stepDefinition;


import com.bella.mobile.page.Bella.HomePage;
import com.bella.mobile.page.Bella.PromotionsPage;

import cucumber.api.java.en.Then;

public class PromotionPageStep {

	private PromotionsPage objPromotionsPage;
	private HomePage objHomePage;
	String promoTitle;
	boolean flag = false;

	public PromotionPageStep(PromotionsPage promotionsPage,HomePage homePage) {
		this.objPromotionsPage = promotionsPage ;
		this.objHomePage = homePage;
	}

	@Then("^Navigate to \"([^\"]*)\" tab$")
	public void navigate_to_tab(String arg1){
		// Write code here that turns the phrase above into concrete actions
		objHomePage.navigateToPrimaryTabs(arg1);
	}
	@Then("^Verify Expiry date is displayed on promorion details page$")
	public void Verify_Expiry_date_is_displayed_on_promorion_details_page(){
		// Write code here that turns the phrase above into concrete actions
		objPromotionsPage.validateActiveExpiryDate();
	}
	@Then("^Open opt-in promotions from listed ones$")
	public void open_opt_in_promotions_from_listed_ones(){
		// Write code here that turns the phrase above into concrete actions
		objPromotionsPage.findPromoBTN("Opt-in");
		objPromotionsPage.clickFollowingMoreInfoLink("Opt-in");
	}

	@Then("^Click on opt-in button$")
	public void click_on_opt_in_button(){
		// Write code here that turns the phrase above into concrete actions
		objPromotionsPage.clickClaimBTN();
	}
	@Then("^Verify system displays all the available promotions on the screen\\.$")
	public void verify_system_displays_all_the_available_promotions_on_the_screen() {
		objPromotionsPage.verifyPromoetionsONPromosPage();
	}
	@Then("^Verify 'Dont have any active promotions' message is displayed$")
	public void verify_Dont_have_any_active_promotions_message_is_displayed() {
		objPromotionsPage.verifyDonthaveanyactivepromotionsInformativeMessage();
	}
	@Then("^Verify 'No Promotions Claimed by you' message is displayed$")
	public void verify_No_Promotions_Claimed_by_you_message_is_displayed() {
		objPromotionsPage.verifyDonthaveanyactivepromotionsInformativeMessage();
	}
	@Then("^Verify Optedin and Opt out button is displayed on page$")
	public void verify_Optedin_and_Opt_out_button_is_displayed_on_page() {
		objPromotionsPage.verifyOptedInTXTDisplayed();
		objPromotionsPage.verifyOptOutBTNDisplayed();
	}

	@Then("^Select the Opt-In or Claim button for Opt In promotion$")
	public void select_the_Opt_In_or_Claim_button_for_Opt_In_promotion() {
		if(objPromotionsPage.findPromoBTN("Claim")) {
			promoTitle = objPromotionsPage.getPromotitle("Claim");
			objPromotionsPage.clickPromoBTNonPromotions("Claim");
			flag=true;}
		else {
			promoTitle = objPromotionsPage.getPromotitle("Opt-in");
			objPromotionsPage.clickPromoBTNonPromotions("Opt-in");
		}
		System.out.println("******************* promoTitle");
	}

	@Then("^Verify Opted-in or Claimed button is displayed on page$")
	public void verify_Opted_in_or_Claimed_button_is_displayed_on_page(){
		if(flag==true)
		{objPromotionsPage.verifyClaimedTXTonPromotionsPage(promoTitle);}
		else
		{	objPromotionsPage.verifyOptedInTXTonPromotionsPage(promoTitle);}
	}
}
