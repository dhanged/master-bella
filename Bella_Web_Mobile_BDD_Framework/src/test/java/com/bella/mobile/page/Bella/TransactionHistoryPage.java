package com.bella.mobile.page.Bella;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class TransactionHistoryPage {

	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	
	public TransactionHistoryPage(MobileActions mobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration) {
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void verifyLatestTransactionDisplayOnTop()
	{
		/*By tblTimeCell = By.xpath("//table[@class='table table-bordered table-cell-bordered']//tbody/tr//td[@class='first-cell']");
		int transactionCount = objMobileActions.processMobileElements(tblTimeCell).size();
		String str[] = new String[transactionCount];
		String[] dateString = new String[transactionCount];
		for(int i=0;i<transactionCount;i++){
			str[i]= objMobileActions.processMobileElements(tblTimeCell).get(i).getText();
			System.out.println(str[i]);
		}
		Date date[] = new Date[str.length];
		SimpleDateFormat sobj = new SimpleDateFormat("yyyy/dd/MM HH:mm");// format specified in double quotes
		for (int index = 0; index < str.length; index++){
			try {
				date[index] = sobj.parse(str[index]);
				System.out.println("**** date "+date[index]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();}}
		Arrays.sort(date,Collections.reverseOrder());
		for (int i = 0; i <date.length; i++) { 
			dateString[i] = sobj.format(date[i]);
			System.out.println(dateString[i]);
		} 
		logReporter.log("Verify Latest message is displyed on Top ", 
				Arrays.equals(str, dateString));
	}*/



		By tblTimeCell = By.xpath("//table[@class='table table-bordered table-cell-bordered']//tbody/tr//td[@class='first-cell']");
		int transactionCount = objMobileActions.processMobileElements(tblTimeCell).size();
		System.out.println("*******transactionCount  "+transactionCount);

		if(transactionCount>0)
		{
			String str[] = new String[transactionCount];
			String[] dateString = new String[transactionCount];
			for(int i=0;i<transactionCount;i++)
			{
				str[i]= objMobileActions.processMobileElements(tblTimeCell).get(i).getText();
				System.out.println(str[i]);
			}
			Date date[] = new Date[str.length];
			SimpleDateFormat sobj = new SimpleDateFormat("yyyy/dd/MM HH:mm");
			//	DateFormat sobj = new SimpleDateFormat("yyyy/dd/MM HH:mm");// format specified in double quotes
			for (int index = 0; index < str.length; index++)
			{
				try {
					System.out.println(str[index]);
					date[index] = sobj.parse(str[index]);
					System.out.println("**** date "+date[index]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();}
			}
			Arrays.sort(date,Collections.reverseOrder());
			for (int i = 0; i <date.length; i++)
			{ 
				dateString[i] = sobj.format(date[i]);
				System.out.println(dateString[i]);
			} 
			logReporter.log("Verify Latest message is displyed on Top ", 
					Arrays.equals(str, dateString));
		}
		else
		{
			logReporter.log("No transactions found ", true);
		}
	}

	By btnFilter = By.xpath("//button[text()='Filter']");


	public void clickActivityType(String activity)
	{
		By activitytype = By.xpath("//button[@class='btn dropdown-toggle' and contains( text(),'"+activity+"')]");

		logReporter.log(" click on selected Activity type as : " +activity +" on Transaction history ", 
				objMobileActions.click(activitytype));
		waitMethods.sleep(8);
	}
	public void verifyFilterByDateLabelDisplayed()
	{
		By filterByDateLabel = By.xpath("//p[text()='Filter by date range']");
		logReporter.log("Verify 'Filter by date range' Label displayed on Transaction History Page ", 
				objMobileActions.checkElementDisplayed(filterByDateLabel));
	}

	public void verifyResetLinkDisplayed()
	{
		By resetLink = By.xpath("//a[@class='reset edit-details']");
		logReporter.log("Verify 'reset Link displayed  ", 
				objMobileActions.checkElementDisplayed(resetLink));
	}
	public void clickResetLink()
	{
		By resetLink = By.xpath("//a[@class='reset edit-details']");
		logReporter.log(" Click on the 'reset Link displayed  ", 
				objMobileActions.click(resetLink));
	}


	public void selectCurrentMonth(String currentMonth , String CurrentDay)	{

		By Month = By.xpath("//div[@class='react-datepicker__current-month']");
		//By Day = By.xpath("//div[@class='react-datepicker__week']/div[text()='"+CurrentDay+"']");
		By Day = By.xpath("//div[@class='react-datepicker__week']/div[text()='"+CurrentDay+"'][not(contains(@class, '--disabled'))]");
		System.out.println("currentMonth : "+currentMonth +"CurrentDay"+CurrentDay);

		while(true)
		{
			if(currentMonth.equals(objMobileActions.getText(Month)))	{
				break;
			}
			else {
				By NextMonthBTN = By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--next' and contains(text(),'Next month')]");
				logReporter.log(" Click on the Next month button on datePicker", 
						objMobileActions.click(NextMonthBTN));
			}
		}
		logReporter.log(" Selected the Day as: "+ objMobileActions.getText(Day)+" on datePicker ", 
				objMobileActions.click(Day));
	}

	public void setFromDate()
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='react-date-input']");

		logReporter.log(" Click 'from date' field to select the date ", 
				objMobileActions.click(inpFromDate));

		this.selectCurrentMonth(Utilities.getRequiredDay("-1", "MMMM yyyy", ""),Utilities.getRequiredDay("-1","d",""));
	}

	public void setFromDateSameDay()
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='react-date-input']");

		logReporter.log(" Click 'from date' field to select the date as current day ", 
				objMobileActions.click(inpFromDate));

		this.selectCurrentMonth(Utilities.getRequiredDay("-1", "MMMM yyyy", ""),Utilities.getRequiredDay("0","d",""));
	}

	public void setToDate()
	{
		By inpToDate= By.xpath("//div[2]/div/input[@class='react-date-input']");

		logReporter.log(" Click 'to date' field to select the date ", 
				objMobileActions.click(inpToDate));

		this.selectCurrentMonth(Utilities.getRequiredDay("-1", "MMMM yyyy", ""),Utilities.getRequiredDay("0","d",""));
	}

	public void verifyFilterButtonDisplayed()
	{

		logReporter.log("Verify 'Filter button displayed on Transaction History Page ", 
				objMobileActions.checkElementDisplayed(btnFilter));
	}

	public void clickFilterButton()
	{		
		logReporter.log("Click Filter button from Transaction History Page", 
				objMobileActions.click(btnFilter));
	}


	public void verifyFilterByActivityLabelDisplayed()
	{
		By FilterByActivityLabel = By.xpath("//p[@class='dropdown-label' and contains( text(),'Filter by activity')]");
		logReporter.log("Verify 'Filter By Activity' Label displayed on Transaction History screen ", 
				objMobileActions.checkElementDisplayed(FilterByActivityLabel));
	}

	public void clickAllActivityDropdown(String activity)
	{
		By AllActivityBTN= By.xpath("//button[@class='btn dropdown-toggle' and contains( text(),'"+activity+"')]");
		logReporter.log(" click "+activity +" Dropdown on transaction history ", 
				objMobileActions.click(AllActivityBTN));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyActivityTypesDisplayed(String type)
	{
		if(type.contains("~"))
		{
			String[] arr1 = type.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//ul[@class='dropdown-menu' ]/li/a[text()='"+labels2+"']");

				logReporter.log(" Verify Activity type : "+labels2+" is displayed under filter dropdown list ",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else {
			By locator = By.xpath("//ul[@class='dropdown-menu' ]/li/a[text()='"+type+"']");
			logReporter.log(" Verify Activity type : "+type+" is displayed under filter dropdown list ",  
					objMobileActions.checkElementDisplayed(locator));
		}
	}
	public void selectActivityType(String activity)
	{
		By activitytype = By.xpath("//ul[@class='dropdown-menu' ]/li/a[text()='"+activity+"']");

		logReporter.log(" Select Activity type as : " +activity +" from list ", 
				objMobileActions.click(activitytype));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	//**************** Net Deposits ********************************************************************

	public void verifySortByLabelDisplayed() {

		By sortByLabel = By.xpath("//p[@class='dropdown-label' and contains(text(),'Sort By')]");
		logReporter.log(" sort By label is displayed ", 
				objMobileActions.checkElementDisplayed(sortByLabel));
	}
	public void clickTimePeriodDropdown(String timeframe)
	{
		By periodBTN= By.xpath("//button[@class='btn dropdown-toggle' and contains(text(),'"+timeframe+"')]");
		logReporter.log(" Click on the timeframe dropdown", 
				objMobileActions.click(periodBTN));
	}
	public void verifyTimeFrameOptions(String period)
	{
		if(period.contains("~"))
		{
			String[] arr1 = period.split("~");
			for (String period2 : arr1) 
			{		
				By periodType = By.xpath("//ul[@class='dropdown-menu']/li/a[text()='"+period2+"']");
				logReporter.log(" " +period2 +" is displayed on the timeframe dropdown list for Net Deposits ", 
						objMobileActions.checkElementDisplayed(periodType));
			}
		}
	}

	public void selectTimeFromDropdown(String timeframe)
	{
		By periodType = By.xpath("//ul[@class='dropdown-menu']/li/a[text()='"+timeframe+"']");
		logReporter.log(" " +timeframe +" is displayed on the timeframe dropdown list for New Deposits ", 
				objMobileActions.click(periodType));
	}

	public void verifyTypeHDRDisplayed(String type)
	{
		By typeValue= By.xpath("//tbody[@class='infinite-scroll table-body']/tr/td/div[@class='row-header']");
		if(type.contains("~"))
		{
			String[] arr1 = type.split("~");
			for (String type2 : arr1) 
			{		
				System.out.println("tyep2 " +type2);

				if((objMobileActions.getText(typeValue)).contains(type2)){
					logReporter.log(" Filtered transaction Activity type is : " +type2 +" on Transaction search ", 
							objMobileActions.checkElementDisplayed(typeValue));
				}
			}
		}		else 
		{
			if((objMobileActions.getText(typeValue)).contains(type)){
				logReporter.log(" Filtered transaction Activity type is : " +type +" on Transaction search ", 
						objMobileActions.checkElementDisplayed(typeValue));
			}	else 			
				this.verifyNoTransactionFoundMSGdisplayed();
		}
	}
	public void verifyResultOnTableDisplayed(String colHeaders , String rowLabels , String Type)
	{
		By locator = By.xpath("//table[@class='table table-bordered table-cell-bordered']");
		List<HashMap<String, String>> tableData = objMobileActions.getTableRows(locator);
		System.out.println("Table data " + tableData);
		int col=0;
		if(colHeaders.contains("~"))
		{
			String[] arr1 = colHeaders.split("~");

			HashMap<String, String> firstRow = tableData.get(0);

			if(firstRow.get("Time").contains("No transactions found")) 
			{
				this.verifyNoTransactionFoundMSGdisplayed();
			}
			else
			{
				for (String colHdr : arr1) 
				{//checking for headers
					logReporter.log(" Verify Header: "+colHdr+" is displayed on transaction search results screen ", firstRow.containsKey(colHdr));
				}
				logReporter.log(" Verify Type as : "+Type+" is displayed on first row ", firstRow.get("Type").contains(Type));

				HashMap<String, String> secondRow = tableData.get(1);

				if(rowLabels.contains("~"))
				{
					String[] arr2 = rowLabels.split("~");
					col=0;
					for (String rowLbls : arr2) 
					{
						String hname = arr1[col++];
						logReporter.log(" Verify Label : "+rowLbls+" is displayed on sec row ", secondRow.get(hname).contains(rowLbls));
					}
				}
				else
				{

					String hname = arr1[col++];
					logReporter.log(" Verify Label : "+rowLabels+" is displayed on sec row ", secondRow.get(hname).contains(rowLabels));
				}
			}
		}
	}

	public void verifyNetDepositResultTable(String colHeaders)
	{
		By locator = By.xpath("//table[@class='table table-bordered table-cell-bordered']");
		List<HashMap<String, String>> tableData = objMobileActions.getTableRows(locator);
		HashMap<String, String> firstRow = tableData.get(0);
		if(colHeaders.contains("~"))
		{
			String[] arr1 = colHeaders.split("~");
			int col=0;
			for (String headers2 : arr1) 
			{
				String hname = arr1[col++];
				logReporter.log(" Verify header : "+headers2+" is displayed on transaction search results screen ",  
						objMobileActions.checkElementDisplayed(locator));
				if(firstRow.get("Deposits").contains("No transactions found"))
				{
					this.verifyNoTransactionFoundMSGdisplayed();
					break;
				}
				logReporter.log(" Verify search transaction displays £ sign ", firstRow.get(hname).contains("£"));
			}
		}
	}

	public void verifyNetDepositINFOtxt()
	{
		By infoLocator1 = By.xpath("//div[@class='myaccount-details']/p[contains(text(),'Net Deposits amount is the sum of total Deposits minus total Withdrawals for the period selected')]");
		By infoLocator2 = By.xpath("//div[@class='myaccount-details']/p[contains(text(),'Bonus funds and pending withdrawals are not included in the calculations of Net Deposits')]");
		By infoLocator3 = By.xpath("//div[@class='myaccount-details']/p/strong[contains(text(),'Please note: if you wish to see your Net Deposit amount including transactions before 1 August 2020, please contact Customer Services.')]");
		By infoNoteLabel = By.xpath("//div[@class='myaccount-details']/p/strong[contains(text(),'Note')]");
		logReporter.log(" Verify 'Net Deposits amount is the sum of total Deposits minus total Withdrawals for the period selected' is displayed ", 
				objMobileActions.checkElementDisplayed(infoLocator1));
		logReporter.log(" Verify 'Bonus funds and pending withdrawals are not included in the calculations of New Deposits' is displayed ", 
				objMobileActions.checkElementDisplayed(infoLocator2));
		logReporter.log(" Verify 'Please note: if you wish to see your Net Deposit amount including transactions before 1 August 2020, please contact Customer Services.' is displayed ", 
				objMobileActions.checkElementDisplayed(infoLocator3));
	}

	public void verifyNoTransactionFoundMSGdisplayed()
	{
		By noTransactionFoundMSG = By.xpath("//td[contains(text(),'No transactions found')]");
		logReporter.log(" 'No transactions found' message is displayed ", 
				objMobileActions.checkElementDisplayed(noTransactionFoundMSG));
	}

	public void clickExpandButton()
	{
		By expandBTN = By.xpath("//td[@class='toggle-btn']");

		if(objMobileActions.checkElementDisplayed(expandBTN))
		{
			logReporter.log(" click 'expand + BTN' displayed", 
					objMobileActions.click(expandBTN));
		}
		else
		{
			this.verifyNoTransactionFoundMSGdisplayed();
		}
	}
	public String capitiliseInitialLetterOfEachWord(String str)
	{
		String output = Arrays.stream(str.split("\\s+"))
				.map(t -> t.substring(0, 1).toUpperCase() + t.substring(1).toLowerCase())
				.collect(Collectors.joining(" "));
		// print the string
		System.out.println(output);
		return output;
	}
	
	public String getSelectedActivity()
	{
		By ActivityBTN= By.xpath("//button[@class='btn dropdown-toggle']");
		return objMobileActions.getText(ActivityBTN);
	}
}
