package com.bella.desktop.runner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.testng.annotations.AfterTest;
import com.generic.utils.Configuration;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "src/test/java/com/bella/desktop/feature/playerInbox.feature", strict = true, plugin = { "json:target/JSON/Desktop.json",
		"rerun:target/SyncFails.txt", "com.generic.cucumberReporting.CustomFormatter" }, 
tags = { "@Desktop"}, 
dryRun = false, monochrome = false,
glue = { "com.hooks", "com.bella.desktop.stepDefinition" })

public class DesktopRunner extends AbstractTestNGCucumberTests {
	@AfterTest	
	public void updateTestResultInCucumberStudio()
	{
		/*try {
			Configuration objConfiguration = new Configuration();
			objConfiguration.loadConfigProperties();
			String testRunID = objConfiguration.getConfig("cucumber.testRunID");
			System.out.println(" test :   "+testRunID);
			String doscommand = "curl -X POST -F file=@"+System.getProperty("user.dir")+ "/target/JSON/Desktop.json"+" https://studio.cucumber.io/import_test_reports/3883554770614020374777055203465372147875630580403624077/"+testRunID+"/cucumber-json?update_last_build";
			System.out.println("curl -X POST -F file=@Desktop.json https://studio.cucumber.io/import_test_reports/3883554770614020374777055203465372147875630580403624077/449745/cucumber-json?update_last_build");
			final Process process = Runtime.getRuntime().exec(doscommand);
			final InputStream in = process.getInputStream();
			int ch;
			while((ch = in.read()) != -1) {
				System.out.print((char)ch);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}