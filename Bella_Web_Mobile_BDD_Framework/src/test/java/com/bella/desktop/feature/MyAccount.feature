Feature: My Account

@Desktop12
Scenario:  Check whether user is allowed to accept / decline the deposit offer
Given Invoke the Bella site on Desktop
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select country as "United Kingdom"
Then Enter address as "wr53da"
Then Verify address suggestion list is displayed
Then Select address "Lock Keepers Cottage, Basin Road, WORCESTER WR5 3DA" from the list
Then Select DepositLimit as "No"
Then Click on Register
Then Verify First Time Deposit Bonus is displayed on screen
Then Verify user can see 'Accept' and 'Decline' option
Then Accept first time deposit bonus
Then Enter deposit amount as "10"
Then Accept First Deposit PoPF Agree checkbox
Then Click on Next
Then Enter following payment details
|Card Number	|4000065919262217|
|Exp Month	|11|
|Exp Year	|30|
|CVV	|123|
Then Accept Deposit Confirmation PopUp

@Desktop
Scenario: Check whether Help link is available on My Account page
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
#Then User enters username "eLkNTNt"
#And User enters password "RiGnAA54"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Verify 'Live Help' link on Registartion page
Then Click on 'Live Help' link on Registartion page

@Desktop
Scenario: Check whether Help link is available on Messages, Account Details, Bonuses pages
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
#Then User enters username "eLkNTNt"
#And User enters password "RiGnAA54"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify 'Live Help' link on Registartion page
Then Navigate back to screen
Then Navigate to "Account Details" tab from my account screen
Then Verify 'Live Help' link on Registartion page
Then Navigate back to screen
Then Verify Help link on following "Account Details" submenus
|Change Password|
|Marketing Preferences|
Then Verify Help link on following "Bonus" submenus
|Enter bonus code|
|Active Bonuses|
|Bonus History|


@Desktop
Scenario: Check whether Help link is available on Responsible Gaming
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
#Then User enters username "eLkNTNt"
#And User enters password "RiGnAA54"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Verify Help link on following "Responsible Gambling" submenus
|Reality Check|
|Take a break|
|Self Exclude|
|Deposit Limits|

@Desktop
Scenario: Check whether Help link is available on Cashier (Transaction History,) page
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
#Then User enters username "eLkNTNt"
#And User enters password "RiGnAA54"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Verify Help link on following "Cashier" submenus
|Deposit|
|Withdrawal|
|Transaction History|
|Balance|

@Desktop
Scenario: Check whether system displays error message when user enters incorrect current password on change password screen
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Change Password" tab from my account screen
Then User enters incorrect current password as "ffhjf"
And Enter valid New password as "Password1234"
And Click on 'Update'
Then Verify "Current password is incorrect" error message

@Desktop
Scenario: Check whether system displays user's name, Email ID, DOB and Postcode in account detail section
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
And Verify following fields are available on screen
|Last login|
|Name|
|Email|
|Date of birth|
|Phone|
|Postcode|

@Desktop
Scenario: Account Information > Check whether customer can view Last Login time on My Account / Account history section under Personal Details section in date-time format
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
Then User can see Last Login time in date-time format
Then Logout user and get logout time
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
And Verify that previous login date and time is displayed as Last login time

@Desktop
Scenario: Check whether user unable to edit name, DOB
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
Then Click on Edit
Then Verify Name and date of birth field is non editable

@Desktop
Scenario: Check whether system allows user to edit email address, Phone number, address for the user.
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Account Details" tab from my account screen
And Verify following fields are available on screen
|Last login|
|Name|
|Email|
|Date of birth|
|Phone|
|Postcode|
Then Click on Edit
Then Edit email address,Phone number and address
And Click on 'Update'
Then User will get "Account details updated" confirmation message

@Desktop
Scenario: Check whether system displays confirmation message when user updates password
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Change Password" tab from my account screen
Then User enters correct current password
And Enter valid New password as "Test12345"
And Click on 'Update'
Then Password updated confirmation message will display
Then Click on logout button
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And Enter updated password
Then User clicks on login button
Then Verify user is successfully logged in

@Desktop
Scenario: Check whether user able to Update Marketing Preferences without any issues
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Marketing Preferences" tab from my account screen
Then User will get the option to select between email, phone, sms, post and Select all
Then User will select any options
And Click on 'Update'
Then User will get "Account details updated" confirmation message

@Desktop
Scenario: Check that customers must see 'Net Deposit' in Activity/Filter type dropdown field
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Verify 'Net Deposit' option is available in Activity/Filter type dropdown field

@Desktop
Scenario: Check that if no net deposit is available for the user then system should display the message accordingly
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Select Filter Activity as "Net Deposits"
Then Net Deposit information text is displayed on screen
Then 'No transactions found' message will display to the user when no net deposit available

@Desktop215
Scenario: Responsible Gambling > Check whether an option is always available for customer to set their deposit limit at all time
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen

@Desktop
Scenario: Check whether all the options and sub options from MyAccount menu loads correctly
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Verify following tabs loads correctly
|Cashier-Deposit,Withdrawal,Transaction History|
|Bonuses-Enter bonus code,Active Bonuses,Bonus History|
|Messages-null|
|Account Details-Change Password,Marketing Preferences|
|Responsible Gambling-Reality Check,Deposit Limits,Take a break,Self Exclude|