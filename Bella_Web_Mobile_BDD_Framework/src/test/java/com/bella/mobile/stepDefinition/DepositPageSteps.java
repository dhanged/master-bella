package com.bella.mobile.stepDefinition;

import java.util.List;

import com.bella.mobile.page.Bella.DepositPage;
import com.bella.mobile.page.Bella.MyAccountPage;
import com.bella.mobile.page.Bella.WithdrawPage;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class DepositPageSteps {

	private DepositPage objDepositPage;
	private Utilities objUtilities;
	private MyAccountPage objMyAccountPage;
	String balance,depAmt,initalbonuseBalance,receivedBonusAmount;
	private WithdrawPage objWithdrawPage;
	
	public DepositPageSteps(DepositPage objDepositPage,Utilities objUtilities,MyAccountPage myAccountPage ,WithdrawPage objWithdrawPageSteps) {		
		this.objDepositPage = objDepositPage;
		this.objUtilities = objUtilities;
		this.objMyAccountPage = myAccountPage;
		this.objWithdrawPage = objWithdrawPageSteps;
	}
	
	
	
	/*@Then("^Enter Deposit amount as \"([^\"]*)\" and click on Next$")
	public void enter_Deposit_amount_as_and_click_on_Next(String inpDepAmt){
		depAmt = inpDepAmt;
		objMyAccountPage.setDepositAmt(inpDepAmt);
		objMyAccountPage.clickOnNext();
	}

	@Then("^Verify 'Add new payment Method' screen is displayed$")
	public void verify_Add_new_payment_Method_screen_is_displayed(){
	    // Write code here that turns the phrase above into concrete actions
		objDepositPage.switchToDepositIframe();
		if(objDepositPage.verifyAddNewMethodButton()){
			objDepositPage.clickAddNewMethodButton();}
		objDepositPage.verifyAddNewPaymentMethodScreen();
	}

	@Then("^Select \"([^\"]*)\" payment method$")
	public void select_payment_method(String arg1){
		objDepositPage.selectDepositPaymentMethod(arg1);   
	}

	@Then("^Verify following errors are displayed when fields are empty$")
	public void verify_following_errors_are_displayed_when_fields_are_empty(DataTable dt){
		objDepositPage.ClickAddCardButton();
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objDepositPage.verifyErrormessage(list.get(i));}
	}

	@Then("^Enter Card number as \"([^\"]*)\"$")
	public void enter_Card_number_as(String cardNumber){
		objDepositPage.setCardNumber(cardNumber);
	}

	@Then("^Select Exp Month as \"([^\"]*)\" and Year as \"([^\"]*)\"$")
	public void select_Exp_Month_as_and_Year_as(String expMon, String expYr) {
		objDepositPage.setExpMonth(expMon);
		objDepositPage.setExpYear(expYr);
	}

	@Then("^Enter CVN as \"([^\"]*)\"$")
	public void enter_CVN_as(String securityCodeCVV) {
		objDepositPage.setCVVcode(securityCodeCVV);
	}
	
	@Then("^Again Enter CVN as \"([^\"]*)\"$")
	public void again_enter_CVN_as(String securityCodeCVV) {
		objDepositPage.setCVncode(securityCodeCVV);
	}
	@Then("^Click On 'Add Card'$")
	public void click_On_Add_Card() {
		objDepositPage.ClickAddCardButton();
	}
	
	@Then("^Verify Deposit Successful confirmation message$")
	public void verify_Deposit_Successful_confirmation_message() {
		objDepositPage.verifyDepositConfirmation();
		objDepositPage.verifyDepositAmountIsDisplayedCorrectlyOrNot(depAmt);
		objDepositPage.verifyCurrentBalanceOnSuccessScreen(balance, depAmt);
		objDepositPage.verifyPlayNowButton();
	}
	
	@Then("^Verify Bonus recieved amount is displayed on Deposit confirmation message$")
	public void verify_Bonus_recieved_amount_is_displayed_on_Deposit_confirmation_message() {
		objDepositPage.verifyDepositConfirmation();
		objDepositPage.verifyDepositAmountIsDisplayedCorrectlyOrNot(depAmt);
	//	objDepositPage.verifyCurrentBalanceOnSuccessScreen(balance, depAmt);
	receivedBonusAmount = objDepositPage.verifyBonusReceived();
		objDepositPage.verifyPlayNowButton();
	}
	
	@Then("^Verify Received Bonus amount is displayed under playable balance$")
	public void verify_Received_Bonus_amount_is_displayed_under_playable_balance() {
		objDepositPage.clickExpandBalanceBTN();
		objDepositPage.verifyBonusBalanceDisplayedCorrectly(initalbonuseBalance, receivedBonusAmount);
	}
	
	@And("^Get current playable balance$")
	public void get_current_playable_balance() {
		balance =  objDepositPage.getPlayableBalanceAmount();
		balance= balance.substring(1);
		System.out.println("***** balance " +balance);
	}
	
	@And("^Get current balance from Deposit screen$")
	public void get_current_balance_from_Deposit_screen() {
		objDepositPage.switchToDepositIframe();
		balance =  objDepositPage.getBalance();
		System.out.println("***** balance " +balance);
	}
	@And("^Get player bonus balance$")
	public void get_current_player_bonus_balance() {
		objDepositPage.clickExpandBalanceBTN();
		objDepositPage.verifyLabelsOnMyAccount("Cash,Bonuses");
		initalbonuseBalance =  objDepositPage.getValueofLabelOnDetailedView("Bonuses");
		System.out.println("***** initalbonuseBalance " +initalbonuseBalance);
	}
	
	@Then("^Verify Deposit amount is credited to account$")
	public void verify_Deposit_amount_is_credited_to_account() {
		
		//objDepositPage.validateBalanceDisplayed(balance);
	}
	
	@Then("^Choose \"([^\"]*)\" option$")
	public void choose_option(String arg1){
		objDepositPage.selectBonusOption(arg1);
	}
	 
	@And("^Select bonus from the list$")
	public void select_bonus_from_the_list() {
		objDepositPage.selectBonusFromList();
	}
	
	@Then("^Click on \"([^\"]*)\" button$")
	public void click_on_button(String btnName) {
		objDepositPage.clickOnButton(btnName);
	}
	
	@Then("^Verify POPF checkbox is displayed to the customer$")
	public void verify_POPF_checkbox_is_displayed_to_the_custome() {
			objDepositPage.verifyPOPFCheckbox();
	}
	
	@Then("^Verify 'Your bonus' section is displayed$")
	public void verify_Your_bonus_section_is_displayed(){
		objDepositPage.verifyYourbonusIsDisplayedOnDepsoitScreen();
	}
	
	@Then("^Again Enter Deposit Amount as \"([^\"]*)\"$")
	public void again_Enter_Deposit_Amount_as(String arg1) {
		depAmt = arg1 ;
		objDepositPage.setDepositAmount(arg1);
		if(objDepositPage.verifyAcknowledgeCheckbox())
			objDepositPage.selectAcknowledgeCheckbox();
	}
	
	@Then("^Verify it redirect to \"([^\"]*)\" window$")
	public void verify_it_redirect_to_window(String title)  {
		objDepositPage.verifyPageTitle(title);
	}

	@Then("^Enter Pin number \"([^\"]*)\" and Accept TnC$")
	public void enter_Pin_number_and_Accept_TnC(String pin) {
		objDepositPage.setPinForPaySafeCard(pin);
		objDepositPage.selectAcceptTermsCheckboxForPaySafeCard();
	}

	@Then("^Click on Pay$")
	public void click_on_Pay(){
		objDepositPage.clickPayButtonOnPaySafeCard();
		objDepositPage.switchToDepositIframe();
	}
	
	@Then("^Enter email address \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void enter_email_address_and_Password(String emailAddress, String password){
		objDepositPage.setPayPalEmailAddress(emailAddress);
		if(objDepositPage.verifyNext())
		{
			objDepositPage.clickNext();
		}
		objDepositPage.setPayPalPassword(password);
	}

	@Then("^Click on Login from Paypal screen$")
	public void click_on_Login_from_Paypal_screen(){
		objDepositPage.clickLoginButtonOnPaypal();;
	}

	@Then("^Click on 'Pay Now' button$")
	public void click_on_Pay_Now_button(){
		objDepositPage.clickPayNowButtonOnPaypal();
	//	objWebActions.switchToDefaultContent();
		objDepositPage.switchToDepositIframe();
	}
	
	@Then("^Select payment method$")
	public void select_payment_method(){
		objDepositPage.switchToDepositIframe();
		if(objDepositPage.verifyAddedMethoddisplayedOrNot())
		{objDepositPage.selectAddedPaymentMethod("Maestro");}
		else 
		{
			if(objDepositPage.verifyAddNewMethodButton()){
				objDepositPage.clickAddNewMethodButton();}
			objDepositPage.verifyAddNewPaymentMethodScreen();
			this.select_payment_method("card");
			this.enter_Card_number_as("4462 0300 0000 0000");
			this.select_Exp_Month_as_and_Year_as("01", "2022");
			this.enter_CVN_as("747");
			this.click_On_Add_Card();
		}
	}

	
	@And("^Verify \"([^\"]*)\" cta on screen$")
	public void verify_cta_on_screen(String btnName) {
		objDepositPage.verifyButton(btnName);
	}
	*/
	
	@Then("^Enter Deposit amount as \"([^\"]*)\" and click on Next$")
	public void enter_Deposit_amount_as_and_click_on_Next(String inpDepAmt){
		depAmt = inpDepAmt;
		objMyAccountPage.setDepositAmt(inpDepAmt);
		objMyAccountPage.clickOnNext();
	}

	@Then("^Verify 'Add new payment Method' screen is displayed$")
	public void verify_Add_new_payment_Method_screen_is_displayed(){
	    // Write code here that turns the phrase above into concrete actions
		objDepositPage.switchToDepositIframe();
		if(objDepositPage.verifyAddNewMethodButton()){
			objDepositPage.clickAddNewMethodButton();}
		objDepositPage.verifyAddNewPaymentMethodScreen();
	}

	@Then("^Select \"([^\"]*)\" payment method$")
	public void select_payment_method(String arg1){
		objDepositPage.selectDepositPaymentMethod(arg1);   
	}

	@Then("^Verify following errors are displayed when fields are empty$")
	public void verify_following_errors_are_displayed_when_fields_are_empty(DataTable dt){
		
		objDepositPage.switchToCardDepositIframe();
		objDepositPage.ClickAddCardButton();
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objDepositPage.verifyErrormessage(list.get(i));}
	}
	
	@Then("^Enter Card number \"([^\"]*)\"$")
	public void enter_Card_number(String cardNumber){
		//objDepositPage.switchToCardDepositIframe();
		objDepositPage.setCardNumber(cardNumber);
	}
	
	@Then("^Enter Card number as \"([^\"]*)\"$")
	public void enter_Card_number_as(String cardNumber){
		objDepositPage.switchToCardDepositIframe();
		objDepositPage.setCardNumber(cardNumber);
	}

	@Then("^Select Exp Month as \"([^\"]*)\" and Year as \"([^\"]*)\"$")
	public void select_Exp_Month_as_and_Year_as(String expMon, String expYr) {
		objDepositPage.setExpMonth(expMon);
		objDepositPage.setExpYear(expYr);
	}

	@Then("^Enter CVN as \"([^\"]*)\"$")
	public void enter_CVN_as(String securityCodeCVV) {
		objDepositPage.setCVVcode(securityCodeCVV);
	}
	
	@Then("^Again Enter CVN as \"([^\"]*)\"$")
	public void again_enter_CVN_as(String securityCodeCVV) {
		objDepositPage.switchToCVNiframe();
		objDepositPage.setCVncode(securityCodeCVV);
	}
	@Then("^Click On 'Add Card'$")
	public void click_On_Add_Card() {
		objDepositPage.ClickAddCardButton();
		objDepositPage.switchToDepositIframe();
	}
	 
	@And("^Click on Deposit button$")
	public void click_On_Deposit() {
		objDepositPage.clickOnDeposit();
		
	}
	
	@Then("^Verify Deposit Successful confirmation message$")
	public void verify_Deposit_Successful_confirmation_message() {
		objDepositPage.switchToDepositIframe();
	//	objDepositPage.verifyDepositConfirmation();
		objWithdrawPage.verifyYourWithdrawalSuccessMessage("Your deposit was successful.");
	//	objDepositPage.verifyDepositAmountIsDisplayedCorrectlyOrNot(depAmt);
	//	objDepositPage.verifyCurrentBalanceOnSuccessScreen(balance, depAmt);
		objDepositPage.verifyAndCloseButton();
	}
	
	@Then("^Verify Bonus recieved amount is displayed on Deposit confirmation message$")
	public void verify_Bonus_recieved_amount_is_displayed_on_Deposit_confirmation_message() {
		objDepositPage.verifyDepositConfirmation();
		objWithdrawPage.verifyYourWithdrawalSuccessMessage("Your deposit was successful.");
	//	objDepositPage.verifyDepositAmountIsDisplayedCorrectlyOrNot(depAmt);
	//	objDepositPage.verifyCurrentBalanceOnSuccessScreen(balance, depAmt);
		//receivedBonusAmount = objDepositPage.verifyBonusReceived();
		//objDepositPage.verifyPlayNowButton();
	}
	
	@Then("^Verify Received Bonus amount is displayed under playable balance$")
	public void verify_Received_Bonus_amount_is_displayed_under_playable_balance() {
		objDepositPage.clickExpandBalanceBTN();
		objDepositPage.verifyBonusBalanceDisplayedCorrectly(initalbonuseBalance, receivedBonusAmount);
	}
	
	@And("^Get current playable balance$")
	public void get_current_playable_balance() {
		balance =  objDepositPage.getPlayableBalanceAmount();
		balance= balance.substring(1);
		System.out.println("***** balance " +balance);
	}
	
	@And("^Get current balance from Deposit screen$")
	public void get_current_balance_from_Deposit_screen() {
		objDepositPage.switchToDepositIframe();
		balance =  objDepositPage.getBalance();
		System.out.println("***** balance " +balance);
	}
	@And("^Get player bonus balance$")
	public void get_current_player_bonus_balance() {
		objDepositPage.clickExpandBalanceBTN();
		objDepositPage.verifyLabelsOnMyAccount("Cash,Bonuses");
		initalbonuseBalance =  objDepositPage.getValueofLabelOnDetailedView("Bonuses");
		System.out.println("***** initalbonuseBalance " +initalbonuseBalance);
	}
	
	@Then("^Verify Deposit amount is credited to account$")
	public void verify_Deposit_amount_is_credited_to_account() {
		
	//	objDepositPage.validateBalanceDisplayed(balance);
		objDepositPage.validateBalanceAfterDeposit(balance, depAmt);
	}
	
	@Then("^Choose \"([^\"]*)\" option$")
	public void choose_option(String arg1){
		objDepositPage.selectBonusOption(arg1);
	}
	
	@Then("^Select payment method$")
	public void select_payment_method(){
		objDepositPage.switchToDepositIframe();
		if(objDepositPage.verifyAddedMethoddisplayedOrNot())
		{
			objDepositPage.selectAddedPaymentMethod("Maestro");}
		else 
		{
			if(objDepositPage.verifyAddNewMethodButton()){
				objDepositPage.clickAddNewMethodButton();}
			objDepositPage.verifyAddNewPaymentMethodScreen();
			this.select_payment_method("card");
			this.enter_Card_number_as("4462 0300 0000 0000");
			this.select_Exp_Month_as_and_Year_as("01", "2022");
			this.enter_CVN_as("747");
			this.click_On_Add_Card();
		}
	}
	
	@And("^Select bonus from the list$")
	public void select_bonus_from_the_list() {
		objDepositPage.selectBonusFromList();
	}
	
	@Then("^Click on \"([^\"]*)\" button$")
	public void click_on_button(String btnName) {
		objDepositPage.clickOnButton(btnName);
	}
	
	@And("^Verify \"([^\"]*)\" cta on screen$")
	public void verify_cta_on_screen(String btnName) {
		objDepositPage.verifyButton(btnName);
	}
	@Then("^Verify POPF checkbox is displayed to the customer$")
	public void verify_POPF_checkbox_is_displayed_to_the_custome() {
			objDepositPage.verifyPOPFCheckbox();
	}
	
	@Then("^Verify 'Your bonus' section is displayed$")
	public void verify_Your_bonus_section_is_displayed(){
		objDepositPage.verifyYourbonusIsDisplayedOnDepsoitScreen();
	}
	
	@Then("^Again Enter Deposit Amount as \"([^\"]*)\"$")
	public void again_Enter_Deposit_Amount_as(String arg1) {
		depAmt = arg1 ;
		objDepositPage.setDepositAmount(arg1);
		if(objDepositPage.verifyAcknowledgeCheckbox())
			objDepositPage.selectAcknowledgeCheckbox();
	}
	
	@Then("^Verify it redirect to \"([^\"]*)\" window$")
	public void verify_it_redirect_to_window(String title)  {
		//objDepositPage.verifyPageTitle(title);
	}

	@Then("^Enter Pin number \"([^\"]*)\" and Accept TnC$")
	public void enter_Pin_number_and_Accept_TnC(String pin) {
		
		objDepositPage.switchToPaysafeIframe();
		objDepositPage.setPinForPaySafeCard(pin);
		objDepositPage.selectAcceptTermsCheckboxForPaySafeCard();
	}

	@Then("^Click on Pay$")
	public void click_on_Pay(){
		objDepositPage.clickPayButtonOnPaySafeCard();
		objDepositPage.switchToDepositIframe();
	}
	
	@Then("^Enter email address \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void enter_email_address_and_Password(String emailAddress, String password){
		objDepositPage.setPayPalEmailAddress(emailAddress);
		if(objDepositPage.verifyNext())
		{
			objDepositPage.clickNext();
		}
		objDepositPage.setPayPalPassword(password);
	}

	@Then("^Click on Login from Paypal screen$")
	public void click_on_Login_from_Paypal_screen(){
		objDepositPage.clickLoginButtonOnPaypal();;
	}

	@Then("^Click on 'Pay Now' button$")
	public void click_on_Pay_Now_button(){
		objDepositPage.clickPayNowButtonOnPaypal();
	//	objWebActions.switchToDefaultContent();
		objDepositPage.switchToDepositIframe();
	}
	
}

