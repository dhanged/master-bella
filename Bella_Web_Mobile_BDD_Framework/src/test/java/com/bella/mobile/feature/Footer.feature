Feature: Footer Links

@Android
Scenario: Check that system displays the footer links in the footer section of all the page(Tab)
Given Invoke the Bella site
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section
Then Navigate to "Slots" tab
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section
Then Navigate to "Jackpots" tab
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section
Then Navigate to "Promos" tab
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section
Then Navigate to "Casino" tab
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section

@Android
Scenario: Check that system displays appropriate page on clicking the links from the footer section
Given Invoke the Bella site
Then Verify 'Userful Links' and 'Help' sections is displayed in footer section
And Click on different links and verify system redirects to the correct page