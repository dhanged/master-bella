package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class FooterPage {
	private MobileActions objMobileActions ;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private AppiumDriverProvider objDriverProvider;

	public FooterPage(MobileActions objMobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,AppiumDriverProvider objDriverProvider) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
		this.objDriverProvider = objDriverProvider;
	}
	//Xpath
	private By linkUKGamblingCommission = By.xpath("//a[text()='The UK Gambling Commission']");
	private By linkAlderneyGamblingControlCommission = By.xpath("//a[text()='The Alderney Gambling Control Commission'][@href='http://www.gamblingcontrol.org/']");
	private By logoUKGamblingCommission = By.xpath("//a[contains(@href,'http://www.gamblingcommission.gov.uk/home.aspx')]/img[contains(@src,'/licensing/logo-gamblingcommision.png')]");
	private By logoAlderneyGamblingControlCommission = By.xpath("//a[contains(@href,'https://www.gamblingcontrol.org/')]/img[contains(@src,'/licensing/logo-alderney.png')]");
	private By logoGameCare = By.xpath("//a[contains(@href,'https://www.gamcare.org.uk/')]/img[contains(@src,'/licensing/gamcare.png')]");
	private By logoGamStop = By.xpath("//a[contains(@href,'https://www.gamstop.co.uk/')]/img[contains(@src,'/licensing/gam-stop_rgb_small.png')]");
	private By logo18 = By.xpath("//img[@alt='Grosvenor Casinos logo 18']//ancestor::a[@target='_blank']");
	private By logoPaypal = By.xpath("//img[contains(@src,'/payment-methods/logo-paypal.png')]");
	private By logoMastercard = By.xpath("//img[contains(@src,'/payment-methods/logo-mastercard.png')]");
	private By logoVisa = By.xpath("//img[contains(@src,'/payment-methods/logo-visa.png')]");
	private By logoPaysafeCard = By.xpath("//img[contains(@src,'/payment-methods/logo-paysafecard.png')]");
	private By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]/img[contains(@src,'/rank-group/logos/logo-keepitfun.png')]");

	//Useful Links
	private By linkPlaySlots = By.xpath("//a[text()='Play Slots'][@href='/slots-and-games/all-slots']");
	private By linkCasino = By.xpath("//a[text()='Casino'][@href='/casino']");
	private By linkGrosvenorCasino = By.xpath("//a[text()='Grosvenor Casinos']");
	private By linkMeccaBingo = By.xpath("//a[text()='Mecca Bingo'][@href='https://www.meccabingo.com/']");

	//Help Links
	private By linkResponsibleGambling = By.xpath("//a[text()='Responsible Gambling'][@href='https://keepitfun.rank.com/']");
	private By linkTnC = By.xpath("//a[text()='Terms & Conditions'][@href='/terms-and-conditions']");
	private By linkPrivacyPolicy = By.xpath("//a[text()='Privacy Policy'][@href='/privacy-policy#privacy']");
	private By linkSupport = By.xpath("//a[text()='Support'][@href='/support']");
	private By linkFAQs = By.xpath("//a[text()='FAQs'][@href='/faqs']");
	By locator = By.xpath("//span[text()='You’re in safe hands']");

	/*public void verifyUKGamblingCommissionLinkDisplayed() {
		logReporter.log("Verify 'UK Gambling Commission Link' displayed", 
				objMobileActions.checkElementDisplayed(linkUKGamblingCommission));
	}

	public void verifyAlderneyGamblingControlCommissionLinkDisplayed() {
		logReporter.log("Verify 'Alderney Gambling Control Commission link' displayed", 
				objMobileActions.checkElementDisplayed(linkAlderneyGamblingControlCommission));
	}

	public void clickUKGamblingCommissionLink() {
		String url = "https://secure.gamblingcommission.gov.uk/PublicRegister/Search/Detail/38750";
		logReporter.log("Click 'UK Gambling Commission' Link", 
				objMobileActions.click(linkUKGamblingCommission));
		objMobileActions.switchToChildWindow();
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));

	}

	public void verifyUKGamblingCommissionLogoDisplayed() {
		logReporter.log("Verify 'UK Gambling Commission Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoUKGamblingCommission));
	}

	public void clickUKGamblingCommissionLogo() {
		String url = "http://www.gamblingcommission.gov.uk/home.aspx";
		logReporter.log("Click 'UK Gambling Commission' Logo", 
				objMobileActions.click(logoUKGamblingCommission));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));

	}

	public void verifyAlderneyGamblingControlCommissionLogoDisplayed() {
		logReporter.log("Verify 'Alderney Gambling Control Commission Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoAlderneyGamblingControlCommission));
	}

	public void clickAlderneyGamblingControlCommissionLogo() {
		String url = "https://www.gamblingcontrol.org/";
		logReporter.log("Click 'Alderney Gambling Control Commission Logo'", 
				objMobileActions.click(logoAlderneyGamblingControlCommission));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyGameCareLogoDisplayed() {
		logReporter.log("Verify 'GameCare Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoGameCare));
	}

	public void clickGameCareLogo() {
		String url = "https://www.gamcare.org.uk/";
		logReporter.log("Click 'GameCare Logo'", 
				objMobileActions.click(logoGameCare));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyGamStopLogoDisplayed() {
		logReporter.log("Verify 'GamStop Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoGamStop));
	}

	public void clickGamStopLogo() {
		String url = "https://www.gamstop.co.uk/";
		logReporter.log("Click 'Gamstop Logo'", 
				objMobileActions.click(logoGamStop));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verify18LogoDisplayed() {
		logReporter.log("Verify '18+ Logo' displayed", 
				objMobileActions.checkElementDisplayed(logo18));
	}

	public void verifyPaymentMethodLogoDisplayed() {
		logReporter.log("Verify 'Paypal Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoPaypal));
		logReporter.log("Verify 'Master Card Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoMastercard));
		logReporter.log("Verify 'Visa Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoVisa));
		logReporter.log("Verify 'PaySafe Card Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoPaysafeCard));
	}

	public void verifyKeepItFunDisplayed() {
		logReporter.log("Verify 'Keep It Fun Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoKeepItFun));

	}

	public void clickKeepItFunLogo() {
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Keep It Fun Logo'", 
				objMobileActions.click(logoKeepItFun));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyUsefulLinksDisplayed() {
		logReporter.log("Verify 'Play Slots link' displayed", 
				objMobileActions.checkElementDisplayed(linkPlaySlots));
		logReporter.log("Verify 'Casino link' displayed", 
				objMobileActions.checkElementDisplayed(linkCasino));
		logReporter.log("Verify 'Grosvenor Casino link' displayed", 
				objMobileActions.checkElementDisplayed(linkGrosvenorCasino));
		logReporter.log("Verify 'Mecca Bingo link' displayed", 
				objMobileActions.checkElementDisplayed(linkMeccaBingo));

	}

	public void verifyHelpLinksDisplayed() {
		logReporter.log("Verify 'Responsible Gambling link' displayed", 
				objMobileActions.checkElementDisplayed(linkResponsibleGambling));
		logReporter.log("Verify 'Terms and Condition link' displayed", 
				objMobileActions.checkElementDisplayed(linkTnC));
		logReporter.log("Verify 'Privacy Policy link' displayed", 
				objMobileActions.checkElementDisplayed(linkPrivacyPolicy));
		logReporter.log("Verify 'Support link' displayed", 
				objMobileActions.checkElementDisplayed(linkSupport));
		logReporter.log("Verify 'FAQs link' displayed", 
				objMobileActions.checkElementDisplayed(linkFAQs));
	}

	public void verifyURLRedirection(String expectedurl) {

		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		System.out.println("currentURL "+currentURL);
		System.out.println("currentURL "+currentURL+" exp "+expectedurl);
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(expectedurl));
	}

	public void clickOnFooterLinks(String sectionName,String lnk)
	{
		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				String[] lnks = labels2.split(",");
				System.out.println("vlnks :: "+lnks);
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnks[0]+"']");

				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				logReporter.log(" click on  '"+lnks[0]+"'  link is displayed under "+sectionName+" footer section",  
						objMobileActions.clickUsingJS(locator));
				verifyURLRedirection(lnks[1]);
			}
		}
		else
		{

			if(lnk.contains(","))
			{
				String[] arr1 = lnk.split(",");


				By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+arr1[0]+"']");
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				logReporter.log("click on  " +arr1[0]+ "link is displayed under "+sectionName+"section ", 
						objMobileActions.clickUsingJS(lnks));
				verifyURLRedirection(arr1[1]);

			}
			else
			{
				By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				logReporter.log("click on  " +lnk+ "link is displayed under "+sectionName+"section ", 
						objMobileActions.clickUsingJS(lnks));
			}
		}

	}

	public void verifyFooterLinks(String sectionName,String lnk)
	{

		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+labels2+"']");

				logReporter.log(" Verify link : '"+labels2+"' is displayed under "+sectionName+" footer section",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
			logReporter.log("verify  " +lnk+ "under "+sectionName+" section ", 
					objMobileActions.checkElementDisplayed(lnks));
		}
	}

	public void clickResponsibleGamblingLink() {
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Responsible Gambling Link'", 
				objMobileActions.click(linkResponsibleGambling));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}
	 */
	public void verifyAssociatedLicenseStatement()
	{
		By locator = By.xpath("//span[text()='Licence']//following-sibling::div/p[contains(text(),'Licensed and regulated by' )and contains(., ' for customers in Great Britain playing on our online sites or at our land based casinos. Licensed by') and contains(., 'for non-GB customers playing at our online sites.') and contains(., 'more fun if you play responsibly.')and contains(., '® and the Bella Casino logo are registered trademarks of ') and contains(., ' All rights reserved.')]");
		logReporter.log("verify Associated license statements ", 
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyLogoInFooterSection(String logoName)
	{
		switch(logoName)
		{
		case "UK Gambling Commission":
			this.verifyUKGamblingCommissionLogoDisplayed();
			break;
		case "Gamcare":
			this.verifyGameCareLogoDisplayed();
			break;
		case "GameStop":
			this.verifyGamStopLogoDisplayed();
			break;
		case "Keep it fun":
			this.verifyKeepItFunDisplayed();
			break;
		case "Payment Methods":
			this.verifyPaymentMethodLogoDisplayed();
			break;
		case "Alderney Gambling Control Commission":
			this.verifyAlderneyGamblingControlCommissionLogoDisplayed();
			break;
		case "18+":
			this.verify18LogoDisplayed();
			break;
		}
	}

	public void verifyLogoLinkRedirectToTheCorrectPage(String logoName)
	{
		switch(logoName)
		{
		case "UK Gambling Commission":
			this.clickUKGamblingCommissionLogo();
			break;
		case "Gamcare":
			this.clickGameCareLogo();
			break;
		case "GameStop":
			this.clickGamStopLogo();
			break;
		case "Keep it fun":
			this.clickKeepItFunLogo();
			break;
		case "Alderney Gambling Control Commission":
			this.clickAlderneyGamblingControlCommissionLogo();
			break;
		}
	}
	public void verifyUKGamblingCommissionLinkDisplayed() {
		logReporter.log("Verify 'UK Gambling Commission Link' displayed", 
				objMobileActions.checkElementDisplayed(linkUKGamblingCommission));
	}

	public void verifyAlderneyGamblingControlCommissionLinkDisplayed() {
		logReporter.log("Verify 'Alderney Gambling Control Commission link' displayed", 
				objMobileActions.checkElementDisplayed(linkAlderneyGamblingControlCommission));
	}

	public void clickUKGamblingCommissionLink() {

		objMobileActions.androidScrollToElement(locator);
		//String url = "https://secure.gamblingcommission.gov.uk/PublicRegister/Search/Detail/38750";
		String url = "https://beta.gamblingcommission.gov.uk/public-register/business/detail/38750";
		
		logReporter.log("Click 'UK Gambling Commission' Link", 
				objMobileActions.click(linkUKGamblingCommission));

		verifyRedirection(url);
	}

	public void verifyRedirection(String url)
	{
		if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
		{objMobileActions.acceptAlertiOS();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));}
		else
		{	objMobileActions.switchToChildWindow();}
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyURLRedirection(String expectedurl) {
		if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
		{objMobileActions.acceptAlertiOS();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		}
		//else
		{
			//objMobileActions.switchToChildWindow();
		}
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		System.out.println("currentURL "+currentURL);
		System.out.println("currentURL "+currentURL+" exp "+expectedurl);
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(expectedurl));
	}
	public void verifyUKGamblingCommissionLogoDisplayed() {
		logReporter.log("Verify 'UK Gambling Commission Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoUKGamblingCommission));
	}

	public void clickUKGamblingCommissionLogo() {
		objMobileActions.androidScrollToElement(locator);
		String url = "http://www.gamblingcommission.gov.uk/home.aspx";
		logReporter.log("Click 'UK Gambling Commission' Logo", 
				objMobileActions.click(logoUKGamblingCommission));
		/*objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
		verifyRedirection(url);

	}

	public void verifyAlderneyGamblingControlCommissionLogoDisplayed() {
		logReporter.log("Verify 'Alderney Gambling Control Commission Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoAlderneyGamblingControlCommission));
	}

	public void clickAlderneyGamblingControlCommissionLogo() {
		objMobileActions.androidScrollToElement(locator);
		String url = "https://www.gamblingcontrol.org/";
		logReporter.log("Click 'Alderney Gambling Control Commission Logo'", 
				objMobileActions.click(logoAlderneyGamblingControlCommission));
		/*		objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
		verifyRedirection(url);
	}

	public void verifyGameCareLogoDisplayed() {

		logReporter.log("Verify 'GameCare Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoGameCare));
	}

	public void clickGameCareLogo() {
		objMobileActions.androidScrollToElement(locator);
		String url = "https://www.gamcare.org.uk/";
		logReporter.log("Click 'GameCare Logo'", 
				objMobileActions.click(logoGameCare));
		/*	objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
		verifyRedirection(url);
	}

	public void verifyGamStopLogoDisplayed() {
		logReporter.log("Verify 'GamStop Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoGamStop));
		objMobileActions.androidScrollToElement(locator);
	}

	public void clickGamStopLogo() {
		String url = "https://www.gamstop.co.uk/";
		logReporter.log("Click 'Gamstop Logo'", 
				objMobileActions.click(logoGamStop));
		/*	objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
		verifyRedirection(url);
	}

	public void verify18LogoDisplayed() {
		logReporter.log("Verify '18+ Logo' displayed", 
				objMobileActions.checkElementDisplayed(logo18));
		objMobileActions.androidScrollToElement(locator);
	}

	public void verifyPaymentMethodLogoDisplayed() {
		objMobileActions.androidScrollToElement(locator);
		logReporter.log("Verify 'Paypal Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoPaypal));
		logReporter.log("Verify 'Master Card Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoMastercard));
		logReporter.log("Verify 'Visa Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoVisa));
		logReporter.log("Verify 'PaySafe Card Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoPaysafeCard));
	}

	public void verifyKeepItFunDisplayed() {
		logReporter.log("Verify 'Keep It Fun Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoKeepItFun));

	}

	public void clickKeepItFunLogo() {
		objMobileActions.androidScrollToElement(linkPlaySlots);
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Keep It Fun Logo'", 
				objMobileActions.click(logoKeepItFun));
		/*objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
	}

	public void verifyUsefulLinksDisplayed() {
		logReporter.log("Verify 'Play Slots link' displayed", 
				objMobileActions.checkElementDisplayed(linkPlaySlots));
		logReporter.log("Verify 'Casino link' displayed", 
				objMobileActions.checkElementDisplayed(linkCasino));
		logReporter.log("Verify 'Grosvenor Casino link' displayed", 
				objMobileActions.checkElementDisplayed(linkGrosvenorCasino));
		logReporter.log("Verify 'Mecca Bingo link' displayed", 
				objMobileActions.checkElementDisplayed(linkMeccaBingo));

	}

	public void verifyHelpLinksDisplayed() {
		logReporter.log("Verify 'Responsible Gambling link' displayed", 
				objMobileActions.checkElementDisplayed(linkResponsibleGambling));
		logReporter.log("Verify 'Terms and Condition link' displayed", 
				objMobileActions.checkElementDisplayed(linkTnC));
		logReporter.log("Verify 'Privacy Policy link' displayed", 
				objMobileActions.checkElementDisplayed(linkPrivacyPolicy));
		logReporter.log("Verify 'Support link' displayed", 
				objMobileActions.checkElementDisplayed(linkSupport));
		logReporter.log("Verify 'FAQs link' displayed", 
				objMobileActions.checkElementDisplayed(linkFAQs));
	}



	public void clickOnFooterLinks(String sectionName,String lnk)
	{
		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				String[] lnks = labels2.split(",");
				System.out.println("vlnks :: "+lnks);
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnks[0]+"']");
				logReporter.log(" click on  '"+lnks[0]+"'  link is displayed under "+sectionName+" footer section",  
						objMobileActions.clickUsingJS(locator));
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				/*if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
				{objMobileActions.acceptAlertiOS();
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				}*/
				//verifyURLRedirection(lnks[1]);
				verifyRedirection(lnks[1]);
			}
		}
		else
		{
			if(lnk.contains(","))
			{
				String[] arr1 = lnk.split(",");


				By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+arr1[0]+"']");
				logReporter.log("click on  " +arr1[0]+ "link is displayed under "+sectionName+"section ", 
						objMobileActions.clickUsingJS(lnks));
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				/*if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
					{objMobileActions.acceptAlertiOS();
					waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
					}*/
				//verifyURLRedirection(arr1[1]);
				verifyRedirection(arr1[1]);
			}
			else
			{
				By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
				logReporter.log("click on  " +lnk+ "link is displayed under "+sectionName+"section ", 
						objMobileActions.clickUsingJS(lnks));
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
				{objMobileActions.acceptAlertiOS();
				waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
				}
			}
		}

	}
	public void verifyFooterLinks(String sectionName,String lnk)
	{

		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+labels2+"']");

				logReporter.log(" Verify link : '"+labels2+"' is displayed under "+sectionName+" footer section",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
			logReporter.log("verify  " +lnk+ "under "+sectionName+" section ", 
					objMobileActions.checkElementDisplayed(lnks));
		}
	}
	public void clickResponsibleGamblingLink() {
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Responsible Gambling Link'", 
				objMobileActions.click(linkResponsibleGambling));
		/*	objMobileActions.switchToChildWindow();
		String currentURL =objPojo.getAppiumDriverProvider().getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));*/
		verifyRedirection(url);
	}
}


