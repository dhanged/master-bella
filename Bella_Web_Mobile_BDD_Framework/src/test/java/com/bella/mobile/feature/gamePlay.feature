Feature: GamePlay

@Android
Scenario:  Game Play - Check whether user can access Terms & Conditions from detailed screen
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on 'I' button for any game
Then Click on Ts & Cs for the selected games.


@Android
Scenario: Check whether system is able to filter games by clicking on Featured games,New,games,Jackpot and All slots Games on slots page
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Slots" tab
Then Verify following tab is displayed under secondary navigation bar
|Featured|
|New|
|Games|
|All Slots|
|Jackpots|
Then select following tabs and verify games is displayed on selected tab
|Featured|
|New|
|Games|
|All Slots|

@Android
Scenario: Check that system redirects to Login window when clicked on Real play mode in log out mode
Given Invoke the Bella site
Then Click on Play button
Then Verify Login Page is displayed