
package com.bella.mobile.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
	
@CucumberOptions(features = "@target/SyncFails.txt", 
		strict = true, plugin = { "json:target/JSON/Mobile_Rerun.json", "com.generic.cucumberReporting.CustomFormatter"}, 		
		monochrome = true,
		glue = { "com.hooks", "com.meccabingo.android.stepDefinition" }
)

public class AndroidRunner_Rerun extends AbstractTestNGCucumberTests {
}
