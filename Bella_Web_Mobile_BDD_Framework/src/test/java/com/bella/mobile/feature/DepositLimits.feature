Feature: Deposit limits

@Android21
Scenario: Check that the user can deposit the amount less than or equal to the deposit limit set up amount
Given Invoke the Bella site
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
And Select DepositLimit as "Yes"
Then Set Deposit "Daily" limit as "250"
Then Set Deposit "Weekly" limit as "550"
Then Set Deposit "Monthly" limit as "980"
Then Click on Register
Then Verify Deposit Now header
And Get current balance from Deposit screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "Card" payment method
Then Enter Card number as "6771 8309 9999 1239"
And Select Exp Month as "01" and Year as "2022"
And Enter CVN as "737"
And Click On 'Add Card'
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "100"
And Again Enter CVN as "737"
And Click on "Deposit" button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account

@Android21
Scenario: Check that the customer can set up the valid Deposit Limit while Registration
Given Invoke the Bella site
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
And Select DepositLimit as "Yes"
Then Set Deposit "Daily" limit as "1"
Then Verify "The deposit value must be greater than £5" error message
Then Set Deposit "Daily" limit as "90"
Then Set Deposit "Weekly" limit as "50"
Then Set Deposit "Monthly" limit as "3"
Then Click on Register
Then Verify below error message
|The daily limit must be less than the weekly limit|
|The weekly limit must be less than the monthly limit|
|The daily limit must be less than the monthly limit|
|The deposit value must be greater than £5|
Then Set Deposit "Daily" limit as "269"
Then Set Deposit "Weekly" limit as "590"
Then Set Deposit "Monthly" limit as "885"
Then Click on Register
Then Verify Deposit Now header


@Android2
Scenario: Check that system allows to override the deposit limits for the user if the deposit limit entered is Lower than Previous deposit limit
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen
Then User will see the below information about deposit limit
|We offer a range of tools to help you stay in control of your gaming including the option to set a Net Deposit Loss limit|
|Please note, this is a shared limit |
|If you would like to set a Net Deposit Loss Limit, please select continue or contact our support team to assist you.|
|*Limit applies across the following partner sites: KittyBingo.com, LuckyPantsBingo.com, SpinandWin.com, MagicalVegas.com, RegalWins.com, LuckyVIP.com, KingJackCasino.com, Aspers.com|
And Verify "Cancel" cta on screen
Then Click on "Continue" button
Then User will get the "About Responsible Gaming" screen
When User clicks on the edit "daily" limit button 
Then User will get the "Set Deposit Limit" screen with following fields
|Deposit limit type |
|Deposit amount |
Then Enter deposit limit Lower than Previous deposit limit
And Click on "Set new deposit limit" button
Then User will get a message "Your Deposit Limit request has been successfully processed. Please note, in-line with regulations, any decrease of limits will happen immediately, but any increase in limits will have to be re-confirmed after 24 hours."
Then Close My account screen
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen
Then Click on "Continue" button
And Verify updated deposit limit is displayed

@Android2
Scenario: Check that system displays error message if the Daily limit is more than weekly and weekly is greater than monthly.
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen
Then Click on "Continue" button
When User clicks on the edit "daily" limit button
Then User set "Daily" limit as "1"
And Click on "Set new deposit limit" button
Then User will get error message "Deposit limit should be £5 or higher" 
Then User set "Daily" limit as "2500"
And Click on "Set new deposit limit" button
Then User will get error message "Your daily limit cannot be more than your weekly or monthly limits."
Then User set weekly limit greater than monthly
And Click on "Set new deposit limit" button
And User will get error message "Your weekly limit cannot be more than your monthly limit or less than your daily limit."


@Android2
Scenario: Check that system displays an error message on trying to deposit an amount greater than the maximum deposit limit set by the customer
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen
Then Click on "Continue" button
When Get "daily" limit value
Then Close My account screen
Then Click on my account button
Then Navigate to "Deposit" tab from my account screen
Then Select payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Enter deposit amount greater than deposit limit set by the customer 
And Click on Deposit button
#And Again Enter CVN as "737"
#And Click on Deposit button
And User will get message "You have reached your deposit limits. Please contact our customer service team to request a limit refresh."

@Android2
Scenario: Check that system allows to set deposit limits higher than previously set limits
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen
Then Click on "Continue" button
Then User will get the "About Responsible Gaming" screen
When User clicks on the edit "daily" limit button 
Then Enter deposit limit higher than Previous deposit limit
And Click on "Set new deposit limit" button
Then User will get a message "Your Deposit Limit request has been successfully processed. Please note, in-line with regulations, any decrease of limits will happen immediately, but any increase in limits will have to be re-confirmed after 24 hours."
Then Close My account screen
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the " Deposit Limit Request" screen
Then New deposit limit value will be display in the pending
