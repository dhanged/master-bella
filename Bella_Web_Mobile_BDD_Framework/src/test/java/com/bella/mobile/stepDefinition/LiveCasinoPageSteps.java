package com.bella.mobile.stepDefinition;


import com.bella.mobile.page.Bella.LiveCasinoPage;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class LiveCasinoPageSteps {
	private LiveCasinoPage objLiveCasinoPage ;
	private Utilities objUtilities;
	private Configuration configuration;
	
	public LiveCasinoPageSteps(LiveCasinoPage objLiveCasinoPage,Utilities objUtilities,Configuration configuration) {		
		this.objLiveCasinoPage = objLiveCasinoPage;
		this.objUtilities = objUtilities;
		this.configuration= configuration;
	}
	
	@And("^Verify Live Casino Games displaying to user$")
	public void verify_Live_Casino_Games_displaying_to_user() {
		objLiveCasinoPage.verifyTopPicksGamesDisplayed();
		objLiveCasinoPage.verifyRouletteGamesDisplayed();
		objLiveCasinoPage.verifyBlackjackGamesDisplayed();	
	}
	
	@Then("^Verify System displays the Live roulette, Blackjack type on Live casinos landing page$")
	public void verify_System_displays_the_Live_roulette_Blackjack_type_on_Live_casinos_landing_page(){
		objLiveCasinoPage.verifyRouletteGamesDisplayed();
		objLiveCasinoPage.verifyBlackjackGamesDisplayed();	
	}
	
	@Then("^Select any sub Category say Roulette$")
	public void select_any_sub_Category_say_Roulette() {
		objLiveCasinoPage.verifyRouletteGamesDisplayed();
	}

	@Then("^Click on I button from Image$")
	public void click_on_I_button_from_Image(){
		objLiveCasinoPage.verifyRouletteGameInfoButtonDisplayed();
		objLiveCasinoPage.clickRouletteGameInfoButton();	
	}

	@Then("^System displays Game Details page with Join button$")
	public void system_displays_Game_Details_page_with_Join_button(){
		objLiveCasinoPage.verifyGameDetailsPageDisplayed();	
		objLiveCasinoPage.verifyJoinButtonDisplayed();	
	}

}
