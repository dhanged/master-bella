package com.bella.desktop.page.Bella;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class RegistrationPage {

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public RegistrationPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	private By inpEmail = By.xpath("//input[@type='email' and @placeholder='Email address']");
	private By inpUserName = By.id("joinnow-username");
	private By inpPassword = By.id("joinnow-password");
	private By inpFirstName = By.id("joinnow-firstname");
	private By inpSurName = By.id("joinnow-surname");
	private By inpMobileNumebr = By.id("joinnow-mobile");
	By inputaddressLine1 = By.id("joinnow-addressLine1");
	By inputaddressLine2 = By.id("joinnow-addressLine2");
	By inputtownCity = By.id("joinnow-townCity");
	By inputcounty = By.id("joinnow-county");
	By inputpostcode = By.id("joinnow-postcode");

	// Button 
	private By btnNext = By.xpath("//button[@type='submit' and text()='Next']");
	private By btnYesNewUser = By.xpath("//p[text()='New to Grosvenor casinos?']/following-sibling::p/button[text()='Yes']");
	private By btnDepositLimitYes = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'Yes')]"); 
	private By btnDepositLimitNo = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'No')]");
	private By btnRegister = By.xpath("//button[@type='submit' and contains(text(),'Register')]");
	private By btnSignUpClose = By.xpath("//div[@class='header']/header/a[@class='close' and contains(text(),'Close')]");
	private By chkJoinnowAgree = By.xpath("//input[@type='checkbox' and @name='joinnow-agree']");
	private By hdrSignUp = By.xpath("//h1[text()='JOIN NOW']");
	private By drpCountry = By.xpath("//select[@id='joinnow-country']");
	By lnkForManualAddress = By.xpath("//a[contains(text(),'Or enter it manually')]");
	//By lnkForManualAddress = By.xpath("//a[contains(text(),'Enter Address manually')]");
	private By lnkLiveHelp = By.xpath("//a[text()='Live Help']");
	
	public void selectNewUserYes() {
		logReporter.log("Click 'Yes' new user", 
				objWebActions.click(btnYesNewUser));
	}

	public void verifySignUpHeaderDisplayed(){
		logReporter.log("Verify 'JOIN NOW' header displayed.", 
				objWebActions.checkElementDisplayed(hdrSignUp));
	}

	public void setEmailAddress(String email){
		logReporter.log("Set Email Address", email, 
				objWebActions.setTextWithClear(inpEmail, email));

		/*objWebActions.set(inpEmail, email);*/
		objWebActions.pressKeybordKeys(inpEmail, "tab");
	}

	public void selectJoinnowAgreeCheckbox() {
		logReporter.log("Select Joinnow Agree Checkbox",  
				objWebActions.selectCheckbox(chkJoinnowAgree, true));
	}

	public void verifyJoinNowAgreeTnCDisplayed()
	{
		By loctaor = By.xpath("//div[@class='info-list']//p//span[contains(.,'I agree that I am at least 18 years old, that I agree with the')]");
		logReporter.log("Verify 'I agree that I am at least 18 years old, that I agree with the'  displayed.", 
				objWebActions.checkElementDisplayed(loctaor));
		this.verifyLinksDisplayedUnderIAgreeTncSection("privacy policy");
		this.verifyLinksDisplayedUnderIAgreeTncSection("terms and conditions");
	}

	public void verifyLinksDisplayedUnderIAgreeTncSection(String lnk)
	{
		By locator = By.xpath("//div[@class='info-list']//p//a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify "+lnk+ " displayed under terms n condition section ", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void clickNext() {
		logReporter.log("Click 'Next' button", 
				objWebActions.click(btnNext));
	}	

	public void setUserName(String userName){
		logReporter.log("Set user name", userName, 
				objWebActions.setTextWithClear(inpUserName, userName));
		objWebActions.pressKeybordKeys(inpUserName, "tab");
	}

	public void setPassword(String password){
		logReporter.log("Set password", password, 
				objWebActions.setTextWithClear(inpPassword, password));
		objWebActions.pressKeybordKeys(inpPassword, "tab");
	}

	public static String getRandomTitleForUser() {
		String[] arrTitles = {"Mr", "Miss", "Mrs", "Ms"};
		int idx = new Random().nextInt(arrTitles.length);
		return (arrTitles[idx]);
	}
	public void selectTitle(String title) {
		By locator = null;
		switch(title.toLowerCase()){
		case "mr":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mr']");
			break;
		case "miss":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Miss']");
			break;
		case "mrs":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mrs']");
			break;
		case "ms":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Ms']");
			break;
		}
		logReporter.log("Select title", title, 
				objWebActions.click(locator));
	}

	public void verifyTitle(String title)
	{
		By locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='"+title+"']");
		logReporter.log("Verify title display as '" +title+" '", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void setFirstName(String firstName){
		logReporter.log("Set first name", firstName, 
				objWebActions.setTextWithClear(inpFirstName, firstName));
	}

	public void setSurName(String surName){
		logReporter.log("Set sur name", surName, 
				objWebActions.setTextWithClear(inpSurName, surName));
	}

	public void selectDateOfBirth(String dateOfBirth){
		
		String[] arrDateOfBirth = dateOfBirth.split("_");
		String dobDay = arrDateOfBirth[0];
		String dobMonth = arrDateOfBirth[1];
		String dobYear = arrDateOfBirth[2];
		By locator = By.xpath("//input[@placeholder='Date of birth']");

		if(objWebActions.checkElementDisplayed(locator))
		{logReporter.log("Activate DOB fileds",   
				objWebActions.click(locator));}

		locator = By.id("dob-day");
		logReporter.log("Set dob day", dobDay, 
				objWebActions.setTextWithClear(locator, dobDay));

		locator = By.id("dob-month");
		logReporter.log("Set dob month", dobMonth, 
				objWebActions.setTextWithClear(locator, dobMonth));

		locator = By.id("dob-year");
		logReporter.log("Set dob year", dobYear, 
				objWebActions.setTextWithClear(locator, dobYear));
		objWebActions.pressKeybordKeys(locator, "tab");
	}

	public void selectCountry(String country) {
		logReporter.log("Select country", country, 
				objWebActions.selectFromDropDown(drpCountry, country));
	}


	public void verifyenteritmanuallylink()
	{
		logReporter.log("verify ' enter it manually' link",   
				objWebActions.checkElementDisplayed(lnkForManualAddress));
	}

	public void Clickenteritmanuallylink()
	{
		logReporter.log("Select enter it manually",   
				objWebActions.clickUsingJS(lnkForManualAddress));
	}
	public void verifyAddressFieldGetPopuplatedWithData(String data)
	{
		String value;
		if(data.contains("-"))
		{
			String str[] = data.split("-");
			By locator = By.xpath("//label[text()='"+str[0]+"']//preceding::input[1]");
			value = objWebActions.getAttribute(locator, "value");
			if(value.equals("")){
				logReporter.log("Verify '"+str[0]+" ' field is displayed blank ", true);}
			else{
				logReporter.log("Verify '"+str[0]+" ' field is populated with ' "+str[1]+" 'value ",   
						value.equals(str[1]));}
		}
	}

	public void verifyAddressField(String fieldnm)
	{
		By locator = By.xpath("//label[text()='"+fieldnm+"']//preceding::input[1]");
		logReporter.log("Verify '"+fieldnm+" ' field is displayed when enter the address manually ", objWebActions.checkElementDisplayed(locator));
	}

	public void setmanualAddress(String addressLine1, String addressLine2, 
			String townCity, String country, String postCode) 
	{
		//By lnkForManualAddress = By.xpath("//a[contains(text(),'enter it manually')]");
		//By lnkForManualAddress = By.xpath("//a[contains(text(),'Enter Address manually')]");

		/*logReporter.log("Select enter it manually",   
				objWebActions.click(lnkForManualAddress));*/
		//Clickenteritmanuallylink();


		/*if(!objWebActions.checkElementElementDisplayedWithMidWait(inputaddressLine1))
		objWebActions.click(lnkForManualAddress);*/

		logReporter.log("Set address line1", addressLine1, 
				objWebActions.setText(inputaddressLine1, addressLine1));


		logReporter.log("Set address line2", addressLine2, 
				objWebActions.setText(inputaddressLine2, addressLine2));


		logReporter.log("Set Town/City", townCity, 
				objWebActions.setText(inputtownCity, townCity));


		logReporter.log("Set country", country, 
				objWebActions.setText(inputcounty, country));


		logReporter.log("Set post code", postCode, 
				objWebActions.setText(inputpostcode, postCode));
	}

	public void setMobileNumber(String mobileNumber) {
		logReporter.log("Set mobile number", mobileNumber, 
				objWebActions.setText(inpMobileNumebr, mobileNumber));
		objWebActions.pressKeybordKeys(inpMobileNumebr, "tab");
	}

	// set marketingPreferences from input field id attribute of page 
	// available are "email~sms~phone~post~selectAll"
	public void setMarketingPreferences(String marketingPreferences) {

		By mktprefHDR = By.xpath("//p[@class='marketing_preferences_header' and contains(text(),'Marketing preferences')]");

		logReporter.log("Verify Marketing preferences header displayed ",
				objWebActions.checkElementDisplayed(mktprefHDR));
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));

		if(marketingPreferences.equalsIgnoreCase("Select all"))
		{
			//By locator = By.xpath("//fieldset[@class='fieldset-inline']//button[text()='Select all']");
			By locator = By.xpath("//input[@id='selectAll' and @type='checkbox']");

			logReporter.log("Select marketing preference as 'Select all' ",  
					objWebActions.click(locator));
		}
		else if(marketingPreferences.contains("~")) 
		{
			String[] arrMarketingPreferences = marketingPreferences.split("~");
			for(String marketingPreference : arrMarketingPreferences) {
				String mktpref = marketingPreference.toLowerCase();
				By locator = By.xpath("//input[@id='"+mktpref+"' and @type='checkbox']");
				//	By locator = By.xpath("//div[@class='buttonsGroup']//button[text()='"+mktpref+"']");

				logReporter.log("Select marketing preference", marketingPreference,  
						objWebActions.click(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@id='"+marketingPreferences.toLowerCase()+"' and @type='checkbox']");
			//By locator = By.xpath("//div[@class='buttonsGroup']//button[text()='"+marketingPreferences+"']");
			logReporter.log("Select marketing preference", marketingPreferences,  
					objWebActions.click(locator));
		}
	}
	public void verifyMarketingPreferencesCheckboxesIsSelectedOrNot(String checkboxes) 
	{
		System.out.println("**** checkboxes "+checkboxes);

		switch (checkboxes) 
		{
		case "Email":
			System.out.println("**** into checkboxes "+checkboxes);
			By chkboxEmail = By.xpath("//input[@id='email' and @type='checkbox']");
			logReporter.log("email checkbox is selected > >", objWebActions.isCheckBoxSelected(chkboxEmail));
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms checkbox is selected > >", objWebActions.isCheckBoxSelected(chkboxSMS));
			break;
		case "Phone":
			By chkboxphone = By.xpath("//input[@id='phone' and @type='checkbox']");
			logReporter.log("phone checkbox is selected > >", objWebActions.isCheckBoxSelected(chkboxphone));
			break;
		case "Post":
			By chkboxpost = By.xpath("//input[@id='post' and @type='checkbox']");
			logReporter.log("post checkbox is selected > >", objWebActions.isCheckBoxSelected(chkboxpost));
			break;
		}
	}

	public void verifyMarketingPreferencesOption(String checkboxes) 
	{
		switch (checkboxes) 
		{
		case "Email":
			By chkboxEmail = By.xpath("//input[@id='email' and @type='checkbox']");
			logReporter.log("Email as Marketing Preferences Option is displayed > >", objWebActions.checkElementDisplayed(chkboxEmail));
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms as Marketing Preferences Option is displayed  > >", objWebActions.checkElementDisplayed(chkboxSMS));
			break;
		case "Phone":
			By chkboxphone = By.xpath("//input[@id='phone' and @type='checkbox']");
			logReporter.log("phone as Marketing Preferences Option is displayed  > >", objWebActions.checkElementDisplayed(chkboxphone));
			break;
		case "Post":
			By chkboxpost = By.xpath("//input[@id='post' and @type='checkbox']");
			logReporter.log("post as Marketing Preferences Option is displayed  > >", objWebActions.checkElementDisplayed(chkboxpost));
			break;
		case "Select All":
			By locator = By.xpath("//input[@id='selectAll' and @type='checkbox']");
			logReporter.log("'Select All' as Marketing Preferences Option is displayed  > >", objWebActions.checkElementDisplayed(locator));
			break;
		}
	}
	public void setMarketingPreferencesAsIdrathernot() 
	{
		By locator = By.xpath("//input[@id='clearAll'][@type='checkbox']");
		logReporter.log("Select 'I'd rather not ' checkbox",  
				objWebActions.click(locator));
	}
	public void selectDepositLimitYes() {
		logReporter.log("Select deposit limit now 'Yes'",   
				objWebActions.clickUsingJS(btnDepositLimitYes));
	}

	public void selectDepositLimitNo() {
		logReporter.log("Select deposit limit now 'No'",   
				objWebActions.click(btnDepositLimitNo));
	}

	public void clickRegister() {
		logReporter.log("Click register",   
				objWebActions.click(btnRegister));
	}

	public void verifyKYC()
	{
		boolean flag=false;
		int count =0;
		//waitMethods.sleep(2);
		By locator = By.xpath("//div[@id='verificationAge_overlay']//div//div[@class='verificationAge_content']//div//p[contains(.,'We are checking your details and setting up your secure account.')]");	
		//objPojo.getLogReporter().mobileLog(" Verify 'We are checking your details and setting up your secure account' ",
		//	objPojo.getWebDriverProvider().getDriver().findElement(locator).isDisplayed());
		do
		{

			flag = objDriverProvider.getWebDriver().findElement(locator).isDisplayed();
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
			count++;
			System.out.println(flag +  "  "+count);
		}while(!flag);

	}

	public void verifysuccessVerificationMessage()
	{
		By locator = By.xpath("//div[@class='verificationUploadDocs']//div[@class='successVerification_text'][text()='Documents Required']");
		logReporter.log("Verify 'Documents Required ' ",
				objWebActions.checkElementDisplayed(locator));
	}
	public void clickCloseSignUp() {
		if(objWebActions.checkElementDisplayed(btnSignUpClose)) {
			logReporter.log("Close Sign Up Popup",   
					objWebActions.click(btnSignUpClose));
		}
	}

	public void verifyYoumustagreetothetermsandconditionsErrorMessage()
	{
		By errMsg = By.xpath("//div[@class='error-msg']//li[text()='You must agree to the terms and conditions...']");
		logReporter.log("Verify 'You must agree to the terms and conditions... ' ",
				objWebActions.checkElementDisplayed(errMsg));
	}

	public void verifyErrorMessage(String err)
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+" displayed  ",
						objWebActions.checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" displayed  ",
					objWebActions.checkElementDisplayed(errMsg));}

	}

	public void verifyErrorMessageDoesNotDisplay(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+"  not displayed  ",
						objWebActions.checkElementNotDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" not displayed  ",
					objWebActions.checkElementNotDisplayed(errMsg));}

	}
	public boolean setAttributeValue(By locator, String attributeName, String attributeValue)
	{
		try {
			objWebActions.click(locator);
			JavascriptExecutor js = (JavascriptExecutor) objDriverProvider.getWebDriver();
			WebElement element = objWebActions.processElement(locator);
			js.executeScript("arguments[0].setAttribute('value', arguments[1])",element,attributeValue);

			return true;
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}

	}

	public void verifyLiveHelpLinkDisplayed() { 
		logReporter.log("Verify 'Live Help' link displayed", 
				objWebActions.checkElementDisplayed(lnkLiveHelp));
	}

	public void clickOnLiveHelpLink() { 
		logReporter.log("Click on 'Live Help' link", 
				objWebActions.click(lnkLiveHelp));
	}

	public void selectDateOfBirthUsingJavaScript(String dateOfBirth){
		String[] arrDateOfBirth = dateOfBirth.split("_");
		String dobDay = arrDateOfBirth[0];
		String dobMonth = arrDateOfBirth[1];
		String dobYear = arrDateOfBirth[2];
		By locator = By.xpath("//input[@placeholder='Date of birth']");

		if(objWebActions.checkElementDisplayed(locator))
		{logReporter.log("Activate DOB fileds",   
				objWebActions.click(locator));}


		locator = By.id("dob-day");
		logReporter.log("Set dob day", dobDay, 
				this.setAttributeValue(locator,"value",dobDay));

		locator = By.id("dob-month");
		logReporter.log("Set dob month", dobMonth, 
				this.setAttributeValue(locator,"value", dobMonth));

		locator = By.id("dob-year");
		logReporter.log("Set dob year", dobYear, 
				this.setAttributeValue(locator,"value", dobYear));
	}

	public void setDepositLimit(String lmt,String value)
	{
		By lblDepositLmt = By.xpath("//fieldset[@id='joinnow-depositLimit']//following-sibling::div//fieldset//div//button[text()='"+lmt+"']");

		logReporter.log("Clik on "+lmt,   
				objWebActions.click(lblDepositLmt));
		clickOnResetLimit();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By inputDepositLimit = By.xpath("//input[@id='joinnow-depositLimit']");
		logReporter.log("Set" +lmt + " as " , value,
				objWebActions.setTextWithClear(inputDepositLimit, value));
	}

	public void clickOnResetLimit()
	{
		By lnkResetLimit = By.xpath("//a[@class='input-inner-link'][contains(.,'Reset Limit')]");
		logReporter.log("Click on 'Reset Limit''",
				objWebActions.click(lnkResetLimit));
	}
	public void verifyDepositNowHeader()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		//By lblDepositNow = By.xpath("//p[@class='heading'][text()='Deposit now']");
		By lblDepositNow = By.xpath("//form[@class=\"safecharge\"]");
		
		logReporter.log(" verify 'Deposit now'  " ,
				objWebActions.checkElementDisplayed(lblDepositNow));
	}

	public void setAddress(String postcode)
	{
		By inputaddress = By.id("joinnow-address");
		logReporter.log("Set postcode" , postcode,
				objWebActions.setTextWithClear(inputaddress, postcode));
		objWebActions.pressKeybordKeys(inputaddress, "tab");
	}
	public void verifyAddressSuggestionPopUp()
	{
		By locator = By.xpath("//div[@class='address-results']");
		logReporter.log(" verify Address suggestion pop up window  " ,
				objWebActions.checkElementDisplayed(locator));
	}
	public void selectAddresFromLookup(String address)
	{
		By locator = By.xpath("//div[@class='address-results']//ul//li//a[contains(.,'"+address+"')]");
		logReporter.log(" verify ' "+address+"' is displayed in address result " ,
				objWebActions.checkElementDisplayed(locator));
		logReporter.log("Select address as "+address,   
				objWebActions.click(locator));
	}

	public void clickOnClearLink()
	{
		By locator = By.xpath("//fieldset[@id='joinnow-address']/p/a[text()=' Clear ']");
		logReporter.log("Click on 'Clear' link ",   
				objWebActions.click(locator));
	}
	public void selectDepositLimit(String option)
	{
		if(option.equals("No"))
			this.selectDepositLimitNo();
		else
			this.selectDepositLimitYes();
	}
	public void verifyerrorMessageOnScreen(String msg)
	{
		By errMsg = By.xpath("//div[contains(@class,'error-msg')]//li[contains(.,'"+msg+"')]");
		logReporter.log("Verify "+msg,
				objWebActions.checkElementDisplayed(errMsg));
	}

	public void verifyRegestion2StepFormDisplay()
	{
		By locator = By.xpath("//div[@id='component_stepmarkers']//div[@class='step2 three-steps']");
		logReporter.log("Verify Registration Step 2 form display ",
				objWebActions.checkElementDisplayed(locator));
	}
	
	public void verifyPageRedirection(String currentURL ,String expectedurl)
	{
		logReporter.log("Redirect to the  '"+currentURL + "'", currentURL.equalsIgnoreCase(expectedurl));
	}
	
	public String returnExpectedUrl(String linkName)
	{
		String url = configuration.getConfig("web.Url");
		switch (linkName) 
		{
		case "Live Help":
				url =url+"support";
			break;
		case "SMS":
			By chkboxSMS = By.xpath("//input[@id='sms' and @type='checkbox']");
			logReporter.log("sms checkbox is selected > >", objWebActions.isCheckBoxSelected(chkboxSMS));
			break;
		}

		return url;
	}
}
