package com.bella.desktop.page.Bella;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

import cucumber.api.java.en.Then;

public class MessagePage {

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	String strdate;

	public MessagePage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public boolean verifyYourInboxiIsEmptyMessageDisplayed()
	{
		By lblYourinboxisempty = By.xpath("//div[@class='component message-list-empty']//p[text()='Your inbox is empty']");
		/*logReporter.log("Verify 'Your inbox is empty' message is displayed", 
				objWebActions.checkElementDisplayed(lblYourinboxisempty));*/
		return objWebActions.checkElementDisplayed(lblYourinboxisempty);
	}

	public void verifyYourInboxiIsEmptyMessageIsDisplayed()
	{
		By lblYourinboxisempty = By.xpath("//div[@class='component message-list-empty']//p[text()='Your inbox is empty']");
		logReporter.log("Verify 'Your inbox is empty' message is displayed", 
				objWebActions.checkElementDisplayed(lblYourinboxisempty));
	}
	public void verifyMessageDeleteIcon(String mode)
	{
		switch (mode) 
		{
		case "disable":
			By locator = By.xpath("//button[contains(@class,'messages__delete-btn btn-link messages__delete-btn--disabled')]");
			logReporter.log("Verify Message Delete icon is displayed in disable mode", 
					objWebActions.checkElementDisplayed(locator));
			break;
		case "enable":
			locator = By.xpath("//button[contains(@class,'messages__delete-btn btn-link')]");
			logReporter.log("Verify Message Delete icon is displayed in active mode", 
					objWebActions.checkElementDisplayed(locator));
			break;
		}

	}
	public String getunreadMessageCountFromHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='mail-btn']//a//data[@class='count ']");
		return objWebActions.getText(locator);
	}

	public void verifyUnReadMessageCountOnHeader(String count)
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='mail-btn']//a//data[@class='count '][text()='"+count+"']");
		logReporter.log("Verify Message count is displayed as " +count +" in header ", 
				objWebActions.checkElementDisplayed(locator));
	}
	public String getMessageCountFromMyAccountScreen()
	{
		By locator = By.xpath("//div[@class='myaccount-menu']//ul//li//a//data[@class='count ']");
		return objWebActions.getText(locator);
	}

	public void verifyunreadMessageCountOnMyAccountScreen(String count)
	{
		By locator = By.xpath("//div[@class='myaccount-menu']//ul//li//a//data[@class='count '][text()='"+count+"']");
		logReporter.log("Verify Message count is displayed as " +count + " on my account screen ", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void clickMessageDeleteIcon()
	{
		By locator = By.xpath("//button[contains(@class,'messages__delete-btn btn-link')]");
		logReporter.log("click on Message Delete icon", 
				objWebActions.click(locator));
	}

	public void verifyMessageIsDisplayedAsReadUnread(String mode)
	{
		switch (mode) 
		{
		case "read":
			By locator = By.xpath("//div//li//input[@class='message-list__checkbox']//following-sibling::button//i[@class='message-list__icon messages-read']");
			logReporter.log("Verify Message is displayed as read", 
					objWebActions.checkElementDisplayed(locator));
			break;
		case "unread":
			locator = By.xpath("//div//li//input[@class='message-list__checkbox']//following-sibling::button//i[@class='message-list__icon message-list__icon--unread messages']");
			logReporter.log("Verify Message is displayed as unread", 
					objWebActions.checkElementDisplayed(locator));
			break;
		}
	}

	public void selectMessageCheckbox(String...subject)
	{
		if(subject!=null)
		{
			System.out.println(">>>>>>>>>> into subject");
			By locator = By.xpath("//button//div[text()='"+subject[0]+"']//preceding::input[@type='checkbox'][1]");
			System.out.println(" locator:::   "+locator);

			logReporter.log("select message checkbox", 
					objWebActions.clickUsingJS(locator));
		}
		else
		{
			By locator = By.xpath("//div//li//input[@class='message-list__checkbox']");
			logReporter.log("select message checkbox", 
					objWebActions.click(locator));
		}
	}

	public void verifyRecivedMessageDetails(String Sender,String subject,String mode)
	{
		By lnkMessageSubject = By.xpath("//button[@class='message-list__details btn-link']//i[contains(@class,'"+mode+"')]//following-sibling::div[@class='message-list__title heading'][contains(text(),'"+subject+"')]");
		logReporter.log("Verify Message Title is displayed as "+subject, 
				objWebActions.checkElementDisplayed(lnkMessageSubject));

		By lblMessageSenderName = By.xpath("//button[@class='message-list__details btn-link']//i[contains(@class,'"+mode+"')]//following-sibling::span[@class='message-list__sender'][text()='"+Sender+"']");
		logReporter.log("Verify Sender is displayed as "+Sender, 
				objWebActions.checkElementDisplayed(lblMessageSenderName));
	}

	public void selectUnreadMessage(String Sender)
	{
		By lblMessageSenderName = By.xpath("//button[@class='message-list__details btn-link']//i[contains(@class,'unread')]//following-sibling::span[@class='message-list__sender'][text()='"+Sender+"']");
		logReporter.log("Select unread message from "+Sender, 
				objWebActions.click(lblMessageSenderName));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyMessageBodyDetails(String Sender,String subject,String  msgdet)
	{
		By lblsender = By.xpath("//section[@class='component message-detail']//h3[text()='"+subject+"']//following::div//span[@class='message-detail__from'][text()='"+Sender+"']");
		logReporter.log("Verify Message ' "+subject+ "' is received from "+Sender , 
				objWebActions.checkElementDisplayed(lblsender));

		By lblmessageDetails = By.xpath("//section[@class='component message-detail']//h3[text()='"+subject+"']//following::article[@class='message-detail__body'][text()='"+msgdet+"']");
		logReporter.log("Verify "+msgdet+ " is displayed under message body details section", 
				objWebActions.checkElementDisplayed(lblmessageDetails));

		By lblMessageDate = By.xpath("//section[@class='component message-detail']//h3[text()='"+subject+"']//following::div//span[@class='message-detail__date']");
		logReporter.log("Verify Message Date is displayed on screen", 
				objWebActions.checkElementDisplayed(lblMessageDate));
	}

	public void verifyImage()
	{
		By locator = By.xpath("//article[@class='message-detail__body']//a//img");
		logReporter.log("Verify image is displayed on message screen ", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyCTAText(String btntext)
	{
		By locator = By.xpath("//article[@class='message-detail__body']//following::a[text()='"+btntext+"']");
		logReporter.log("Verify ' "+ btntext + "' cta is displayed " ,
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyPaginationLinkOnMessageBodyScreen()
	{
		By lnkPrevious = By.xpath("//div[@class='link-bar']//a//following-sibling::nav[@class='component message-pagination']//button[contains(.,'Previous')]");
		logReporter.log("Verify 'Previous' pagination is displayed on message screen ", 
				objWebActions.checkElementDisplayed(lnkPrevious));

		By lnkNext = By.xpath("//div[@class='link-bar']//a//following-sibling::nav[@class='component message-pagination']//button[contains(.,'Next')]");
		logReporter.log("Verify 'Next' pagination is displayed on message screen ", 
				objWebActions.checkElementDisplayed(lnkNext));
	}

	public void clickOnPagination(String lnk)
	{
		By locator = By.xpath("//div[@class='link-bar']//a//following-sibling::nav[@class='component message-pagination']//button[contains(.,'"+lnk+"')]");
		logReporter.log("Click on  "+lnk, 
				objWebActions.click(locator));
	}
	public void verifyDeletedMessageNotDisplayedInList(String Sender,String subject)
	{
		if(this.verifyYourInboxiIsEmptyMessageDisplayed())
		{
			logReporter.log("Verify 'Your inbox is empty' message is displayed", 
					true);
		}
		else
		{
			By lnkMessageSubject = By.xpath("//button[@class='message-list__details btn-link']//i//following-sibling::div[@class='message-list__title heading'][text()='"+subject+"']");
			logReporter.log("Verify Message Title is not displayed as "+subject, 
					objWebActions.checkElementNotDisplayed(lnkMessageSubject));

			/*By lblMessageSenderName = By.xpath("//button[@class='message-list__details btn-link']//i//following-sibling::span[@class='message-list__sender'][text()='"+Sender+"']");
			logReporter.log("Verify Sender name is not displayed as "+Sender, 
					objWebActions.checkElementNotDisplayed(lblMessageSenderName));*/}

	}

	public void verifyMessageDisplayedInMessageScreen()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By locator = By.xpath("//ol[@class='component message-list list-unstyled']//div//li[contains(@class,'message-list__item  ')]");
		logReporter.log("Verify Messages Displayed on Message Screen ", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyLatestMessageDisplayInTop()
	{
		By locator = By.xpath("//ol[@class='component message-list list-unstyled']//div//li[contains(@class,'message-list__item  ')]");
		if(objWebActions.checkElementDisplayed(locator))
		{
			List<WebElement> webEle = objDriverProvider.getWebDriver().findElements(locator);
			By dateLocator = By.xpath("//ol[@class='component message-list list-unstyled']//div//li[contains(@class,'message-list__item  ')]//button//span[@class='message-list__date']");
			List<WebElement> webDate = objDriverProvider.getWebDriver().findElements(dateLocator);
			int list = webEle.size();
			String[]  str = new String[list];
			String[] dateString = new String[list];
			for(int i=0;i<webEle.size();i++)
			{
				strdate =  webDate.get(i).getText(); 
				strdate = strdate.replaceAll("(?<=\\d)(ST|ND|RD|TH)", "");
				str[i] = strdate;
				System.out.println(str[i]);
			}

			Date date[] = new Date[str.length];
			SimpleDateFormat sobj = new SimpleDateFormat("MMMM d, yyyy");// format specified in double quotes
			for (int index = 0; index < str.length; index++){
				try {
					date[index] = sobj.parse(str[index]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();}}
			Arrays.sort(date,Collections.reverseOrder());
			for (int i = 0; i <date.length; i++) { 
				dateString[i] = sobj.format(date[i]);
				dateString[i] = dateString[i].toUpperCase(); } 
			logReporter.log("Verify Latest message is displyed on Top ", 
					Arrays.equals(str, dateString));
		}
	}

	public void verifySystemdisplaysUpdatedMessageCountorNot(String oldcnt,String newCnt)
	{
		logReporter.log("Verify message count is displayed correctly ", 
				(Integer.valueOf(newCnt)>Integer.valueOf(oldcnt)));
	}

	public void verifyMessageContentDetailsScreen()
	{
		By locator = By.xpath("//section[@class='component message-detail']//h3//following::div//span[@class='message-detail__from']");
		logReporter.log("Verify Message Content Details Screen is displayed", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void selectAnyMessageFromList()
	{
		By locator = By.xpath("//ol[@class='component message-list list-unstyled']//div//li[contains(@class,'message-list__item  ')][1]");
		if(objWebActions.checkElementDisplayed(locator))
		{
			//List<WebElement> weRsults = (java.util.List<WebElement>) objDriverProvider.getWebDriver().findElements(locator);	
			//int index = RandomUtils.nextInt(0,weRsults.size());
			//System.out.println("index........    "+index);
			//	logReporter.log("Select any message from the list ", 
			//		objWebActions.Click(objWebActions.getElemnetOfIndex(locator,index)));
			logReporter.log("Select any message from the list ", 
					objWebActions.click(locator));
		}
	}

	public void SelectAllMessageFromInbox()
	{
		By locator = By.xpath("//div//li//input[@class='message-list__checkbox']");
		if(objWebActions.checkElementDisplayed(locator))
		{
			List<WebElement> webEle = objDriverProvider.getWebDriver().findElements(locator);
			int index = 0;
			for(WebElement webElement : webEle)
			{
				logReporter.log("Select  message from the list ", 
						objWebActions.Click(objWebActions.getElemnetOfIndex(locator,index)));
				waitMethods.sleep(2);
				index++;
			}

			clickMessageDeleteIcon();
		}

	}


}

