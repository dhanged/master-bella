Feature: Navigation

@Android
Scenario: Check that the canonical tag does not display in the source code of any broken URL which shows '404 response error'
Given Invoke the Bella site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to invalid url
Then Verify 404 error title

@Android
Scenario: Check whether system does not allows user to opt-in to the promotion in logged OUT state
Given Invoke the Bella site
Then Navigate to "Promos" tab
Then Open opt-in promotions from listed ones
Then Click on opt-in button
Then Verify Login Page is displayed

@Android
Scenario: Check whether system allows user to apply 'All' filter.
Given Invoke the Bella site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Promos" tab
Then Verify following tab is displayed under secondary navigation bar
|All|
|My Promotions|
Then Select "All" tab from navigation tab
And Verify system displays all the available promotions on the screen.

@Android
Scenario: Check whether system displays appropriate informative message when user selects 'Active' filter and he / she doesn’t opted-in to any of the promotions available on the site
Given Invoke the Bella site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Promos" tab
Then Verify following tab is displayed under secondary navigation bar
|All|
|My Promotions|
Then Select "My Promotions" tab from navigation tab
Then Verify 'Dont have any active promotions' message is displayed
#Then Verify 'No Promotions Claimed by you' message is displayed

@Android
Scenario: Live Casino_Check that system displays Live casinos games on Live casino landing page
Given Invoke the Bella site
Then Navigate to "Casino" tab
And Verify Live Casino Games displaying to user

@Android
Scenario: Live Casino_Check that system displays Live roulette, Blackjack type on Live casinos landing page
Given Invoke the Bella site
Then Navigate to "Casino" tab
And Verify System displays the Live roulette, Blackjack type on Live casinos landing page

@Android
Scenario: Live Casino_Check that system allows to access Live casinos games details page on clicking 'I' button from game
Given Invoke the Bella site
Then Navigate to "Casino" tab
Then Select any sub Category say Roulette
And Click on I button from Image
Then System displays Game Details page with Join button

@Android
Scenario: Navigation Primary Tab_Check that the customer is able to access the primary navigation options
Given Invoke the Bella site
Then Verify following tabs is displayed on primary navigation bar
|Slots|
|Jackpots|
|Promos|
|Casino|
And Verify Primary tabs are accessible and it redirects to correct page
|Slots|
|Jackpots|
|Promos|
|Casino|

@Android
Scenario: Navigation Secondary Tab_Check that the customer is able to access the secondary navigation options
Given Invoke the Bella site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Verify content on Home page
Then Navigate to "Slots" tab
And Verify secondary tabs are accessible on "Slots" page
Then Navigate to "Jackpots" tab
And Verify secondary tabs are accessible on "Jackpots" page
Then Navigate to "Casino" tab
And Verify secondary tabs are accessible on "Casino" page
Then Navigate to "Promos" tab
And Verify secondary tabs are accessible on "Promos" page