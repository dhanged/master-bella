Feature: PMI-228

@Desktop
Scenario: Check Player Online flag
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username "ic5hrGzV"
And User enters password "5xHDX6Vy"
Then User clicks on login button
Then Verify user is successfully logged in
Then Navigate to new tab
Then Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players using "ic5hrGzV"
Then Verify Player online flag is displayed in "Green"
Then Back to Bell site
Then Click on My Account
Then Click on logout button
Then Navigate to "Player Details - ic5hrgzv" site
Then Verify Player online flag is displayed in "Gray"