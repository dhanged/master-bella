Feature: Deposit


@Desktop5555
Scenario: Check whether system allows to Login digital only customer to the site without any issue
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "please1"
And User enters password "Please12"
#Then User enters username "rtyui2"
#And User enters password "Password1234"
Then User clicks on login button
Then Verify user is successfully logged in
Then Verify following links are displayed in header
|Mail|
|Deposit|
|Balance|
|My Account|
Then Click on My Account
#Then Verify "rtyui2" username is displayed on header
Then Verify "please1" username is displayed on header
Then Click on logout button
