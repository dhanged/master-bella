package com.bella.mobile.stepDefinition;

import com.bella.desktop.page.cogs.Cogs_LoginPage;
import com.bella.mobile.page.PAM.PAMLoginPage;
import com.generic.WebActions;

import cucumber.api.java.en.Then;

public class PAMLoginPageStep {

	PAMLoginPage objPAMLoginPage;
	private WebActions objWebActions;

	Cogs_LoginPage objCogs_LoginPage ;
	
	public PAMLoginPageStep(PAMLoginPage objPAMLoginPage,WebActions objWebActions,Cogs_LoginPage objCogs_LoginPage) {		
		this.objPAMLoginPage = objPAMLoginPage;
		this.objWebActions = objWebActions;
		this.objCogs_LoginPage = objCogs_LoginPage;
	}
	

	@Then("^Enter username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void Enter_username_as_and_password_as(String userName, String password) {
		// Write code here that turns the phrase above into concrete actions
		//objPAMLoginPage.setUserName(userName);
		//objPAMLoginPage.setPassword(password);
		objCogs_LoginPage.verifyPrivacyError();
		objCogs_LoginPage.setUserName(userName);
		objCogs_LoginPage.setPassword(password);
	}

	@Then("^Click on Sign In button$")
	public void Click_on_Sign_In_button() {
		// Write code here that turns the phrase above into concrete actions
		//objPAMLoginPage.clickSignIn();
		objCogs_LoginPage.clickLogin();
	}


	@Then("^Navigate to new tab$")
	public void navigate_to_new_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		objWebActions.openNewTab();
		objWebActions.switchToChildWindow();
	}
	
	@Then("^Invoke the Cogs portal \"([^\"]*)\"$")
	public void invoke_the_Cogs_portal(String arg1){
	    // Write code here that turns the phrase above into concrete actions
		objPAMLoginPage.invokePAMURL(arg1);
	}
	@Then("^Invoke the Bede Admin portal \"([^\"]*)\"$")
	public void invoke_the_Bede_Admin_portal(String arg1){
	    // Write code here that turns the phrase above into concrete actions
		objPAMLoginPage.invokePAMURL(arg1);
	}

	@Then("^Back to Bell site$")
	public void back_to_Bell_site() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("************** window");
	    objWebActions.switchToWindowUsingTitle("Play Casino Games Online with Bella Casino");
	}
	
	@Then("^Navigate to \"([^\"]*)\" site$")
	public void navigate_to_site(String arg1){
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("************** window");
	    objWebActions.switchToWindowUsingTitle(arg1);
	}
}
