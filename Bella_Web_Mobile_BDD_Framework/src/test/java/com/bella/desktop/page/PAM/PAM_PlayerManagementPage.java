package com.bella.desktop.page.PAM;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class PAM_PlayerManagementPage {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public PAM_PlayerManagementPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration,Utilities objUtilities) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = objUtilities;
	}

	By inpPlayerSearch = By.xpath("//input[@id='unifiedSearch' and @placeholder='Player Search']");
	By btnSerach = By.id("unifiedSubmit");
	By newMessageTab = By.xpath("//a[contains(@class,'btn')][contains(.,'+ New Message')]");
	By senderName = By.xpath("//input[@placeholder='Sender Name']");
	By messageTitle = By.xpath("//input[@placeholder='Message Title']");
	By textArea = By.xpath("//textarea[@name='body']");
	By ifame_id = By.cssSelector("iframe#message-body_ifr");
	By msg_body = By.cssSelector("#tinymce");
	By CurrentsHours = By.xpath("//td//span[@class='timepicker-hour'][@data-action='showHours']");
	By CurrentsMinutes = By.xpath("//td//span[@class='timepicker-minute'][@data-action='showMinutes']");
	By CurrentsSeconds = By.xpath("//td//span[@class='timepicker-second'][@data-action='showSeconds']");
	By btnIncrementHours = By.xpath("//a[@class='btn'][@data-action='incrementHours']");
	By btnIncrementMinutes = By.xpath("//a[@class='btn'][@data-action='incrementMinutes']");
	By btnIncrementSeconds = By.xpath("//a[@class='btn'][@data-action='incrementSeconds']");
	By btndecrementHours = By.xpath("//a[@class='btn'][@data-action='decrementHours']");
	By btndecrementMinutes = By.xpath("//a[@class='btn'][@data-action='decrementMinutes']");
	By btndecrementSeconds = By.xpath("//a[@class='btn'][@data-action='decrementSeconds']");


	public void setPlayerSearch(String player){
		logReporter.log("Set player search", player,
				objWebActions.setText(inpPlayerSearch, player));
	}

	public void clickSearch(){
		logReporter.log("Click 'Search'", 
				objWebActions.click(btnSerach));
	}

	public void selectPraimaryPlayerManagementTab(String tab){
		By locator = By.xpath("//a[@class='local-menu-link' and contains(text(),'" + tab +"')]");
		logReporter.log("Select tab" , tab, 
				objWebActions.click(locator));
	}

	public void selectSecondaryPlayerManagementTab(String tab){
		By locator = By.xpath("//ul[@class='player-actions']//li//a//span[contains(.,'"+ tab + "')]");
		logReporter.log("Select tab" , tab, 
				objWebActions.click(locator));
	}

	public void verifyNewMessageButton()
	{
		logReporter.log("Verify 'New Message 'cta " , 
				objWebActions.checkElementDisplayed(newMessageTab));
	}

	public void ClickOnNewMessageButton()
	{
		logReporter.log("Click 'New Message 'cta " , 
				objWebActions.click(newMessageTab));
	}
	public void setSenderName(String sendername)
	{
		logReporter.log("Enter Sender", objWebActions.setText(senderName, sendername));
	}

	public void setMessageTitle(String msgTitle)
	{
		logReporter.log("Enter title ",
				objWebActions.setText(messageTitle,msgTitle));
	}

	public void setMessageText(String msgTitle)
	{
		logReporter.log("Enter Description",
				objWebActions.setText(textArea, msgTitle));
	}

	public void clickOnCreateButton()
	{
		By btnCreate = By.xpath("//button[contains(.,' Create ')]");
		logReporter.log("Click on Create ", objWebActions.click(btnCreate));
	}

	public void clickOnConfirm()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		By btnConfirm = By.xpath("//button[contains(.,' Confirm ')]");
		logReporter.log("click on confirm " ,objWebActions.click(btnConfirm));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void navigateToPlayerMessenger()
	{
		By locator = By.xpath("//a[@class='menu-link'][contains(.,'Player Messenger')]");
		logReporter.log("Navigate to 'Player Messenger ' tab " ,objWebActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void clickOnNewMessage()
	{
		By locator = By.xpath("//button[@title='Create New']");
		logReporter.log("Click on 'Create New ' " ,objWebActions.click(locator));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void setMessageInternalName(String name)
	{
		By internal_name = By.xpath("//input[@id='internalName']");
		logReporter.log("Set message internal name as " +name,objWebActions.setText(internal_name, name));
	}

	public void setFrom(String frm)
	{
		By from_name = By.xpath("//input[@id='from']");
		logReporter.log("Set From as " +frm,objWebActions.setText(from_name, frm));
	}

	public void setSubject(String subject)
	{
		By inputsubject = By.xpath("//input[@id='subject']");
		logReporter.log("Set From as " +subject,objWebActions.setText(inputsubject, subject));
	}

	public void setMessageBody(String msg)
	{
		objWebActions.switchToFrameUsingIframe_Element(ifame_id);
		logReporter.log("Set Message text as " +msg,objWebActions.setText(msg_body, msg));
objWebActions.pressKeybordKeys(msg_body, "enter");
		objWebActions.switchToDefaultContent();
	}

	public void clickOnSaveSchedule()
	{
		By btnsaveNschedule = By.xpath("//button[@type='submit'][text()='Save & Schedule']");	
		logReporter.log("click on 'Save & Schedule' ",objWebActions.click(btnsaveNschedule));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void selectSite(String sitname)
	{
		By siteDropdown = By.xpath("//span[contains(.,'-- Please Select Site --')]//following-sibling::span//b");
		By drpsiteName = By.xpath("//ul//li//div[contains(.,'"+sitname+"')]");
		logReporter.log("click on Site dropdown ",objWebActions.click(siteDropdown));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log("select Site as "+ sitname ,objWebActions.click(drpsiteName));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}


	public void selectTimeZone(String timeZone)
	{//United Kingdom Time - London
		By timeZoneDropdown = By.xpath("//label[text()='Time Zone']//following::select2[@name='timeZoneId']//div");
		logReporter.log("click on timezone ", objWebActions.click(timeZoneDropdown));

		By timeZone_Option = By.xpath("//ul[@role='listbox']/li/div[contains(text(),'"+ timeZone +"')]");
		logReporter.log("select timezone", objWebActions.click(timeZone_Option));

	}
	public void setSendTime()
	{
		String time = objUtilities.getRequiredTime("2","yyyy-MM-dd HH:mm:ss","Europe/London");
		System.out.println(" time = " +time);

		By  inputsendTime = By.xpath("//input[@name='dateTime']");
		objWebActions.click(inputsendTime);
		setSendTime(time);
		objWebActions.pressKeybordKeys(inputsendTime, "tab");
	}

	public void setSendTime(String time)
	{
		String a = StringUtils.substringAfter(time, " ");
		String[] expectedtime = a.split(":");
		System.out.println("a " +a.toString());
		clickOnCurrentHours();
		selectHoursFromCalender(expectedtime[0]);
		selectminutes(expectedtime[1]);
		selectSeconds(expectedtime[2]);
	}

	public void selectHoursFromCalender(String hrs)
	{
		By btnhrs = By.xpath("//td[@data-action='selectHour'][text()='"+hrs+"']");
		logReporter.log("Select hours as "+hrs, objWebActions.click(btnhrs));
	}
	public void selectminutes(String Minute)
	{

		System.out.println("*** into minutes ");
		String currntMinutes = objWebActions.getText(CurrentsMinutes) ;
		System.out.println("currntMinutes  "+currntMinutes);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		boolean flag =false;

		if(Integer.valueOf(currntMinutes) > Integer.valueOf(Minute))
		{
			System.out.println("*** into dece ");
			do
			{
				clickOndecrementMinutes();
				currntMinutes = objWebActions.getText(CurrentsMinutes);
				if(Integer.valueOf(currntMinutes)==Integer.valueOf(Minute)){
					flag = true;}
				else{
					flag =false;}
			}while(!flag);
		}
		else
		{
			do
			{
				System.out.println("*** into incre seconds ");
				clickOnIncrementMinutes();
				currntMinutes = objWebActions.getText(CurrentsMinutes);
				//System.out.println(" currntMinutes ::: "+currntMinutes);
				//	System.out.println(" Minutes ::: "+Minute);
				if(Integer.valueOf(currntMinutes)==Integer.valueOf(Minute)){
					flag = true;}
				else{
					flag =false;}
			}while(!flag);
		}

	}

	public void selectSeconds(String seconds)
	{

		System.out.println("*** into minutes ");
		String currntseconds = objWebActions.getText(CurrentsSeconds) ;
		System.out.println("currntseconds  "+currntseconds);
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		boolean flag =false;

		if(Integer.valueOf(currntseconds) > Integer.valueOf(seconds))
		{
			System.out.println("*** into dece ");
			do
			{
				clickOndecrementSecondas();
				currntseconds = objWebActions.getText(CurrentsSeconds);
				if(Integer.valueOf(currntseconds)==Integer.valueOf(seconds))
				{
					flag = true;
				}
				else
				{
					flag =false;
				}
			}while(!flag);
		}
		else
		{


			do
			{
				System.out.println("*** into incre minutes ");
				clickOnIncrementSecondas();
				currntseconds = objWebActions.getText(CurrentsSeconds);
				//	System.out.println(" currntseconds ::: "+currntseconds);
				//	System.out.println(" seconds ::: "+seconds);

				if(Integer.valueOf(currntseconds)==Integer.valueOf(seconds))
				{
					flag = true;
				}
				else
				{
					flag =false;
				}
			}while(!flag);
		}

	}
	public void clickOnIncrementMinutes()
	{
		logReporter.log("click on Increment Minutes btn ", objWebActions.click(btnIncrementMinutes));
		System.out.println("******clicked ");
	}

	public void clickOnIncrementSecondas()
	{
		logReporter.log("click on Increment Seconds btn ", objWebActions.click(btnIncrementSeconds));
	}
	public void clickOndecrementMinutes()
	{
		logReporter.log("click on decrement Minutes btn ", objWebActions.click(btndecrementMinutes));
		System.out.println("******clicked ");
	}

	public void clickOndecrementSecondas()
	{
		logReporter.log("click on decrement Seconds btn ", objWebActions.click(btndecrementSeconds));
	}

	public void clickOnCurrentHours()
	{
		logReporter.log("click showHours", objWebActions.click(CurrentsHours));
	}

	public void clickOnCurrentMinutes()
	{
		logReporter.log("click Minutes", objWebActions.click(CurrentsMinutes));
	}

	public void clickOnCurrentSeconds()
	{
		logReporter.log("click Seconds", objWebActions.click(CurrentsSeconds));
	}

	public void setinsertImageField(String label,String textdata)
	{						//label[text()='Image description']//following-sibling::input
		By locator = By.xpath("//label[text()='"+label+"']//following-sibling::div//input");
		logReporter.log("set "+label, objWebActions.setText(locator, textdata));
	}

	public void insertDimensions(String Width,String Height)
	{
		By inputWidth = By.xpath("//label[text()='Dimensions']//following-sibling::div//input[1]");
	
		logReporter.log("set dimension", objWebActions.setText(inputWidth, Width));
		By inputHeight = By.xpath("//label[text()='Dimensions']//following-sibling::div//input[2]");
		
		logReporter.log("set dimension", objWebActions.setText(inputHeight, Height));
	}
	
	public void setimageDescription(String imgdescription)
	{
		By inputImageDescription = By.xpath("//label[text()='Image description']//following-sibling::input");
		logReporter.log("Set Image Description ", objWebActions.setText(inputImageDescription, imgdescription));
	}
	public void clickOnInsertImage()
	{
		By locator = By.xpath("//div[@aria-label='Insert/edit image']");
		logReporter.log("click 'Insert/edit image'", objWebActions.click(locator));
	}
	public void clickOnOk()
	{
		By btnok = By.xpath("//button//span[text()='Ok']");
		logReporter.log("click on 'Ok'", objWebActions.click(btnok));
	}

	public void verifyMessageCreatedSuccessfully(String messageSubject)
	{
		By locator = By.xpath("//div[@class='message-definitions-list']//ul//li[1]//label//p");
		String msgText = objWebActions.getText(locator) ;
		System.out.println(" locator " +objWebActions.getText(locator));
		if(msgText.contains(messageSubject)){
			logReporter.log("verify created message is displayed under message list ", objWebActions.checkElementDisplayed(locator));}
		else{
			logReporter.log(" Message is not created ", false);}
		boolean flag = false;
		do
		{
			msgText = objWebActions.getText(locator) ;
			if(msgText.contains("Sent")){
				flag = true;}
			else
			{	objWebActions.pageRefresh();
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
			flag = false;}

		}while(!flag);
	}
	
	public void includeFooterButton()
	{
		By locator = By.id("includeFooterButton");
		logReporter.log("Select 'includeFooterButton' checkbox", 
				objWebActions.click(locator));
	}

	public void addFooterButton(String button,String url)
	{
		By dropdownUrl = By.xpath("//span[contains(.,'-- Please Select Action--')]//following-sibling::span//b");
		logReporter.log("click on 'Please Select Action' dropdown", 
				objWebActions.click(dropdownUrl));

		By dropdownURL_Option = By.xpath("//ul[@role='listbox']//li//div[text()='"+button+"']");
		logReporter.log("Select button as "+button, 
				objWebActions.click(dropdownURL_Option));

		By inputURL = By.xpath("//input[@placeholder='Url']");	
		logReporter.log("Set URL as "+url, 
				objWebActions.setText(inputURL, url));
	}
}
