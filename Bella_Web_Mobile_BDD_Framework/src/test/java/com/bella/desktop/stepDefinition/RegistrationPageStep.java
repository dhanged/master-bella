package com.bella.desktop.stepDefinition;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;

import com.bella.desktop.page.Bella.RegistrationPage;
import com.generic.WebActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class RegistrationPageStep {

	private RegistrationPage objRegistrationPage;
	private Utilities utilities;	
	private WebActions objWebActions;
	private Configuration configuration;
	String userName ,password,emailAddress;

	public RegistrationPageStep(RegistrationPage objRegistrationPage,Utilities utilities,WebActions objWebActions,Configuration configuration) {		
		this.objRegistrationPage = objRegistrationPage ;
		this.utilities = utilities;
		this.objWebActions = objWebActions;
		this.configuration = configuration;
	}

	@Then("^Enter email address$")
	public void enter_email_address() {
		emailAddress = RandomStringUtils.randomAlphanumeric(6)+"@mailinator.com";
		objRegistrationPage.setEmailAddress(emailAddress);
	}

	@And("^Click on Next button$")
	public void click_on_Next_button() {
		objRegistrationPage.clickNext();

	}

	@Then("^Click on select all in marketing preference section$")
	public void click_on_select_all_in_marketing_preference_section() {
		objRegistrationPage.setMarketingPreferences("Select all");
	}


	@Then("^Verify following checkboxes are selected$")
	public void verify_following_checkboxes_are_selected(DataTable dt){
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyMarketingPreferencesCheckboxesIsSelectedOrNot(list.get(i));}
	}

	@Then("^Verify following Marketing preferences options are displayed$")
	public void verify_following_Marketing_preferences_options_are_displayed(DataTable dt){
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyMarketingPreferencesOption(list.get(i));}
	}

	@Then("^Click on Register CTA$")
	public void click_on_Register_CTA() {
		objRegistrationPage.clickRegister();
	}

	@Then("^unselect all marketing preference options$")
	public void unselect_all_marketing_preference_options(){
		objRegistrationPage.setMarketingPreferences("Select all");
	}

	@Then("^Select \"([^\"]*)\" marketing preference option$")
	public void select_marketing_preference_option(String arg1){
		objRegistrationPage.setMarketingPreferences(arg1);
	}

	@Then("^verify \"([^\"]*)\" checkboxe is selected$")
	public void verify_checkboxe_is_selected(String arg1){
		objRegistrationPage.verifyMarketingPreferencesCheckboxesIsSelectedOrNot(arg1);
	}

	@Then("^Verify Registration page is appered$")
	public void verify_Registration_page_is_appered(){
		objRegistrationPage.verifySignUpHeaderDisplayed();
	}

	@Then("^Enter username$")
	public void enter_username(){
		userName = RandomStringUtils.randomAlphanumeric(8);
		objRegistrationPage.setUserName(userName);
	}

	@Then("^Enter Password$")
	public void enter_Password(){
		//password ="R"+utilities.getRandomAlphabeticString(6)+utilities.getRandomNumber(2);
		password = objWebActions.genarateRandomPassword();
		System.out.println(" ************* password : "+password);
		objRegistrationPage.setPassword(password);
	}

	@Then("^Enter only characters in password field$")
	public void enter_only_characters_in_password_field(){
		objRegistrationPage.setPassword("hfghgfA");
	}

	@Then("^Enter only numbers in password field$")
	public void enter_only_numbers_in_password_field(){
		objRegistrationPage.setPassword("234234324");
	}

	@Then("^Select Title$")
	public void select_Title(){
		objRegistrationPage.selectTitle(objRegistrationPage.getRandomTitleForUser());
	}


	@Then("^Enter FirstName$")
	public void enter_FirstName(){
		objRegistrationPage.setFirstName(RandomStringUtils.randomAlphabetic(6));
	}

	@Then("^Enter Surname$")
	public void enter_Surname(){
		objRegistrationPage.setSurName(RandomStringUtils.randomAlphabetic(6));
	}

	@Then("^Enter data of birth$")
	public void enter_data_of_birth(){
		objRegistrationPage.selectDateOfBirth(utilities.getRequiredDateWithCustomYear("-41","dd_MM_YYYY", ""));
	}

	@Then("^Enter Mobile Number$")
	public void enter_Mobile_Number(){
		objRegistrationPage.setMobileNumber((utilities.getRandomNumeric(11)));
	}

	@Then("^Select country as \"([^\"]*)\"$")
	public void select_country_as(String country){
		objRegistrationPage.selectCountry(country);
	}

	@Then("^Enter address as \"([^\"]*)\"$")
	public void enter_address_as(String postcode){
		objRegistrationPage.setAddress(postcode);
	}

	@Then("^Verify address suggestion list is displayed$")
	public void verify_address_suggestion_list_is_displayed(){
		objRegistrationPage.verifyAddressSuggestionPopUp();
	}

	@Then("^Select address \"([^\"]*)\" from the list$")
	public void select_address_from_the_list(String address){
		objRegistrationPage.selectAddresFromLookup(address);
	}
	@Then("^Verify following fields get populated with correct data$")
	public void verify_following_fields_get_populated_with_correct_data(DataTable dt)throws Throwable 
	{ 
		System.out.println("size...." +utilities.getListDataFromDataTable(dt).size());
		for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) 
		{
			System.out.println(utilities.getListDataFromDataTable(dt).get(i));
			objRegistrationPage.verifyAddressFieldGetPopuplatedWithData(utilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Verify link 'enter it manually available' near Address field$")
	public void verify_link_enter_it_manually_available_near_Address_field() {
		objRegistrationPage.verifyenteritmanuallylink();
	}

	@Then("^Select the option enter it manually available near Address field$")
	public void select_the_option_enter_it_manually_available_near_Address_field(){
		objRegistrationPage.Clickenteritmanuallylink();
	}

	@Then("^Verify following fields get displayed to enter the address manually$")
	public void verify_following_fields_get_displayed_to_enter_the_address_manually(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyAddressField(list.get(i));}
	}

	@Then("^Click on Clear link$")
	public void click_on_Clear_link(){
		objRegistrationPage.clickOnClearLink();
	}

	@Then("^Select DepositLimit as \"([^\"]*)\"$")
	public void select_DepositLimit_as(String arg1){
		objRegistrationPage.selectDepositLimit(arg1);
	}

	@Then("^Set Deposit \"([^\"]*)\" limit as \"([^\"]*)\"$")
	public void set_Deposit_limit_as(String arg1, String arg2) {
		objRegistrationPage.setDepositLimit(arg1, arg2);
	}

	@Then("^Enter address manually$")
	public void enter_address_manually(){
		objRegistrationPage.setmanualAddress(utilities.getRandomAlphanumericString(5) ,utilities.getRandomAlphanumericString(5), utilities.getRandomAlphanumericString(5),  utilities.getRandomAlphanumericString(5), "ha01sg");
	}
	@Then("^Click on Register$")
	public void click_on_Register(){
		objRegistrationPage.clickRegister();
	}

	@Then("^Verify Deposit Now header$")
	public void verify_Deposit_Now_header(){
		// Write code here that turns the phrase above into concrete actions
		objRegistrationPage.verifyDepositNowHeader();
		configuration.updateParameterDetailsInConfig("web.userName", userName);
		configuration.updateParameterDetailsInConfig("web.password", password);
		configuration.updateParameterDetailsInConfig("web.emailID", emailAddress);
	}
	@Then("^Enter password less than (\\d+) character password$")
	public void enter_password_less_than_character_password(int arg1)  {
		objRegistrationPage.setPassword("cert");
	}

	@Then("^Enter (\\d+) character password and a mix of letters and numbers$")
	public void enter_character_password_and_a_mix_of_letters_and_numbers(int arg1)  {
		String pwd = "R"+utilities.getRandomAlphabeticString(5)+utilities.getRandomNumber(2);
		System.out.println(pwd);
		objRegistrationPage.setPassword(pwd);
	}
	@And("^Enter the age less than (\\d+) years in DOB field$")
	public void enter_the_age_less_thanyears_in_DOB_field(int arg1)  {
		objRegistrationPage.selectDateOfBirth("21_12_2009");
	}

	@Then("^Enter the correct age i\\.e\\. Greater than (\\d+) years in DOB field$")
	public void enter_the_correct_age_i_e_Greater_than_years_in_DOB_field(int arg1) {
		objRegistrationPage.selectDateOfBirth("07_01_1985");  
	}

	@Then("^Verify \"([^\"]*)\" error message$")
	public void verify_error_message(String arg1){
		objRegistrationPage.verifyErrorMessage(arg1);
	}

	@Then("^Enter \"([^\"]*)\" invalid mobile number$")
	public void Enter_invalid_mobile_number(String arg1){
		objRegistrationPage.setMobileNumber(arg1);
	}

	@Then("^Enter Password with Space$")
	public void enter_Password_with_Space(){
		objRegistrationPage.setPassword("Te s sd 1s");
	}

	@Then("^Enter Username and Password as \"([^\"]*)\"$")
	public void enter_Username_and_Password_same_as(String arg1){
		objRegistrationPage.setUserName(arg1);
		objRegistrationPage.setPassword(arg1);
	}

	@Then("^Verify error message \"([^\"]*)\"$")
	public void verify_error_messages(String arg1){
		objRegistrationPage.verifyerrorMessageOnScreen(arg1);
	}


	@Then("^Enter existing email address as \"([^\"]*)\"$")
	public void enter_existing_email_address_as(String arg1){
		objRegistrationPage.setEmailAddress(arg1);
	}

	@Then("^Enter email address as \"([^\"]*)\"$")
	public void enter_email_address_as(String arg1){
		objRegistrationPage.setEmailAddress(arg1);
	}

	@And ("^Enter already taken username$")
	public void enter_already_taken_username(){
		///objRegistrationPage.setUserName("BC_Aut9I8v");;
		objRegistrationPage.setUserName(configuration.getConfig("web.userName"));
	}
	@Then("^Enter username as \"([^\"]*)\"$")
	public void enter_username_as(String arg1){
		objRegistrationPage.setUserName(arg1);
	}

	@Then("^Enter valid username$")
	public void enter_valid_username(){
		String usernm = utilities.getRandomAlphanumericString(9);
		objRegistrationPage.setUserName(usernm);
	}

	@Then("^Verify all titles are displayed on page$")
	public void verify_all_titles_are_displayed_on_page(DataTable dt)
	{
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyTitle(list.get(i));}
	}
	@Then("^Verify Registration Step2 is displayed$")
	public void verify_Registration_Step2_is_displayed()
	{
		objRegistrationPage.verifyRegestion2StepFormDisplay();
	}

	@Then("^Verify below error message$")
	public void verify_below_error_message(DataTable dt)
	{
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyErrorMessage(list.get(i));}
	}


	@Then("^Verify system does not display any below error message$")
	public void verify_system_does_not_display_any_below_error_message(DataTable dt)
	{
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objRegistrationPage.verifyErrorMessageDoesNotDisplay(list.get(i));}
	}
	@Then("^Verify 'Live Help' link on Registartion page$")
	public void verify_Live_Help_link_on_Registartion_page(){
		objRegistrationPage.verifyLiveHelpLinkDisplayed();
	}

	@Then("^Click on 'Live Help' link on Registartion page$")
	public void click_on_Live_Help_link_on_Registartion_page(){
		objRegistrationPage.clickOnLiveHelpLink();
	}

	@Then("^Verify \"([^\"]*)\" Link opened in New tab$")
	public void verify_Link_opened_in_New_tab(String arg1){
		objWebActions.switchToChildWindow();
		String expectedURL = objRegistrationPage.returnExpectedUrl(arg1);
		String currentURL = objWebActions.getUrl();
		System.out.println(currentURL+ "    "+expectedURL);
		objRegistrationPage.verifyPageRedirection(currentURL, expectedURL);
	}

	@Then("^Verify live help Link opened in New tab$")
	public void verify_live_helpLink_opened_in_New_tab(){
		objWebActions.switchToChildWindow();
		String expectedURL = "https://rank.secure.force.com/chat?cid=46281f3b63d487499c12f7acb6d1178e#/liveSupport";
		String currentURL = objWebActions.getUrl();
		System.out.println(currentURL+ "    "+expectedURL);
		objRegistrationPage.verifyPageRedirection(currentURL, expectedURL);
	}
}
