Feature: SelfExclusion

@Desktop21
Scenario: Check that player is able to set Self Exclude from Responsible gaming
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Self Exclude" tab from my account screen
Then 'Do you feel you have a problem with gambling?' screen displayed with Yes and No option
Then Verify following break periods
|6 Months|
|1 Year|
|2 Years|
|5 Years|
Then select the amount of time to self exclude the account
Then Verify date is displayed on the screen until the account will be locked
Then Verify self exclusion info text
Then Keep password field blank 
And Click on 'Yes, I want to Self Exclude' button
Then Verify "Please enter your password" error message
Then Enter invalid password as "dsf"
And Click on 'Yes, I want to Self Exclude' button
Then Verify "Incorrect password, please try again" error message
Then Enter valid password and hit 'Yes, I want to Self Exclude' button
Then Check that the confirmation pop up is displayed with Cancel and Self exclude and log out CTA
And Select 'Self exclude and log out CTA' button
Then Accept Self exclusion complete pop up
Then Verify user get logged out 
And User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Verify error message "The user is self excluded."
#Then Verify error message "To self-exclude from all online gambling companies licensed in Great Britain go to"
Then Navigate to new tab
Then Invoke the Bede Admin portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
And Verify Player Account status is displayed as "Excluded"