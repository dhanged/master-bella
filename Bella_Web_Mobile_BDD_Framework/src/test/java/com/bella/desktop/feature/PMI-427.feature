Feature: PMI-427

@Desktop427
Scenario: Check Transaction History in cogs
Given Invoke the Cogs portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
Then Navigate to Player "Transactions" tab
And Filter transactions By Quick Date
Then Filter transactions By Transaction Type
Then Filter transactions By Device Category