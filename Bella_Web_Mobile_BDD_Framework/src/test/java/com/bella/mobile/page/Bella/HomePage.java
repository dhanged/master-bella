/**
 * 
 */
package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

/**
 * @author vaishali bhad
 *
 */
public class HomePage {

	private By btnContinue = By.xpath("//div[@class='cookie-message-text']/following-sibling::button[text()='Continue']");
	private By btnOpenLogin = By.xpath("//button[@class='open-login' and text()='Login']");
	private By logoBC = By.xpath("//a[@class='logo']/img[contains(@src,'globalassets/brand/bella')]");
	private By btnJoinNow = By.xpath("//li[@class='join-now-btn']/a[@class='btn' and text()='Sign Up']");
	private By lnkMyAccount = By.xpath("//a[@class='open-myaccount']");
	By SearchBtn = By.xpath("//button[@class='search__open-btn']");
	By inpSearchTxt = By.xpath("//input[@class='search-bar__input icon-search-white' and @placeholder='Search for a game']");
	By PlayBtnOnSearchResults = By.xpath("//button[@class='search-results__play-btn btn']");
	By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
	By browseSectionBTN = By.xpath("//button[@class='search-no-results__browse-btn' and contains(text(),'Browse sections')]");
	By GameInfoOnSearchResults = By.xpath("//a[@class='search-results__info']/i[@class='search-results__info-icon']");
	
	
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods objwaitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	private AppiumDriverProvider objDriverProvider;
	
	
	public HomePage(MobileActions mobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities,AppiumDriverProvider driverProvider) {
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
		this.objwaitMethods = waitMethods;
		this.configuration= configuration;
		this.objDriverProvider = driverProvider;
	}
	
	public void verifyBellaCasinosLogoDisplayed(){
		logReporter.log("Verify Bella Casinos logo displayed.", 
				objMobileActions.checkElementDisplayed(logoBC));
		if(objMobileActions.checkElementDisplayedWithMinWait(btnContinue))
			this.clickContinueCookie();
	}

	public void clickContinueCookie() {
		logReporter.log("Click 'Continue' Cookie button", 
				objMobileActions.click(btnContinue));
	}
	
	public void VerifyLogin() {
		logReporter.log("Verify 'Login' button", 
				objMobileActions.checkElementDisplayed(btnOpenLogin));
		objwaitMethods.sleep(5);
		
	}
	public void clickLogin() {
		logReporter.log("Click 'Login' button", 
				objMobileActions.click(btnOpenLogin));
	}
	public void verifyJoinNow()
	{
		logReporter.log("Verify 'Join Now ' button displayed in header", 
				objMobileActions.checkElementDisplayed(btnJoinNow));
	}
	
	public void clickJoinNow() {
		logReporter.log("Click 'Sign Up' button", 
				objMobileActions.click(btnJoinNow));
	}
	public void verifyUserLoggedOutSuccessfuly()
	{
		logReporter.log("User Logged Out Successfully - Join Now displayed", 
				objMobileActions.checkElementDisplayed(btnJoinNow));
		logReporter.log("User Logged Out Successfully - Login button Displayed", 
				objMobileActions.checkElementDisplayed(btnOpenLogin));
	}
	public void VerifyMyAccount() {
		logReporter.log("Verify 'My Account' link is displayed in header", 
				objMobileActions.checkElementDisplayed(lnkMyAccount));
	}
	public void clickMyAccount() {
		objwaitMethods.sleep(5);
		logReporter.log("Click 'My Account' link", 
				objMobileActions.click(lnkMyAccount));
	}
	public void NavigateToInvalidUrl() {
		logReporter.log("navigate to invalid url", objMobileActions.navigateToInvalidUrl());
	}
	
	public void verifyErrorTitle() 
	{
		objwaitMethods.sleep(5);
		By error = By.xpath("//p[contains(.,'404 Error')]");
		logReporter.log("verify 404 error title", objMobileActions.checkElementDisplayed(error));
	}
	public void navigateToPrimaryTabs(String tabName)
	{
		if(tabName.contains(",")){
			String[] arr1 = tabName.split(",");
			for (String links : arr1  ) {
				By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
				logReporter.log(links+" displayed in Primary navigation bar ",
						objMobileActions.click(lnkPrimaryTAB));}}
		else{
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
			logReporter.log(tabName+" displayed in Primary navigation bar ",
					objMobileActions.click(lnkPrimaryTAB));}
	}
	public void verifySecondaryTABdisplayed(String links){
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" verify "+ links2+" link displayed from secondary navigation",
						objMobileActions.checkElementDisplayed(locator));}
		}
		else
		{
			By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" verify "+ links+" link displayed from secondary navigation",
					objMobileActions.checkElementDisplayed(locator));
		}
	}

	public void clickSecondaryTABdisplayed(String links)
	{
		if(links.contains(",")){
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				logReporter.log(" Click on '"+ links2+"' link displayed from secondary navigation",
						objMobileActions.click(locator));}
		}
		else
		{
			By locator2= By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			logReporter.log(" Click on '"+ links+" link 'displayed from secondary navigation",
					objMobileActions.click(locator2));
		}
	}
	
	
	public void verifyGamesOnSlotsPage()
	{
		By lblgamesPanel = By.xpath("//div[@class='game-panels container-fluid']//div[@class='row']//div[contains(@class,'game-panel')]");
		logReporter.log(" Verify games panel ",
				objMobileActions.checkElementDisplayed(lblgamesPanel));	
	}
	public void verifyLinksOnFlogScreen(String lnk)
	{
		By locator = By.xpath("//p[@class='useful-links'][text()='Forgot your ']//following::a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify " +lnk + "on page", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));
	}
		
	public void verifyEmailDisplayedOnFlogConfirmationScreen(String txt,String lnk)
	{
		By locator = By.xpath("//p[@class='heading'][contains(.,'"+txt+"')]//following::span[@class='email']//a[contains(.,'"+lnk+"')]");
		logReporter.log("Verify '"+lnk+ "' on flog confirmation page", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));
	}
	public void verifyTelephoneNumberDisplayedOnFlogConfirmationScreen()
	{
		By locator = By.xpath("//p[@class='heading'][text()='Live Help']//following::span[@class='telephone']//a[contains(.,'0330 102 8582')]");
		logReporter.log("Verify '0330 102 8582' on flog confirmation page", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));
	}
	
	public void verifyInvalidlogindetailsErrorMessage()
	{
		By locator = By.xpath("//p[contains(text(),'Invalid log in details. You can request') and contains(.,'a username reminder') and contains(.,' OR ' ) and contains(.,'reset your password')]");
		logReporter.log("Verify 'Invalid log in details. You can request a username reminder OR reset your password' error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));
		By lnkUserNameReminder = By.xpath("//div[@class='error-msg']//p//a[text()='a username reminder']");
		logReporter.log("Verify 'a username reminder' is displayed as link in error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(lnkUserNameReminder));
		By lnkresetyourpassword = By.xpath("//div[@class='error-msg']//p//a[text()='reset your password']");
		logReporter.log("Verify 'reset your password' is displayed as link in error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(lnkresetyourpassword));
	}
	
	public void verifyWarningMessage()
	{
		By locator = By.xpath("//li[contains(text(),'Warning! You have') and contains(.,' 1 attempt remaining') and contains(.,' before your account is locked' )]");
		logReporter.log("Verify 'Warning! You have 1 attempt remaining  before your account is locked ' error message is displayed ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));
	}
	
	public void verifyYourAccountHasBeenLockedErrorMessage()
	{
		By locator = By.xpath("//p[@class='account-locked'][contains(text(),'Your account has been locked. ') and contains(.,'Reset your password') and contains(.,' or ' ) and contains(.,'chat to us') and contains(.,' to unlock your account.' )]");
		logReporter.log("Verify 'Your account has been locked.Reset your password or chat to us to unlock your account.' error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));	
		
		By lnkResetyourpassword = By.xpath("//p[@class='account-locked']//a[text()='Reset your password']");
		logReporter.log("Verify 'Reset your password' is displayed as link in error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(lnkResetyourpassword));
		
		By lnkchattous = By.xpath("//p[@class='account-locked']//a[text()='chat to us']");
		logReporter.log("Verify 'chat to us' is displayed as link in error message ", 
				objMobileActions.checkElementDisplayedWithMinWait(lnkchattous));
		
		logReporter.log("click 'Reset your password'  link ", 
				objMobileActions.click(lnkResetyourpassword));
	}
	
	public void verifyMessageLinkDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='mail-btn']//a");
		logReporter.log("Verify 'Mail ' link is displayed in header ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));	
	}
	public void verifyDepositLinkDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='deposit-btn']//a");
		logReporter.log("Verify 'Deposit' link is displayed in header ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));	
	}
	public void verifyBalanceDisplayedInHeader()
	{
		By locator = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='text-data']//a[@id='component_balance']");
		logReporter.log("Verify 'Balance' text is displayed in header ", 
				objMobileActions.checkElementDisplayedWithMinWait(locator));	
		
		By btnToggle = By.xpath("//div[@id='logged-in-bar']//ul//li[@class='text-data']//a[@id='component_balance']//div[@class='balance-toggle balance-off']");
		logReporter.log("Verify 'Balance' Toggle button is displayed in header ", 
				objMobileActions.checkElementDisplayedWithMinWait(btnToggle));	
	}
	
	//Search Functionality	
	public void verifySearchBtnDisplayed()
	{
		logReporter.log("Verify Search button is displayed on Homepage ",
				objMobileActions.checkElementDisplayed(SearchBtn));
	}
	public void clickSearchBtn()
	{
		objwaitMethods.sleep(6);
		logReporter.log(" Click on Search button  on Homepage ",
				objMobileActions.click(SearchBtn));
	}
	public void verifyinputSearchNameFieldDisplayed()
	{
		logReporter.log("Verify Search input field is displayed on Homepage ",
				objMobileActions.checkElementDisplayed(SearchBtn));
	}
	public void setSearchNameField(String Gamename)
	{
		logReporter.log(" input text to search on Homepage ",
				objMobileActions.setText(inpSearchTxt, Gamename));
	}

	public void verifyGameIamgeOnSearchResultsDisplayed(String Gamename)
	{
		By GameNameOnSearchResults = By.xpath("//div[@class='search-results__title heading']/span/strong[contains(text(),'"+Gamename+"')]/preceding::span/img");
		logReporter.log(" Verify Game image is displayed on Search Results ",
				objMobileActions.checkElementDisplayed(GameNameOnSearchResults));
	}
	public void verifyGameNameOnSearchResultsDisplayed(String Gamename)
	{
		By GameNameOnSearchResults = By.xpath("//div[@class='search-results__title heading']/span/strong[contains(text(),'"+Gamename+"')]");

		logReporter.log(" Verify Game Name containing "+ objMobileActions.getText(GameNameOnSearchResults)+ " On Search Results displayed",
				objMobileActions.checkElementDisplayed(GameNameOnSearchResults));
	}

	public void verifyInfoIconOnSearchResultsDisplayed(String Gamename)
	{
		//a[@class='search-results__info']/i[@class='search-results__info-icon']/following::span[contains(text(),'euro')]

		logReporter.log("Verify Info Icon On Search Results displayed",
				objMobileActions.checkElementDisplayed(GameInfoOnSearchResults));
	}

	public void clickInfoIconOnSearchResults()
	{
		logReporter.log(" click on Info Icon On Search Results displayed",
				objMobileActions.click(GameInfoOnSearchResults));
	}

	public void verifyPlayBtnOnSearchResultsDisplayed()
	{
		logReporter.log("Verify Play Button On Search Results displayed",
				objMobileActions.checkElementDisplayed(PlayBtnOnSearchResults));
	}

	public void clickPlayBtnOnSearchResults()
	{
		logReporter.log("Verify Play Button On Search Results displayed",
				objMobileActions.click(PlayBtnOnSearchResults));
	}

	public void verifyNoResultsErrorMSGDisplayed()
	{
		By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
		logReporter.log(" Verify 'Sorry but no matches were found' Message displayed",
				objMobileActions.checkElementDisplayed(NoResultsErrorMSG));
	}
	public void verifybrowseSectionBTNDisplayed()
	{
		logReporter.log(" Verify 'Browse Sections BTN' displayed",
				objMobileActions.checkElementDisplayed(browseSectionBTN));
	}
	public void clickbrowseSectionBTN()
	{
		logReporter.log(" Click 'Browse Sections BTN' displayed",
				objMobileActions.click(browseSectionBTN));
	}

	public void verifycloseBTNDisplayed()
	{
		By closeBTN = By.xpath("//button[@class='search-no-results__close-btn info' and contains(text(),'Close')]");
		logReporter.log(" Verify 'close BTN' on error message  displayed",
				objMobileActions.checkElementDisplayed(closeBTN));
	}

	public void clickCloseSearchBTN()
	{
		By CloseSearchBTN = By.xpath("//button[@class='search__close-btn']");

		logReporter.log("click on X button to close the search results",
				objMobileActions.click(CloseSearchBTN));
	}

	public void verifyIconsDisplayed()
	{
		By SevenIcon = By.xpath("//i[@class='sitemap__icon seven']");
		By chipsIcon = By.xpath("//i[@class='sitemap__icon chip-solid']");

		logReporter.log(" Verify 'Seven Icon' displayed",
				objMobileActions.checkElementDisplayed(SevenIcon));

		logReporter.log(" Verify 'Solid Chips Icon' displayed",
				objMobileActions.checkElementDisplayed(chipsIcon));

	}

	public void verifySectionsDisplayed(String sections)
	{
		if(sections.contains(","))
		{
			String[] arr1 = sections.split(",");

			for (String sections2 : arr1) 
			{
				By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+sections2+"')]");

				logReporter.log(" Verify "+sections2+" is displayed when clicked on Browse section button ",  
						objMobileActions.checkElementDisplayed(sectionName));
			}
		}
	}
	public void clickOnSection(String secname , String url ,String pageTitle)
	{
		By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+secname+"')]");

		objMobileActions.openNewWindow(sectionName);
		objwaitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		objMobileActions.switchToWindowUsingTitle(pageTitle);
		objwaitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		System.out.println("currentURL :: "+currentURL);
		logReporter.log(secname +" Redirected to the correct url : ", currentURL.equalsIgnoreCase(url));
		objMobileActions.switchToWindowUsingTitle("Play Casino Games Online with Bella Casino");
	}

	public void verifyPlayBtnOnGameDetails()
	{
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		logReporter.log(" Verify 'RealPlay Button' displayed",
				objMobileActions.checkElementDisplayed(RealPlayButton));
	}

	public void clickPlayBtnOnGameDetails()
	{
		objwaitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		logReporter.log(" click'RealPlay Button' displayed",
				objMobileActions.click(RealPlayButton));
		objwaitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
	}
	public void verifyDemoPlayBtnOnGameDetails()
	{
		By DemoPlayButton = By.xpath("//button[@class='load-freeplay-game column-4-exsmall column-4-small' and contains(text(),'Demo')]");
		logReporter.log(" Verify 'Demo Play Button' displayed",
				objMobileActions.checkElementDisplayed(DemoPlayButton));
	}

	public void verifyTabsOnGameInfo(String tabname)
	{
		if(tabname.contains("~"))
		{
			String[] arr1 = tabname.split("~");

			for (String tabname2 : arr1) 
			{
				By sectionName = By.xpath("//a[contains(text(),'"+tabname2+"')]");

				logReporter.log(" Verify "+tabname2+" section is displayed on Game info page ",  
						objMobileActions.checkElementDisplayed(sectionName));
			}
		}
	}

	public void clickCloseXonGameInfoPage()
	{
		By CloseXBTN = By.xpath("//button[@class='close']");
		logReporter.log("click on X button on game info page",
				objMobileActions.click(CloseXBTN));
	}

	public void verifySearchOverLay() {
		By search_overLay = By.xpath("//div[@class='field-container']");
		logReporter.log("Check 'search OverLay' ", objMobileActions.checkElementDisplayed(search_overLay));
	}


	public void verifySearchResults() {
		By search_overLay = By.xpath("//li[@class='search-results__category']");
		logReporter.log("Check 'search result section' ", objMobileActions.checkElementDisplayed(search_overLay));
	}

	public void verifyGameDetails(String option,String gamenm) {
		switch (option) {

		case "Image": {
			verifyGameIamgeOnSearchResultsDisplayed(gamenm);
			break;
		}

		case "Name": {
			verifyGameNameOnSearchResultsDisplayed(gamenm);
			break;
		}

		case "Play Now CTA": {
			verifyPlayBtnOnSearchResultsDisplayed();
			break;
		}

		case "Info CTA": {
			verifyInfoIconOnSearchResultsDisplayed(gamenm);
			break;
		}
		}
	}
}