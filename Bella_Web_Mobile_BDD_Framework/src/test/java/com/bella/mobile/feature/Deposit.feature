Feature: Deposit

@Android2
Scenario: Check whether POPF (Protection of Customer Funds) message / pop-up screen is displayed to the customer while trying to make the customers 1st deposit
Given Invoke the Bella site
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header
Then Verify 'Add new payment Method' screen is displayed
Then Select "paypal" payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Verify POPF checkbox is displayed to the customer

@Android2
Scenario: Check that system displays error message on trying to deposit an amount less than the minimum deposit limit of the site
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "paypal" payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "1"
And User will get "The deposit value must be greater than £5" message

@Android2
Scenario: Check whether user able to deposit using Bonus code
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get player bonus balance
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "Card" payment method
Then Enter Card number as "4462 0300 0000 0000"
And Select Exp Month as "02" and Year as "2022"
And Enter CVN as "737"
And Click On 'Add Card'
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
And Click on Deposit button
And Again Enter CVN as "737"
And Click on Deposit button
#Then Verify Bonus recieved amount is displayed on Deposit confirmation message
#And Click on " Play now" button
#Then Click on my account button
Then Verify Deposit Successful confirmation message
Then Click on my account button
And Verify Received Bonus amount is displayed under playable balance

@Android2
Scenario: New user will be redirected to the home page when user make an immediate successful deposit after registration
Given Invoke the Bella site
When Click on Sign Up button
Then Verify Registration page is appered
Then Enter email address 
And Click on Next button
Then Enter username
Then Enter Password
Then Select Title
Then Enter FirstName
Then Enter Surname
Then Enter data of birth
Then Enter Mobile Number
Then Select the option enter it manually available near Address field
And Verify following fields get displayed to enter the address manually 
|Address Line 1|
|Address Line 2|
|Town/City|
|County|
|Postcode|
Then Enter address manually
#Then Select DepositLimit as "No"
Then Click on Register
Then Verify Deposit Now header
And Get current balance from Deposit screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "Card" payment method
Then Enter Card number as "4462 0300 0000 0000"
And Select Exp Month as "01" and Year as "2022"
And Enter CVN as "737"
And Click On 'Add Card'
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "5"
And Click on Deposit button
And Again Enter CVN as "737"
And Click on Deposit button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account


@Android2
Scenario: Check whether user able to deposit using Card payment method
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "Card" payment method
Then Verify following errors are displayed when fields are empty
|Card Number is required|
|Expiry Month is required|
|Expiry Year is required|
|CVN is required|
Then Enter Card number "4462 0300 0000 0000"
And Select Exp Month as "01" and Year as "2022"
And Enter CVN as "737"
And Click On 'Add Card'
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
And Click on Deposit button
And Again Enter CVN as "737"
And Click on Deposit button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account

@Android211
Scenario: Check whether user is able to deposit using Paypal payment method
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "paypal" payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
And Click on "Deposit" button
Then Enter email address "paypal1@aquagaming.com" and Password "Qqq.111!"
And Click on Login from Paypal screen
And Click on 'Pay Now' button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account

@Android2
Scenario: Check whether user is able to deposit using PaySafeCard payment method
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "paysafecard" payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
#And Click on "Deposit" button
Then Click on "Next" button
#Then Verify it redirect to "Pay with paysafecard" window
Then Enter Pin number "0000 0000 0990 5165" and Accept TnC 
And Click on Pay
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account


@Android2
Scenario: Check that system displays an error message on trying to withdraw an amount less than the minimum withdraw limit of the site
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "4"
And Click on " Withdraw" button
Then Verify 'The minimum amount you can withdraw is  £10.00 ' message should be display

@Android2
Scenario: Check whether system displays success message after performing withdrawal successfully
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "10"
And Click on " Withdraw" button
Then Verify "Your withdrawal request for £10 was successful. Withdrawal reference" message is displayed with " Ok" button
Then Verify Menu screen get closed when click on " Ok" button

@Android2
Scenario: Check that system allows to withdraw the amount minimum 10
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "10"
And Click on " Withdraw" button
Then Verify "Your withdrawal request for £10 was successful. Withdrawal reference" message is displayed with " Ok" button
Then Verify Menu screen get closed when click on " Ok" button

@Android2
Scenario: Check whether system displays transaction history is ordered by date with the latest date on the top of the list
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Verify latest date transaction history is displayed on the top of the list

@Android
Scenario: Transaction History_Check whether system can filter transaction by Date and display correctly
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Select Filter Activity as "Deposits"
Then Set Date range and click on Filter button
Then Verify transaction history is displayed as per selected date
Then Set same date filter and verify Transcation history results

@Android
Scenario: Transaction History_Check whether system can filter transaction by its type and display correctly
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Verify following options are displayed under activity filter dropdown
|All Activity|
|Net Deposits|
|Deposits|
|Withdrawals|
|Bonuses|
|Wagers|
Then Select Filter Activity as "Deposits"
Then Verify filter results are displayed as per selected activity
Then Select Filter Activity as "Withdrawals"
Then Verify filter results are displayed as per selected activity

@Android
Scenario: Check that customers must see 'Sort by period' with the following dropdown options:Last 24 Hours, Last 7 days, Last 30 Days,3 months
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Select Filter Activity as "Net Deposits"
And Verify following period options are displayed under 'Sort by Period' dropdown
|Last 24 Hours|
|Last Week|
|Last 30 Days|
|3 Months|
Then Select period as "Last 24 Hours"
And Verify transaction is displayed within the period selected
