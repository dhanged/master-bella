Feature: Promotions

@Desktop
Scenario: Promotion Page_check that system displays promotions details page with all the required details
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Promos" tab
Then Verify following tab is displayed under secondary navigation bar
|All|
|My Promotions|
And Verify system displays all the available promotions on the screen.
Then Open opt-in promotions from listed ones
Then Verify Expiry date is displayed on promorion details page

@Desktop
Scenario: Check that customer can Opt In for a promotion from the Promotions details page
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Promos" tab
Then Open opt-in promotions from listed ones
Then Verify Expiry date is displayed on promorion details page
Then Click on opt-in button
And Verify Optedin and Opt out button is displayed on page

@Desktop
Scenario: Check that customer can Opt In/claim for a promotion from the Promotions list
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button
Then Navigate to "Promos" tab
And Verify system displays all the available promotions on the screen.
Then Select the Opt-In or Claim button for Opt In promotion
Then Verify Opted-in or Claimed button is displayed on page