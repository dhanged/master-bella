Feature: Reality Check

@Android
Scenario: Check that system allows to set the reality check for the user
Given Invoke the Bella site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Reality Check" tab from my account screen
Then Verify infotext on Reality checks screen
Then Verify following reminder options
|15 mins|
|30 mins|
|1 hour|
Then Select reminder option
And Click on 'Save a changes' button
Then Verify updated option displayed correctly