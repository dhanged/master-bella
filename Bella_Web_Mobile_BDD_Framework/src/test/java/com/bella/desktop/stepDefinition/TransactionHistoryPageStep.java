package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.TransactionHistoryPage;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class TransactionHistoryPageStep {

	private TransactionHistoryPage objTransactionHistoryPage;	
	String activityType;
	
	public TransactionHistoryPageStep(TransactionHistoryPage objTransactionHistoryPage) {		
		this.objTransactionHistoryPage = objTransactionHistoryPage ;

	}
	
	@Then("^Verify latest date transaction history is displayed on the top of the list$")
	public void verify_latest_date_transaction_history_is_displayed_on_the_top_of_the_list(){
		objTransactionHistoryPage.verifyLatestTransactionDisplayOnTop();
	}

	@Then("^Verify 'Net Deposit' option is available in Activity/Filter type dropdown field$")
	public void verify_Net_Deposit_option_is_available_in_Activity_Filter_type_dropdown_field() {
		objTransactionHistoryPage.clickAllActivityDropdown("All Activity");
		objTransactionHistoryPage.verifyActivityTypesDisplayed("Net Deposits");
	}

	@Then("^Select Filter Activity as \"([^\"]*)\"$")
	public void select_Filter_Activity_as(String activity){
		activityType = activity ;
		String CurrentSelectedactivity = objTransactionHistoryPage.getSelectedActivity();
		
		CurrentSelectedactivity = objTransactionHistoryPage.capitiliseInitialLetterOfEachWord(CurrentSelectedactivity);
		System.out.println("**************  activity  "+CurrentSelectedactivity);
		objTransactionHistoryPage.clickAllActivityDropdown(CurrentSelectedactivity);
		objTransactionHistoryPage.selectActivityType(activity);
	}

	@Then("^'No transactions found' message will display to the user when no net deposit available$")
	public void no_transactions_found_message_will_display_to_the_user_when_no_net_deposit_available() {
		objTransactionHistoryPage.verifyNoTransactionFoundMSGdisplayed();
	}

	@Then("^Net Deposit information text is displayed on screen$")
	public void net_Deposit_information_text_is_displayed_on_screen() {
		objTransactionHistoryPage.verifyNetDepositINFOtxt();
	}

	@Then("^Set Date range and click on Filter button$")
	public void set_Date_range_and_click_on_Filter_button()  {
		objTransactionHistoryPage.setFromDate();
		objTransactionHistoryPage.setToDate();
		objTransactionHistoryPage.verifyFilterButtonDisplayed();
		objTransactionHistoryPage.clickFilterButton();
	}

	@Then("^Verify transaction history is displayed as per selected date$")
	public void verify_transaction_history_is_displayed_as_per_selected_date()  {
		objTransactionHistoryPage.clickExpandButton();
		objTransactionHistoryPage.verifyResultOnTableDisplayed("Time~Type~Amount", "Deposit","Reference");
	}

	@Then("^Set same date filter and verify Transcation history results$")
	public void set_same_date_filter_and_verify_Transcation_history_results()  {
		objTransactionHistoryPage.setFromDateSameDay();
		objTransactionHistoryPage.setToDate();
		objTransactionHistoryPage.clickFilterButton();
		objTransactionHistoryPage.clickExpandButton();
		objTransactionHistoryPage.verifyResultOnTableDisplayed("Time~Type~Amount", "Deposit","Reference");
	}

	@Then("^Verify following options are displayed under activity filter dropdown$")
	public void verify_following_options_are_displayed_under_activity_filter_dropdown(DataTable dt) {
		objTransactionHistoryPage.clickAllActivityDropdown("All Activity");
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objTransactionHistoryPage.verifyActivityTypesDisplayed(list.get(i));}
		objTransactionHistoryPage.clickAllActivityDropdown("All Activity");
	}

	@Then("^Verify filter results are displayed as per selected activity$")
	public void verify_filter_results_are_displayed_as_per_selected_activity() {
		if(activityType.equalsIgnoreCase("Deposits"))
		{objTransactionHistoryPage.verifyResultOnTableDisplayed("Time~Type~Amount", "Deposit","Reference");}
		else if(activityType.equalsIgnoreCase("Withdrawals"))
		{objTransactionHistoryPage.verifyResultOnTableDisplayed("Time~Type~Amount", "Withdrawal","Reference:");}
	}

	@Then("^Verify following period options are displayed under 'Sort by Period' dropdown$")
	public void verify_following_period_options_are_displayed_under_Sort_by_Period_dropdown(DataTable dt) {
		objTransactionHistoryPage.verifySortByLabelDisplayed();
		objTransactionHistoryPage.clickTimePeriodDropdown("Last 24 Hours");
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objTransactionHistoryPage.verifyTimeFrameOptions(list.get(i));}
		objTransactionHistoryPage.clickTimePeriodDropdown("Last 24 Hours");
	}

	@Then("^Select period as \"([^\"]*)\"$")
	public void select_period_as(String arg1) {
		objTransactionHistoryPage.clickTimePeriodDropdown("Last 24 Hours");
		objTransactionHistoryPage.selectTimeFromDropdown(arg1);
	}

	@Then("^Verify transaction is displayed within the period selected$")
	public void verify_transaction_is_displayed_within_the_period_selected(){
		objTransactionHistoryPage.verifyNetDepositResultTable("Deposits~Withdrawals~Net Deposits");
	}
}
