
package com.bella.mobile.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "src/test/java/com/bella/mobile/feature", strict = true, plugin = { "json:target/JSON/Android.json",
		"rerun:target/SyncFails.txt", "com.generic.cucumberReporting.CustomFormatter" }, 
		tags = { "@Android" }, 
		dryRun = false, monochrome = true,
		glue = { "com.hooks", "com.bella.mobile.stepDefinition"}
)

public class AndroidRunner extends AbstractTestNGCucumberTests { }