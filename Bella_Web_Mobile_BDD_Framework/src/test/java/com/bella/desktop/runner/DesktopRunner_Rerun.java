
package com.bella.desktop.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "@target/SyncFails.txt", 
				strict = true, 
				plugin = { "json:target/JSON/DesktopRerun.json", "com.generic.cucumberReporting.CustomFormatter"}, 		
				monochrome = true,
				glue = { "com.hooks",	"com.bella.desktop.stepDefinition" }
		)

public class DesktopRunner_Rerun extends AbstractTestNGCucumberTests {
}