package com.generic;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mifmif.common.regex.Generex;

public class Test {
	public static void main(String[] args) {
		try {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/chromedriver_win32/chromedriver.exe");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--incognito");
			chromeOptions.setCapability("initialBrowserUrl", "about:blank");
			chromeOptions.setBinary(System.getProperty("user.dir") + "/src/main/resources/browsers/Chrome/Application/chrome.exe");
			WebDriver driver = new ChromeDriver(chromeOptions);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
			
			driver.get("https://np01-mecca-cms.rankgrouptech.net/registration");
			driver.findElement(By.xpath("//div[contains(@class,'join-now')]/form/fieldset[2]/div/div/input")).sendKeys("abc@gmail.com");
			driver.findElement(By.id("id-agreed")).click();
			driver.findElement(By.id("id-agreed")).click();
			
			Actions action = new Actions(driver);
			action.moveToElement(driver.findElement(By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button"))).click().build().perform();
			WebElement elem = driver.findElement(By.xpath("//div[contains(@class,'join-now')]/form/div/div/div[3]"));
			String errorVisibleClass = elem.getAttribute("class");
			System.out.println(errorVisibleClass);
			if (!errorVisibleClass.contains("show")) {			
				action.moveByOffset(200,200).click().build().perform();
				elem = driver.findElement(By.xpath("//div[contains(@class,'join-now')]/form/div/div/div[3]"));
				errorVisibleClass = elem.getAttribute("class");
				System.out.println(errorVisibleClass);
			}
			if (errorVisibleClass.contains("show")) {
				WebElement errorMessage = elem.findElement(By.tagName("li"));
				System.out.println(errorMessage.getText());
				System.out.println(errorMessage.getText().contentEquals("Please confirm you are at least 18 years old"));
			}
			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void regnerex() {
		String regex = "[a-zA-Z0-9]{5}\\@gmail\\.com";
		Generex g = new Generex(regex);
		for (int i = 0; i < 15; i++) {			
			String randomEmail = g.random();
			// System.out.println(randomEmail);
		}
		System.out.println("---");
		for (int i = 0; i < 15; i++) {			
			// System.out.println(getSaltString()+"@gmail.com");
		}
		System.out.println("---");
		for (int i = 0; i < 25; i++) {			
			System.out.println(getSaltString(15));
		}
		System.out.println("---");
		for (int i = 0; i < 25; i++) {			
			System.out.println(getSaltString(5));
		}
	}
	
	private static String getSaltString(int size) {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < size) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
