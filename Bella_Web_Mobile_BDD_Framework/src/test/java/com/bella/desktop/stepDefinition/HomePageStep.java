/**
 * 
 */
package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.HomePage;
import com.bella.desktop.page.Bella.PageValidation;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author vaishali bhad
 *
 */
public class HomePageStep {
	
	private HomePage objHomePage;
	private Utilities objUtilities;
	private Configuration configuration;
	private PageValidation objPageValidation ;
	
	public HomePageStep(HomePage objHomePage,Utilities objUtilities,Configuration configuration,PageValidation objPageValidation) {		
		this.objHomePage = objHomePage;
		this.objUtilities = objUtilities;
		this.configuration= configuration;
		this.objPageValidation = objPageValidation ;
	}
	
	@Given("^Invoke the Bella site on Desktop$")
	public void invoke_the_Bella_site_on_Desktop(){
		objHomePage.verifyBellaCasinosLogoDisplayed();
	}

	@Then("^Close Sign up popup$")
	public void close_Sign_up_popup() {
		
		objHomePage.clickCloseSignUp();
	}
	@When("^User clicks on Login Button from header of the Page$")
	public void user_clicks_on_Login_Button_from_header_of_the_Page() {
		objHomePage.VerifyLogin();
		objHomePage.clickLogin();
	}
	
	@Then("^Verify user is successfully logged in$")
	public void Verify_user_is_successfully_logged_in() {
		objHomePage.VerifyMyAccount();
	
	}
	
	@Then("^Verify header displays join now button$")
	public void verify_header_displays_join_now_button() {
		objHomePage.verifyJoinNow();	
	}
	
	@Then("^Click on my account button$")
	public void click_on_my_account_button() {
		objHomePage.clickMyAccount();
	}
	@Then("^Navigate to invalid url$")
	public void navigate_to_invalid_url(){
		objHomePage.NavigateToInvalidUrl();
	}

	@Then("^Verify (\\d+) error title$")
	public void verify_error_title(int arg1){
		objHomePage.verifyErrorTitle();
	}
	@When("^Click on Sign Up button$")
	public void click_on_Sign_Up_button()  {
		objHomePage.clickJoinNow();
	}
	
	@Then("^Verify following links are displayed in header$")
	public void verify_following_links_are_displayed_in_header(DataTable dt)
	{
		objHomePage.verifyBalanceDisplayedInHeader();
		objHomePage.verifyDepositLinkDisplayedInHeader();
		objHomePage.verifyMessageLinkDisplayedInHeader();
	}
	
	@Then("^Verify following tab is displayed under secondary navigation bar$")
	public void verify_following_tab_is_displayed_under_secondary_navigation_bar(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objHomePage.verifySecondaryTABdisplayed(list.get(i));}
	}
	@Then("^select following tabs and verify games is displayed on selected tab$")
	public void select_following_tabs_and_verify_games_is_displayed_on_selected_tab(DataTable dt) throws Throwable {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objHomePage.clickSecondaryTABdisplayed(list.get(i));
			//objHomePage.verifyGamesOnSlotsPage();
			}
	}
	@Then("^Select \"([^\"]*)\" tab from navigation tab$")
	public void select_tab_from_navigation_tab(String arg1) {
		objHomePage.clickSecondaryTABdisplayed(arg1);
	}

//search functionality
	@When("^User clicks on search field$")
	public void user_clicks_on_search_field() {
		objHomePage.verifySearchBtnDisplayed();
		objHomePage.clickSearchBtn();
	}

	@Then("^Verify Search overlay opens$")
	public void verify_Search_overlay_opens() {
		objHomePage.verifySearchOverLay();
	}

	@When("^Search Game \"([^\"]*)\" in Search field from header$")
	public void search_Game_in_Search_field_from_header(String txt) {
		objHomePage.setSearchNameField(txt);
	}

	@Then("^Verify system displays search results to user$")
	public void verify_system_displays_search_results_to_user() {
		objHomePage.verifySearchResults();
	}

	@Then("^Verify system displays following options for search game \"([^\"]*)\"$")
	public void verify_system_displays_following_options_for_search_game(String txt, DataTable dt) throws Throwable {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objHomePage.verifyGameDetails(objUtilities.getListDataFromDataTable(dt).get(i),txt);
		}
	}
	
	@Then("^Verify system displays no search is found error message$") // random values in search box
	public void verify_system_displays_no_search_is_found_error_message() {
		objHomePage.verifyNoResultsErrorMSGDisplayed();
	}
	
	@Then("^System displays browse section with error message$")
	public void system_displays_browse_section_with_error_message() {
		objHomePage.verifyNoResultsErrorMSGDisplayed();
		objHomePage.verifybrowseSectionBTNDisplayed();
		objHomePage.verifycloseBTNDisplayed();
	}

	@And("^Click on browse section$")
	public void click_on_browse_section() {
		objHomePage.clickbrowseSectionBTN();
	}

	@Then("^System allows to browse the sections on clicking on error messages observed for wrong search$")
	public void system_allows_to_browse_the_sections_on_clicking_on_error_messages_observed_for_wrong_search() {
		
		objHomePage.verifyIconsDisplayed();
		objHomePage.verifySectionsDisplayed("All Slots,Top Slots,Jackpots,New Slots,All Live Casino");
		
		String url = configuration.getConfig("web.Url");
		
		objHomePage.clickOnSection("Jackpots",url+"jackpots" , "Jackpots | Bella Casino");
		objHomePage.clickOnSection("New Slots",url+"slots-and-games/new" ,"Play New Slots Online | Bella Casino");
		objHomePage.clickOnSection("Top Slots",url+"slots-and-games/featured" ,"Play Featured Slots Online | Bella Casino");
		objHomePage.clickOnSection("All Live Casino",url+"casino" ,"Online Casino | Play Casino Games Online | Bella Casino");
		objHomePage.clickOnSection("All Slots",url+"slots-and-games/all-slots" ,"Play Slots Online | Bella Casino");
	}
	
	@Then("^Verify following tabs is displayed on primary navigation bar$")
	public void verify_following_tabs_is_displayed_on_primary_navigation_bar(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objPageValidation.verifyPrimaryTabs(list.get(i));}
	}
	
	@Then("^Verify Primary tabs are accessible and it redirects to correct page$")
	public void verify_Primary_tabs_are_accessible_and_it_redirects_to_correct_page(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objPageValidation.verifyPrimaryTabsRedirectToTheCorrectPage(list.get(i));}
	}
	
	@Then("^Verify content on Home page$")
	public void verify_content_on_Home_page() {
		objPageValidation.verifySerachFinderInHeader();
		//objPageValidation.verifyCarouselDisplayedOnPage();
		
		objHomePage.verifyBalanceDisplayedInHeader();
		objHomePage.verifyDepositLinkDisplayedInHeader();
		objHomePage.verifyMessageLinkDisplayedInHeader();
	}

	@Then("^Verify secondary tabs are accessible on \"([^\"]*)\" page$")
	public void verify_secondary_tabs_are_accessible_on_page(String arg1) {
		objPageValidation.verifySecondaryPagesContent(arg1);
	}
}
