package com.bella.mobile.page.PAM;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class PAMLoginPage {
	
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	
	public PAMLoginPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}
	
	public void setUserName(String userName){
		By inpUserName = By.id("Username");
		logReporter.log("Set user name", 
				objWebActions.setText(inpUserName, userName));
	}
	
	public void setPassword(String password){
		By inpPassword = By.id("Password");
		logReporter.log("Set password", 
				objWebActions.setText(inpPassword, password));
	}
	
	public void clickSignIn(){
		By btnSignIn = By.id("loginSubmit");
		logReporter.log("Click Sign In button", 
				objWebActions.click(btnSignIn));
	}
	
	public void invokePAMURL(String url)
	{
		objDriverProvider.initialize();
		objDriverProvider.getWebDriver().get(url);;
	}
	
}
