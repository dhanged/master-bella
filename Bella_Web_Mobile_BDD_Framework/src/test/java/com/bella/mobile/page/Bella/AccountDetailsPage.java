package com.bella.mobile.page.Bella;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class AccountDetailsPage {
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public AccountDetailsPage(MobileActions mobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ,Utilities utilities) {
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = utilities ;
	}

	By logoutLink = By.xpath("//a[@class='logout-link']");
	By lastLoginLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]");
	By NameLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Name')]");
	By EmailLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Email')]");
	By DOBLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Date of birth')]");
	By PhoneLabel =By.xpath("//span[@class='list-detail' and contains(text(),'Phone')]");
	By PostcodeLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Postcode')]");
	By EditLink = By.xpath("//a[@class='edit-details']");
	By NonEditableTXT = By.xpath("//span[@class='non-editable']");
	By editEmail = By.xpath("//input[@id='edit-email']");
	By editPhone = By.xpath("//input[@id='edit-phone']");

	public void clickAccountDetail()
	{
		By locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'Account Details')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Account details ",  
				objMobileActions.click(locator));
	}

	public void clickChangePassword()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Change Password')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Change Password  ",  
				objMobileActions.click(locator));
	}

	public void clickMarketingPreferences()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Marketing Preferences')]");
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

		logReporter.log(" Click on Marketing Preferences",  
				objMobileActions.click(locator));

		By header = By.xpath("//span[@class='h2 '][contains(text(),'Marketing Preferences')]");
		logReporter.log(" Verify Marketing Preferences Header displayed ",  
				objMobileActions.checkElementDisplayed(header));
	}

	//update Marketing Preferences

	public void verifyMarketingInfoText()
	{
		By infotxt = By.xpath("//fieldset[@class='change_password_marketing_preferences']//p[contains(text(),'How do you prefer to be kept up to date with awesome offers, prime promos and fun freebies on Bella')]");

		logReporter.log(" Verify 'How do you prefer to be kept up to date with awesome offers, prime promos and fun freebies on Bella ...' Info message displayed ",  
				objMobileActions.checkElementDisplayed(infotxt));
	}


	public void updateMarketingPreferences(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				logReporter.log(" Click on Marketing Preferences: " +pref1,  
						objMobileActions.click(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			logReporter.log(" Click on Marketing Preferences" +pref,  
					objMobileActions.click(locator));
		}
	}

	public void verifyMarketingPreferencesCheckboxesIsSelectedOrNot(String pref) 
	{
		if(pref.contains("~")){
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) {
				By locator = By.xpath("//input[contains(@id,'"+pref1 +"')][@type='checkbox']");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objMobileActions.isCheckBoxSelected(locator));}}
		else{
			By locator = By.xpath("//input[contains(@id,'"+pref+"')][@type='checkbox']");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objMobileActions.isCheckBoxSelected(locator));}
	}


	public void verifyMarketingPreferencesOption(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objMobileActions.checkElementDisplayed(locator));
		}
	}

	public void clickUpdatePreferencesBtn()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");

		logReporter.log(" Click on Update button",  
				objMobileActions.click(locator));

	}
	//Change Password

	public void setCurrentPassword(String currpassword)
	{
		By locator= By.xpath("//input[@id='edit-current-password']");
		logReporter.log(" Set on Current Password  ",  
				objMobileActions.setText(locator, currpassword));

	}
	public void setNewPassword(String newPwd)
	{
		By locator= By.xpath("//input[@id='edit-new-password']");
		logReporter.log(" Set New Password: "+newPwd,  
				objMobileActions.setText(locator, newPwd));
	}

	public void clickUpdatePasswordBTN()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");
		logReporter.log(" Click on Update button to change password   ",  
				objMobileActions.click(locator));

	}


	public void verifyLabels(String label )
	{
		/*	By Label01 = By.xpath("//span[@class='list-detail' and contains(text(),'"+label+"')]");
				//System.out.println(" Label  " +labl1);
				logReporter.log(" Verify "+label+"  is displayed",
						objMobileActions.checkElementDisplayed(Label01));*/

		if(label.contains(","))
		{
			String labl[] = label.split(",");
			for (String labl1 : labl)
			{
				By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labl1+"')]");
				//System.out.println(" Label  " +labl1);
				logReporter.log(" Verify "+label+"  is displayed",
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+label+"')]");
			//System.out.println(" Label  " +labl1);
			logReporter.log(" Verify "+label+"  is displayed",
					objMobileActions.checkElementDisplayed(locator));
		}
	}

	public String getLastLoginFormat()
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");
		return objMobileActions.getText(lastLoginDate);
	}

	public void verifyEditlinkDisplayed()
	{
		logReporter.log(" Verify Edit Link is displayed",
				objMobileActions.checkElementDisplayed(EditLink));
	}

	public void clickEditLink()
	{
		logReporter.log(" Click on Edit Link is displayed",
				objMobileActions.click(EditLink));
	}

	public void verifyNonEditableTXTDisplayed()
	{
		logReporter.log(" Verify Non Editable TXT is displayed",
				objMobileActions.checkElementDisplayed(NonEditableTXT));
	}

	public void clickClearBtn()
	{
		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));

	}
	public void EditEmail()
	{
		String newEmail = objUtilities.getRandomAlphanumericEmailString(10, "@mailinator.com");

		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));

		logReporter.log(" Set Email ",  
				objMobileActions.setText(editEmail, newEmail));
	}

	public void EditPhoneNumber()
	{
		String newMobile ="077009" + objUtilities.getRandomNumeric(5);

		By clearBtn = By.xpath("//input[@id='edit-phone']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));

		logReporter.log(" Set Mobile number ",  
				objMobileActions.setText(editPhone, newMobile));
	}
	public void inputPassword(String password)
	{
		By inputPassword = By.xpath("//input[@id='edit-password']");
		logReporter.log(" Enter your password to update",  
				objMobileActions.setText(inputPassword, password));
	}

	public void showPasswordBTNdisplayed()
	{
		By showBTN = By.xpath("//button[contains(text(),'Show')]");

		logReporter.log(" Verify Show button is displayed for Password ",
				objMobileActions.checkElementDisplayed(showBTN));

		logReporter.log(" click on Show button is displayed for Password ",
				objMobileActions.click(showBTN));

	}


	public void clickUpdateBtn()
	{
		By updateButton = By.xpath("//fieldset[contains(@class,'form-buttons ')]/div/button[contains(text(),'Update')]");
		logReporter.log(" Click on Update button  ",  
				objMobileActions.click(updateButton));
	}

	public void verifyAccountUpdateSucsessMSGdisplayed(String msg)
	{
		By updateMsg = By.xpath("//div[@class='myaccount-success' and contains(text(),'"+msg+"')]");

		logReporter.log(" Verify Account details updated is displayed",
				objMobileActions.checkElementDisplayed(updateMsg));

	}

	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");
		logReporter.log(" click on ' X' Button'displayed on Bonuse History screen ", 
				objMobileActions.click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");

		logReporter.log(" click on logout-link displayed  ", 
				objMobileActions.click(logoutLink));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));

	}

	public void validateLastLoginFormatDisplayed(String lastLoginTime)
	{
		/*By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");
		String lastLogin =objWebActions.getText(lastLoginDate);
		 */
		/*if(lastLogin.equalsIgnoreCase("Never"))
		{
			logReporter.log("last login date displayed as: "+lastLogin , true);
		}
		else*/
		{
			lastLoginTime=lastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
			DateFormat lastFormat = new SimpleDateFormat("dd MMM yyyy, hh:mm:ss a");
			try
			{
				Date lastlogindate = lastFormat.parse(lastLoginTime);
				logReporter.log("last login date: "+lastLoginTime+" displayed in correct format" , true);

			}catch(Exception e)
			{
				logReporter.log("last login date :"+lastLoginTime+" displayed in correct format" , false);
			}
		}

	}

	public void verifyThatLastLoginTimeIsDisplayedCorrectly(String expectedLoginTime)
	{
		try {
			String CurrentlastLoginTime = this.getLastLoginFormat();
			CurrentlastLoginTime = CurrentlastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
			expectedLoginTime = expectedLoginTime.substring(0,expectedLoginTime.lastIndexOf(":"));
			CurrentlastLoginTime = CurrentlastLoginTime.substring(0,CurrentlastLoginTime.lastIndexOf(":"));
			DateFormat lastFormat = new SimpleDateFormat("dd MMMM yyyy, hh:mm");
			Date lastlogindate = lastFormat.parse(CurrentlastLoginTime);
			CurrentlastLoginTime = lastFormat.format(lastlogindate);
			System.out.println(" expectedLoginTime ::: "+expectedLoginTime+ "  CurrentlastLoginTime :: "+CurrentlastLoginTime);
			logReporter.log("last login Time is displayed correctly" , CurrentlastLoginTime.contains(expectedLoginTime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
