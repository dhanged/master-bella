package com.bella.desktop.page.Bella;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class WithdrawPage {

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	boolean flag = false;

	public WithdrawPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}
	
	
	public void clickContinue() {
		By btnContinue = By.xpath("//button[@type='submit' and contains(text(),'Continue')]");
		logReporter.log("Click 'Continue' button ",  
				objWebActions.click(btnContinue));
	}

	public boolean verifyAreYouSureToWithdrawHeadeDisplayed() {
		By locator = By.xpath("//div[contains(text(),'Are you sure')]");
		return objWebActions.checkElementDisplayed(locator);
	}

	public void switchToWithdrawalIframe()
	{
		objWebActions.switchToFrameUsingNameOrId("withdrawal-process");
	}
	
	public void verifyMyAccountMenuScreenGetClosed()
	{
		By locator = By.xpath("//div[@class='component slideout-overlay myaccount open display-enter-done']");
		logReporter.log("Verify my account menu screen does not displayed ", 
				objWebActions.checkElementNotDisplayed(locator));
	}
	public void verifyTextOnWithdrawScreen(String txt)
	{
		//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'Total Balance:')]
		//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'Available to Withdraw:')]
		By locator = By.xpath("//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'"+txt+":')]");
		logReporter.log("Verify ' " +txt+ " ' is displayed on withdraw sreen", 
				objWebActions.checkElementDisplayed(locator));
	}
	
	public void setWithdrawAmount(String amount)
	{
		By inpWithdrawAmount = By.xpath("//label[contains(.,'£')]//following::input[@id='withdraw-amount-box']");
		logReporter.log("Set Withdraw Amount ", amount, 
				objWebActions.setTextWithClear(inpWithdrawAmount, amount));
	}
	
	public void verifyYourWithdrawalSuccessMessage(String txt) 
	{
		By imgsymbolsuccess = By.xpath("//*[@class='symbol success-mark medium']");
		logReporter.log("Verify ' success image ' ", 
				objWebActions.checkElementDisplayed(imgsymbolsuccess));
		
		//By locator = By.xpath("//p[@class=\"text-center text-success\"][contains(.,'Your withdrawal request for £')]");
		By locator = By.xpath("//p[@class='text-center text-success'][contains(.,'"+txt+"')]");
		logReporter.log("Verify '"+txt+ " ' ", 
				objWebActions.checkElementDisplayed(locator));
	}
	
	public void verifyMinimumWithdrawLimitErrorMessage()
	{
		By locator = By.xpath("//div[contains(@class,' alert alert-danger')][contains(.,' The minimum amount you can withdraw is')]");
		logReporter.log("Verify 'The minimum amount you can withdraw is  £10.00  ' ", 
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyTransactionWillAppearAsBellaInfoText()
	{
		By locator = By.xpath("//span[contains(.,'The transaction will appear as ')][contains(.,'BELLA')][contains(.,' on your bank statement.')]");
		logReporter.log("Verify 'The transaction will appear as BELLA on your bank statement. ' ", 
				objWebActions.checkElementDisplayed(locator));
	}
	
	public String getAmount(String txt)
	{
		By locator = By.xpath("//span[contains(.,'"+txt+":')]//following-sibling::span");
		return objWebActions.getText(locator);
	}
	
	public void verifyInfoText(String txt)
	{
		By locator = By.xpath("//p[contains(.,'"+txt+"')]");
		logReporter.log("Verify " +txt , 
				objWebActions.checkElementDisplayed(locator));
	}
	public void verifyPleasecontactsupportshouldyouneedanyassistancemessage()
	{
		By locator = By.xpath("//p[contains(text(),'Please contact')  and contains(.,' should you need any assistance.')]//a[contains(.,'support')]");
		logReporter.log("Verify 'Please contact support should you need any assistance.' ", 
				objWebActions.checkElementDisplayed(locator));
	}
}
