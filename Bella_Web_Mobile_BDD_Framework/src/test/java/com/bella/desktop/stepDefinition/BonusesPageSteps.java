package com.bella.desktop.stepDefinition;

import java.util.List;

import com.bella.desktop.page.Bella.BonusesPage;
import com.bella.desktop.page.Bella.LogInPage;
import com.bella.desktop.page.cogs.Cogs_PlayerDetailsPage;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class BonusesPageSteps {

	private BonusesPage objBonusesPage;
	private Configuration objconfiguration;
	Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage;
	private LogInPage objLogInPage;
	private Utilities utilities;	
	String	BonusName ,BonusType,fromDate,toDate;

	public BonusesPageSteps(BonusesPage objBonusesPage,Configuration configuration,Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage,LogInPage objLogInPage,Utilities utilities) 
	{		
		this.objBonusesPage = objBonusesPage ;
		this.objconfiguration = configuration ;
		this.objCogs_PlayerDetailsPage = objCogs_PlayerDetailsPage ;
		this.objLogInPage = objLogInPage ;
		this.utilities = utilities ;
	}

	@Then("^Enter invalid bonus code$")
	public void enter_invalid_bonus_code(){
		objBonusesPage.verifyEnterBonusCodeTXTDisplayed();
		objBonusesPage.setPromoCode("dfgf");
	}

	@Then("^Click on submit$")
	public void Click_on_submit(){
		objBonusesPage.clickSubmitcode();
	}

	@Then("^Click on \\+ button of any claimed bonus$")
	public void click_on_button_of_any_claimed_bonus()  {
		if(objBonusesPage.verifyExpandButtonDisplayed())
			objBonusesPage.clickExpandButton();
	}

	@Then("^System should expand bonus details$")
	public void system_should_expand_bonus_details(){
		objBonusesPage.VerifyCollapseButton();
	}

	@Then("^Click on - button of any claimed bonus$")
	public void click_on_Collapse_button_of_any_claimed_bonus()  {
		objBonusesPage.clickCollapseButton();
	}

	@Then("^System should collapse the bonus details$")
	public void system_should_collapse_the_bonus_details(){
		objBonusesPage.verifyExpandButtonDisplayed();
	}

	@Then("^Verify following details should be displayed on Expanded view$")
	public void verify_following_details_should_be_displayed_on_Expanded_view(DataTable dt){
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objBonusesPage.verifyBonusDetailsAfterExpandingBonus(list.get(i));}
	}

	@Then("^Enter bonus code as \"([^\"]*)\"$")
	public void enter_bonus_code_as(String promocode)  {
		objBonusesPage.setPromoCode(promocode);
	}

	@Then("^Promotion details will display to user$")
	public void promotion_details_will_display_to_user() {
		objBonusesPage.verifyTermsnConditionsLinkDisplayed();
		BonusName = objBonusesPage.getBonusName();
		System.out.println("Bonus Name : " +BonusName);
		objBonusesPage.verifyAcceptCheckboxDisplayed();
		objBonusesPage.verifyAcceptTnCsLabelDisplayed();
		objBonusesPage.verifyClaimPromotionBTN();
		objBonusesPage.verifyCancelLinkDisplayed();
	}

	@Then("^Select the T & C option and click on Claim button$")
	public void select_the_T_C_option_and_click_on_Claim_button() {
		objBonusesPage.SelectAcceptCheckboxDisplayed();
		objBonusesPage.clickClaimPromotionBTN();
	}

	@Then("^Bonus success screen will display to the user$")
	public void bonus_success_screen_will_display_to_the_user(){
		objBonusesPage.verifySuccessHeaderDisplayed();
		objBonusesPage.verifyClaimedBonusNameDisplayed(BonusName);
		objBonusesPage.verifyCloseBTNDisplayed();
		objBonusesPage.verifyActivePromotionsBTNDisplayed();
		objBonusesPage.ClickCloseBTNDisplayed();
	}

	@Then("^Verify following options are displayed under bonus type filter dropdown$")
	public void verify_following_options_are_displayed_under_bonus_type_filter_dropdown(DataTable dt) {
		objBonusesPage.clickBonusType("All types");

		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objBonusesPage.verifyBonusTypesDisplayed(list.get(i));}
	}

	@Then("^Select type as \"([^\"]*)\" from bonus type filter$")
	public void select_type_as_from_bonus_type_filter(String arg1) {
		BonusType = arg1;
		//	//objBonusesPage.clickResetLink();
		//objBonusesPage.clickFilterButton();
		//objBonusesPage.clickBonusType("All types");
		objBonusesPage.selectBonusType(arg1);
	}

	@Then("^Verify filter results are displayed as per selected bonus type$")
	public void verify_filter_results_are_displayed_as_per_selected_bonus_type() {
		objBonusesPage.verifyTypeHDRDisplayed(BonusType);
	}

	@Then("^Change the bonus Date and click on Filter button$")
	public void change_the_bonus_Date_and_click_on_Filter_button()  {
		fromDate = utilities.getRequiredDay("-1", "dd MMMM yyyy", "");
		toDate = utilities.getCurrentDate("dd/MM/YYYY");
		System.out.println(fromDate+ " *******************   "+toDate);
		objBonusesPage.setFromDate(fromDate);
		objBonusesPage.verifyToDate(toDate);
		objBonusesPage.clickFilterButton();
	}

	@Then("^Verify Bonus history is displayed as per selected bonus date$")
	public void verify_Bonus_history_is_displayed_as_per_selected_date_range(){
		objBonusesPage.verifyExpandButtonDisplayed();
		objBonusesPage.clickExpandButton();
		objBonusesPage.verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
		objBonusesPage.verifyBonusHistoryIsDisplayedAsPerSelectedDateRange(fromDate, toDate);
		//objBonusesPage.verifyOptOutLinkDisplayed();
		objBonusesPage.verifyViewTCsLinkDisplayed();
	}

	@Then("^Verify Claimed bonus details are displayed on screen$")
	public void verify_Claimed_bonus_details_are_displayed_on_screen() {
		objBonusesPage.verifyBonusNameOnBonusHistoryDisplayed("test");
		objBonusesPage.clickExpandButton();
		objBonusesPage.verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
	}

	@Then("^Click on Opt Out link$")
	public void click_on_Opt_Out_link() {
		objBonusesPage.verifyOptOutLinkDisplayed();
		objBonusesPage.verifyViewTCsLinkDisplayed();
		objBonusesPage.clickOptOutLink();		
	}

	@Then("^Accept confirmation pop up$")
	public void accept_confirmation_pop_up(){
		objBonusesPage.verifyAreUSureTXTonPopupDisplayed();
		objBonusesPage.verifyOptOutTXTonPopupDisplayed();
		objBonusesPage.verifyYesBTNonPopupDisplayed();
		objBonusesPage.verifyNoBTNonPopupDisplayed();
		objBonusesPage.clickYesBTNonPopup();
	}

	@Then("^Verify that Bonus status displayed as \"([^\"]*)\"$")
	public void verify_that_Bonus_status_displayed_as(String arg1)  {
		objBonusesPage.validateBonusStatus(arg1);
	}

	@Then("^Select bonus type as \"([^\"]*)\" from filter$")
	public void select_bonus_type_as_from_filter(String arg1) {
		BonusType = arg1;
		objBonusesPage.clickBonusType("All types");
		objBonusesPage.selectBonusType(arg1);
	}
}
