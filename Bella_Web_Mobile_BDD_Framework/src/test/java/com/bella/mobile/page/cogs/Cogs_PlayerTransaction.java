package com.bella.mobile.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PlayerTransaction {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public Cogs_PlayerTransaction(WebActions webActions,DriverProvider driverProvider,LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void setUserName(String userName)
	{
		By inpUserName = By.xpath("//input[@id='DefaultLoginView_CogsLoginControl_UserName']");
		logReporter.log("Set username ", userName,
				objWebActions.setTextWithClear(inpUserName, userName));
	}

	//*[@id="ctl00_ContentPlaceHolder1_TopMenu_ButtonMenu_DXI0_T"]





	//td[contains(@id,'_TransactionDates_QuickDateCombo_')]

	//td[@class='FilterLabel'][text()='Quick Date:']

	
	//Transaction Type:
	//	Device Category:


	public void verifyDropdownFields(String fieldNm)
	{
		By locator = By.xpath("//td[@class='FilterLabel'][contains(.,'"+fieldNm+":')]//following-sibling::td//table[@class='dxeButtonEditSys dxeButtonEdit FilterControl']");	
		logReporter.log(" Verify "+fieldNm ,
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyPageTitle(String title)
	{
		//div[@class="pageTitle"]//table//tbody//tr//td[contains(.,'Player Transactions -')]
		By locator = By.xpath("//div[@class='pageTitle']//table//tbody//tr//td[contains(.,'Player "+title+" -')]");	
		logReporter.log(" Verify page title is displayed as " +title,  
				objWebActions.checkElementDisplayed(locator));
	}

	public void selectTopMenu(String menu)
	{	
		//	Transactions
		By locator = By.xpath("//div[@id='topButtonMenu']//li//a[contains(@id,'ContentPlaceHolder1_TopMenu_ButtonMenu_')]//span[text()='"+menu+"']");	
		logReporter.log("select "+menu,  
				objWebActions.click(locator));
	}

	public void selectQuickDate(String dateFilter)
	{
		/*Yesterday
		Specific Date
		Today*/

		By locator = By.xpath("//table[contains(@id,'_TransactionDates_QuickDateCombo_DDD_L_LBT')]//tbody//tr//td[text()='"+dateFilter+"']");	
		logReporter.log("click on sumbit",  
				objWebActions.click(locator));
	}

	public void clickOnSumbit()
	{
		By locator = By.xpath("//div[contains(@id,'SubmitButton')]//span[text()='Submit']");	
		logReporter.log("click on sumbit",  
				objWebActions.click(locator));
	}

	public void verifyTransactionTableHeader(String hdrs)
	{
		/*Transaction
		Real Amount
		Bonus Amount
		Bonus Wins Amount
		Points Amount
		Balance Real
		Balance Bonus
		Balance Bonus Wins
		Total Balance
		Balance Points
		Device Category*/

		if(hdrs.contains("~")){
			String[] arr1 = hdrs.split("~");
			for (String hdrs1 : arr1) {
				By locator = By.xpath("//table[contains(@id,'PlayerTransactionsGrid_DXMainTable')]//tbody//tr//td[contains(@id,'PlayerTransactionsGrid')]//td[text()='"+hdrs1+"']");
				logReporter.log("Verify '" +hdrs1 + " ' option is displayed on marketing preferences screen",  
						objWebActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//table[contains(@id,'PlayerTransactionsGrid_DXMainTable')]//tbody//tr//td[contains(@id,'PlayerTransactionsGrid')]//td[text()='"+hdrs+"']");
			logReporter.log("Verify '" +hdrs + " ' header is displayed",  
					objWebActions.checkElementDisplayed(locator));}
	}
}
