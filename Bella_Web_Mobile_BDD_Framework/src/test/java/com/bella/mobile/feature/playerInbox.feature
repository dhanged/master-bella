Feature: Player Inbox 

@Android
Scenario: Check whether system displays notification on the ‘My Account’ start screen if there is any new message in the inbox
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
#Then User enters username "xzJfZhu"
#And User enters password "RChncwq60"
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Get current message count from my account screen
Then Invoke the Bede Admin portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "xzJfZhu"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "expleo test message"
And Schedule Message
Then Verify system displays notification on the My Account screen for new message in the inbox

@Android
Scenario: Check whether user can see a clear visual cue on the ‘Messages’ list screen if a message is unread
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then verify unread message icon is display for unread message

@Android
Scenario: Check whether system displays all messages on the screen
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify user receive the message

@Android
Scenario: Check whether system displays latest message on top of the screen
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify latest message display on top

@Android
Scenario: Check whether unread message changes to read, when user reads the message
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Click on unread Message from list of message
Then Verify message subject,sender name and message content is display on screen
Then Navigate back to screen
Then verify unread message icon change into read icon

@Android
Scenario: Check whether user can open any message displayed on the ‘Messages’ list screen
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Open Message from list of message

@Android
Scenario: Check whether system deletes the selected message from message list
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify message display on screen
Then Verify Delete icon display in disable mode
Then select message to delete
Then click on Delete icon
Then verify selected message gets delete

@Android
Scenario: Check whether system displays option empty inbox when user needs to delete all messages
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Click on Message option
Then Verify 'Your inbox is empty' message is displayed when user delete all message

@Android
Scenario: Check that  content of the messages can be made up of images, text and CTAs
Given Invoke the Bella site
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Invoke the Bede Admin portal "https://cogs_staging.dagacube.net/Default.aspx"
And Enter username as "Aishwarya" and password as "Aishwarya4321*"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "xzJfZhu"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "Bella Casino Test"
And Schedule Message
Then Click on Message option
Then verify user receive configured message
Then Open Message from list of message
Then verify Content of the messages like images, text and CTAs