package com.bella.desktop.stepDefinition;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.bella.desktop.page.Bella.AccountDetailsPage;
import com.bella.desktop.page.Bella.LogInPage;
import com.bella.desktop.page.cogs.Cogs_PlayerDetailsPage;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class AccountDetailsPageStep {

	private AccountDetailsPage objAccountDetailsPage;
	private Configuration objconfiguration;
	String mbNumber,newPassword,lastLoginTime,currentLogoutTime;
	Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage;
	private LogInPage objLogInPage;
	private Utilities utilities;	
	
	public AccountDetailsPageStep(AccountDetailsPage objAccountDetailsPage,Configuration configuration,Cogs_PlayerDetailsPage objCogs_PlayerDetailsPage,LogInPage objLogInPage,Utilities utilities) 
	{		
		this.objAccountDetailsPage = objAccountDetailsPage;
		this.objconfiguration = configuration;
		this.objCogs_PlayerDetailsPage = objCogs_PlayerDetailsPage;
		this.objLogInPage = objLogInPage;
		this.utilities = utilities;
	}

	@Then("^User enters incorrect current password as \"([^\"]*)\"$")
	public void user_enters_incorrect_current_password_as(String arg1){
		objAccountDetailsPage.setCurrentPassword(arg1);
	}

	@Then("^Enter valid New password as \"([^\"]*)\"$")
	public void enter_valid_New_password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		newPassword = arg1;
		objAccountDetailsPage.setNewPassword(newPassword);
	}

	@Then("^Click on 'Update'$")
	public void click_on_Update(){
		objAccountDetailsPage.clickUpdateBtn();
	}

	@Then("^User will select any options$")
	public void user_will_select_any_options() {
		objAccountDetailsPage.updateMarketingPreferences("Select All");
		objAccountDetailsPage.verifyMarketingPreferencesCheckboxesIsSelectedOrNot("Email~Sms~Phone~Post~SelectAll");
		objAccountDetailsPage.updateMarketingPreferences("Select All");
		objAccountDetailsPage.updateMarketingPreferences("Email");
	}

	@Then("^User will get the option to select between email, phone, sms, post and Select all$")
	public void user_will_get_the_option_to_select_between_email_phone_sms_post_and_Select_all(){
		objAccountDetailsPage.verifyMarketingPreferencesOption("Email~SMS~Phone~Post~Select All");
	}

	@Then("^Change the phone number$")
	public void Change_the_phone_number() 
	{
		objAccountDetailsPage.clickEditLink();
		mbNumber = RandomStringUtils.randomNumeric(11);
		objAccountDetailsPage.EditPhoneNumber(mbNumber);
	}

	@And("^Enter password$")
	public void Enter_password() 
	{
		String password = objconfiguration.getConfig("web.password");
		objAccountDetailsPage.inputPassword(password);
	}

	@Then("^User will get \"([^\"]*)\" confirmation message$")
	public void user_will_get_confirmation_message(String txt) {
		objAccountDetailsPage.verifyAccountUpdateSucsessMSGdisplayed(txt);
	}

	@Then("^Updated phone number should be display$")
	public void Updated_phone_number_should_be_display() {
		objCogs_PlayerDetailsPage.verifyUpdatedMobilenumberIsDisplayedOrNot(mbNumber);
	}

	@Then("^Verify following fields are available on screen$")
	public void verify_following_fields_are_available_on_screen(DataTable dt) throws Throwable {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objAccountDetailsPage.verifyLabels(list.get(i));
		}
	}
	
	@Then("^Click on Edit$")
	public void click_on_Edit(){
		objAccountDetailsPage.verifyEditlinkDisplayed();
		objAccountDetailsPage.verifyLabels("Last login,Name,Email,Date of birth,Phone,Postcode");
		objAccountDetailsPage.verifyEditlinkDisplayed();
		objAccountDetailsPage.clickEditLink();
	}

	@Then("^Verify Name and date of birth field is non editable$")
	public void Verify_Name_and_date_of_birth_field_is_non_editable(){
		objAccountDetailsPage.verifyLabels("Name,Date of birth");
		objAccountDetailsPage.verifyNonEditableTXTDisplayed();
	}
	
	@Then("^User enters correct current password$")
	public void user_enters_correct_current_password(){
		objAccountDetailsPage.setCurrentPassword(objconfiguration.getConfig("web.password"));
	}
	
	 
	@Then("^Enter updated password$")
	public void enter_updated_password(){
		
		objLogInPage.setPassword(newPassword);
	}
	@Then("^Edit email address,Phone number and address$")
	public void edit_email_address_Phone_number_and_address() {
		objAccountDetailsPage.EditEmail();
		objAccountDetailsPage.EditPhoneNumber((utilities.getRandomNumeric(11)));
 		objAccountDetailsPage.inputPassword(objconfiguration.getConfig("web.password"));
 		objAccountDetailsPage.showPasswordBTNdisplayed();
 		//objAccountDetailsPage.clickUpdateBtn();
 		//objAccountDetailsPage.verifyAccountUpdateSucsessMSGdisplayed("Account details updated");
	}

	@Then("^Password updated confirmation message will display$")
	public void password_updated_confirmation_message_will_display() {
		objAccountDetailsPage.verifyAccountUpdateSucsessMSGdisplayed("Password updated");
		System.out.println("********** newPassword "+newPassword);
		objconfiguration.updateParameterDetailsInConfig("web.password",newPassword);
	}
	
	@Then("^Logout user and get logout time$")
	public void Logout_user_and_get_logout_time(){
		
		currentLogoutTime = utilities.getCurrentDate("dd MMMM yyyy, hh:mm:ss a");
		System.out.println("******* currentLogoutTime ::  "+currentLogoutTime);
		objLogInPage.clickLogoutlnk();
	}
	
	@Then("^User can see Last Login time in date-time format$")
	public void user_can_see_Last_Login_time_in_date_time_format(){
		lastLoginTime = objAccountDetailsPage.getLastLoginFormat();
		System.out.println("***** lastLoginTime  "+lastLoginTime);
		objAccountDetailsPage.validateLastLoginFormatDisplayed(lastLoginTime);
	}

	@Then("^Verify that previous login date and time is displayed as Last login time$")
	public void verify_that_previous_login_date_and_time_is_displayed_as_Last_login_time() {
		objAccountDetailsPage.verifyThatLastLoginTimeIsDisplayedCorrectly(currentLogoutTime);
	}
}
