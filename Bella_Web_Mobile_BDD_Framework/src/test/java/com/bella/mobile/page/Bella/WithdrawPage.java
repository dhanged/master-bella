package com.bella.mobile.page.Bella;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;


public class WithdrawPage {
	AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public WithdrawPage(MobileActions objMobileActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration,AppiumDriverProvider objAppiumDriverProvider ) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objAppiumDriverProvider = objAppiumDriverProvider;
	}


	public void clickContinue() {
		By btnContinue = By.xpath("//button[@type='submit' and contains(text(),'Continue')]");
		logReporter.log("Click 'Continue' button ",  
				objMobileActions.click(btnContinue));
	}

	public boolean verifyAreYouSureToWithdrawHeadeDisplayed() {
		By locator = By.xpath("//div[contains(text(),'Are you sure')]");
		return objMobileActions.checkElementDisplayed(locator);
	}

	public void switchToWithdrawalIframe()
	{
		if (configuration.getConfig("mobile.ios").equalsIgnoreCase("true")) 
		{	By locator = By.id("withdrawal-process");

		String a = objMobileActions.getAttribute(locator,"src");
		objAppiumDriverProvider.getAppiumDriver().navigate().to(a);
		waitMethods.sleep(5);	}
		else
		{
			objMobileActions.switchToFrameUsingNameOrId("withdrawal-process");}
	}

	public void verifyMyAccountMenuScreenGetClosed()
	{
		By locator = By.xpath("//div[@class='component slideout-overlay myaccount open display-enter-done']");
		logReporter.log("Verify my account menu screen does not displayed ", 
				objMobileActions.checkElementNotDisplayed(locator));
	}
	public void verifyTextOnWithdrawScreen(String txt)
	{
		//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'Total Balance:')]
		//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'Available to Withdraw:')]
		By locator = By.xpath("//div[@class='cashier-title'][contains(.,' Withdraw')]//following::div//strong[contains(.,'"+txt+":')]");
		logReporter.log("Verify ' " +txt+ " ' is displayed on withdraw sreen", 
				objMobileActions.checkElementDisplayed(locator));
	}

	public void setWithdrawAmount(String amount)
	{
		By inpWithdrawAmount = By.xpath("//label[contains(.,'£')]//following::input[@id='withdraw-amount-box']");
		logReporter.log("Set Withdraw Amount ", amount, 
				objMobileActions.setTextWithClear(inpWithdrawAmount, amount));
	}

	public void verifyYourWithdrawalSuccessMessage(String txt) 
	{
		By imgsymbolsuccess = By.xpath("//*[@class='symbol success-mark medium']");
		logReporter.log("Verify ' success image ' ", 
				objMobileActions.checkElementDisplayed(imgsymbolsuccess));

		//By locator = By.xpath("//p[@class=\"text-center text-success\"][contains(.,'Your withdrawal request for £')]");
		By locator = By.xpath("//p[@class=\"text-center text-success\"][contains(.,'"+txt+"')]");
		logReporter.log("Verify '"+txt+ " ' ", 
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyTransactionWillAppearAsBellaInfoText()
	{
		By locator = By.xpath("//span[contains(.,'The transaction will appear as ')][contains(.,'BELLA')][contains(.,' on your bank statement.')]");
		logReporter.log("Verify 'The transaction will appear as BELLA on your bank statement. ' ", 
				objMobileActions.checkElementDisplayed(locator));
	}

	public String getAmount(String txt)
	{
		By locator = By.xpath("//span[contains(.,'"+txt+":')]//following-sibling::span");
		return objMobileActions.getText(locator);
	}

	public void verifyInfoText(String txt)
	{
		By locator = By.xpath("//p[contains(.,'"+txt+"')]");
		logReporter.log("Verify " +txt , 
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyPleasecontactsupportshouldyouneedanyassistancemessage()
	{
		By locator = By.xpath("//p[contains(text(),'Please contact')  and contains(.,' should you need any assistance.')]//a[contains(.,'support')]");
		logReporter.log("Verify 'Please contact support should you need any assistance.' ", 
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyMinimumWithdrawLimitErrorMessage()
	{
		By locator = By.xpath("//div[contains(@class,' alert alert-danger')][contains(.,' The minimum amount you can withdraw is  £10.00 ')]");
		logReporter.log("Verify 'The minimum amount you can withdraw is  £10.00  ' ", 
				objMobileActions.checkElementDisplayed(locator));
	}
}
